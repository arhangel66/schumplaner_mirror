#!/bin/bash

# Install base applications
apt-get update
apt-get install -y \
    postgresql postgresql-server-dev-9.3 python-setuptools python-dev \
    gcc gettext libjpeg-dev libfreetype6-dev libxml2-dev libxslt-dev \
    build-essential postgresql-client libpq-dev libffi-dev rabbitmq-server \
    nginx \
    --no-install-recommends && rm -rf /var/lib/apt/lists/*
apt-get clean
# Consider these below
# dd if=/dev/zero of=/EMPTY bs=1M
# rm -f /EMPTY

# Pre-install Python requirements
easy_install pip
pip install --upgrade pip
pip install http://effbot.org/media/downloads/Imaging-1.1.7.tar.gz
pip install -r /vagrant/provision/requirements.txt

shutdown -h now
