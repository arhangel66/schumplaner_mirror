To build base box for smplan:

0. Build up the base VM: 
    
    $ cd <REPO>/vagrant/base
    $ vagrant up --provision --provider=virtualbox

1. Export to Vagrant box file:

    $ vagrant package --output smplan.base.box

2. Add the box into Vagrant registry:
    
    $ vagrant box add 'smplan/base' smplan.base.box 
