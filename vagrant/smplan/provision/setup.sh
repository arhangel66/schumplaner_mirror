#!/bin/bash

USER="vagrant"
BASE_DIR="/home/$USER/sites/develop"
RELEASES_DIR="$BASE_DIR/releases"

# apt-get update
# apt-get install -y \
#     postgresql postgresql-server-dev-9.3 python-setuptools python-dev \
#     gcc gettext libjpeg-dev libfreetype6-dev libxml2-dev libxslt-dev \
#     build-essential postgresql-client libpq-dev libffi-dev rabbitmq-server \
#     nginx \
#     --no-install-recommends && rm -rf /var/lib/apt/lists/*
# easy_install pip
# 
# # Clean unused stuff
# apt-get clean
# # dd if=/dev/zero of=/EMPTY bs=1M
# # rm -f /EMPTY
 
# Create user account and project directories
# adduser $USER -q --disabled-password

# Perhaps useless as the script supposed to run only once
# rm -rf $BASE_DIR/*
mkdir -p $RELEASES_DIR $BASE_DIR/logs $BASE_DIR/usercontent $BASE/conf
# ln -s /tmp/kuechengeister $RELEASES_DIR/a
ln -s $RELEASES_DIR/a $BASE_DIR/current

# Web server, reverse proxy,... 
rm -f /etc/nginx/sites-enabled/*
cp /vagrant/provision/nginx.conf /etc/nginx/sites-available/kg.conf
ln -s /etc/nginx/sites-available/kg.conf /etc/nginx/sites-enabled/kg.conf
cat /vagrant/provision/rc.local > /etc/rc.local

# Initialize instance

cd $BASE_DIR/current 

pip install --upgrade pip
pip install http://effbot.org/media/downloads/Imaging-1.1.7.tar.gz
pip install -r requirements_production.txt

su -c "psql -c \"CREATE USER smenu_user WITH PASSWORD 'smenu_password';\"" postgres
su -c "psql -c \"ALTER ROLE smenu_user WITH LOGIN CREATEDB;\"" postgres
su -c "psql -c \"DROP DATABASE smenu;\"" postgres
su -c "psql -c \"DROP DATABASE test_smenu;\"" postgres
su -c "psql -c \"CREATE DATABASE smenu OWNER smenu_user;\"" postgres
su -c "psql -c \"CREATE DATABASE test_smenu OWNER smenu_user;\"" postgres

# In case of having initial DB, please uncomment below line
# su -c "psql -d smenu < /vagrant/provision/smenu.psql" postgres

python manage.py collectstatic --noinput
python manage.py syncdb --noinput
python manage.py migrate --noinput

# Create default super user
echo "from django.contrib.auth.models import User; User.objects.create_superuser('verwaltung', 'admin@smplan.de', 'test123')" | python manage.py shell

chown $USER.$USER -R $BASE_DIR

su -c 'uwsgi --ini uwsgi.ini' $USER

service nginx restart
