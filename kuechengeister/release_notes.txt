SCHULMENUEPLANER - RELEASE NOTES
--------------------------------


19.07.2016  
    
    Branch [develop]
    
    * Add effective periods to Meal data model
    * Improve meal plan management: More friendly, messages, changes
      indications, DB transactions...
    * Add basic tests to applications "kg" and "menus"
    * Fixes:
        - Issue when importing SEPA Mandate


09.07.2016
    
    Branch [develop]

    * Menu release feature:
        - There are two "Release.." buttons, at top and bottom of the meal plan
          management page
        - Only when having changes in mealplan(s), above buttons will be shown
        - New unpublished/preparation menus is marked in brown
        - Release process is taken by asynchronous tasks (by Celery). In case of
          missing Celery, the task will be performed sequentially.
        - After releasing, new menus in all meal plans will be published
          Note: Actually, the feature is not very intuitive, and easy to track:
            + User don't know which meal plan(s) modified without browsing
                the correct week (having new menus) of it.
            + There are no message mentioning about newly published menus.
        - Unpublished menus (menu preparations) is marked differently (light brown)
            + They cannot be dragged/copied
        - New menu preparation will also be created after drag-drop into blank slot
        - Editing of existing menu has effect immediately (no need to release)

    * General and specific meal plans:
        - Now there are a general meal plan and other specific meal plans which
          inherit from the general one with their own customization.
        - The public index page only shows meals of general meal plan
        - Customized menus (in specific meal plan) have mark at top right corner
        - New menu attribute, disabled, is for exclude the menu of general meal
          plan from specific one.

    * Menu deletion and reset:
        - Customized menus of specific meal plan can be resetted to be inherited
          from general meal plan.
        - User can choose to disable (or not) the customized menu after being
          deleted

    * Menus of past weeks are read-only: cannot be edited, dragged or deleted

    * Fixes:
        - Keep the price (in same pricegroup) after drag-drop menu
        - Display error message and prevent saving in case entering invalid
          price value (menu modal)

    * Notes: Some labels, texts are still in English. While the system doesn't
      use i18n, translation into German is possibly required.

    Branch [vagrant]

    * New branch `vagrant` holding configuration, scripts,.. for creating a VM
      machine with Kuechengeister site running. It has: Kuechengeister instance,
      PostgreSQL, Nginx, uWSGI, Celery, RabbitMQ, etc. The Ansible provision will
      be added next. Check vagrant/README.txt for basic instructions.

