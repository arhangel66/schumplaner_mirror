from StringIO import StringIO

from fabric.api import env, task, local, run, get, execute, settings
from fabric.context_managers import shell_env
from fabric.utils import abort

#env.use_ssh_config = True
env.roledefs = {'production': {
                    'hosts': ['smplan@prod.smplan.ovh:10022'],
                    'dbs': ['abraxas',
                            'altendiez',
                            'awessen',
                            'bantschow',
                            'beinrode',
                            'biofuerkids',
                            'bodelessen',
                            'bossow',
                            'clauert',
                            'freshdachs',
                            'gastroevents',
                            'hytera',
                            'kobrow',
                            'kuechengeister',
                            'lfg',
                            'mevissen',
                            'm1catering',
                            'remuss',
                            'rjo',
                            'schlemmerzwerg',
                            'schoolkoek',
                            'schulrestaurant',
                            'somic',
                            'sosprignitz',
                            'svmv',
                            'tommys',
                            'uwe',
                            'vollwertkueche',
                            'wildenhain',
                            'wimpassing',
                            'zingst'
                            ]
                },
                'prod_neu': {
                    'hosts': ['smplan@prodkey.smplan.ovh:10022'],
                    'dbs': ['cupedia',
                            'dksbradebeul',
                            'esd',
                            'esl',
                            'genussmanufaktur',
                            'joehstadt',
                            'leibundseele',
                            'muehlenmenue',
                            'oschatz',
                            'ralbitz',
                            'rudolstadt',
                            'schmalkalden',
                            'sjk',
                            'sonnenblick',
                            'soulfood',
                            'tfabistro',
                            'warnowservice',

                            ]
                },
                'testing': {
                    'hosts': ['smplan@demo.smplan.ovh:20022'],
                    'dbs': ['demo', 'demo1', 'demo2', 'demo3']
                },
                'prodtest': {
                    'hosts': ['smplan@demo.smplan.ovh:9022'],
                    'dbs': ['', ]
                }
}


@task
def test():
    local('ls')


def cpdb_worker(db_name, remote):
    # configure path and filenames
    local_working_dir = "./../../db_dumps"
    remote_site_dir_path = "/home/smplan/sites/%s" % db_name
    dump_file_name = "%s_dump.sql.gz" % db_name
    local_dump_file_path = local_working_dir + "/" + dump_file_name

    # create and retrieve remote DB dump
    if remote != 'local':
        # retrieve db password from remote
        remote_settings_file_path = remote_site_dir_path + "/conf/settings_local.py"
        fd = StringIO()
        get(remote_settings_file_path, fd)
        content=fd.getvalue()
        DATABASES = {}
        exec content

        # get db dump file from remote and delete it after that
        with shell_env(PGPASSWORD="%s" % DATABASES['default']['PASSWORD']):
            run("pg_dump -U %s %s | gzip > %s" % (db_name, db_name, dump_file_name))
        get("%s" % dump_file_name, "%s" % local_dump_file_path)
        run("rm %s" % dump_file_name)

    # Drop db, create new one, load data
    with settings(warn_only=True):
        local("dropdb %s" % db_name)
    local("createdb -E UTF-8 -l de_DE.UTF-8 %s" % db_name)
    local("gunzip -c %s | psql %s >/dev/null 2>&1" % (local_dump_file_path, db_name))

    # set all password to "test"
    # TODO: set all password to "test"
    #local("~/.virtualenvs/kg/bin/python manage.py settestpassword")


@task(alias="cpdb")
def copy_remote_db_to_local(db_name='', remote='remote'):
    """ Import a remote database to the local database server
        Set the remote parameter to 'local' if you want to restore the locally saved dump instead of retrieving a new one
    """

    if not db_name:
        abort("Please set the name of the DB to copy as parameter.")
    if remote not in ['remote', 'local']:
        abort("Valid values for parameter local are: remote, local")

    # find out on which host the database is located and set host accordingly
    host_list = []
    for name, role in env.roledefs.iteritems():
        if db_name in role['dbs']:
            host_list = role['hosts']

    execute(cpdb_worker, hosts=host_list, db_name=db_name, remote=remote)
