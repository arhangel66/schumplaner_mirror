from django.conf import settings

USERNAME_MAXLENGTH = getattr(settings, 'USERNAME_MAXLENGTH', 75)