# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'FinTSProfile.customer'
        db.delete_column('bank_transactions_fintsprofile', 'customer_id')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'FinTSProfile.customer'
        raise RuntimeError("Cannot reverse this migration. 'FinTSProfile.customer' and its values cannot be restored.")

        # The following code is provided here to aid in writing a correct migration        # Adding field 'FinTSProfile.customer'
        db.add_column('bank_transactions_fintsprofile', 'customer',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Customer']),
                      keep_default=False)


    models = {
        'bank_transactions.fintsprofile': {
            'Meta': {'object_name': 'FinTSProfile'},
            'account_no': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fints_blz': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fints_login': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fints_url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'pin': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {}),
            'transaction_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        'bank_transactions.fintsprofilestatus': {
            'Meta': {'object_name': 'FinTSProfileStatus'},
            'id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['bank_transactions']
