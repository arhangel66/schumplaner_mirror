# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AQWrapperLog'
        db.create_table('bank_transactions_aqwrapperlog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tool', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('command', self.gf('django.db.models.fields.TextField')()),
            ('run_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('output', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('bank_transactions', ['AQWrapperLog'])


    def backwards(self, orm):
        # Deleting model 'AQWrapperLog'
        db.delete_table('bank_transactions_aqwrapperlog')


    models = {
        'bank_transactions.aqwrapperlog': {
            'Meta': {'object_name': 'AQWrapperLog'},
            'command': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'output': ('django.db.models.fields.TextField', [], {}),
            'run_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'tool': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'bank_transactions.fintsprofile': {
            'Meta': {'object_name': 'FinTSProfile'},
            'account_no': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fints_blz': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fints_login': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fints_url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'information': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'last_update_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'pin': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {}),
            'transaction_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['bank_transactions']