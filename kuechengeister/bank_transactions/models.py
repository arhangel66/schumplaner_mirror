# -*- coding: utf-8 -*-

import os
import datetime

from django.db import models
from django import forms

from aq_wrapper import AqWrapper

TOOL_AQHBCI_TOOL4 = 'aqhbci_tool4'
TOOL_AQBANKING_CLI = 'aqbanking_cli'
TOOL_CHOICES = ((TOOL_AQHBCI_TOOL4, 'AQHBCI Tool4'),
                (TOOL_AQBANKING_CLI, 'AQBanking CLI'))


class AQWrapperLog(models.Model):
    tool = models.CharField(max_length=30, choices=TOOL_CHOICES)
    command = models.TextField()
    run_at = models.DateTimeField(auto_now_add=True)
    output = models.TextField()


class FinTSProfile(models.Model):
    """Represent Customer bank account FinTS API access required information
       and API access maintinance information"""
    name = models.CharField(max_length=80)
    fints_login = models.CharField(max_length=255)
    fints_url = models.CharField(max_length=255)
    fints_blz = models.CharField(max_length=255)
    account_no = models.CharField(max_length=255)
    transaction_count = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    last_update_at = models.DateTimeField(blank=True, null=True)
    # PIN is not stored, we need PIN column to get PIN in to Django admin form validation an model save function
    pin = models.CharField(max_length=100, blank=True, null=True)
    
    class Meta:
        verbose_name = 'Bankkonto'
        verbose_name_plural = 'Bankkonten'
    
    def clean(self):
        """On form validation simulate FinTSProfile creation. 
           If it is successful then return True otherwise False
        """
        oAqWrapper = AqWrapper(self.fints_url, self.id, self.fints_blz, self.fints_login, self.account_no, self.pin)
        # Simulate creation because no real ID exist yet
        bStatus = oAqWrapper.simulateCreation()
        if not bStatus:
            raise forms.ValidationError("Fehler beim Einrichten des Kontos")

    def build_pin_binary(self):
        """Build and Return binary FinTSAPI PIN object
        """
        bPinFileBinary = b'PIN_%s_%s="%s"' % (self.fints_blz,
                                              self.fints_login,
                                              self.pin)

        return bPinFileBinary

    def build_profile_path(self):
        """Build and Return FinTS Profile system account path
        """

        sFinTSProfilePath = os.path.join('/', 'fints_profiles')
        sFullPath = os.path.join(sFinTSProfilePath, str(self.id))

        return sFullPath
    
    def getLastUpdateDate(self):
        """Return 01.01.2016 as last update if it is empyt otherwise return existing value
        """
        if self.last_update_at == None:
            # 20160101
            return datetime.date(2016, 1, 1)
        else:
            return self.last_update_at
    
    def updateLastUpdateDate(self, psUpdateDate):
        """Update last update date with transaction date if empty then set to 01.01.2016
           This ensures that we keep only newest transaction date and later load new trasactions from this date.
        """
        dDate = datetime.datetime.strptime(psUpdateDate, '%Y-%m-%d')
        if self.last_update_at == None:
            self.last_update_at = datetime.datetime(2016, 1, 1)
        if self.last_update_at < dDate:
            self.last_update_at = dDate
            self.save()
