# -*- coding: utf-8 -*-

def bulk_transaction_loading(transactions):
    """Loop through all transactions and pass every transaction to
       load_transaction function
    """
    pass

def load_transactions(transactions):
    """Do all required tasks to save transaction into db.
       Use this function every time whn there is need to save transaction
       into db from external source like HBCI API or CSV upload.
    """
    pass
