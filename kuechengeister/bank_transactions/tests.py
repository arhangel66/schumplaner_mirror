# -*- coding: utf-8 -*-

import os

from django.utils.html import mark_safe
from django.core.urlresolvers import (
    reverse,
    resolve
)

from django.test import (
    TestCase,
    RequestFactory
)
from django.contrib.auth.models import User
from django.contrib import admin

from views_admin import load_transactions

from models import FinTSProfile

from admin import FinTSProfileAdmin

class ModelTests(object):
    def test_fields(self):
        """Test whether all required fields is defined

        :return:
        """
        msg_ptr = 'Fields %s missing from "%s" model'

        expected_fields = self.fields
        real_fields = []
        difference = []

        # Loop through all fields and test whtether it is defined
        for field_name in expected_fields:
            if not hasattr(self.model, field_name):
                difference.append(field_name)
            else:
                real_fields.append(field_name)

        self.assertEqual(expected_fields,
                         real_fields,
                         msg=msg_ptr % (difference, self.model.__class__.__name__))

    def test_not_null_constraints(self):
        """Test whether NOT NULL constraint set correctly to required fields

        :return:
        """
        msg_ptr = '"%s" model field "%s" must have NOT NULL constraint'

        # Add all nullable fields here
        expected_not_null_fields = self.not_null_fields

        real_not_null_fields = []
        difference = []

        # Loop through all fields and test whtether it has NOT NULL constraint
        for field_name in expected_not_null_fields:
            field = self.model._meta.get_field(field_name)
            if field.null:
                difference.append(field_name)
            else:
                real_not_null_fields.append(field_name)

        self.assertEqual(expected_not_null_fields,
                         real_not_null_fields,
                         msg=msg_ptr % (self.model.__class__.__name__, difference))

    def test_nullable_fields(self):
        """Test whether nullable fields is set correctly

        :return:
        """
        msg_ptr = '"%s" model field %s must be nullable'

        # Add all nullable fields here
        expected_nullable_fields = self.nullable_fields
        real_nullable_fields = []
        difference = []

        # Loop through all fields and test whtether it is nullable
        for field_name in expected_nullable_fields:
            field = self.model._meta.get_field(field_name)
            if not field.null:
                difference.append(field_name)
            else:
                real_nullable_fields.append(field_name)

        self.assertEqual(expected_nullable_fields,
                         real_nullable_fields,
                         msg=msg_ptr % (self.model.__class__.__name__, difference))




class FinTSProfileModelTest(ModelTests, TestCase):
    def setUp(self):
        self.fintsprofile = FinTSProfile()
        self.model = self.fintsprofile
        self.fields = ['id', 'name', 'fints_login', 'fints_url',
                       'fints_blz', 'account_no', 'transaction_count',
                       'created_at', 'updated_at', 'pin', 'last_update_at']
        self.nullable_fields = ['updated_at', 'pin', 'last_update_at']
        self.not_null_fields = ['name', 'fints_login', 'fints_url',
                                'fints_blz', 'account_no', 'transaction_count',
                                'created_at']

    def test_char_field_max_len(self):
        """Test whether char fields have required max length

        :return:
        """
        self.assertEqual(self.fintsprofile._meta.get_field('name').max_length, 80,
                         msg='"FinTSProfile" model field "name" max_length must be 80')

        self.assertEqual(self.fintsprofile._meta.get_field('fints_login').max_length, 255,
                         msg='"FinTSProfile" model field "fints_login" max_length must be 255')

        self.assertEqual(self.fintsprofile._meta.get_field('fints_url').max_length, 255,
                         msg='"FinTSProfile" model field "fints_url" max_length must be 255')

        self.assertEqual(self.fintsprofile._meta.get_field('fints_blz').max_length, 255,
                         msg='"FinTSProfile" model field "fints_blz" max_length must be 255')

        self.assertEqual(self.fintsprofile._meta.get_field('account_no').max_length, 255,
                         msg='"FinTSProfile" model field "account_no" max_length must be 255')

        self.assertEqual(self.fintsprofile._meta.get_field('pin').max_length, 100,
                         msg='"FinTSProfile" model field "pin" max_length must be 100')

    def test_default_values(self):
        """Test whether fields have correct default values

        :return:
        """
        # Transaction count field must have default value 0
        self.assertEqual(self.fintsprofile._meta.get_field('transaction_count').default, 0,
                         msg='"FinTSProfile" field "transaction_count" have incorrect default value')

        # Created_at efault value has set to datetitme now in Django way
        self.assertEqual(self.fintsprofile._meta.get_field('created_at').auto_now_add, True,
                         msg='"FinTSProfile" field "created_at" must have auto_now_add=True')

    def test_fints_api_pin_build_function(self):
        """Test whether model build correct FINTAS API pin binnary object

           :return:
        """
        self.model.fints_blz = 'blz'
        self.model.fints_login = 'login'
        self.model.pin = '123'

        bExpectedPinFileBinary = b'PIN_%s_%s="%s"' % (self.model.fints_blz,
                                                      self.model.fints_login,
                                                      self.model.pin)

        self.assertEqual(bExpectedPinFileBinary, self.model.build_pin_binary())

    def test_fints_profile_path(self):
        """Test whether model build corect FinTS Profile path

           :return:
        """

        self.model.id = 30

        sFinTSProfilePath = os.path.join('/', 'fints_profiles')
        sExpectedFullPath = os.path.join(sFinTSProfilePath, str(self.model.id))

        self.assertEqual(sExpectedFullPath, self.model.build_profile_path())


class FinTSProfileAdminTest(TestCase):
    def setUp(self):
        self.site = admin.AdminSite()
        # Prepare request factory for tests
        self.factory = RequestFactory()
        # Create Django user
        self.user = User.objects.create_user(
            username='kristaps',
            email='kb@mieranav.lv',
            password='123')

    def test_fints_profile_admin_model_is_defined(self):
        """Test whether FinTSProfile admin model is defined and extend ModelAdmin

        :return:
        """
        self.assertIn(admin.ModelAdmin,
                      FinTSProfileAdmin.mro(),
                      msg='"FinTSProfileAdmin" not extending ModelAdmin')

    def test_fints_profile_list_display(self):
        """Test whether extended model list_display is set correctly

        :return:
        """

        list_display = ('id', 'name')

        admin_model = FinTSProfileAdmin(FinTSProfile(), admin.site)

        self.assertEqual(admin_model.list_display,
                         list_display,
                         msg='"FinTSProfileAdmin" list_display is set incorrectly')

    def test_fints_profile_exclude(self):
        """Test whether extended model exclude is set correctly

        :return:
        """
        # Hint: no need to exclude "created_at" - it is automaticaly removed if it have attribute auto_now_add=True
        exclude = ('transaction_count', 'updated_at', 'last_update_at')

        admin_model = FinTSProfileAdmin(FinTSProfile(), admin.site)

        self.assertEqual(admin_model.exclude,
                         exclude,
                         msg='"FinTSProfileAdmin" exclude is set incorrectly')

    def test_fints_profile_model_registered_from_django_admin_site(self):
        """Test whether new FinTSProfile model registered for Django admin site.

        :return:
        """
        # Use private attribute but there is no other way to do this in Django 1.4
        # In newest Djangos ther is function is_registered.
        self.assertIn(FinTSProfile, admin.site._registry,
                      msg='"FinTSProfile" model not registered for Django admin site')

    def test_fints_profile_admin_save_model(self):
        """Test whether save_model function is defined and works correctly.
           I mean customeri sett correctly and status is set to CREATING
           and on update status is set to CHNAGED

           :return:
        """
        request = self.factory.get('/admin/')

        # Recall that middleware are not supported. You can simulate a
        # logged-in user by setting request.user manually.
        request.user = self.user

        oModelAdmin = FinTSProfileAdmin(FinTSProfile, self.site)

        oFinTSProfile = FinTSProfile(
            name='Test profile',
            fints_login='fints_login',
            fints_url='fints_url',
            fints_blz='fints_blz',
            account_no='account_no',
            pin='account_no')

        oModelAdmin.save_model(request, oFinTSProfile, None, False)
        
        self.assertEqual(type(oFinTSProfile.id), type(1))

        oModelAdmin.save_model(request, oFinTSProfile, None, True)
        oFinTSProfile = FinTSProfile.objects.get(id=oFinTSProfile.id)
        self.assertEqual(type(oFinTSProfile.id), type(1))


class AdminViewsTest(TestCase):
    def setUp(self):
        self.site = admin.AdminSite()
        # Prepare request factory for tests
        self.factory = RequestFactory()
        # Create Django user
        self.user = User.objects.create_user(
            username='kristaps',
            email='kb@mieranav.lv',
            password='123')
        self.user.is_staff = True
        self.user.is_active = True
        self.is_superuser = True
        self.user.save()

    def test_load_transactions_url(self):
        """Test whether is possible to resolve load_transaction view url

           :return:
        """
        found = resolve('/admin/bank_transactions/load_transactions/')

        self.assertEqual(found.func, load_transactions)

    def test_can_reverse_load_transactions_view(self):
        """Test whether is possible to use reverse to get url of load_transactions view

           :return:
        """
        found = resolve(reverse('bank_transactions.views_admin.load_transactions'))

        self.assertEqual(found.func, load_transactions)

    def test_load_transaction_return_redirect_to_fints_profile_changelist(self):
        """Test whether load_transactions view return redirect to
           FinTSProfile changelist
        """
        self.client.login(username='kristaps', password='123')
        expected_url = reverse('admin:bank_transactions_fintsprofile_changelist')
        response = self.client.get(reverse('bank_transactions.views_admin.load_transactions'))

        # Can't use got 403
        # self.assertRedirects(response, expected_url, status_code=302, target_status_code=200, host=None, msg_prefix='')
        self.assertIn('kg/bankaccounttransaction', dict(response.items())['Location'])
