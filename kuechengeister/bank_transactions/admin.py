# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.html import mark_safe
from django.core.urlresolvers import reverse

from django import forms

from aq_wrapper import AqWrapper

from models import (
    FinTSProfile,
    AQWrapperLog
)

class AQWrapperLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'tool', 'run_at', 'full_command', 'full_output')
    list_display_links = ('id', 'tool', 'run_at')
    list_filter = ('tool',)
    search_fields = ['command', 'output']
    
    def full_command(self, poItem):
        """Return part of command and in other html element is full comand. Full comand is used to show in modal window"""
        return '<span id="cmd%d" class="cmd">%s</span><span id="fullcmd%d" class="none">%s</span>' % (poItem.id, poItem.command[0:40],
                                                                                                      poItem.id, poItem.command)
    full_command.allow_tags = True
    
    def full_output(self, poItem):
        """Return part of output and in other html element is full output. Full output is used to show in modal window"""
        return '<span id="out%d" class="out">%s</span><span id="fullout%d" class="none">%s</span>' % (poItem.id, poItem.output[0:100],
                                                                                                      poItem.id, poItem.output.replace('\r', '<br />').replace('\n', '<br />'))
    full_output.allow_tags = True


class FinTSProfileForm(forms.ModelForm):
    pin = forms.CharField(label='', widget=forms.HiddenInput)
    class Meta:
        model = FinTSProfile


class FinTSProfileAdmin(admin.ModelAdmin):
    """Change default Django admin ModelAdmin functionality to
       meet system requirements
    """

    list_display = ('id', 'name')
    list_display_links = ('id', 'name')

    exclude = ('transaction_count', 'updated_at', 'last_update_at')
    
    form = FinTSProfileForm

    def save_model(self, poRequest, poInstance, poForm, pbChange):
        # Load PIN in local variable
        sPin = poInstance.pin
        # Clear PIN attribute to make sure it is not stored
        poInstance.pin = None
        poInstance.save()
        # After instance is saved we can create FinTSProfile
        oAqWrapper = AqWrapper(poInstance.fints_url, poInstance.id, poInstance.fints_blz, poInstance.fints_login, poInstance.account_no, sPin)
        if not pbChange:
            bStatus = oAqWrapper.createFinTSAccount()
        else:
            bStatus = oAqWrapper.changeFinTSAccount()

# Register admin models
admin.site.register(FinTSProfile, FinTSProfileAdmin)
admin.site.register(AQWrapperLog, AQWrapperLogAdmin)
