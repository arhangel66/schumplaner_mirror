# -*- coding: utf-8 -*-

from __future__ import with_statement
from django.http import HttpResponse, Http404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.db import transaction
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import simplejson
from django.utils.encoding import force_unicode
from datetime import date, datetime, timedelta

from menus.models import Menu, Meal, Mealtime
from menus.helpers import get_mealplan_pdf
from kg.models import (
    Child, Customer, FacilityType, FacilitySubunit, Flatrate, InvoiceLog,
    ChildReduction, PayType, ActionLog, WEEKDAYS, SepaMandate,
    CustomerAccountTransaction, SepaDebit, Order

)
from kg_utils.menu_helper import MenuRenderer
from kg_utils.date_helper import DateHelper
from kg_utils.sepa_generator import SepaHelper
from kg_utils.order_helper import order_menu, cancel_dependent_menus
from kg.forms import CustomerForm, ChildForm, CustomerPasswordChangeForm, PayTypeForm, SepaMandateForm, ChargeCreditForm
from settings import (MEALTIMES, INVOICE_ROOT, MENU_WEEKS_CUSTOMER, MODULE_PREPAID, PREPAID_SETTINGS,
                      MODULE_PAPER_INVOICE, PAPER_INVOICE_SETTINGS, SEPA, OVERLOAD_SITE, MODULE_FLATRATE)


@login_required
def index(request):
    #redirect admin
    if request.user.is_staff:
        return redirect('/admin/')
    elif OVERLOAD_SITE == 'sonnenblick':
        return redirect('kg.views_kunden.rechnungen')
    else:
        return redirect('kg.views_kunden.essen')


def customer_required(func):
    """ Decorator for checking existing of customer object linking to current
        user. If not index page will be redirected to.
    """
    def check_customer(request, *args, **kwargs):
        has_customer = False
        try:
            has_customer = (request.user.customer is not None)
            if has_customer:
                return func(request, *args, **kwargs)
        except Customer.DoesNotExist:
            pass
        return redirect('kg.views_index.index')
    return check_customer


@login_required
@customer_required
@csrf_exempt
def essen(request, year=None, week=None, kwargs={}):

    # set defaults
    mealtime_id = 0  # all mealtimes
    if OVERLOAD_SITE == 'somic':
        mealtime_id = MEALTIMES['lunch']  # show only lunch as default for SOMIC
    view = 'kg.views_kunden.essen'
    template = 'kg/kunden_essen.html'

    # handle kwargs
    if 'mealtime_id' in kwargs:
        mealtime_id = kwargs['mealtime_id']
    if 'view' in kwargs:
        view = kwargs['view']
    if 'template' in kwargs:
        template = kwargs['template']

    # set week
    oNow = MenuRenderer.getMenuStartDate()
    if not year:
        year = oNow.isocalendar()[0]
    if not week:
        week = oNow.isocalendar()[1]

    oCustomer = request.user.get_profile()

    # hack to exclude Children in Group 'Kundenparkplaz' / ZZGEKUENDIGT
    oChildren = Child.objects.select_related().filter(customer__user=request.user)

    # customer is suspended
    if oCustomer.is_suspended:
        return render_to_response('kg/kunden_sperre.html',
                                  {'suspension_reason': oCustomer.suspension_reason},
                                  context_instance=RequestContext(request))

    # no children configured
    if not oChildren:
        return render_to_response('kg/kunden_keine_kinder.html', {}, context_instance=RequestContext(request))

    # Download menu PDF with order info
    if request.method == 'GET' and request.GET.get('pdf') == '1':
        child = oChildren.filter(id=request.GET.get('child'))[0]
        mealplan_id = child.facility_subunit.mealplan.id
        pricegroup_id = child.facility_subunit.price_group.id
        return get_mealplan_pdf(
            int(year), int(week), mealplan_id, pricegroup_id, child=child)

    # POST request (fallback if JavaScript is disabled)
    if request.method == 'POST':
        try:
            iChildId, iMenuId = map(int, request.GET.get('order').split('-')[:2])
            iQuantity = int(request.GET.get('quantity', 0))

            # Set order quantity of dependent menus to 0 if canceling all
            if iQuantity == 0:
                cancel_dependent_menus(iMenuId, iChildId, request)

            dReturnDict = order_menu(
                iMenuId, iChildId, iQuantity, request,
                reason=Order.REASON_USER
            )
            if dReturnDict['success']:
                sAnchor = "#%d" % iChildId
                return redirect(reverse(view, args=[year, week]) + sAnchor)
            else:
                messages.error(request, dReturnDict['message'])
                return redirect(reverse(view, args=[year, week]))

        except Exception as exc:
            #print exc.message
            messages.error(request, 'Es ist ein Fehler aufgetreten! Ihre Bestellung wurde nicht gespeichert.')
            return redirect(reverse(view, args=[year, week]))

    # Ajax request
    if request.is_ajax():
        try:
            iChildId, iMenuId = map(int, request.GET.get('order').split('-')[:2])
            iQuantity = int(request.GET.get('quantity', 0))

            # Set order quantity of dependent menus to 0 if canceling all
            if iQuantity == 0:
                cancel_dependent_menus(iMenuId, iChildId, request)

            dReturnDict = order_menu(
                iMenuId, iChildId, iQuantity, request,
                reason=Order.REASON_USER
            )
            if dReturnDict['success']:
                # reload customer and get get account balance if prepaid is active
                if MODULE_PREPAID:
                    oReloadedCustomer = Customer.objects.get(pk=oCustomer.id)
                    sAccountBalance = str(oReloadedCustomer.account_balance).replace('.', ',')
                else:
                    sAccountBalance = '-1'

                # Generate brief day order status for small view
                order_brief_html = MenuRenderer.getDayOrderBrief(
                    iChildId, Menu.objects.get(id=iMenuId).date)

                return HttpResponse(
                    simplejson.dumps({
                        'error': False,
                        'quantity': dReturnDict['quantity'],
                        'order_possible': dReturnDict['order_possible'],
                        'id': "%d-%d" % (iChildId, iMenuId),
                        'account_balance': sAccountBalance,
                        'order_brief_html': order_brief_html
                    }),
                    mimetype='application/javascript'
                )
            else:
                return HttpResponse(simplejson.dumps({'error': True,
                                                      'message': dReturnDict['message']}),
                                    mimetype='application/javascript')

        except Exception as exc:
            #print exc.message
            return HttpResponse(simplejson.dumps({'error': True,
                                                  'message': 'Es ist ein Fehler aufgetreten!'}),
                                mimetype='application/javascript')

    lMenus = []
    if MenuRenderer.isMenuAvailable(year, week):
        for oChild in oChildren:
            dItem = {}
            if oChild.facility.type.name == 'Kundenparkplatz':
                continue
            elif not oChild.facility.is_active:
                small_view_menu_html = menu_html = u'<br />&nbsp;&nbsp;&nbsp;Keine Bestellung möglich'
            else:
                menu_html, small_view_menu_html = MenuRenderer.getMenuHtml(
                    year, week, child=oChild, form=True,
                    form_action=reverse('kg.views_kunden.essen',
                                        args=[year, week]),
                    request=request)

            dItem = {
                'menu_html': menu_html,
                'small_view_menu_html': small_view_menu_html,
                'child': oChild
            }
            lMenus.append(dItem)

    return render_to_response(template,
                              {'menus': lMenus,
                               'weeks': DateHelper.getWeeks(oNow, MENU_WEEKS_CUSTOMER),
                               'selected': int(week)},
                              context_instance=RequestContext(request))

@login_required
@csrf_exempt
def brotzeit(request, year=None, week=None):
    """ essen view proxy for SOMIC that shows only the brotzeit"""
    if OVERLOAD_SITE not in ['somic',]:
        raise Http404
    else:
        return essen(request, year=year, week=week, kwargs={'mealtime_id': 1,
                                                            'view': 'kg.views_kunden.brotzeit',
                                                            'template': 'kg/kunden_brotzeit.html'})

@login_required
@customer_required
def rechnungen(request):
    oCustomer = request.user.get_profile()
    oChildren = Child.objects.filter(customer=oCustomer)

    #no child configured
    if not oChildren:
        return render_to_response('kg/kunden_keine_kinder.html', {}, context_instance=RequestContext(request))

    if request.method == 'POST':
        with transaction.commit_manually():
            try:
                if request.POST.get('activate_online_invoice'):
                    oCustomer.paper_invoice = False
                    oCustomer.save()
                    ActionLog.objects.log_change(request=request, object=None,
                                                 message=u'Rechnung per Post deaktiviert: %s' % force_unicode(oCustomer))
            except:
                transaction.rollback()
                messages.error(request,
                               "Es ist ein Fehler aufgetreten! Ihre Änderungen wurden nicht gespeichert. Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.")
                return redirect('kg.views_kunden.rechnungen')
            else:
                transaction.commit()
                if request.POST.get('activate_online_invoice'):
                    messages.success(request, "Ihre Änderungen wurden gespeichert.")
                else:
                    messages.success(request,
                                     "Bitte markieren Sie die Auswahlbox und senden Sie das Formular erneut, um Online-Rechnung zu aktivieren.")
                return redirect('kg.views_kunden.rechnungen')

    if not oCustomer.paper_invoice:

        # retrieve invoice data, show invoices of about last 18 months
        oStartDate = DateHelper.getLastDayOfLastMonth(date.today()) - timedelta(days=540)
        oInvoices = (InvoiceLog.objects.filter(child__in=oChildren, invoice_date__gt=oStartDate)
                                       .exclude(released=None)
                                       .order_by('-invoice_date', 'child__id'))

    else:
        oInvoices = []

    return render_to_response('kg/kunden_rechnungen.html',
                              {'is_active': not oCustomer.paper_invoice,
                               'invoices': oInvoices,
                               'module_paper_invoice': MODULE_PAPER_INVOICE},
                              context_instance=RequestContext(request))


@login_required
@customer_required
def rechnungen_papier(request):

    # test if MODULE_PAPER_INVOICE is active, redirect otherwise
    if not MODULE_PAPER_INVOICE:
        return redirect('kg.views_kunden.rechnungen')

    if request.method == 'POST':

        if 'cancel' in request.POST:
            return redirect('kg.views_kunden.rechnungen')

        else:

            if request.POST.get('deactivate_online_invoice'):
                with transaction.commit_manually():
                    try:
                        oCustomer = request.user.get_profile()
                        if oCustomer:
                            oCustomer.paper_invoice = True
                            oCustomer.save()
                            ActionLog.objects.log_change(request=request, object=None,
                                                         message=u'Rechnung per Post aktiviert: %s' % force_unicode(oCustomer))
                    except:
                        transaction.rollback()
                        messages.error(request,
                                       "Es ist ein Fehler aufgetreten! Ihre Änderungen wurden nicht gespeichert. Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.")
                        return redirect('kg.views_kunden.rechnungen_papier')
                    else:
                        transaction.commit()
                        messages.success(request, "Ihre Änderungen wurden gespeichert.")
                        return redirect('kg.views_kunden.rechnungen')

            else:
                messages.success(request,
                                 "Bitte markieren Sie die Auswahlbox und senden Sie das Formular erneut.")
                return redirect('kg.views_kunden.rechnungen_papier')

    oCustomer = request.user.get_profile()

    # redirect to invoice index if paper invoice is active
    if oCustomer.paper_invoice:
        return redirect('kg.views_kunden.rechnungen')

    return render_to_response('kg/kunden_rechnungen_papier.html',
                              {'individual_text': PAPER_INVOICE_SETTINGS['SITE_TEXT']},
                              context_instance=RequestContext(request))


@login_required
@customer_required
def rechnungen_download(request, invoice_name):
    # search for invoice and check permission
    try:
        invoice = InvoiceLog.objects.get(invoice_file__endswith=invoice_name,
                                         child__customer=request.user.get_profile())
        if invoice.first_downloaded is None:
            invoice.first_downloaded = date.today()
            invoice.save()

        response = HttpResponse(file(INVOICE_ROOT + "/" + invoice.invoice_file).read())
        response['Content-Type'] = 'application/pdf'
        response['Content-Disposition'] = 'attachment'
        #response['Content-Disposition'] = 'attachment; filename=somefilename.pdf'
        return response

    except:
        raise Http404


@login_required
@customer_required
def abo(request):

    # Redirect if module flatrate is not activated
    if not MODULE_FLATRATE:
        return redirect('kg.views_kunden.index')
    # flatrate maybe deactivated in prepaid mode
    if MODULE_PREPAID and not PREPAID_SETTINGS['FLATRATE']:
        return redirect('kg.views_kunden.index')

    #hack to exclude Children in Group 'Kundenparkplaz' / ZZGEKUENDIGT
    qsChildren = Child.objects.select_related().filter(
        customer__user=request.user
        ).exclude(facility__type__name='Kundenparkplatz')

    # no child configured
    if not qsChildren:
        return render_to_response('kg/kunden_keine_kinder.html', {}, context_instance=RequestContext(request))

    #show actual flatrate status
    lChildren = []
    for oChild in qsChildren:
        mealtimes_list = []

        qsMeal = Meal.objects.filterAvailable(
            mealplan=oChild.facility_subunit.mealplan,
            allergy=oChild.order_allergy_meal
        )

        is_school = oChild.facility.type.id == FacilityType.SCHOOL
        flatrate_customizable = oChild.facility.flatrate_customizable

        for oMeal in qsMeal:

            weekday_list = []
            holiday_list = []
            weekdays = WEEKDAYS[:oChild.facility_subunit.mealplan.weekdays]
            for oDay in weekdays:

                try:
                    flatrate = Flatrate.objects.get(child=oChild, meal=oMeal, weekday=oDay[0])
                except Flatrate.DoesNotExist:
                    active = False
                    active_on_holidays = False
                else:
                    active = flatrate.active
                    active_on_holidays = flatrate.active_on_holidays

                weekday_list.append({'weekday': oDay,
                                     'mealtime_name': oMeal.name,
                                     'mealtime': oMeal,
                                     'kind': 'normal',
                                     'selected': active})

                if is_school:
                    holiday_list.append({'weekday': oDay,
                                         'mealtime_name': oMeal.name + u' (Ferien)',
                                         'mealtime': oMeal,
                                         'kind': 'holiday',
                                         'selected': active_on_holidays})

            mealtimes_list.append(weekday_list)
            if is_school:
                mealtimes_list.append(holiday_list)

        lChildren.append({
            'child': oChild,
            'weekdays': weekdays,
            'mealtimes': mealtimes_list,
            'flatrate_customizable': flatrate_customizable
            })

    return render_to_response('kg/kunden_abo.html', {'child_list': lChildren},
                              context_instance=RequestContext(request))


@login_required
@customer_required
def abo_edit(request, child_id):

    # Redirect if module flatrate is not activated
    if not MODULE_FLATRATE:
        return redirect('kg.views_kunden.index')
    # flatrate maybe deactivated in prepaid mode
    if MODULE_PREPAID and not PREPAID_SETTINGS['FLATRATE']:
        return redirect('kg.views_kunden.index')

    if request.method == 'POST':

        # check for cancelation
        if request.POST.get('cancel'):
            return redirect('kg.views_kunden.abo')

        sErrorMessage = "Es ist ein Fehler aufgetreten! Ihre Änderungen wurden nicht gespeichert."

        with transaction.commit_manually():

            try:
                customer = request.user.get_profile()
                child = Child.objects.get(pk=request.POST.get('child_id'))
                if not child.customer == customer:
                    raise StandardError
                weekdays = WEEKDAYS[:child.facility_subunit.mealplan.weekdays]

                # check if flatrate is changeable
                flatrate_customizable = child.facility.flatrate_customizable
                if not flatrate_customizable:
                    raise StandardError

                qsMeals = Meal.objects.filterAvailable(
                    mealplan=child.facility_subunit.mealplan,
                    allergy=child.order_allergy_meal
                )
                is_school = child.facility.type.id == FacilityType.SCHOOL

                # check if more than one meal per mealtime per day is activated
                oMealplan = child.facility_subunit.mealplan
                if not oMealplan.duplicate_order_mealtime:
                    qsMealtimes = Mealtime.objects.distinct().filter(meal__in=qsMeals)
                    for oMealtime in qsMealtimes:
                        oTempMealtimes = qsMeals.filter(mealtime=oMealtime)
                        lSelectedMealsNormalIds = []
                        lSelectedMealsHolidayIds = []
                        for oTempMealtime in oTempMealtimes:
                            lSelectedMealsNormalIds += request.POST.getlist(
                                'mealtime_' + str(oTempMealtime.id) + '_normal')
                            if is_school:
                                lSelectedMealsHolidayIds += request.POST.getlist(
                                    'mealtime_' + str(oTempMealtime.id) + '_holiday')
                        if len(lSelectedMealsNormalIds) is not len(set(lSelectedMealsNormalIds)) or len(
                            lSelectedMealsHolidayIds) is not len(set(lSelectedMealsHolidayIds)):
                            sErrorMessage = u"Eingabefehler: Sie dürfen nur ein %s pro Tag auswählen. Bitte korrigieren Sie Ihre Eingabe." % oMealtime.name
                            raise StandardError

                for oMeal in qsMeals:

                    post_mealtimes_normal = request.POST.getlist('mealtime_' + str(oMeal.id) + '_normal')
                    post_mealtimes_holiday = request.POST.getlist('mealtime_' + str(oMeal.id) + '_holiday')

                    #walk through all weekdays and check if we got flatrate bookings
                    for oDay in weekdays:

                        try:
                            # search in  all objects, also in the canceled ones
                            flatrate = Flatrate.objects.get(child=child, meal=oMeal, weekday=oDay[0])

                        except Flatrate.DoesNotExist:
                            # create new flatrate
                            flatrate = Flatrate(child=child, meal=oMeal, weekday=oDay[0],
                                                created_by=request.user)

                        except:
                            raise  # re-raise the last exception

                        # check for normal weekdays
                        if str(oDay[0]) in post_mealtimes_normal:
                            flatrate.active = True
                        else:
                            flatrate.active = False

                        # set active_on_holidays to the same value as active
                        flatrate.active_on_holidays = flatrate.active

                        # now handle the holidays if child is in school
                        if is_school:
                            if str(oDay[0]) in post_mealtimes_holiday:
                                flatrate.active_on_holidays = True
                            else:
                                flatrate.active_on_holidays = False

                        flatrate.modified_by = request.user
                        flatrate.save()

            except Exception as exc:
                #print exc.message
                transaction.rollback()
                messages.error(request, sErrorMessage)
                #return redirect('kg.views_kunden.abo_edit')

            else:
                transaction.commit()

                # log action
                ActionLog.objects.log_change(request=request, object=None,
                                             message=u'Abo geändert: %s' % force_unicode(child))
                transaction.commit()  # extra commit for ActionLog

                # message for user and redirect
                messages.success(request, "Ihre Änderungen wurden gespeichert.")
                return redirect('kg.views_kunden.abo')

    # show actual flatrate status
    try:
        child = Child.objects.get(customer__user=request.user, pk=child_id)
    except Child.DoesNotExist:
        return redirect('kg.views_kunden.abo')

    lMealtimes = []
    weekdays = WEEKDAYS[:child.facility_subunit.mealplan.weekdays]

    # qsMeals = Meal.objects.filter(mealplan__facilitysubunit__child=child)
    qsMeals = Meal.objects.filterAvailable(
        mealplan=child.facility_subunit.mealplan,
        allergy=child.order_allergy_meal
    )

    is_school = child.facility.type.id == FacilityType.SCHOOL
    flatrate_customizable = child.facility.flatrate_customizable

    for oMeal in qsMeals:

        weekday_list = list()
        holiday_list = list()

        for oDay in weekdays:

            try:
                flatrate = Flatrate.objects.get(child=child, meal=oMeal, weekday=oDay[0])
            except Flatrate.DoesNotExist:
                active = False
                active_on_holidays = False
            else:
                active = flatrate.active
                active_on_holidays = flatrate.active_on_holidays

            weekday_list.append({'weekday': oDay,
                                 'mealtime_name': oMeal.name,
                                 'mealtime': oMeal,
                                 'kind': 'normal',
                                 'selected': active})

            if is_school:
                holiday_list.append({'weekday': oDay,
                                     'mealtime_name': oMeal.name + u' (Ferien)',
                                     'mealtime': oMeal,
                                     'kind': 'holiday',
                                     'selected': active_on_holidays})

        lMealtimes.append(weekday_list)
        if is_school:
            lMealtimes.append(holiday_list)

    return render_to_response('kg/kunden_abo_edit.html', {'child': child,
                                                          'weekdays': weekdays,
                                                          'mealtimes': lMealtimes,
                                                          'flatrate_customizable': flatrate_customizable,
                                                          'is_school': is_school},
                              context_instance=RequestContext(request))


@login_required
@customer_required
def kinder(request):
    childs = list()
    #hack to exclude Children in Group 'Kundenparkplaz' / ZZGEKUENDIGT
    child_result = Child.objects.select_related().filter(customer__user=request.user).exclude(
        facility__type__name='Kundenparkplatz')

    for child in child_result:
        reductions = ChildReduction.objects.filter(child=child, date_end__gte=date.today()).order_by(
            'date_end').reverse()
        childs.append({'child': child, 'reductions': reductions})
    return render_to_response('kg/kunden_kinder.html', {'childs': childs}, context_instance=RequestContext(request))


@login_required
@customer_required
def kinder_add(request):
    facility_subunits = FacilitySubunit.objects.select_related('facility').filter(facility__is_active=True)

    if request.method == 'POST':

        # check for cancelation
        if request.POST.get('cancel'):
            return redirect('kg.views_kunden.kinder')

        form = ChildForm(request.POST)

        if form.is_valid():
            oNewChild = form.save(commit=False)
            oNewChild.customer = request.user.get_profile()
            oNewChild.save()
            messages.success(request, "Ihre Angaben wurden gespeichert.")
            ActionLog.objects.log_addition(request=request, object=oNewChild,
                                           message=u'Kind neu: %s' % force_unicode(oNewChild))
            return redirect('kg.views_kunden.kinder')

    else:
        form = ChildForm()

    return render_to_response('kg/kunden_kinder_edit.html', {'form': form,
                                                             'facility_subunits': facility_subunits},
                              context_instance=RequestContext(request))


@login_required
@customer_required
def kinder_edit(request, child_id):
    try:
        try:
            child = Child.objects.get(pk=child_id)
        except Child.DoesNotExist:
            raise StandardError
        except:
            raise

        # security
        customer = request.user.get_profile()
        if not child.customer == customer:
            return redirect('kg.views_kunden.kinder')

        facility_subunits = FacilitySubunit.objects.select_related('facility').filter(facility__is_active=True)
        selected_fs = FacilitySubunit.objects.filter(child=child)

        if request.method == 'POST':

            # check for cancelation
            if request.POST.get('cancel'):
                return redirect('kg.views_kunden.kinder')

            form = ChildForm(request.POST, instance=child)

            if form.is_valid():
                form.save()
                messages.success(request, "Ihre Änderungen wurden gespeichert.")
                ActionLog.objects.log_change(request=request, object=child,
                                             message=u'Kind geändert: %s' % force_unicode(child))
                return redirect('kg.views_kunden.kinder')

        else:
            form = ChildForm(instance=child)

    except:
        messages.error(request,
                       "Es ist ein Fehler aufgetreten! Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.")
        return redirect('kg.views_kunden.kinder')

    else:
        return render_to_response('kg/kunden_kinder_edit.html', {'form': form, 'child': child,
                                                                 'edit': True, 'facility_subunits': facility_subunits,
                                                                 'selected_fs': selected_fs},
                                  context_instance=RequestContext(request))


@login_required
@customer_required
def profil(request):
    """
    Shows the profile of the customer
    The edit pay type link is only shown if the customer has received one invoice
    """
    customer = request.user.get_profile()

    try:
        sepa_mandate = SepaMandate.objects.get(customer=customer, is_valid=True)
    except Exception:
        sepa_mandate = None

    # Check if this customer can change to any other pay type
    paytype_available = False
    for obj in PayType.objects.exclude(pk=customer.pay_type.pk):
        if obj.customer_satisfied(customer):
            paytype_available = True
            break
    paytype_editable = paytype_available and customer.isNewCustomer()

    return render_to_response(
        'kg/kunden_profil.html',
        {
            'cust': customer,
            'sepa_mandate': sepa_mandate,
            'paytype_editable': paytype_editable,
        },
        context_instance=RequestContext(request)
    )


@login_required
@customer_required
def profil_edit(request):
    customer = request.user.get_profile()

    if request.method == 'POST':

        # check for cancelation
        if request.POST.get('cancel'):
            return redirect('kg.views_kunden.profil')

        form = CustomerForm(request.POST, instance=customer)

        if form.is_valid():
            form.save()
            ActionLog.objects.log_change(request=request, object=customer,
                                         message=u'Kundendaten geändert: %s' % force_unicode(customer))
            return redirect('kg.views_kunden.profil')

    else:
        form = CustomerForm(instance=customer)

    return render_to_response('kg/kunden_profil_edit.html', {'form': form}, context_instance=RequestContext(request))


@login_required
@customer_required
def paytype_edit(request):
    customer = request.user.get_profile()

    try:
        sepa_mandate = SepaMandate.objects.get(customer=customer, is_valid=True)
    except Exception:
        sepa_mandate = None

    if request.method == 'POST':

        # check for cancelation
        if request.POST.get('cancel'):
            return redirect('kg.views_kunden.profil')

        paytype_form = PayTypeForm(request.POST, instance=customer)
        sepa_form = SepaMandateForm(request.POST, instance=sepa_mandate, prefix="sepa")

        if paytype_form.is_valid():
            if (paytype_form.cleaned_data['pay_type'].id != PayType.DEBIT or
                paytype_form.cleaned_data['pay_type'].id == PayType.DEBIT and sepa_form.is_valid()):

                paytype_form.save()

                if paytype_form.cleaned_data['pay_type'].id == PayType.DEBIT:
                    if PayType.is_sepa_mandate_editable():
                        new_sepa_mandate = sepa_form.save(commit=False)
                        new_sepa_mandate.customer = customer
                        new_sepa_mandate.save()
                    else:
                        messages.warning(
                            request,
                            u'Können Lastschriftverfahren nicht aktualisiert werden'
                        )

                ActionLog.objects.log_change(request=request, object=customer,
                                         message=u'Zahlungsdaten geändert: %s' % force_unicode(customer))

                return redirect('kg.views_kunden.profil')

    else:
        paytype_form = PayTypeForm(instance=customer)
        sepa_form = SepaMandateForm(instance=sepa_mandate, prefix="sepa")

    return render_to_response('kg/kunden_profil_zahlungsart.html',
                              {'paytype_form': paytype_form, 'sepa_form': sepa_form, 'sepa_mandate': sepa_mandate},
                              context_instance=RequestContext(request))


@login_required
@customer_required
def sepa_mandate_edit(request):
    # Show error if updating SEPA mandate is not permitted.
    # Bot GET and POST are affected.
    if not PayType.is_sepa_mandate_editable():
        messages.error(request, u'Der Betrieb ist nicht erlaubt')
        return redirect('kg.views_kunden.profil')

    customer = request.user.get_profile()
    try:
        sepa_mandate = SepaMandate.objects.get(customer=customer, is_valid=True)
    except Exception:
        sepa_mandate = None

    if request.method == 'POST':

        # check for cancelation
        if request.POST.get('cancel'):
            return redirect('kg.views_kunden.profil')

        form = SepaMandateForm(request.POST, instance=sepa_mandate)

        if form.is_valid():
            form.save()
            ActionLog.objects.log_change(request=request, object=sepa_mandate,
                                         message=u'SEPA Mandat geändert: %s' % force_unicode(customer))
            return redirect('kg.views_kunden.profil')

    else:
        form = SepaMandateForm(instance=sepa_mandate)

    return render_to_response('kg/kunden_profil_sepa_mandate.html', {'sepa_form': form, 'sepa_mandate': sepa_mandate},
                              context_instance=RequestContext(request))


@login_required
def password_change(request):
    if request.method == 'POST':

        # check for cancelation
        if request.POST.get('cancel'):
            return redirect('kg.views_kunden.profil')

        form = CustomerPasswordChangeForm(request.POST)

        if form.is_valid():

            try:
                request.user.set_password(form.cleaned_data['password'])
                request.user.save()
                messages.success(request, 'Das Passwort wurde erfolgreich geändert.')
            except:
                messages.error(request, 'Es ist ein Fehler aufgetreten. Das Passwort konnte nicht geändert werden.')

            return redirect('kg.views_kunden.profil')

    else:
        form = CustomerPasswordChangeForm()

    return render_to_response('kg/kunden_password_change.html', {'form': form},
                              context_instance=RequestContext(request))


@login_required
@customer_required
def guthaben(request):

    # show view only if MODULE_PREPAID is active
    if not MODULE_PREPAID:
        raise Http404

    oCustomer = request.user.customer
    qsChildren = Child.objects.filter(customer=oCustomer)

    # no child configured
    if not qsChildren:
        return render_to_response('kg/kunden_keine_kinder.html', {}, context_instance=RequestContext(request))

    sCustomerIdentifier = str(qsChildren[0].cust_nbr)
    if OVERLOAD_SITE in ['rudolstadt']:
        sCustomerIdentifier = str(oCustomer.extra_attribute)
    if OVERLOAD_SITE == 'schmalkalden':
        # set additional identifier for every facility
        if qsChildren and qsChildren[0].facility.id == 1:
            sCustomerIdentifier = '%s GSR' % oCustomer.extra_attribute
        elif qsChildren and qsChildren[0].facility.id == 2:
            sCustomerIdentifier = '%s BBZ' % oCustomer.extra_attribute
        else:
            sCustomerIdentifier = '%s' % oCustomer.extra_attribute  # default


    sBankTransferDescription = "%s %s" % (sCustomerIdentifier, oCustomer.name)
    lBankTransferDescriptionList = []  # only used for special instances
    if oCustomer.surname != '':
        sBankTransferDescription += ", " + oCustomer.surname

    # special case for esd-essen.de
    if OVERLOAD_SITE == 'esd':
        for oChild in qsChildren:
            lBankTransferDescriptionList.append('Verpflegung: KUNDENNUMMER, %s, %s' % (oChild.name, oChild.surname))

    qsBookings = CustomerAccountTransaction.objects.filter(customer=oCustomer).all()
    items_in_page = 20
    paginator = Paginator(qsBookings, items_in_page) # Show 50 contacts per page
    page = request.GET.get('page')

    try:
        paged_bookings = paginator.page(page)
        # Convert to int to correctly detect active page in template
        page = int(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        paged_bookings = paginator.page(1)
        page = 1
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        paged_bookings = paginator.page(paginator.num_pages)
        page = paginator.num_pages

    bookings = []
    # clean description for CAT types debit and debit return
    for booking in paged_bookings:

        new_booking = {'date': booking.transaction_date,
                       'amount': booking.amount}

        if booking.type == CustomerAccountTransaction.TYPE_DEBIT:
            new_booking['description'] = 'Lastschrift'
        elif booking.type == CustomerAccountTransaction.TYPE_DEBIT_RETURN:
            new_booking['description'] = 'Lastschriftrückgabe'
        else:
            new_booking['description'] = booking.description

        bookings.append(new_booking)
    #Load cleaned bookings back in pager
    paged_bookings.object_list = bookings

    bShowSepa = False
    if PREPAID_SETTINGS['SEPA_DEBIT'] == True and oCustomer.pay_type_id == PayType.DEBIT:
        bShowSepa = True

    return render_to_response('kg/kunden_guthaben.html',
                              {'customer': oCustomer,
                               'paged_bookings': paged_bookings,
                               'page': page,
                               'bank_transfer_purpose': sBankTransferDescription,
                               'bank_transfer_purpose_list': lBankTransferDescriptionList,
                               'show_sepa': bShowSepa,
                               'show_bank_transfer': PREPAID_SETTINGS['BANK_TRANSFER'],
                               },
                              context_instance=RequestContext(request))


@login_required
@customer_required
def guthaben_aufladen(request):
    """ Customer view to charge credit per SEPA debit payment """

    #  show view only if MODULE_PREPAID is activated
    if not MODULE_PREPAID == True:
        return redirect('kg.views_kunden.guthaben')

    # show view only if SEPA payment is activated
    if not PREPAID_SETTINGS['SEPA_DEBIT'] == True:
        return redirect('kg.views_kunden.guthaben')

    oCustomer = request.user.customer

    # redirect if customers paytype is not debit payment
    if oCustomer.pay_type_id != PayType.DEBIT:
        return redirect('kg.views_kunden.guthaben')

    show_subform1 = False
    show_subform2 = False
    sepa_error = False
    sepa_error_no_mandate = False
    sepa_error_to_many_mandates = False
    sepa_mandate = None
    cleaned_amount = None

    if request.method == 'POST':

        # check for cancelation
        if request.POST.get('cancel'):
            return redirect('kg.views_kunden.guthaben')

        form = ChargeCreditForm(request.POST)

        if form.is_valid():

            if 'subform2_sent' in request.POST:

                # save the SEPA debit, create corresponding CAT and redirect to guthaben index site
                with transaction.commit_manually():
                    try:
                        sepa_mandate = SepaMandate.objects.get(customer=oCustomer, is_valid=True)
                        # TODO: next sequence type is fix at the moment
                        oSepaDebit = SepaDebit(customer=oCustomer,
                                               amount=form.cleaned_data['amount'],
                                               description=u"Essengeld Guthaben Aufladung, Ihr Auftrag vom %s" % datetime.now().strftime("%d.%m.%Y"),
                                               bank_account_owner=sepa_mandate.name,
                                               bank_account_iban=sepa_mandate.iban,
                                               bank_account_bic=sepa_mandate.bic,
                                               mandate_reference=sepa_mandate.mandate_id,
                                               mandate_date=sepa_mandate.created_at,
                                               endtoend_id=SepaHelper().createEndToEndId(SEPA['SEPA_EREF_PREFIX']),
                                               sequence_type='RCUR'
                                               )
                        oSepaDebit.save()

                        oCAT = CustomerAccountTransaction(customer=oCustomer,
                                                          transaction_id="sepadebit_%d" % oSepaDebit.id,
                                                          type=CustomerAccountTransaction.TYPE_DEBIT,
                                                          status=CustomerAccountTransaction.STATUS_OPEN,
                                                          amount = oSepaDebit.amount,
                                                          description = u"Lastschrifteinzug",
                                                          transaction_date=oSepaDebit.submission_time)
                        oCAT.save()

                        transaction.commit()
                        messages.success(request, u'%s Euro werden in den nächsten Tagen von Ihrem Konto abgebucht und wurden Ihrem Guthaben bereits hinzugefügt.' % str(form.cleaned_data['amount']).replace('.', ','))
                    except Exception as e:
                        transaction.rollback()
                        messages.error(request, 'Es ist ein Fehler aufgetreten. Bitte kontaktieren Sie uns.')

                return redirect('kg.views_kunden.guthaben')

            else:
                # show confirmation form
                show_subform2 = True
                try:
                    cleaned_amount = form.cleaned_data['amount']
                    sepa_mandate = SepaMandate.objects.get(customer=oCustomer, is_valid=True)
                except Exception as e:
                    #print e.message
                    messages.error(request, 'Es ist ein Fehler aufgetreten. Bitte kontaktieren Sie uns.')
                    return redirect('kg.views_kunden.guthaben')

        else:
            show_subform1 = True

    else:
        # check if customer has a valid sepa mandate
        sepa_mandates = SepaMandate.objects.filter(customer=oCustomer, is_valid=True)
        if len(sepa_mandates) != 1:
            sepa_error = True
            if len(sepa_mandates) < 1:
                sepa_error_no_mandate = True
            else:
                sepa_error_to_many_mandates = True

        # show subform 1
        form = ChargeCreditForm()
        show_subform1 = True

    return render_to_response('kg/kunden_guthaben_aufladen.html',
                              {'form': form,
                               'cleaned_amount': cleaned_amount,
                               'show_subform1': show_subform1,
                               'show_subform2': show_subform2,
                               'sepa_mandate': sepa_mandate,
                               'sepa_error': sepa_error,
                               'sepa_error_no_mandate': sepa_error_no_mandate,
                               'sepa_error_to_many_mandates': sepa_error_to_many_mandates,
                               'SEPA': SEPA},
                              context_instance=RequestContext(request))

@login_required
@customer_required
def guthaben_minimum(request):
    '''
    View to adjust the minimum credit amount.
    If the credit falls below this amount the user want's to be warned.
    '''

    #  show view only if MODULE_PREPAID is active
    if not MODULE_PREPAID:
        raise Http404

    if request.method == 'POST':

        # check for cancelation
        if request.POST.get('cancel'):
            return redirect('kg.views_kunden.guthaben')

        form = ChargeCreditForm(request.POST)

        if form.is_valid():
            try:
                customer = request.user.customer
                customer.credit_minimum = form.cleaned_data['amount']
                customer.save()
                messages.success(request, u"Ihr Guthaben Minimum wurde geändert.")
            except Exception as e:
                messages.error(request, 'Es ist ein Fehler aufgetreten. Bitte kontaktieren Sie uns.')

            return redirect('kg.views_kunden.guthaben')

    else:
        form = ChargeCreditForm()

    return render_to_response('kg/kunden_guthaben_minimum.html',
                              {'form': form},
                              context_instance=RequestContext(request))
