# -*- coding: utf-8 -*-
import os
import re
import hashlib
from random import randint
from decimal import Decimal
from datetime import date, datetime

from django.db import models, connection, transaction
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.util import quote
from django.utils.encoding import smart_unicode
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.utils.functional import cached_property
from django.db.models import Max, Sum, Count
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import RegexValidator
from django.core.urlresolvers import reverse
from django.template.defaultfilters import escape
from smart_selects.db_fields import ChainedForeignKey
from model_utils.managers import PassThroughManager
from simple_history.models import HistoricalRecords

from kg_utils.date_helper import DateHelper
from kg_utils.validators import IbanValidator, BicValidator, validate_email
from settings import (
    REMINDER_ROOT, ACCOUNTING_SETTINGS, MEALTIMES, MODULE_PREPAID,
    OVERLOAD_SITE, CANTEEN_MODE, CUST_EXTRA, CUST_EXTRA2, CHILD_EXTRA,
    CHILD_EXTRA2
)

ANREDE_CHOICES_DEFAULT = (
    ('Herr', 'Herr'),
    ('Frau', 'Frau'),
)
if CANTEEN_MODE:
    ANREDE_CHOICES = ANREDE_CHOICES_DEFAULT
else:
    ANREDE_CHOICES = ANREDE_CHOICES_DEFAULT + (('Familie', 'Familie'),)

ORDER_CANCEL_TIMES = (
    (0, 'gleicher Tag'),
    (1, 'vorheriger Tag'),
    (2, 'zwei Tage vorher'),
    (90, 'vorheriger Tag bzw. Freitag')
)

REDUCTION_FACILITY_TYPES = (
    ('schule', 'Schule'),
    ('hort', 'Hort'),
    ('kita', 'Kita')
)

PAYTYPE_CONDITION_MONTH = 'month'
PAYTYPE_CONDITION_INVOICE = 'invoice'

PAYTYPE_CONDITION_CHOICES = (
    (PAYTYPE_CONDITION_INVOICE, u'Invoice'),
    (PAYTYPE_CONDITION_MONTH, u'Month'),
)

'''
The businessdays of a week

The key is set according to the python date.isoweekday() function
Use date.isoweekday() instead of date.weekday() when calculating with dates
'''
WEEKDAYS = (
    (1, 'Montag'),
    (2, 'Dienstag'),
    (3, 'Mittwoch'),
    (4, 'Donnerstag'),
    (5, 'Freitag'),
    (6, 'Samstag'),
    (7, 'Sonntag')
)


#Einrichtungstyp: z.B. Schule, Kindergarten
class FacilityType(models.Model):

    SCHOOL = 1
    KINDERGARTEN = 2
    KUNDENPARKPLATZ = 3

    FACILITY_TYPE_CHOICES = (
        (SCHOOL, 'Schule'),
        (KINDERGARTEN, 'Kindergarten'),
        (KUNDENPARKPLATZ, 'Kundenparkplatz'),
    )

    id = models.PositiveSmallIntegerField(primary_key=True, choices=FACILITY_TYPE_CHOICES, verbose_name="Id")
    name = models.CharField(max_length=50, verbose_name="Bezeichnung")
    name_short = models.CharField(max_length=20, blank=True, verbose_name="Kürzel")

    class Meta:
        ordering = ['name']
        verbose_name = "Einrichtung - Typ"
        verbose_name_plural = "Einrichtungen - Typen"

    def __unicode__(self):
        return self.name


class FacilityManager(models.Manager):
    def get_query_set(self):
        return super(FacilityManager, self).get_query_set().exclude(type__name='Kundenparkplatz')


# konkrete Einrichtung
class Facility(models.Model):

    TAX0 = 0
    TAX7 = 7
    TAX10 = 10
    TAX19 = 19

    TAX_RATE_CHOICES = (
        (TAX0, '0 %'),
        (TAX7, '7 %'),
        (TAX10, '10 %'),
        (TAX19, '19 %')
    )

    is_active = models.BooleanField(default=True, verbose_name="Aktiv")
    name = models.CharField(max_length=100, verbose_name="Name")
    name_short = models.CharField(max_length=20, verbose_name="Kürzel")
    type = models.ForeignKey(FacilityType, verbose_name="Art")
    flatrate_customizable = models.BooleanField(default=True, verbose_name="Abonnement durch Kunden änderbar")
    tax_rate = models.PositiveSmallIntegerField(default=TAX19, choices=TAX_RATE_CHOICES, verbose_name="Mehrwertsteuer-Satz")

    objects = models.Manager()  # retrieve all facilities; including the Kundenparkplatz Facility type
    ex_objects = FacilityManager()  # retrieve all but the Facility type Kundenparkplatz

    class Meta:
        ordering = ['name']
        verbose_name = "Einrichtung"
        verbose_name_plural = "Einrichtungen"

    def __unicode__(self):
        return self.name

    @property
    def is_empty(self):
        """Determine if the object containing any children."""
        return not self.child_set.exists()

    @cached_property
    def is_kindergarten(self):
        return self.type.id == FacilityType.KINDERGARTEN

    @cached_property
    def is_school(self):
        return self.type.id == FacilityType.SCHOOL


# Untereinheit einer Einrichtung: z.B. Gruppe, Klasse
class FacilitySubunit(models.Model):
    TAX_INHERIT = 9999  # Will take tax rate of facility at saving
    TAX_RATE_CHOICES = ((TAX_INHERIT, "Einrichtung"),) + Facility.TAX_RATE_CHOICES

    name = models.CharField(max_length=100, verbose_name="Name")
    name_short = models.CharField(max_length=20, verbose_name="Kürzel")
    facility = models.ForeignKey(Facility, verbose_name="Einrichtung")
    mealplan = models.ForeignKey('menus.MealPlan', verbose_name="Speiseplan")
    price_group = models.ForeignKey('menus.PriceGroup', verbose_name="Preisgruppe")
    sortKey = models.PositiveSmallIntegerField(default=0, verbose_name="Sortierung")
    tax_rate = models.PositiveSmallIntegerField(
        default= TAX_INHERIT,
        choices = TAX_RATE_CHOICES,
        verbose_name="Mehrwertsteuer-Satz")

    class Meta:
        ordering = ['sortKey', 'name']
        verbose_name = "Gruppe/Klasse"
        verbose_name_plural = "Gruppen/Klassen"

    def __unicode__(self):
        return self.name

    @property
    def is_empty(self):
        """Determine if the object containing any children."""
        return not self.child_set.exists()

    def save(self, *args, **kwargs):
        """When there is not mealplan provided (in subfacility inline),
        it will be extracted from self.price_group."""
        if self.mealplan_id is None and self.price_group is not None:
            self.mealplan = self.price_group.mealplan
        if self.tax_rate == self.TAX_INHERIT:
            self.tax_rate = self.facility.tax_rate
        super(FacilitySubunit, self).save(*args, **kwargs)


# Ermäßigung allgemein
class Reduction(models.Model):
    name = models.CharField(max_length=100, verbose_name="Bezeichnung")
    reduction = models.DecimalField(
        max_digits=4, decimal_places=2, verbose_name="Anteil Kunde")
    mealtimes = models.ManyToManyField(
        'menus.Mealtime',
        default=[MEALTIMES['lunch']],
        verbose_name="Mahlzeiten")
    valid_on_holiday = models.BooleanField(
        default=False, verbose_name=u'Auch in den Ferien gültig',
        help_text=u'Auswahl gilt nur für die Schule. '
                  u'In Kitas wird die Ermäßigung immer berücksichtigt.')
    per_menu = models.BooleanField(
        default=True, verbose_name="Kundenanteil gilt pro Essen",
        help_text="Bitte lassen Sie diese Checkbox leer, wenn der Anteil Kunde eine Monatspauschale ist.")

    class Meta:
        ordering = ['name']
        verbose_name = "Ermäßigung"
        verbose_name_plural = "Ermäßigungen"

    def __unicode__(self):
        return self.name


# Zahlungsart
class PayType(models.Model):

    DEBIT = 1
    INVOICE = 2
    CASH = 3
    OTHER = 9

    if MODULE_PREPAID:
        sInvoiceName = u'Überweisung'
    else:
        sInvoiceName = 'Rechnung'

    if OVERLOAD_SITE == 'sonnenblick':
        PAYTYPE_CHOICES = (
            (DEBIT, 'Mitarbeiter - Lastschriftverfahren'),
            (INVOICE, sInvoiceName),
            (CASH, 'Bar'),
            (OTHER, 'Praktikanten - Keine')
        )
    else:
        PAYTYPE_CHOICES = (
            (DEBIT, 'Lastschriftverfahren'),
            (INVOICE, sInvoiceName),
            (CASH, 'Bar'),
            (OTHER, 'Sonstige')
        )

    id = models.PositiveSmallIntegerField(primary_key=True, choices=PAYTYPE_CHOICES, verbose_name="Zahlungsart")
    new_customers = models.BooleanField(default=False, verbose_name="Für Neukunden erlaubt")
    change_condition_type = models.CharField(
        max_length=32,
        choices=PAYTYPE_CONDITION_CHOICES,
        verbose_name=u'Zustand ändern',
        default=PAYTYPE_CONDITION_CHOICES[0][0])
    change_condition_value = models.PositiveIntegerField(
        verbose_name=u'Bedingungswert',
        default=3)
    editable_sepa_mandate = models.BooleanField(
        default=True,
        verbose_name=u'Bearbeitbare Lastschrift')

    class Meta:
        ordering = ['id']
        verbose_name = "Zahlungsart"
        verbose_name_plural = "Zahlungsarten"

    def __unicode__(self):
        return self.get_id_display()

    def customer_satisfied(self, customer):
        """Check if the current customer is qualified to change the payment
        type."""
        if self.change_condition_type == PAYTYPE_CONDITION_MONTH:
            today = datetime.now().date()
            month_count = 12*(today.year - customer.created_at.year) + \
                (today.month - customer.created_at.month)
        if self.change_condition_type == PAYTYPE_CONDITION_INVOICE:
            # Count number of invoiced months present in DB
            truncate_date = connection.ops.date_trunc_sql('month', 'released')
            qs = InvoiceLog.objects.filter(child__customer=customer).exclude(
                released=None)
            qs = qs.extra({'month': truncate_date})
            month_count = qs.values('month').distinct().count()
        return month_count >= self.change_condition_value

    @classmethod
    def is_sepa_mandate_editable(cls):
        try:
            paytype_debit = PayType.objects.get(id=cls.DEBIT)
            return paytype_debit.editable_sepa_mandate
        except PayType.DoesNotExist:
            pass
        return False


# Gründe für Abbestellung: z.B. Krankheit, Urlaub
class CancelReason(models.Model):
    name = models.CharField(max_length=100, verbose_name="Bezeichnung")
    name_short = models.CharField(max_length=20, blank=True, verbose_name="Kürzel")

    class Meta:
        ordering = ['name']
        verbose_name = "Abbestell-Grund"
        verbose_name_plural = "Abbestell-Gründe"

    def __unicode__(self):
        return self.name


# Kunde, also meistens die Eltern; Empfänger der Rechnung
class Customer(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, verbose_name="User")
    title = models.CharField(max_length=50, blank=True, choices=ANREDE_CHOICES, verbose_name="Anrede")
    surname = models.CharField(max_length=50, blank=True, verbose_name="Vorname")
    name = models.CharField(max_length=50, verbose_name="Nachname")
    street = models.CharField(max_length=50, blank=True, verbose_name="Straße und Hausnummer")
    zip_code = models.CharField(max_length=50, blank=True, verbose_name="Postleitzahl")
    city = models.CharField(max_length=50, blank=True, verbose_name="Ort")
    phone = models.CharField(max_length=50, blank=True, verbose_name="Telefon")
    phone2 = models.CharField(max_length=50, blank=True, verbose_name="Telefon 2")
    email = models.CharField(max_length=75, blank=True, verbose_name="E-Mail", validators=[validate_email,])  # use validator from Django 1.9 branch because of new top level domains (e.g. .hamburg)
    pay_type = models.ForeignKey(PayType, default=PayType.INVOICE, verbose_name="Zahlungsart")
    note = models.TextField(null=True, blank=True, verbose_name="Notiz")
    extra_attribute = models.CharField(max_length=50, default='',
                                       blank=CUST_EXTRA['BLANK'], verbose_name=CUST_EXTRA['VERBOSE_NAME'],
                                       unique=CUST_EXTRA['UNIQUE'], help_text=CUST_EXTRA['HELP_TEXT'],
                                       choices=CUST_EXTRA['CHOICES'])
    extra_attribute2 = models.CharField(max_length=50, default='',
                                        blank=CUST_EXTRA2['BLANK'], verbose_name=CUST_EXTRA2['VERBOSE_NAME'],
                                        unique=CUST_EXTRA2['UNIQUE'], help_text=CUST_EXTRA2['HELP_TEXT'],
                                        choices=CUST_EXTRA2['CHOICES'])
    is_suspended = models.BooleanField(default=False, verbose_name="Gesperrt",
                                       help_text=u"Nach Aktivierung der Sperre kann der Kunde keine Bestellungen "
                                                 u"mehr tätigen.<br />"
                                                 u"ACHTUNG: Schon bestellte Essen werden NICHT automatisch gelöscht.")
    suspension_reason = models.TextField(blank=True, verbose_name="Sperrgrund",
                                         help_text="Dieser Text wird im Kundenbereich angezeigt.")
    new_password = models.CharField(max_length=128, blank=True)
    new_password_check = models.CharField(max_length=30, blank=True)
    credit_minimum = models.DecimalField(max_digits=5, decimal_places=2, default=Decimal('20.00'), verbose_name="Guthaben Minimum")
    account_balance = models.DecimalField(max_digits=8, decimal_places=2, default=0, verbose_name="Saldo")
    account_balance.account_balance_filter = True
    paper_invoice = models.BooleanField(default=False, verbose_name="Rechnung per Post",
                                        help_text=u"Wenn das Kästchen aktiviert ist, wünscht der Kunde Rechnung per Post. Die voreingestellten Unkosten werden bei Rechnungslegung automatisch berücksichtigt.")
    last_modified = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True, default=datetime(2000,1,1,0,0,0,0))

    class Meta:
        ordering = ['name', 'surname']
        if CANTEEN_MODE:
            verbose_name = "Mitarbeiter"
            verbose_name_plural = "Mitarbeiter"
        else:
            verbose_name = "Kunde"
            verbose_name_plural = "Kunden"

    def __unicode__(self):
        if self.surname == '':
            return self.name
        else:
            return self.name + ', ' + self.surname

    def _greeting(self):
        return self.title

    def isNewCustomer(self):
        """
        Decides whether the customer is new or not
        Customer is not new if he received an invoice
        """
        number_of_invoices = InvoiceLog.objects.filter(child__customer=self).count()
        return number_of_invoices > 0

    def delete(self, using=None):
        """
        The customer deletion is implemented to single object only. This
        is for making any cleaning actions required before the target
        object is really deleted.
        """
        with transaction.commit_on_success():
            CustomerAccountTransaction.objects.filter(customer=self).delete()
            CustomerAccountTransaction.history.filter(customer=self).delete()
            super(Customer, self).delete(using)


class CustomerAccountTransactionQuerySet(models.query.QuerySet):
    def open(self):
        return self.filter(status=CustomerAccountTransaction.STATUS_OPEN)

    def booked(self):
        return self.filter(status=CustomerAccountTransaction.STATUS_BOOKED)


class AdvancedHistory(models.Model):
    """Add more utility methods to historical models."""

    history_message = models.TextField(
        _('change message'), blank=True, null=True)

    class Meta:
        abstract = True

    # This one should be customizable by each model class
    __exclude_diff = ('last_modified',)

    def get_changes(self, with_values=True):
        """Return list of changed fields with old and new values"""
        changed_values = []
        try:
            prev_record = self.__class__.objects.filter(
                id=self.id, pk__lt=self.pk).order_by('-pk')[0]
        except IndexError:
            prev_record = None
        if prev_record:
            for field in self._meta.get_all_field_names():
                if field.find('history_') == 0 or field in self.__exclude_diff:
                    continue
                cur_val = getattr(self, field)
                old_val = getattr(prev_record, field)
                if old_val != cur_val:
                    entry = (field, old_val, cur_val) if with_values else field
                    changed_values.append(entry)
        return changed_values

    def display_general_change(self):
        if self.history_type == '~':
            if self.history_message:
                text = self.history_message
            else:
                changes = self.get_changes(False)
                if changes:
                    text = ', '.join(changes) + ' geändert.'
                else:
                    text = u'Kein Wechselgeld.'
        else:
            text = self.get_history_type_display()
        return text


# Kundenkontobuchungen
class CustomerAccountTransaction(models.Model):

    TYPE_BANK_TRANSFER_IN = 1  # same value as TYPE_BANK_TRANSFER_IN in model BankAccountTransaction
    TYPE_DEBIT = 2  # same value as TYPE_DEBIT in model BankAccountTransaction
    TYPE_DEBIT_RETURN = 3  # same value as TYPE_DEBIT_RETURN in model BankAccountTransaction
    TYPE_CASH = 4
    TYPE_INVOICE = 5
    TYPE_CREDIT = 6
    TYPE_REMINDER_FEE = 7
    TYPE_DEBIT_RETURN_FEE = 8
    TYPE_CHARGE = 9
    TYPE_DEPOSIT = 10
    TYPE_BANK_TRANSFER_OUT = 11
    TYPE_ADDITIONAL_CHARGE = 12
    TYPE_INVOICE_CORRECTION = 13
    TYPE_PAYMENT_LOSS = 14

    TYPE_CHOICES = (
        (TYPE_CASH, 'Bareinzahlung'),
        (TYPE_CHARGE, 'Belastung'),
        (TYPE_CREDIT, 'Gutschrift'),
        (TYPE_DEBIT, 'Lastschrift'),
        (TYPE_DEBIT_RETURN, u'Lastschriftrückgabe'),
        (TYPE_DEBIT_RETURN_FEE, u'Lastschriftrückgabe - Kosten'),
        (TYPE_REMINDER_FEE, u'Mahnkosten'),
        (TYPE_ADDITIONAL_CHARGE, 'Nachbelastung'),
        (TYPE_DEPOSIT, 'Pfand'),
        (TYPE_INVOICE, 'Rechnung'),
        (TYPE_INVOICE_CORRECTION, 'Rechnungskorrektur'),
        (TYPE_BANK_TRANSFER_OUT, u'Überweisungsausgang'),
        (TYPE_BANK_TRANSFER_IN, u'Überweisungseingang'),
        (TYPE_PAYMENT_LOSS, u'Forderungsverlust')
    )

    STATUS_OPEN = 0
    STATUS_BOOKED = 1

    STATUS_CHOICES = (
        (STATUS_OPEN, 'Offen'),
        (STATUS_BOOKED, 'Gebucht'),
    )

    customer = models.ForeignKey(Customer, verbose_name="Kunde")
    bat_hash = models.CharField(max_length=50, blank=True, verbose_name="Bank Account Transaction Hash")
    transaction_id = models.CharField(max_length=100, blank=True, verbose_name="Identifier")
    amount = models.DecimalField(max_digits=8, decimal_places=2, default=0, verbose_name="Betrag")
    description = models.CharField(max_length=1000, blank=True, verbose_name="Beschreibung")
    type = models.PositiveSmallIntegerField(choices=TYPE_CHOICES, verbose_name="Typ")
    transaction_date = models.DateTimeField(default=datetime.now, verbose_name="Buchungsdatum")
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=STATUS_OPEN, verbose_name="Status")
    connected_to = models.CharField(max_length=100, blank=True, verbose_name="Verknuepft mit")
    invoice_id = models.PositiveIntegerField(default=0, verbose_name="Rechnung")
    reminder_id = models.PositiveIntegerField(default=0, verbose_name="Mahnung Id")
    on_next_invoice = models.BooleanField(default=False, verbose_name=u"Auf nächste Rechnung")
    last_modified = models.DateTimeField(auto_now=True)

    # TODO: Replace with native django code from Django 1.7 on
    # https://docs.djangoproject.com/en/1.7/topics/db/managers/#creating-manager-with-queryset-methods
    objects = PassThroughManager.for_queryset_class(CustomerAccountTransactionQuerySet)()
    history = HistoricalRecords(bases=[AdvancedHistory])

    class Meta:
        ordering = ['-transaction_date']
        verbose_name = "Kundenkonto-Buchung"
        verbose_name_plural = "Kundenkonto-Buchungen"

    def __unicode__(self):
        return (self.transaction_date.strftime('%d.%m.%Y') + ' - ' + str(self.amount) + ' - ' +
                self.customer.name + ', ' + self.customer.surname + ' - ' + self.description)

    def setStatusOpen(self):
        self.status = self.STATUS_OPEN
        return self

    def setStatusBooked(self):
        self.status = self.STATUS_BOOKED
        return self


class BankAccountTransactionQuerySet(models.query.QuerySet):
    def open(self):
        return self.filter(status=BankAccountTransaction.STATUS_OPEN)

    def booked(self):
        return self.filter(status=BankAccountTransaction.STATUS_BOOKED)


# Bankkontobuchungen
class BankAccountTransaction(models.Model):

    STATUS_OPEN = 0
    STATUS_BOOKED = 1

    STATUS_CHOICES = (
        (STATUS_OPEN, 'Offen'),
        (STATUS_BOOKED, 'Gebucht'),
    )

    TYPE_NOT_SPECIFIED = 0
    TYPE_BANK_TRANSFER_IN = 1  # same value as TYPE_BANK_TRANSFER_IN in model CustomerAccountTransaction
    TYPE_DEBIT = 2  # same value as TYPE_DEBIT in model CustomerAccountTransaction
    TYPE_DEBIT_RETURN = 3  # same value as TYPE_DEBIT_RETURN in model CustomerAccountTransaction

    TYPE_CHOICES = (
        (TYPE_BANK_TRANSFER_IN, u'Überweisungseingang'),
        (TYPE_DEBIT, 'Lastschrift'),
        (TYPE_DEBIT_RETURN, u'Rücklastschrift'),
        (TYPE_NOT_SPECIFIED, 'Sonstige'),

    )

    t_hash = models.CharField(max_length=50, unique=True, verbose_name="Hash")
    t_date = models.DateField(verbose_name="Buchungsdatum")
    booking_type = models.CharField(max_length=100, blank=True, default='', verbose_name="Buchungsart")
    subject = models.CharField(max_length=500, blank=True, verbose_name="Verwendungszweck")
    amount = models.DecimalField(max_digits=8, decimal_places=2, default=0, verbose_name="Betrag")
    type = models.PositiveSmallIntegerField(choices=TYPE_CHOICES, default=TYPE_NOT_SPECIFIED, verbose_name="Kategorie")
    bank_account_owner = models.CharField(max_length=100, blank=True, verbose_name=u'Auftraggeber/Empfänger')
    bank_account_number = models.CharField(max_length=34, blank=True, verbose_name="Kontonummer")
    bank_icode = models.CharField(max_length=11, blank=True, verbose_name="Bankleitzahl")
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=STATUS_OPEN, verbose_name="Status")
    last_modified = models.DateTimeField(auto_now=True, verbose_name=u'Zuletzt geändert')

    # TODO: Replace with native django code from Django 1.7 on
    # https://docs.djangoproject.com/en/1.7/topics/db/managers/#creating-manager-with-queryset-methods
    objects = PassThroughManager.for_queryset_class(BankAccountTransactionQuerySet)()

    class Meta:
        ordering = ['-t_date', '-amount', ]
        verbose_name = "Bankkonto-Buchung"
        verbose_name_plural = "Bankkonto-Buchungen"

    def __unicode__(self):
        return self.t_date.strftime('%d.%m.%Y') + ' - ' + str(self.amount) + ' - ' + self.subject

    def setHash(self, raw_data):
        """
        Calculate hash from input and set t_hash accordingly
        """
        oHashFunc = hashlib.sha1(raw_data)
        self.t_hash = oHashFunc.hexdigest()

    def setStatusOpen(self):
        self.status = self.STATUS_OPEN
        return self

    def setStatusBooked(self):
        self.status = self.STATUS_BOOKED
        return self
    
    def generateDataString(self):
        """Generate transaction data string. Use to generate transaction hash
        """
        
        sHashData = '%s%s%s%s%s%s' % (self.bank_account_number, 
                                      self.bank_icode, 
                                      self.bank_account_owner, 
                                      self.t_date, 
                                      self.subject, 
                                      str(self.amount))
        
        return sHashData.encode('utf-8')
    
    def cleanAmount(self, psAmount):
        """Return new amount formated for saving. Comma replaced with point.
        """
        # clean the amount so that also numbers in format 8.000,00 and 8000,00 are possible
        if type(psAmount) != type(Decimal()):
            sCleanedAmount = psAmount.replace(',', '.')
            sCleanedAmount = sCleanedAmount.replace('.', '', sCleanedAmount.count('.') -1)
            return Decimal(sCleanedAmount)
        else:
            return psAmount
    
    def checkForDuplicate(self, psHash):
        # check for duplicate
        iDupTest = BankAccountTransaction.objects.filter(t_hash=psHash).count()
        if iDupTest == 0:
            return False
        else:
            return True
    
    def saveTransaction(self, psSubject, psAmount, psType, psBankAccountOwner, psBankAccountNumber, psBankIcode, psBookingType, psDate):
        """Save transaction bu given data. It also generate hash and check for duplicates.
        """
        self.subject = psSubject.strip()
        self.amount = self.cleanAmount(psAmount)
        self.bank_account_owner = psBankAccountOwner.strip()
        self.bank_account_number = psBankAccountNumber.strip()
        self.bank_icode = psBankIcode.strip()
        self.booking_type = psBookingType
        self.t_date = datetime.strptime(psDate, '%Y-%m-%d').date()
        
        # Do manipulations with transaction data
        self.setHash(self.generateDataString())
        
        # Set type
        # 004, 005             | TYPE_DEBIT
        # 009, 010             | TYPE_DEBIT_RETURN
        # 051, 052             | TYPE_BANK_TRANSFER_IN
        # all the rest          | TYPE_NOT_SPECIFIED
        if psBookingType == '004' or psBookingType == '005':
            self.type = self.TYPE_DEBIT
        elif psBookingType == '009' or psBookingType == '010':
            self.type = self.TYPE_DEBIT_RETURN
        elif psBookingType == '051' or psBookingType == '052':
            self.type = self.TYPE_BANK_TRANSFER_IN
        else:
            self.type = self.TYPE_NOT_SPECIFIED
        
        # set subject
        self.subject = ''
        for subject_column_name in ACCOUNTING_SETTINGS['CSV_SUBJECT_FIELDS']:
            self.subject += self.subject + " "
        if 'CSV_EREF' in ACCOUNTING_SETTINGS and ACCOUNTING_SETTINGS['CSV_EREF'] in psSubject:
            self.subject += ' EREF+ %s MREF' % psSubject.strip()

        # clean mandate id and invoice number in subject for later use
        self.subject = re.sub('(RG|MA)[ 0-9]+',
                              lambda x: str(x.group()).replace(' ', '') + ' ',
                              self.subject)
        
        if not self.checkForDuplicate(self.t_hash):
            self.save()
            return True, None
        else:
            return False, 'DUP'
            


# Kind, hat Kundennummer
class Child(models.Model):
    customer = models.ForeignKey(Customer, null=True, verbose_name="Kunde")
    cust_nbr = models.IntegerField(blank=True, unique=True, verbose_name="Kundennummer")
    name = models.CharField(max_length=50, verbose_name="Nachname")
    surname = models.CharField(max_length=50, verbose_name="Vorname")
    birthday = models.DateField(null=True, blank=True, verbose_name="Geburtstag")
    facility = models.ForeignKey(Facility, null=True, verbose_name="Einrichtung")
    facility_subunit = ChainedForeignKey(  # chained select in admin interface / django-smart-selects
        FacilitySubunit,
        chained_field="facility",
        chained_model_field="facility",
        show_all=False,
        auto_choose=True,
        null=True,
        verbose_name="Klasse/Gruppe"
    )

    allergen = models.ManyToManyField('menus.Allergen', null=True, blank=True, verbose_name="Allergene")
    order_allergy_meal = models.BooleanField(
        verbose_name=u'Kann nur von Allergikern bestellt werden', default=False)
    flatrate = models.BooleanField(default=False, verbose_name="Abonnement")
    note = models.TextField(null=True, blank=True, verbose_name="Notiz")
    rfid_tag = models.CharField(max_length=100, blank=True, verbose_name="Interne Chip-ID")
    rfid_tag_update_time = models.IntegerField(blank=True, default=0)
    rfid_tag.child_has_chip_filter = True
    chip_number = models.CharField(max_length=10, blank=True, default='', verbose_name="Chip-Nummer")
    chip_number_update_time = models.IntegerField(blank=True, default=0)
    chip_deposit = models.BooleanField(default=False, verbose_name="Chip-Pfand bezahlt")
    chip_deposit_update_time = models.IntegerField(blank=True, default=0)
    extra_attribute = models.CharField(max_length=50, default='',
                                       blank=CHILD_EXTRA['BLANK'], verbose_name=CHILD_EXTRA['VERBOSE_NAME'],
                                       unique=CHILD_EXTRA['UNIQUE'], help_text=CHILD_EXTRA['HELP_TEXT'],
                                       choices=CHILD_EXTRA['CHOICES'])
    extra_attribute2 = models.CharField(max_length=50, default='',
                                        blank=CHILD_EXTRA2['BLANK'], verbose_name=CHILD_EXTRA2['VERBOSE_NAME'],
                                        unique=CHILD_EXTRA2['UNIQUE'], help_text=CHILD_EXTRA2['HELP_TEXT'],
                                        choices=CHILD_EXTRA2['CHOICES'])
    last_modified = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True, default=datetime(2000,1,1,0,0,0,0))

    class Meta:
        ordering = ['name', 'surname']
        if CANTEEN_MODE:
            verbose_name = "Mitarbeiter"
            verbose_name_plural = "Mitarbeiter"
        else:
            verbose_name = "Kind"
            verbose_name_plural = "Kinder"

    def __unicode__(self):
        return self.name + ', ' + self.surname

    def customer_link(self):
        """
        Creates link to customer admin for child admin table view
        """
        return '<a href="%s">%s</a>' % (reverse("admin:kg_customer_change", args=(self.customer.id,)),
                                        escape(self.customer))

    customer_link.allow_tags = True
    customer_link.short_description = "Kunde"

    def get_childfacility_for_date(self, oDate):
        """ return the ChildFacility object for a given date """
        '''
        qsChildFacility = ChildFacility.objects.filter(child=self, start_date__lte=oDate).order_by('-start_date')
        if qsChildFacility:
            return qsChildFacility[0]
        else:
            return None
        '''

    def get_current_childfacility(self):
        """ return the ChildFacility object for the facility/facilitysubunit the child is currently in """
        '''
        return self.get_childfacility_for_date(date.today())
        '''

    def get_current_and_future_childfacilities(self):
        """ return the ChildFacility object for the facility/facilitysubunit the child is currently in
            and all future ChildFacility objects
        """
        '''
        qsFutureChildFacilities = ChildFacility.objects.filter(child=self, start_date__gt=date.today())
        return ChildFacility.objects.filter(child=self).order_by('-start_date')[:len(qsFutureChildFacilities)+1]
        '''

    def get_facilitysubunits_for_days(self, days=None):
        """ return ordered dictionary with days as keys and the facility subunit as value for the given days """
        '''
        dFacilitySubunits = OrderedDict()
        ntFacility = namedtuple('Facility', ['facility', 'facility_subunit'])
        for oDay in days:
            lResult = list(ChildFacility.objects.filter(child=self, start_date__lte=oDay).order_by('-start_date')[:1])
            if lResult:
                dFacilitySubunits[oDay] = ntFacility(lResult[0].facility, lResult[0].facility_subunit)
            else:
                dFacilitySubunits[oDay] = ntFacility(None, None)
        return dFacilitySubunits
        '''

    def get_reduction_for_date(self, oDate, mealtime=None):
        """ returns ChildReduction object """
        qsChildReduction = ChildReduction.objects.filter(
            child=self, date_start__lte=oDate, date_end__gte=oDate)
        if mealtime:
            qsChildReduction = qsChildReduction.filter(
                reduction__mealtimes__id=mealtime)
        if qsChildReduction:
            return qsChildReduction[0]
        else:
            return None


class ChildReduction(models.Model):
    child = models.ForeignKey(Child, verbose_name="Kind")
    reduction = models.ForeignKey(Reduction, verbose_name="Art der Ermäßigung")
    reference_number = models.CharField(
        max_length=50, blank=True, verbose_name="Aktenzeichen")
    date_start = models.DateField(verbose_name="Beginn")
    date_end = models.DateField(verbose_name="Ende")

    class Meta:
        ordering = ['child__name']
        verbose_name = "Ermäßigung"
        verbose_name_plural = "Ermäßigungen"

    def __unicode__(self):
        return self.child.name + ', ' + self.child.surname


class Holiday(models.Model):
    name = models.CharField(max_length=50, verbose_name="Name")
    date_start = models.DateField(verbose_name="Beginn")
    date_end = models.DateField(verbose_name="Ende")

    class Meta:
        ordering = ['-date_start']
        verbose_name = "Ferien"
        verbose_name_plural = "Ferien"

    def __unicode__(self):
        return self.name

    @classmethod
    def is_holiday(cls, single_date):
        """ Standardize the way checking holiday of a given date. """
        return cls.objects.filter(
            date_start__lte=single_date,
            date_end__gte=single_date
        ).exists()


# SEPA Mandat
class SepaMandate(models.Model):

    SEQUENCE_TYPE_FRST = 'FRST'
    SEQUENCE_TYPE_RCUR = 'RCUR'

    customer = models.ForeignKey(Customer, verbose_name="Kunde")
    name = models.CharField(max_length=70, verbose_name="Vorname und Name (Kontoinhaber)")
    street = models.CharField(max_length=50, verbose_name="Straße und Hausnummer")
    city = models.CharField(max_length=50, verbose_name="Postleitzahl und Ort")
    mandate_id = models.CharField(max_length=30, unique=True, default='MA',  verbose_name="Mandatsreferenz",
                                  help_text="Bei neu erstellten Mandaten wird die Mandatsreferenz automatisch erzeugt, "
                                            "wenn der Inhalt des Feldes (\"MA\") nicht geändert wird.")
    bank = models.CharField(max_length=50, blank=True, verbose_name="Kreditinstitut Name")
    iban = models.CharField(max_length=34, verbose_name="IBAN",
                            validators=[IbanValidator])
    bic = models.CharField(max_length=11, verbose_name="BIC",
                           validators=[BicValidator])
    is_valid = models.BooleanField(default=True, verbose_name="Gültig",
                                   help_text=u"Falls Sie das Mandat manuell auf nicht gültig setzen, "
                                             u"können Sie im Feld Notiz den Grund dafür angeben.")
    on_paper = models.BooleanField(default=False, verbose_name="Liegt schriftlich vor")
    note = models.TextField(blank=True, default="", verbose_name="Notiz")
    last_debit_date = models.DateField(null=True, blank=True, verbose_name="Letzte Lastschrift am")
    last_debit_sequence = models.CharField(max_length=4, blank=True, default="", verbose_name="Letzte Lastschrift Sequenztyp")
    created_at = models.DateTimeField(default=datetime.now, verbose_name="Datum",
                                      help_text="Bei schriftlich vorliegenden Mandaten hier bitte "
                                                "das Datum auf dem Mandat eintragen.")
    last_modified = models.DateTimeField(auto_now=True, blank=True, verbose_name="Zuletzt geändert am")

    class Meta:
        verbose_name = "SEPA Mandat"
        verbose_name_plural = "SEPA Mandate"

    def __unicode__(self):
        return self.name + ' - ' + self.mandate_id

    def get_next_sequence_type(self):
        # TODO: should be able to prevent that a RCUR debit is done before a FRST booking
        if self.last_debit_sequence == '':
            return self.SEQUENCE_TYPE_FRST
        else:
            return self.SEQUENCE_TYPE_RCUR


class InvoiceLogQuerySet(models.query.QuerySet):
    def released(self):
        return self.exclude(released=None)

    def unreleased(self):
        return self.filter(released=None)


class InvoiceLog(models.Model):
    child = models.ForeignKey(Child, verbose_name="Kind")
    facility = models.ForeignKey(Facility, null=True, verbose_name="Einrichtung")
    invoice_date = models.DateField(verbose_name="Rechnungsdatum")
    invoice_number = models.CharField(max_length=30, verbose_name="Rechnungsnummer")
    invoice_subtotal = models.DecimalField(max_digits=5, decimal_places=2, default=0, verbose_name="Rechnungsbetrag Essen")
    invoice_total = models.DecimalField(max_digits=5, decimal_places=2, default=0, verbose_name="Rechnungsbetrag Gesamt")
    invoice_file = models.CharField(max_length=100, verbose_name="Dateiname")
    pay_type_id = models.PositiveSmallIntegerField(choices=PayType.PAYTYPE_CHOICES, default=0,
                                                   verbose_name="Zahlungsart")
    pay_until = models.DateField(default=date(1970, 1, 1), verbose_name="Zahlbar bis")
    bank_account_owner = models.CharField(max_length=70, blank=True, verbose_name="Kontoinhaber")
    bank_account_iban = models.CharField(max_length=34, blank=True, verbose_name="IBAN")
    bank_account_bic = models.CharField(max_length=11, blank=True, verbose_name="BIC")
    sepa_mandate_reference = models.CharField(max_length=35, blank=True, default="",
                                              verbose_name="SEPA Mandatsreferenz")
    sepa_transaction_id = models.CharField(max_length=50, blank=True, default="", verbose_name="SEPA TransaktionsId")
    sepa_mandate_date = models.DateField(null=True, verbose_name="Datum Erteilung Mandat")
    sepa_description = models.CharField(max_length=140, blank=True, default="", verbose_name="Verwendungszweck")
    sepa_seq_type = models.CharField(max_length=4, blank=True, default="", verbose_name="SEPA Sequence Type")
    first_downloaded = models.DateField(blank=True, null=True, verbose_name="Zuerst abgerufen")
    notification_sent = models.DateTimeField(blank=True, null=True, verbose_name="Mail gesendet am")
    released = models.DateTimeField(blank=True, null=True, verbose_name="Freigegeben")
    cat_created = models.BooleanField(default=False, verbose_name="Corresponding CustomerAccountTrasaction created")

    # TODO: Replace with native django code from Django 1.7 on
    # https://docs.djangoproject.com/en/1.7/topics/db/managers/#creating-manager-with-queryset-methods
    objects = PassThroughManager.for_queryset_class(InvoiceLogQuerySet)()

    class Meta:
        unique_together = ("child", "invoice_date")

    def __unicode__(self):
        return self.child.name + ' + ' + self.child.surname + ' - ' + self.invoice_date.isoformat()

    def is_released(self):
        if self.released is not None:
            return True
        else:
            return False


class Reminder(models.Model):

    STATUS_OPEN = 0
    STATUS_PAYED = 1
    STATUS_ENCASHMENT = 2
    STATUS_FAILED = 3

    STATUS_CHOICES = (
        (STATUS_OPEN, 'Offen'),
        (STATUS_PAYED, 'Bezahlt'),
        (STATUS_ENCASHMENT, 'Inkasso'),
        (STATUS_FAILED, 'Ausgemahnt')
    )

    TYPE_FIRST = 1
    TYPE_SECOND = 2
    TYPE_THIRD = 3

    TYPE_CHOICES = (
        (TYPE_FIRST, '1. Mahnung'),
        (TYPE_SECOND, '2. Mahnung'),
        (TYPE_THIRD, '3. Mahnung')
    )

    customer = models.ForeignKey(Customer, verbose_name="Kunde")
    number = models.CharField(max_length=20, blank=True, verbose_name="Nummer")
    parent = models.ForeignKey('self', related_name='children', blank=True, null=True)
    content = models.TextField(blank=True, verbose_name="Mahnungsinhalt")
    type = models.PositiveSmallIntegerField(choices=TYPE_CHOICES, default=TYPE_FIRST, verbose_name="Mahnungsart")
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=STATUS_OPEN, verbose_name="Status")
    create_date = models.DateField(verbose_name="Erstellt am")
    pay_until = models.DateField(verbose_name=u"Fällig am")
    total = models.DecimalField(max_digits=5, decimal_places=2, default=0, verbose_name="Betrag")
    filename = models.CharField(max_length=100, blank=True, verbose_name="Dateiname")
    first_downloaded = models.DateField(blank=True, null=True, verbose_name="Heruntergeladen")
    first_downloaded.reminder_downloaded_filter = True

    class Meta:
        ordering=['-number']
        verbose_name = "Mahnung"
        verbose_name_plural = "Mahnungen"

    def __unicode__(self):
        return self.customer.__unicode__() + " - %s. Mahnung - %s" % ( self.type, self.create_date.strftime('%d.%m.%Y'))

    def delete(self):
        """ Before deleting the Reminder, make sure to change/delete the corresponding CATs and the reminder file """

        # make sure to delete the Reminders in the right order
        if self.children.exists():
            for obj in self.children.all():
                obj.delete()

        # retrieve all connected CATs
        qsConnectedCATs = CustomerAccountTransaction.objects.filter(customer=self.customer,
                                                                    reminder_id=self.id)

        # retrieve reminder fee CAT
        qsReminderFeeCAT = CustomerAccountTransaction.objects.filter(type=CustomerAccountTransaction.TYPE_REMINDER_FEE,
                                                                     customer=self.customer,
                                                                     reminder_id=self.id)

        # change/delete reminder fee and update connected CATs
        if self.type == Reminder.TYPE_FIRST:
            # if it is first reminder
            qsReminderFeeCAT.delete()  # delete reminder fee
            qsConnectedCATs.update(reminder_id=0)  # disconnect other CATs
        else:
            if self.type == Reminder.TYPE_SECOND:
                oReminderFee = Decimal(str(ACCOUNTING_SETTINGS['REMINDER_FEE']))
            elif self.type == Reminder.TYPE_THIRD:
                oReminderFee = Decimal(str(ACCOUNTING_SETTINGS['REMINDER_FEE_2']))

            # update reminder fee to value of last reminder or delete if zero
            if oReminderFee > 0:
                qsReminderFeeCAT.update(amount=oReminderFee.copy_negate(),
                                        transaction_date=self.parent.create_date,
                                        description=u"Mahnkosten der Mahnung vom %s" % self.parent.create_date.strftime("%d.%m.%Y"))
            else:
                qsReminderFeeCAT.delete()
            # set reminder_id of CATs to the parent reminder id
            qsConnectedCATs.update(reminder_id=self.parent.id)

        # delete reminder file
        if self.filename != '' and not '..' in self.filename:
            filename = REMINDER_ROOT + '/' + self.filename
            if os.path.exists(filename):
                os.remove(filename)

        super(Reminder, self).delete()


class OrderManager(models.Manager):
    def get_query_set(self):
        return super(OrderManager, self).get_query_set().filter(quantity__gt=0)


class Order(models.Model):

    REASON_NOT_SPECIFIED = None
    REASON_USER = 1
    REASON_FLATRATE = 2
    REASON_STAFF = 3
    REASON_CHOICES = (
        (REASON_NOT_SPECIFIED, u'Not specified'),
        (REASON_USER, u'User ordered'),
        (REASON_FLATRATE, u'Flatrate ordered'),
        (REASON_STAFF, u'Staff ordered')
    )

    child = models.ForeignKey(Child, verbose_name="Kind")
    menu = models.ForeignKey('menus.Menu', verbose_name="Essen")
    quantity = models.SmallIntegerField(default=0, verbose_name="Anzahl")
    created_by = models.ForeignKey(User, related_name="created_orders", null=True, blank=True, verbose_name="Erstellt von")
    modified_by = models.ForeignKey(User, related_name="modified_orders", null=True, blank=True, verbose_name="Geändert von")
    last_modified = models.DateTimeField(auto_now=True, blank=True, verbose_name="Geändert am")
    order_time = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name="Bestellzeitpunkt")
    order_reason = models.PositiveSmallIntegerField(
        choices=REASON_CHOICES,
        default=REASON_NOT_SPECIFIED,
        null=True,
        verbose_name="Grund")
    price = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True, verbose_name="Preis")
    reduction = models.ForeignKey(Reduction, null=True, blank=True, verbose_name="Ermäßigung")
    pickup_time = models.IntegerField(default=0, verbose_name="Abholzeit")
    
    objects = OrderManager()  # retrieve only not canceled orders, quantity > 0
    all_objects = models.Manager()  # retrieve all orders; includes canceled orders
    
    class Meta:
        unique_together = ("child", "menu")
        verbose_name = "Essenbestellung"
        verbose_name_plural = "Essenbestellungen"

    def __unicode__(self):
        return self.child.name + " - " + self.menu.__unicode__()

    @classmethod
    def count_ordered_weekdays(cls, child, year, week):
        """ Returns number of days in week having order placed. """
        week_days = DateHelper().getDaysInCalendarWeek(year, week)
        queryset = cls.objects.filter(child=child, menu__date__in=week_days)
        return queryset.distinct('menu__date').count()


class OrderHistory(models.Model):
    child = models.ForeignKey(Child, verbose_name="Kind")
    menu = models.ForeignKey('menus.Menu', verbose_name="Essen")
    modified_by = models.ForeignKey(User, related_name="modified_order_history", null=True, blank=True, verbose_name="Geändert von")
    last_modified = models.DateTimeField(blank=True, verbose_name="Geändert am")
    quantity = models.SmallIntegerField(default=0, verbose_name="Anzahl")
    reason = models.PositiveSmallIntegerField(
        choices=Order.REASON_CHOICES,
        default=Order.REASON_NOT_SPECIFIED,
        null=True,
        verbose_name="Grund")

    class Meta:
        verbose_name = "Essenbestellung-Historie"
        verbose_name_plural = "Essenbestellungen-Historie"
        ordering = ('last_modified',)

    def __unicode__(self):
        return self.child.name + " - " + self.menu.__unicode__()


class FlatrateQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)

    def active_on_holidays(self):
        return self.filter(active_on_holidays=True)


class Flatrate(models.Model):
    child = models.ForeignKey(Child, related_name="flat", verbose_name="Kind")
    meal = models.ForeignKey('menus.Meal', verbose_name="Essenszeit")
    weekday = models.SmallIntegerField(choices=WEEKDAYS, verbose_name="Wochentag")
    created_by = models.ForeignKey(User, related_name="created_flatrate", null=True, blank=True, verbose_name="Erstellt von")
    modified_by = models.ForeignKey(User, related_name="modified_flatrate", null=True, blank=True, verbose_name="Geändert von")
    last_modified = models.DateTimeField(auto_now=True, blank=True, verbose_name="Geändert am")
    active = models.BooleanField(default=True, verbose_name="Aktiv")
    active_on_holidays = models.BooleanField(default=True, verbose_name="Aktiv in Ferien")

    # TODO: Replace with native django code from Django 1.7 on
    # https://docs.djangoproject.com/en/1.7/topics/db/managers/#creating-manager-with-queryset-methods
    objects = PassThroughManager.for_queryset_class(FlatrateQuerySet)()
    #all_objects = models.Manager() #retrieve all flatrates; includes canceled ones

    class Meta:
        unique_together = ("child", "meal", "weekday")
        verbose_name = "Abonnement"
        verbose_name_plural = "Abonnements"

    @staticmethod
    def make_key(child_id, meal_id, weekday, postfix=None):
        key = '{}-{}-{}'.format(child_id, meal_id, weekday)
        return key + '-' + postfix if postfix else key

    @cached_property
    def key(self):
        return '{}-{}-{}'.format(
            self.child.id, self.meal.id, self.weekday)

    def __unicode__(self):
        return self.child.__unicode__() + " - " + self.meal.__unicode__()


class ActionLogManager(models.Manager):

    def log(self, request, object, action_flag=0, change_message=''):

        if request.user.is_authenticated():
            user_id = request.user.pk
        else:
            user_id = None

        if object:
            content_type_id = ContentType.objects.get_for_model(object).pk
            object_id = object.pk
        else:
            content_type_id = None
            object_id = ''

        try:
            e = self.model(None, None, user_id, content_type_id, smart_unicode(object_id), action_flag, change_message)
            e.save()
            return True
        except:
            return False

    def log_general(self, request, object, message=''):
        return self.log(request=request, object=object, action_flag=ActionLog.GENERAL, change_message=message)

    def log_addition(self, request, object, message=''):
        return self.log(request=request, object=object, action_flag=ActionLog.ADDITION, change_message=message)

    def log_change(self, request, object, message=''):
        return self.log(request=request, object=object, action_flag=ActionLog.CHANGE, change_message=message)

    def log_deletion(self, request, object, message=''):
        return self.log(request=request, object=object, action_flag=ActionLog.DELETION, change_message=message)

    def log_warning(self, request, object, message=''):
        return self.log(request=request, object=object, action_flag=ActionLog.WARNING, change_message=message)


class ActionLog(models.Model):

    GENERAL = 0
    ADDITION = 1
    CHANGE = 2
    DELETION = 3
    WARNING = 4

    ACTION_CHOICES = (
        (GENERAL, 'Allgemein'),
        (ADDITION, 'Neu'),
        (CHANGE, 'Änderung'),
        (DELETION, 'Löschung'),
        (WARNING, 'Warnung')
    )

    action_time = models.DateTimeField(auto_now=True, verbose_name='Zeitpunkt der Aktion')
    user = models.ForeignKey(User, null=True, verbose_name='Benutzer')
    content_type = models.ForeignKey(ContentType, blank=True, null=True, verbose_name='Inhaltstyp')
    object_id = models.TextField(blank=True, null=True, verbose_name='Objekt ID')
    action_flag = models.PositiveSmallIntegerField(choices=ACTION_CHOICES, verbose_name='Art')
    change_message = models.TextField(blank=True, verbose_name='Aktion')
    objects = ActionLogManager()

    class Meta:
        verbose_name = 'Log'
        verbose_name_plural = 'Logs'
        ordering = ('-action_time',)

    def __unicode__(self):
        return self.change_message

    def __repr__(self):
        return smart_unicode(self.action_time)

    def is_general(self):
        return self.action_flag == self.GENERAL

    def is_addition(self):
        return self.action_flag == self.ADDITION

    def is_change(self):
        return self.action_flag == self.CHANGE

    def is_deletion(self):
        return self.action_flag == self.DELETION

    def is_warning(self):
        return self.action_flag == self.WARNING

    def get_edited_object(self):
        """Returns the edited object represented by this log entry"""
        return self.content_type.get_object_for_this_type(pk=self.object_id)

    def get_admin_url(self):
        """
        Returns the admin URL to edit the object represented by this log entry.
        This is relative to the Django admin index page.
        """
        if self.content_type and self.object_id:
            return mark_safe(u"%s/%s/%s/" % (self.content_type.app_label, self.content_type.model, quote(self.object_id)))
        return None


class ViewPermission(models.Model):
    """
    A dummy class. It is only used for the permissions that are used to limit access to certain views.
    Use python manage.py syncdb --all to import permissions into db
    """

    dummy_content = models.CharField(max_length=1)

    class Meta:
        verbose_name = 'Berechtigung'
        verbose_name_plural = 'Berechtigungen'
        permissions = (
            ("create_reports", "Can create daily reports"),
            ("use_mailtest", "Can use mailtest"),
            ("create_backup", "Can create backup (Excel)"),
            ("view_statistics", "Can view statistics"),
            ("view_invoices", "Can view invoices"),
            ("create_invoices", "Can create invoices"),
            ("export_import_pos", "Can export/import data for POS"),
        )

    def __unicode__(self):
        return u'Permission dummy object'


class SepaDebitDownload(models.Model):
    first_downloaded = models.DateTimeField(verbose_name="Heruntergeladen am")
    message_id = models.CharField(max_length=35, unique=True, verbose_name="SEPA Message-Id")
    sequence_type = models.CharField(max_length=4, verbose_name="SEPA Typ")

    class Meta:
        verbose_name = 'SEPA Lastschrift Download'
        verbose_name_plural = 'SEPA Lastschriften Downloads'
        ordering = ('-first_downloaded', 'sequence_type')

    def __unicode__(self):
        return u"%s - %s - %s" % (self.first_downloaded.strftime('%d.%m.%Y %H:%M'), self.message_id, self.sequence_type)


class SepaDebitQuerySet(models.query.QuerySet):
    def new(self):
        return self.filter(download__isnull=True)

    def frst(self):
        return self.filter(sequence_type='FRST')

    def rcur(self):
        return self.filter(sequence_type='RCUR')


class SepaDebit(models.Model):
    customer = models.ForeignKey(Customer, verbose_name="Kunde")
    submission_time = models.DateTimeField(auto_now_add=True, verbose_name="Einreichungszeitpunkt")
    endtoend_id = models.CharField(max_length=35, unique=True, verbose_name="SEPA Ende-Zu-Ende-Id")
    sequence_type = models.CharField(max_length=4, verbose_name="SEPA Sequence Type")
    amount = models.DecimalField(max_digits=5, decimal_places=2, default=0, verbose_name="Betrag")
    description = models.CharField(max_length=140, blank=True, default="", verbose_name="Verwendungszweck")
    bank_account_owner = models.CharField(max_length=70, verbose_name="Kontoinhaber")
    bank_account_iban = models.CharField(max_length=34, verbose_name="IBAN")
    bank_account_bic = models.CharField(max_length=11, verbose_name="BIC")
    mandate_reference = models.CharField(max_length=35, verbose_name="SEPA Mandatsreferenz")
    mandate_date = models.DateField(verbose_name="Datum Erteilung SEPA-Mandat")
    debit_received = models.BooleanField(default=False, verbose_name="Lastschrift eingegangen")
    download = models.ForeignKey(SepaDebitDownload, null=True, verbose_name="Download")

    # TODO: Replace with native django code from Django 1.7 on
    # https://docs.djangoproject.com/en/1.7/topics/db/managers/#creating-manager-with-queryset-methods
    objects = PassThroughManager.for_queryset_class(SepaDebitQuerySet)()

    class Meta:
        verbose_name = 'SEPA Lastschrift'
        verbose_name_plural = 'SEPA Lastschriften'

    def __unicode__(self):
        return u"%s EUR - %s, %s" % (self.amount, self.customer.name, self.customer.surname)


from kg import receivers
