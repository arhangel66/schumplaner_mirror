# -*- coding: utf-8 -*- 

from decimal import Decimal

from django.template.loader import render_to_string
from form_utils.forms import BetterForm, BetterModelForm
from django import forms
from kg.models import Child, Facility, PayType, Customer, SepaMandate
from django.db.models import Q
from kg_utils.validators import *
from django.forms.widgets import PasswordInput
from settings import EXIST_CUST_REGISTRATION, NEW_CUSTOMER_FORM_AGB_CHECKBOX, OVERLOAD_SITE, CANTEEN_MODE

        
class ChildForm(BetterModelForm):

    facility = forms.ModelChoiceField(queryset=Facility.ex_objects.filter(is_active=True), label="Einrichtung")

    class Meta:
        model = Child

        fieldsets = [('Allgemeine Angaben', {'fields':
                                                 ['surname', 'name', 'birthday', 'facility', 'facility_subunit']})]

        row_attrs = {'surname': {'class': 'type-text form-group'},
                     'name': {'class': 'type-text form-group'},
                     'birthday': {'class': 'type-text form-group'},
                     'facility': {'class': 'type-select form-group'},
                     'facility_subunit': {'class': 'type-text form-group'}}

    def __init__(self, *args, **kwargs):
        super(ChildForm, self).__init__(*args, **kwargs)
        self.fields['surname'].widget.attrs['class'] = 'form-control'
        self.fields['name'].widget.attrs['class'] = 'form-control'

        self.fields['birthday'].widget.attrs['class'] = 'form-control datepicker'
        self.fields['birthday'].widget.attrs['autocomplete'] = 'off'

        self.fields['facility'].widget.attrs['class'] = 'form-control'
        self.fields['facility_subunit'].widget.attrs['class'] = 'form-control'
        
    def clean_facility_subunit(self):
        if 'facility' in self.cleaned_data:
            facility = self.cleaned_data['facility']
        else:
            facility = None
        subunit = self.cleaned_data['facility_subunit']
        error_message = "Die gewählte Klasse/Gruppe gehört nicht zur ausgewählten Einrichtung."
        if subunit.facility != facility:
            raise forms.ValidationError(error_message) 

        # Always return the cleaned data, whether you have changed it or not
        return subunit


class SepaMandateForm(BetterModelForm):

    bank = forms.CharField(max_length=50)

    class Meta:
        model = SepaMandate

        fieldsets = [('SEPA-Lastschriftmandat',
                      {'fields': ['name', 'street', 'city', 'bank', 'bic', 'iban'],
                       'classes': ['paytype-' + str(PayType.DEBIT), ],
                       'description': ''}),
                     ]

        row_attrs = {'name': {'class': 'type-text form-group'},
                     'street': {'class': 'type-text form-group'},
                     'city': {'class': 'type-text form-group'},
                     'bank': {'class': 'type-text form-group'},
                     'iban': {'class': 'type-text form-group'},
                     'bic': {'class': 'type-text form-group'},
                     }

    def clean_iban(self):
        return self.cleaned_data['iban'].upper().replace(' ', '')

    def __init__(self, *args, **kwargs):
        super(SepaMandateForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['street'].widget.attrs['class'] = 'form-control'
        self.fields['city'].widget.attrs['class'] = 'form-control'
        self.fields['bank'].widget.attrs['class'] = 'form-control'
        self.fields['bic'].widget.attrs['class'] = 'form-control'
        self.fields['iban'].widget.attrs['class'] = 'form-control'

        # Load SEPA description from template it is just to big and with dynamic parts
        self.fieldsets.fieldsets[0][1]['description'] = render_to_string("kg/partials/client_profile_sepa_mandates_description.html",
                                                                         {'sepa_mandate': self.instance})

    def clean(self):
        cleaned_data = super(SepaMandateForm, self).clean()
        return cleaned_data


class PayTypeForm(BetterModelForm):
    class Meta:
        model = Customer
        fieldsets = [('Zahlungsart', {'fields': ['pay_type', ], }), ]

        row_attrs = {'pay_type': {'class': 'type-select form-group'}, }

    def __init__(self, *args, **kwargs):
        super(PayTypeForm, self).__init__(*args, **kwargs)
        self.fields['pay_type'].widget.attrs['class'] = 'form-control'

    def clean_pay_type(self):
        """Make sure customer satisfies condition of new pay type."""
        pay_type = self.cleaned_data['pay_type']
        if self.instance is not None and pay_type != self.instance.pay_type:
            if not pay_type.customer_satisfied(self.instance):
                self.errors['pay_type'] = msg = u'Bedingung nicht erfüllt'
                raise ValidationError(msg)
        return pay_type


class CustomerForm(BetterModelForm):
    class Meta:
        model = Customer

        #fields = ('title', 'name', 'surname', 'email', 'street', 'zip_code', 'city', 'phone', 'phone2')
        fieldsets = [('Allgemeine Angaben', {'fields': ['title', 'surname', 'name', 'street', 'zip_code',
                                                        'city', 'phone', 'phone2']})]

        row_attrs = {'title': {'class': 'type-select form-group'},
                     'surname': {'class': 'type-text form-group'},
                     'name': {'class': 'type-text form-group'},
                     'street': {'class': 'type-text form-group'},
                     'zip_code': {'class': 'type-text form-group'},
                     'city': {'class': 'type-text form-group'},
                     'phone': {'class': 'type-text form-group'},
                     'phone2': {'class': 'type-text form-group'},
                     }

    def __init__(self, *args, **kwargs):
        super(CustomerForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['surname'].widget.attrs['class'] = 'form-control'
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['street'].widget.attrs['class'] = 'form-control'
        self.fields['zip_code'].widget.attrs['class'] = 'form-control'
        self.fields['city'].widget.attrs['class'] = 'form-control'
        self.fields['phone'].widget.attrs['class'] = 'form-control'
        self.fields['phone2'].widget.attrs['class'] = 'form-control'


class NewCustomerForm(CustomerForm):

    sFacilityLabel = u'Schule / Kindergarten des Kindes'
    if CANTEEN_MODE:
        sFacilityLabel = u'Einrichtung'

    email = forms.CharField(max_length=75, required=True, label="E-Mail")
    facility = forms.ChoiceField(choices=[], label=sFacilityLabel)
    pay_type = forms.ModelChoiceField(queryset=PayType.objects.filter(new_customers=True), label="Zahlungsart")
    if NEW_CUSTOMER_FORM_AGB_CHECKBOX:
        agb = forms.BooleanField(label="")
    if OVERLOAD_SITE == 'zingst':
        agb_zingst = forms.BooleanField(required=False, label="")

    class Meta:
        model = Customer

        # add helptext for Abraxas
        # TODO: remove this hack trough transalation
        sPayTypeDescription = ''
        if OVERLOAD_SITE == 'abraxas':
            sPayTypeDescription = u'Hinweis: Wenn ihr Kind den Hort besucht, wählen Sie bitte trotzdem "Rechnung" aus.<br />Für Sie wird sich nichts ändern. Sie bekommen von uns keine Rechnung, sondern zahlen wie gewohnt an das Bezirksamt.'

        sFacilityLabel = u'Schule / Kindergarten'
        sFacilityDescription = u'Eine Anmeldung ist nur möglich, wenn Ihr Kind eine der von uns belieferten Einrichtungen besucht.'
        sNameLabel = u'Name (Eltern)'
        if CANTEEN_MODE:
            sFacilityLabel = u'Einrichtung'
            sFacilityDescription = ''
            sNameLabel = u'Name'

        fieldsets = [(sFacilityLabel, {'fields': ['facility'],
                                                'description': sFacilityDescription,
                                                }),
                     ('E-Mail', {'fields': ['email'],
                                 'description': 'Diese E-Mail-Adresse wird in Zukunft auch Ihr Login sein.',
                                 }),
                     (sNameLabel, {'fields': ['title', 'surname', 'name'], }),
                     ('Anschrift', {'fields': ['street', 'zip_code', 'city'], }),
                     ('Telefon', {'fields': ['phone', 'phone2'], }),
                     ('Zahlungsart', {'fields': ['pay_type', ],
                                      'description': sPayTypeDescription,
                                      }),
                     ]

        row_attrs = {'facility': {'class': 'type-select form-group'},
                     'title': {'class': 'type-select form-group'},
                     'surname': {'class': 'type-text form-group'},
                     'name': {'class': 'type-text form-group'},
                     'email': {'class': 'type-text form-group'},
                     'street': {'class': 'type-text form-group'},
                     'zip_code': {'class': 'type-text form-group'},
                     'city': {'class': 'type-text form-group'},
                     'phone': {'class': 'type-text form-group'},
                     'phone2': {'class': 'type-text form-group'},
                     'pay_type': {'class': 'type-select form-group'},
                     }

        if NEW_CUSTOMER_FORM_AGB_CHECKBOX:
            fieldsets += [('AGB', {'fields': ['agb', ],}),]
            row_attrs['agb'] = {'class': 'type-check form-group'}

        if OVERLOAD_SITE == 'zingst':
            fieldsets += [(u'Gebührensatzung', {'fields': ['agb_zingst', ],}),]
            row_attrs['agb_zingst'] = {'class': 'form-group'}


    def __init__(self, *args, **kwargs):
        super(NewCustomerForm, self).__init__(*args, **kwargs)

        # set required fields
        self.fields['title'].required = True
        self.fields['street'].required = True
        self.fields['city'].required = True
        self.fields['zip_code'].required = True
        if CANTEEN_MODE:
            self.fields['surname'].required = True

        # set the choices for the facility dropdown
        lFacChoices = []
        for oFac in Facility.ex_objects.filter(is_active=True):
            lFacChoices.append((str(oFac.id), oFac.name))
        self.fields['facility'].choices = lFacChoices

        self.fields['facility'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['pay_type'].widget.attrs['class'] = 'form-control'
        if 'agb' in self.fields:
            self.fields['agb'].widget.attrs['class'] = ''
        if 'agb_zingst' in self.fields:
            self.fields['agb_zingst'].widget.attrs['class'] = ''

    def clean_email(self):
        try:
            User.objects.get(username__iexact=self.cleaned_data['email'])
        except User.DoesNotExist:
            return self.cleaned_data['email']
        else:
            raise forms.ValidationError("Ein Nutzer mit der angegebenen E-Mailadresse existiert schon.")

        
class RegistrationForm(BetterForm):
    rnum = forms.CharField(max_length=20, required=True, label=EXIST_CUST_REGISTRATION)
    name = forms.CharField(max_length=30, required=True, label="Nachname des Rechnungsempfängers")
    email = forms.EmailField(max_length=75, required=True, validators=[EmailUniqueValidator], label="E-Mailadresse")
    
    class Meta:
        model = Customer
        fieldsets = [('Online Zugang beantragen', {'fields': ['rnum', 'name', 'email'],  
                      'description': 'Die E-Mailadresse wird in Zukunft auch Ihr Login sein.',
                      #'classes': ['Personal data']  
                     }),
                    ]
        row_attrs = {'rnum': {'class': 'type-text'},  
                     'name': {'class': 'type-text'},  
                     'email': {'class': 'type-text'},
                    }


class CustomerPasswordChangeForm(BetterForm):
    password      = forms.CharField(max_length=20, required=True, widget=PasswordInput(), label="Passwort")
    password_test = forms.CharField(max_length=20, required=True, widget=PasswordInput(), label="Passwort Wiederholung")

    
    class Meta:
        fieldsets = [('Neues Passwort eingeben', {'fields': ['password', 'password_test'],  
                      #'description': '',
                      #'classes': ['Personal data']  
                     }),
                    ]
        row_attrs = {'password': {'class': 'type-text form-group'},
                     'password_test': {'class': 'type-text form-group'},
                    }

    def clean(self):
        cleaned_data = self.cleaned_data
        password = cleaned_data.get("password")
        password_test = cleaned_data.get("password_test")

        if password != password_test:
            raise forms.ValidationError("Die Passwörter stimmen nicht überein.")

        # Always return the full collection of cleaned data.
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(CustomerPasswordChangeForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget.attrs['class'] = 'form-control'
        self.fields['password_test'].widget.attrs['class'] = 'form-control'


class PasswordRecoverForm(BetterForm):
    email = forms.EmailField(required=True, label="E-Mail-Adresse")
        
    class Meta:
        fieldsets = [('Passwort ändern', {'fields': ['email',],  
                      #'description': '',
                      #'classes': ['Personal data']  
                     }),
                    ]
        row_attrs = {'email': {'class': 'type-text form-group'},
                     }

    def clean_email(self):
        sEmail = self.cleaned_data['email']
        try:
            user = User.objects.get(Q(username__iexact=sEmail) | Q(email__iexact=sEmail))
        except:
            raise forms.ValidationError(
                "Es existiert kein Nutzer mit dieser E-Mail-Adresse. Bitte überprüfen Sie Ihre Eingabe.")

        return sEmail

    def __init__(self, *args, **kwargs):
        super(PasswordRecoverForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({
            'class': 'form-control',
            'autocomplete': 'off',
            'autocorrect': 'off',
            'autocapitalize': 'none',
            'spellcheck': 'false'
        })


class ChargeCreditForm(BetterForm):
    amount = forms.CharField(max_length=8, required=True, label="Betrag")

    def clean_amount(self):
        try:
            amount = self.cleaned_data['amount'].replace(',', '.').strip()
            amount = Decimal(amount).quantize(Decimal('.01'))
        except Exception as e:
            raise forms.ValidationError(u"Ungültiger Betrag!<br />Bitte geben Sie einen Betrag in der Form 00,00 ein. Beispiel: 30,00")

        if amount < 0:
            raise forms.ValidationError(u"Ungültiger Betrag!<br />Bitte geben Sie einen Betrag größer als 0,00 ein.")

        return amount
