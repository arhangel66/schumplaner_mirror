from datetime import date

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse

from kg_utils.menu_helper import MenuRenderer
from kg_utils.date_helper import DateHelper
from reports.helpers import generate_pdf

from settings import (
    SITE_CONTEXT, SITE_OVERLOAD_STATICFILES_DIR, MENU_WEEKS_HOMEPAGE,
    NEW_CUSTOMER_REGISTRATION
)


def index(request):
    """Public starting page to all kind of users."""
    dData = {
        'registration': NEW_CUSTOMER_REGISTRATION,
    }
    return render_to_response(
        'kg/index.html', dData, context_instance=RequestContext(request))


# Menue plan PDF
def menue_print(request):

    iShowWeeks = request.GET.get('weeks', MENU_WEEKS_HOMEPAGE)

    menus = []
    weeks = DateHelper.getWeeks(DateHelper.getNextBusinessday(MenuRenderer.getMenuStartDate()), iShowWeeks)
    for week in weeks:
        if MenuRenderer.isMenuAvailable(week['year'], week['week']):
            menu_html, small_view_menu_html = MenuRenderer.getMenuHtml(week['year'], week['week'], request=request)
            item = {'menu_html': menu_html,
                    'small_view_menu_html': small_view_menu_html,
                    'week': week}
            menus.append(item)

    # prepare the context for pdf rendering
    dContext = {'menus': menus, 'weeks': weeks, 'STATICFILES_DIR': SITE_OVERLOAD_STATICFILES_DIR}
    dContext.update(SITE_CONTEXT)

    resp = HttpResponse(content_type='application/pdf')
    resp['Content-Disposition'] = 'attachment; filename=' + 'Speiseplan_' + date.today().strftime('%Y_%m_%d') + '.pdf'
    result = generate_pdf('kg/pdf/menue_print.html', response=resp, context=dContext )
    return result
