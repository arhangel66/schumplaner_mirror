# -*- coding: utf-8 -*-
import StringIO
import cStringIO
import xlsxwriter
from datetime import date, datetime
from PyPDF2 import PdfFileMerger

from django.db import models
from django.db.models import Sum
from django.contrib import admin, messages
from django.contrib.admin.models import DELETION
from django.contrib.admin.actions import delete_selected
from django.contrib.admin.widgets import AdminDateWidget
from django.contrib.admin.util import model_ngettext
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin
from django.core.exceptions import PermissionDenied, ValidationError
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.utils.timezone import now
from django.conf.urls.defaults import *
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import BaseInlineFormSet, ModelForm
from django.template.defaultfilters import escape

from simple_history.admin import SimpleHistoryAdmin
from tinymce.widgets import TinyMCE

from kg.models import (
    Customer, SepaMandate, Child, PayType, FacilityType, Facility,
    FacilitySubunit, ActionLog, BankAccountTransaction, CancelReason,
    Reduction, CustomerAccountTransaction, ChildReduction, Flatrate,
    Holiday, Reminder, SepaDebitDownload, SepaDebit
    )
from kg.forms_admin import (
    CustomerAdminForm, CustomerAccountTransactionAdminForm, ReductionForm,
    ChildAdminForm
    )
from kg.filters import (
    AccountBalanceListFilter, ChildHasChipListFilter,
    ReminderDownloadedListFilter, CustomTitledFieldListFilter,
    ChildFacilityFilter, ChildSubunitFilter, ValidReductionFilter,
    LastMonthValidReductionFilter
    )
from kg_utils.reminder_generator import ReminderGenerator
from kg_utils.admin_helper import get_cat_standard_view_url_params
from reports.helpers import WorksheetRow
from settings import (
    MODULE_ACCOUNTING, MODULE_PREPAID, CANTEEN_MODE, OVERLOAD_SITE, CUST_EXTRA,
    CUST_EXTRA2, CHILD_EXTRA, CHILD_EXTRA2, REMINDER_ROOT
)


def create_modeladmin(model, modeladmin, name = None):
    class  Meta:
        proxy = True
        app_label = model._meta.app_label

    dAttrs = {'__module__': '', 'Meta': Meta}

    oNewmodel = type(name, (model,), dAttrs)

    admin.site.register(oNewmodel, modeladmin)
    return modeladmin


class SepaMandateInline(admin.StackedInline):
    model = SepaMandate
    extra = 0
    fields = ('name', 'street', 'city', 'mandate_id', 'bank', 'iban', 'bic', 'created_at',
              'on_paper', 'note', 'is_valid')
    formfield_overrides = {
        models.DateTimeField: {'widget': AdminDateWidget},
    }


class CustomerAdmin(admin.ModelAdmin):

    form = CustomerAdminForm

    search_fields = ['name', 'surname', 'user__username', 'extra_attribute', 'extra_attribute2']

    actions = ['export_xlsx']
    if MODULE_ACCOUNTING:
        actions += ['create_reminder_action', ]

    list_display = ('__unicode__',)
    if CUST_EXTRA['ACTIVE']:
        list_display += ('extra_attribute',)
    if CUST_EXTRA2['ACTIVE']:
        list_display += ('extra_attribute2',)
    if not CANTEEN_MODE:
        list_display += ('child_link',)
    list_display += ('user_link', 'pay_type')
    if MODULE_ACCOUNTING or MODULE_PREPAID:
        list_display += ('account_balance_link',)
    list_display += ('show_suspension_status',)
    if not CANTEEN_MODE:
        list_display += ('show_paper_invoice_status',)

    if CUST_EXTRA['ACTIVE']:
        cust_fields =  ('title', 'surname', 'name', 'extra_attribute', 'email')
    else:
        cust_fields = ('title', 'surname', 'name', 'email')

    readonly_fields = []
    if 'READONLY' in CUST_EXTRA and CUST_EXTRA['READONLY'] == True:
        readonly_fields += ['extra_attribute']
    if 'READONLY' in CUST_EXTRA2 and CUST_EXTRA2['READONLY'] == True:
        readonly_fields += ['extra_attribute2']

    fieldsets = (
        ('Allgemeine Angaben', {
            'fields': cust_fields
        }),
        ('Adresse', {
            'fields': ('street', 'zip_code', 'city')
        }),
        ('Telefon', {
            'fields': ('phone', 'phone2')
        }),
        ('Rechnung per Post', {
            'fields': ('paper_invoice', )
        }),
        ('Zahlungsart', {
            'fields': ('pay_type', )
        }),
        ('Notizen', {
            'fields': ('note',)
        }),
        ('Sperre', {
            'fields': ('is_suspended', 'suspension_reason')
        }),
    )

    if OVERLOAD_SITE == 'somic':
        list_filter = (('extra_attribute2', CustomTitledFieldListFilter('Firma')), 'is_suspended')
    elif OVERLOAD_SITE == 'hytera':
        list_filter = (('extra_attribute2', CustomTitledFieldListFilter('Typ')), 'is_suspended')
    else:
        list_filter = ('child__facility', 'pay_type',)
        if MODULE_ACCOUNTING or MODULE_PREPAID:
            list_filter += (AccountBalanceListFilter,)
        list_filter += ('is_suspended', 'paper_invoice',)

    inlines = [SepaMandateInline]

    def get_actions(self, request):
        actions = super(CustomerAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def child_link(self, obj):
        """
        Creates link to child admin for customer admin table view
        """
        a_string = ''
        children = Child.objects.filter(customer=obj)
        for child in children:
            a_string += '<a href="%s">%s</a><br />' % (reverse("admin:kg_child_change", args=(child.id,)) , escape(child))
        return a_string
    child_link.allow_tags = True
    child_link.short_description = "Kinder"

    def user_link(self, obj):
        """
        Creates link to user admin
        """
        if obj.user and '_old_' not in obj.user.username and obj.user.is_active:
            # reverse("admin:auth_user_change", args=(obj.user.id,)) , escape(obj.user)
            return '<a href="#" id="cl%s" class="customer_login">%s</a>' % (obj.id, escape(obj.user))
        else:
            return '<a href="#" id="cl%s" class="customer_login">Login anlegen</a>' % obj.id
    user_link.allow_tags = True
    user_link.short_description = "Benutzer/Login"
    user_link.empty_value_display = "Login anlegen"

    def show_suspension_status(self, obj):
        """ Show status of suspension """
        if obj.is_suspended:
            return u'Ja'
        else:
            return u'Nein'
    show_suspension_status.short_description = "Sperre"

    def show_balance_status(self, obj):
        """ Show status of account_balance """
        if obj.account_balance < 0:
            return u'Negativ'
        elif obj.account_balance > 0:
            return u'Positiv'
        else:
            return u'Ausgeglichen'
    show_balance_status.short_description = "Saldo Kundenkonto"

    def show_paper_invoice_status(self, obj):
        """ Show status of paper_invoice """
        if obj.paper_invoice:
            return u'Ja'
        else:
            return u'Nein'
    show_paper_invoice_status.short_description = "RpP"

    def account_balance_link(self, obj):
        """
        Creates link to CustomerAccountTransactions in field account_balance
        """
        a_string = '<div class="align-right"><a href="%s?customer__id=%s%s">%s</a></div>' % (reverse("admin:kg_customeraccounttransaction_changelist"),
                                                                                                           obj.id,
                                                                                                           get_cat_standard_view_url_params(OVERLOAD_SITE),
                                                                                                           str(obj.account_balance).replace('.', ','))
        return a_string
    account_balance_link.allow_tags = True
    account_balance_link.short_description = "Saldo"
    account_balance_link.admin_order_field = 'account_balance'

    def create_reminder_action(self, request, queryset):
        """
        Create reminder for each of the selected customers
        """
        iCounter = 0
        for oCustomer in queryset:
            oRemGen = ReminderGenerator(oCustomer, request)
            if oRemGen.createReminder():
                iCounter += 1

        if iCounter == 1:
            messages.info(request, 'Es wurde %d Mahnung erstellt.' % iCounter)
        else:
            messages.info(request, 'Es wurden %d Mahnungen erstellt.' % iCounter)
    create_reminder_action.short_description = "Mahnung erstellen"

    def export_xlsx(self, request, queryset):
        """ Action for downloading details of a given customer list
        """

        dPayTypes = dict(PayType.PAYTYPE_CHOICES)

        def refine_value(field, value):
            """ Add special refinement to cell value before putting to
                worksheet
            """
            if field == 'pay_type':
                return dPayTypes[value]
            if isinstance(value, bool):
                return u'Ja' if value else u'Nein'
            elif isinstance(value, datetime):
                return value.strftime('%d.%m.%Y %H:%M')
            elif isinstance(value, date):
                return value.strftime('%d.%m.%Y')
            return value

        customer_fields = [
            'title', 'surname', 'name', 'street', 'zip_code', 'city', 'phone',
            'phone2', 'email', 'is_suspended', 'paper_invoice', 'pay_type']

        field_pos = customer_fields.index('email')+1
        if CUST_EXTRA2['ACTIVE']:
            customer_fields.insert(field_pos, 'extra_attribute2')
        if CUST_EXTRA['ACTIVE']:
            customer_fields.insert(field_pos, 'extra_attribute')
        if MODULE_ACCOUNTING or MODULE_PREPAID:
            customer_fields.insert(field_pos, 'account_balance')

        sepamandate_fields = [
            'name', 'street', 'city', 'mandate_id', 'bank', 'iban', 'bic']

        output = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(output)
        format_bold = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()

        # Format width and name of columns
        worksheet.set_column(
            0, len(customer_fields)+len(sepamandate_fields)-1, 16)
        col_names = []
        for key in customer_fields:
            col_names.append(
                Customer._meta.get_field(key).verbose_name.decode('utf-8'))
        col_names.insert(customer_fields.index('name')+1, 'Kinder')
        for key in sepamandate_fields:
            col_names.append(
                SepaMandate._meta.get_field(key).verbose_name.decode('utf-8'))
        worksheet.write_row(0, 0, col_names, format_bold)

        # Next rows, recond details
        fields = customer_fields + \
            ['sepamandate__'+key for key in sepamandate_fields]
        query_fields = fields + ['child__name', 'child__surname', 'id',
                                 'sepamandate__is_valid']
        child_col_id = fields.index('name') + 1
        fields.insert(child_col_id, 'kinder')

        wsrow = WorksheetRow(worksheet, customer_id=None)
        for record in queryset.values(*query_fields):

            if record['child__surname']:
                record['kinder'] = u'{0}, {1}'.format(
                    record['child__surname'], record['child__name'])

            # Append up child values, or write the row to worksheet
            if record['id'] == wsrow.extras['customer_id']:
                wsrow.values[child_col_id] += '\n' + record['kinder']
                wsrow.height += 1
                continue
            else:
                # Flush old and build current row
                wsrow.flush()
                wsrow = WorksheetRow(
                    worksheet, id=wsrow.id+1, customer_id=record['id'])
                no_sepamandate = not record['sepamandate__is_valid']
                for key in fields:
                    if no_sepamandate and key.startswith('sepamandate__'):
                        refined_value = ''
                    else:
                        refined_value = refine_value(key, record.get(key, ''))
                    wsrow.values.append(refined_value)

        # Flush last row.. ugly!
        wsrow.flush()
        workbook.close()

        output.seek(0)
        response = HttpResponse(
            output.read(), content_type="application/octet-stream")
        response['Content-Disposition'] = \
            'attachment; filename=Kunden_Export.xlsx'
        return response
    export_xlsx.short_description = u'XLSX exportieren'


class CustomerAccountAdmin(CustomerAdmin):

    list_display = ('customer_name', 'child_link', 'pay_type', 'account_balance_link', 'show_suspension_status',)
    list_filter = ('child__facility', 'pay_type', 'account_balance', 'is_suspended',)

    actions = ['create_reminder_action',]

    '''
    def get_urls(self):
        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.module_name

        urlpatterns = patterns('',
            url(r'^customer/(?P<cust_id>\d+)/$',
                self.changelist_view,
                name='%s_%s_my_changelist' % info)
        )
        urlpatterns += super(CustomerAccountAdmin, self).get_urls()
        return urlpatterns
    '''

    def get_actions(self, request):
        """
        Do not show delete action
        """
        actions = super(CustomerAccountAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def customer_name(self, obj):
        return obj.__unicode__()
    customer_name.short_description = "Kunde"

    def account_balance_link(self, obj):
        """
        Creates link to CustomerAccountTransactions in field account_balance
        """
        a_string = '<a href="%s?customer__id=%s">%s</a><br />' % (reverse("admin:kg_customeraccounttransaction_changelist"), obj.id, str(obj.account_balance))
        return a_string
    account_balance_link.allow_tags = True
    account_balance_link.short_description = "Saldo"

    def show_balance_status(self, obj):
        """
        Show status of account_balance
        """
        if obj.account_balance < 0:
            return u'Negativ'
        elif obj.account_balance > 0:
            return u'Positiv'
        else:
            return u'Ausgeglichen'
    show_balance_status.short_description = "Saldo Kundenkonto"

    def create_reminder_action(self, request, queryset):
        """
        Create reminder for each of the selected customers
        """
        iCounter = 0
        for oCustomer in queryset:
            oRemGen = ReminderGenerator(oCustomer, request)
            if oRemGen.createReminder():
                iCounter += 1

        if iCounter == 1:
            messages.info(request, 'Erfolgreich %d Mahnung erstellt.' % iCounter)
        else:
            messages.info(request, 'Erfolgreich %d Mahnungen erstellt.' % iCounter)
    create_reminder_action.short_description = "Mahnung erstellen"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return False

create_modeladmin(Customer, CustomerAccountAdmin, name="Kundenkonto")


class CustomerAccountTransactionAdmin(SimpleHistoryAdmin):

    form = CustomerAccountTransactionAdminForm

    list_display = ('customer', 'show_transaction_date', 'amount', 'type', 'description')
    list_filter = ('type',)
    search_fields = ('customer__name','customer__surname', )
    fields = ('customer', 'type', 'description', 'amount', 'transaction_date')

    if not MODULE_PREPAID:
        list_display += ('status', 'show_reminder_number',)
        list_filter += ('status',)
        actions = ['make_status_open', 'make_status_booked']

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        if 'customer__id' in request.GET:
            extra_context['customer_id'] = request.GET['customer__id']
        return super(CustomerAccountTransactionAdmin, self).changelist_view(request, extra_context=extra_context)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if 'customer_id' in request.GET and db_field.name == "customer":
            kwargs["queryset"] = Customer.objects.filter(id=request.GET['customer_id'])
        return super(CustomerAccountTransactionAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def response_add(self, request, obj, post_url_continue=''):
        post_url_continue = "%s?customer__id=%s%s" % (reverse("admin:kg_customeraccounttransaction_changelist"),
                                                      obj.customer.id,
                                                      get_cat_standard_view_url_params(OVERLOAD_SITE))
        return HttpResponseRedirect(post_url_continue)

    def response_change(self, request, obj):
        post_url_continue = "%s?customer__id=%s%s" % (reverse("admin:kg_customeraccounttransaction_changelist"),
                                                      obj.customer.id,
                                                      get_cat_standard_view_url_params(OVERLOAD_SITE))
        return HttpResponseRedirect(post_url_continue)

    def show_invoice_number(self, obj):
        if obj.invoice_id > 0:
            return obj.invoice_id
        else:
            return ''
    show_invoice_number.short_description = u'Rechnung'

    def show_reminder_number(self, obj):
        try:
            reminder = Reminder.objects.get(pk=obj.reminder_id)
        except Reminder.DoesNotExist:
            return ''
        else:
            return reminder.number
    show_reminder_number.short_description = u'Mahnung'

    def show_transaction_date(self, obj):
        return '<span title="%s">%s</span>' % (obj.transaction_date.strftime("%d.%m.%Y %H:%M:%S"), obj.transaction_date.strftime("%d.%m.%Y") )
    show_transaction_date.allow_tags = True
    show_transaction_date.short_description = u'Buchungsdatum'

    def make_status_open(self, request, queryset):
        queryset.update(status=CustomerAccountTransaction.STATUS_OPEN)
    make_status_open.short_description = u'Status auf Offen setzen'

    def make_status_booked(self, request, queryset):
        queryset.update(status=CustomerAccountTransaction.STATUS_BOOKED)
    make_status_booked.short_description = u'Status auf Gebucht setzen'


class ChildAdmin(admin.ModelAdmin):

    form = ChildAdminForm
    search_fields = ['name', 'surname', 'cust_nbr', 'rfid_tag']
    actions = ['export_xlsx']

    if CANTEEN_MODE:
        list_display = ('__unicode__', 'chip_column', 'chip_number',
                        'rfid_tag')
        readonly_fields = ('name', 'surname',)
    else:
        list_display = ('__unicode__', 'cust_nbr',)
        if CHILD_EXTRA['ACTIVE']:
            list_display += ('extra_attribute',)
        list_display += ('customer_link', 'facility_column', 'chip_column')
        #readonly_fields = ('rfid_tag', )

    if CANTEEN_MODE:
        fieldsets = (
            ('Allgemeine Angaben', {
                'fields': ('surname', 'name',)
            }),
            ('Chipkarte', {
                'fields': ('chip_number', 'rfid_tag', 'chip_deposit')
            }),
        )
    else:
        child_fields = ('customer', 'cust_nbr', 'surname', 'name', 'birthday')
        if CHILD_EXTRA['ACTIVE']:
            child_fields += ('extra_attribute',)
        if CHILD_EXTRA2['ACTIVE']:
            child_fields += ('extra_attribute2',)
        fieldsets = (
            ('Allgemeine Angaben', {
                'fields': child_fields
            }),
            ('Chipkarte', {
                'fields': ('chip_number', 'rfid_tag', 'chip_deposit')
            }),
            ('Einrichtung', {
                'fields': ('facility', 'facility_subunit')
            }),
            ('Allergene', {
                'fields': ('allergen', 'order_allergy_meal')
            }),
            ('Notizen', {
                'fields': ('note',)
            }),
        )

    if CANTEEN_MODE:
        list_filter = (ChildHasChipListFilter,)
    else:
        filter_horizontal = ('allergen',)
        list_filter = (ChildFacilityFilter, ChildSubunitFilter,
                       ChildHasChipListFilter, 'allergen')

    if OVERLOAD_SITE == 'abraxas':
        list_filter = ('facility', 'extra_attribute', ChildHasChipListFilter, 'allergen')

    def facility_column(self, obj):
        return obj.facility.name + ', ' + obj.facility_subunit.name_short

    facility_column.allow_tags = True
    facility_column.short_description = 'Einrichtung, Klasse/Gruppe'

    def chip_column(self, obj):
        if obj.rfid_tag != '':
            return u'Ja'
        else:
            return u'Nein'
    chip_column.allow_tags = True
    chip_column.short_description = 'Chip zugeordnet'

    def export_xlsx(self, request, queryset):
        """ Action for downloading details of a given customer list
        """
        def refine_value(field, value):
            """ Add special refinement to cell value before putting to
                worksheet
            """
            if isinstance(value, bool):
                return u'Ja' if value else u'Nein'
            elif isinstance(value, datetime):
                return value.strftime('%d.%m.%Y %H:%M')
            elif isinstance(value, date):
                return value.strftime('%d.%m.%Y')
            return value

        child_fields = [
            'name', 'surname', 'cust_nbr', 'birthday', 'facility',
            'facility_subunit', 'chip_number', 'rfid_tag', 'chip_deposit']

        customer_fields = [
            'title', 'surname', 'name', 'street', 'zip_code', 'city', 'phone',
            'phone2', 'email']

        if CUST_EXTRA['ACTIVE']:
            customer_fields.append('extra_attribute')
        if CUST_EXTRA2['ACTIVE']:
            customer_fields.append('extra_attribute2')

        output = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(output)
        format_bold = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()

        # Format width and name of columns
        worksheet.set_column(0, len(child_fields)+len(customer_fields)-1, 16)
        col_names = []
        for key in child_fields:
            col_names.append(
                Child._meta.get_field(key).verbose_name.decode('utf-8'))
        for key in customer_fields:
            col_names.append(
                Customer._meta.get_field(key).verbose_name.decode('utf-8'))
        worksheet.write_row(0, 0, col_names, format_bold)

        # Next rows, record details
        fields = child_fields + \
            ['customer__'+key for key in customer_fields]
        fields[fields.index('facility')] = 'facility__name'
        fields[fields.index('facility_subunit')] = 'facility_subunit__name'
        for row_id, record in enumerate(queryset.values(*fields)):
            wsrow = WorksheetRow(worksheet, id=row_id+1)
            for key in fields:
                wsrow.values.append(refine_value(key, record.get(key, '')))
            wsrow.flush()

        workbook.close()
        output.seek(0)
        response = HttpResponse(
            output.read(), content_type="application/octet-stream")
        response['Content-Disposition'] = \
            'attachment; filename=Kinder_Export.xlsx'
        return response
    export_xlsx.short_description = u'XLSX exportieren'


class SubFacilityFormSet(BaseInlineFormSet):

    def clean(self):
        """Ensure that non-empty subfacility will not be cleaned."""
        super(SubFacilityFormSet, self).clean()
        for form in self.forms:
            if not hasattr(form, 'cleaned_data'):
                continue
            if form.cleaned_data.get('DELETE'):
                if not form.instance.is_empty:
                    raise ValidationError(
                        u'Diese Untereinheit enthält noch Kinder')


class FacilitySubunitInline(admin.TabularInline):
    formset = SubFacilityFormSet
    model = FacilitySubunit
    extra = 1
    exclude = ('mealplan',)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        """Exclude pricegroups not bound to any mealplan."""
        field = super(FacilitySubunitInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs)
        if db_field.name == 'price_group':
            field.queryset = field.queryset.exclude(mealplan=None)
        return field


class FacilityAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_short', 'type', 'tax_rate', 'is_active')
    fieldsets = (
        ('Allgemeine Angaben', {
            'fields': ('is_active', 'name', 'name_short', 'type', 'tax_rate', 'flatrate_customizable')
        }),
    )
    inlines = [FacilitySubunitInline]

    def has_delete_permission(self, request, obj=None):
        """Prevent delete facility when still having children inside."""
        if obj is not None:
            return not obj.child_set.exists()
        elif request.POST:
            for pk in request.POST.getlist('_selected_action', []):
                facility = Facility.objects.get(pk=pk)
                if not facility.is_empty:
                    return False
        return super(FacilityAdmin, self).has_delete_permission(request, obj)


class PayTypeAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'new_customers')


class FlatrateAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'active', 'last_modified', 'created_by', 'modified_by')
    list_filter = ('child__facility',)
    readonly_fields = ('last_modified',)


class ReductionAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'reduction',)
    form = ReductionForm


class ChildReductionAdmin(admin.ModelAdmin):
    search_fields = ['child__name', 'child__surname']
    list_display = ('__unicode__', 'date_start', 'date_end',
                    'reference_number', 'reduction')
    list_filter = (
        'child__facility', ValidReductionFilter, LastMonthValidReductionFilter
    )


class HolidayAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'date_start', 'date_end')


class ActionLogAdmin(admin.ModelAdmin):

    date_hierarchy = 'action_time'

    readonly_fields = ActionLog._meta.get_all_field_names()

    actions = None

    list_filter = [
        'action_flag'
    ]

    search_fields = [
        'change_message',
        'user__username',
    ]

    list_display = [
        'action_time',
        'change_message',
        'user_column',
        'action_flag',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return False

    def user_column(self, obj):
        if obj.user is None:
            return 'System'
        else:
            return unicode(obj.user)

    user_column.allow_tags = True
    user_column.short_description = 'Benutzer/Login'

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = u'<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return link

    object_link.allow_tags = True
    object_link.admin_order_field = 'object_repr'
    object_link.short_description = u'object'


class BankAccountTransactionAdmin(admin.ModelAdmin):

    readonly_fields = BankAccountTransaction._meta.get_all_field_names()

    actions = ['manual_match_selected_transactions', ]

    fieldsets = (
        ('Buchungsdetails', {
            'fields': ('t_date', 'amount', 'booking_type', 'subject',
                       'bank_account_owner', 'bank_account_number', 'bank_icode',
                       'type', 'last_modified', )
        }),
    )

    list_filter = ['booking_type', 'type', ]

    search_fields = [
        't_date',
        'amount',
        'subject',
        'bank_account_owner',
    ]


    list_display = [
        't_date',
        'amount',
        'booking_type',
        'bank_account_owner',
        'subject',
        'type_column',
    ]

    def type_column(self, obj):
        return '<span class="nowrap">%s</span>' % obj.get_type_display()

    type_column.allow_tags = True
    type_column.short_description = 'Kategorie'

    def queryset(self, request):
        """
        show only the open transactions
        """
        qs = super(BankAccountTransactionAdmin, self).queryset(request)
        return qs.filter(status=BankAccountTransaction.STATUS_OPEN)

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def manual_match_selected_transactions(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        oResponse = redirect('kg.views_admin.bat_manual_matching')
        oResponse['Location'] += '?ids=%s' % ",".join(selected)
        return oResponse
    manual_match_selected_transactions.short_description = u'Ausgewählte Bankkonto-Buchungen zuordnen'


class ReminderAdmin(admin.ModelAdmin):
    list_display = ('customer', 'number', 'total', 'create_date', 'pay_until', 'type', 'status', 'first_downloaded' )
    list_filter = ('type', 'status', ReminderDownloadedListFilter)

    actions=['download_selected', 'create_reminder', 'set_status_payed', 'set_status_encashment', 'set_status_failed', 'set_status_open', 'delete_selected']

    # TODO: in Django 1.7 remove the init method and set "list_display_links = None" in Admin instead
    def __init__(self, *args, **kwargs):
        super(ReminderAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )

    def has_change_permission(self, request, obj=None):
        if obj is None:
            return True
        else:
            return False

    def has_add_permission(self, request):
        return False

    def show_download_status(self, obj):
        """
        Show if reminder was downloaded
        """
        if obj.first_downloaded:
            return u'Ja'
        else:
            return u'Nein'
    show_download_status.short_description = "Heruntergeladen"

    def download_selected(self, request, queryset):

        merger = PdfFileMerger(strict=False)

        # add all reminders
        iCounter = 0
        sFilename = 'Mahnung'
        for oReminder in queryset:
            try:
                merger.append(REMINDER_ROOT + "/" + oReminder.filename)
                if iCounter == 0:
                    sFilename += '_%s' % oReminder.number
                if iCounter == 1:
                    sFilename += '_und_weitere'
                iCounter += 1
            except IOError:
                pass
            else:
                if not oReminder.first_downloaded:
                    oReminder.first_downloaded=date.today()
                    oReminder.save()

        sFilename += '.pdf'
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=' + sFilename
        oOutputStream = cStringIO.StringIO()
        merger.write(oOutputStream)
        response.write(oOutputStream.getvalue())
        oOutputStream.close()
        return response

    download_selected.short_description = u'Ausgewählte Mahnungen herunterladen'

    def create_reminder(self, request, queryset):
        """
        Create reminder for each of the selected customers
        """
        iCounter = 0
        for oReminder in queryset:
            oRemGen = ReminderGenerator(oReminder.customer, request, reminder=oReminder)
            if oRemGen.createReminder():
                iCounter += 1

        if iCounter == 1:
            messages.info(request, 'Es wurde %d Mahnung erstellt.' % iCounter)
        else:
            messages.info(request, 'Es wurden %d Mahnungen erstellt.' % iCounter)
    create_reminder.short_description = u"Mahnung nächste Mahnstufe erstellen"

    def delete_selected(self, request, queryset):
        if not self.has_delete_permission(request):
            raise PermissionDenied
        if request.POST.get('post'):
            queryset = queryset.order_by('-type')
            iCounter = 0
            lReminders = []
            for oObj in queryset:
                iCounter += 1
                lReminders.append(oObj.id)
                if oObj.children.exists():
                    iCounter += 1
                    oChild = oObj.children.all()[0]
                    if oChild.children.exists():
                        iCounter += 1
                oObj.delete()
            messages.info(request, _("Successfully deleted %(count)d %(items)s.") % {
                "count": iCounter, "items": model_ngettext(self.opts, iCounter)
            })
        else:
            return delete_selected(self, request, queryset)
    delete_selected.short_description = u"Ausgewählte Mahnungen löschen"

    def set_status_payed(self, request, queryset):
        for oReminder in queryset:
            if oReminder.parent:
                if oReminder.parent.parent:
                    oReminder.parent.parent.status = Reminder.STATUS_PAYED
                    oReminder.parent.parent.save()
                oReminder.parent.status = Reminder.STATUS_PAYED
                oReminder.parent.save()
            oReminder.status = Reminder.STATUS_PAYED
            oReminder.save()
    set_status_payed.short_description = u'Status auf Bezahlt setzen'

    def set_status_encashment(self, request, queryset):
        for oReminder in queryset:
            if oReminder.parent:
                if oReminder.parent.parent:
                    oReminder.parent.parent.status = Reminder.STATUS_ENCASHMENT
                    oReminder.parent.parent.save()
                oReminder.parent.status = Reminder.STATUS_ENCASHMENT
                oReminder.parent.save()
            oReminder.status = Reminder.STATUS_ENCASHMENT
            oReminder.save()
    set_status_encashment.short_description = u'Status auf Inkasso setzen'

    def set_status_failed(self, request, queryset):
        for oReminder in queryset:
            if oReminder.parent:
                if oReminder.parent.parent:
                    oReminder.parent.parent.status = Reminder.STATUS_FAILED
                    oReminder.parent.parent.save()
                oReminder.parent.status = Reminder.STATUS_FAILED
                oReminder.parent.save()
            oReminder.status = Reminder.STATUS_FAILED
            oReminder.save()
    set_status_failed.short_description = u'Status auf Ausgemahnt setzen'

    def set_status_open(self, request, queryset):
        queryset.update(status=Reminder.STATUS_OPEN)
    set_status_open.short_description = u'Status auf Offen setzen'

    def get_actions(self, request):
        actions = super(ReminderAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            delete_selected_item = actions['delete_selected']
            del actions['delete_selected']
            actions['delete_selected'] = delete_selected_item
        return actions


class SepaDebitDownloadAdmin(admin.ModelAdmin):
    list_display = ('first_downloaded', 'show_sequence_type', 'debit_amount', 'download_link')

    actions = None

    # disable add button
    def has_add_permission(self, request):
        return False

    # disable edit link
    def __init__(self, *args, **kwargs):
        super(SepaDebitDownloadAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )

    # prevent user from editing objects
    def change_view(self, request, obj=None, extra_context=None):
        return HttpResponseRedirect(reverse('admin:kg_sepadebitdownload_changelist'))

    # change the site title
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['title'] = SepaDebitDownload._meta.verbose_name_plural
        return super(SepaDebitDownloadAdmin, self).changelist_view(request, extra_context=extra_context)

    def show_sequence_type(self, obj):
        """ Return the SEPA sequence type in german """
        if obj.sequence_type == 'FRST':
            return u"Erstlastschrift"
        elif obj.sequence_type == 'RCUR':
            return u"Folgelastschrift"
        else:
            return u"unbekannt"

    show_sequence_type.short_description = "Lastschrift Typ"

    def debit_amount(self, obj):
        """ Return the amount of the SEPA Debit """
        result = SepaDebit.objects.filter(download=obj).aggregate(Sum('amount'))
        return u"%s EUR" % result['amount__sum']

    debit_amount.short_description = "Betrag"

    def download_link(self, obj):
        return "<a href=\"%s\">erneut herunterladen</a>" % reverse('kg.views_admin.sepa_debit_download', args=[obj.id, 'none'])

    download_link.short_description = "Herunterladen"
    download_link.allow_tags = True


class TinyMCEFlatPageAdmin(FlatPageAdmin):
    """Make content field of FlatPage as a WYSIWYG."""
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'content':
            return db_field.formfield(widget=TinyMCE(
                attrs={'cols': 80, 'rows': 30},
                mce_attrs={
                    'theme': 'advanced',
                    'position': 'top',
                    'external_link_list_url': reverse(
                        'tinymce.views.flatpages_link_list')
                },
            ))
        return super(TinyMCEFlatPageAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)


admin.site.register(Facility, FacilityAdmin)
admin.site.register(FacilityType)

admin.site.register(CancelReason)
admin.site.register(Reduction, ReductionAdmin)
admin.site.register(PayType, PayTypeAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(CustomerAccountTransaction, CustomerAccountTransactionAdmin)
admin.site.register(BankAccountTransaction, BankAccountTransactionAdmin)
admin.site.register(Child, ChildAdmin)
admin.site.register(ChildReduction, ChildReductionAdmin)

admin.site.register(Flatrate, FlatrateAdmin)
admin.site.register(Holiday, HolidayAdmin)

admin.site.register(ActionLog, ActionLogAdmin)

admin.site.register(Reminder, ReminderAdmin)

admin.site.register(SepaDebitDownload, SepaDebitDownloadAdmin)

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, TinyMCEFlatPageAdmin)
