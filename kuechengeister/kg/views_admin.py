# -*- coding: utf-8 -*-

from __future__ import with_statement, absolute_import
from datetime import date, datetime, timedelta
from decimal import Decimal
from urllib import urlencode

import urlparse
import json
import random
import string
import tempfile
import cStringIO

from PyPDF2 import PdfFileMerger, PdfFileReader, PdfFileWriter
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import RequestContext
from django.template.loader import render_to_string
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render_to_response, redirect
from django.contrib import messages
from django.db import transaction
from django.db.models import Sum, Count, Q
from django.core import urlresolvers
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.utils import simplejson
from django.utils.log import getLogger

from celery.result import AsyncResult

from reports.helpers import generate_pdf

from kg.models import (
    Child, Customer, Order, Facility, FacilityType, FacilitySubunit, Flatrate,
    Reduction, InvoiceLog, ActionLog, WEEKDAYS, BankAccountTransaction,
    CustomerAccountTransaction, PayType, SepaDebit, SepaDebitDownload,
    SepaMandate, ChildReduction
    )
from menus.models import Meal, Menu, Mealtime, Price, MealPlan
from kg_utils.helpers import clean_not_ascii_chars
from kg_utils.date_helper import DateHelper
from kg_utils.admin_helper import (
    create_filter_list, update_children_from_pos_data, deactivate_user,
    update_orders_from_pos_data, get_voucher_html)
from kg_utils.invoice_generator import InvoiceGenerator
from kg_utils.invoice_helper import InvoiceHelper
from kg_utils.order_helper import get_order_history
from kg_utils.accounting_helper import (
    match_bank_account_transactions, createCustAccTransOutOfBankAccTrans,
    connectCustAccTrans, parse_and_save_bank_csv_upload,
    parse_and_save_bank_csv_upload_bodelschwinghhaus, parse_and_save_bank_xlsx_upload_rudolstadt,
    )
from kg_utils import ucsv as csv
from kg_utils.sepa_generator import SEPAConfig, SepaDD, Payment, SepaHelper
from kg_utils.order_helper import order_menu
from settings import (
    SITE_CONTEXT, DEFAULT_FROM_EMAIL, INVOICE_ROOT, SEPA, DEMO_MODUS,
    MODULE_ACCOUNTING, MODULE_INVOICE, MODULE_INVOICE_CUST, MODULE_PREPAID,
    MODULE_REDUCTION, MODULE_FLATRATE, PREPAID_SETTINGS, OVERLOAD_SITE,
    SITE_OVERLOAD_STATICFILES_DIR, CANTEEN_MODE, MODULE_TERMINAL,
    MODULE_ABSENCE, ORDER_EXCEED_WARNING, MODULE_INVOICE_BILLING_MONTHS,
    MEALTIMES
    )

from kg.forms_admin import (
    InvoiceSelectionForm, TaxCsvSelectionForm, MailTestForm,
    VoucherPerFacilityForm, MonthSelectionForm, WeekSelectionForm,
    UploadFileForm, EmailExportForm, SepaSelectionForm, VoucherPerChildForm,
    FibuBuchForm, FibuStammForm
    )
from kg.billing import perform_billing, perform_release


logger = getLogger(__name__)

CHOICE_ALL = '0'
CHOICE_YES = '1'
CHOICE_NO = '2'


@staff_member_required
def index(request):
        dSepaDebits = {}
        if MODULE_PREPAID:
            dSepaDebits['FRST'] = SepaDebit.objects.new().frst().aggregate(sum=Sum('amount'))['sum'] or Decimal('0.00')
            dSepaDebits['RCUR'] = SepaDebit.objects.new().rcur().aggregate(sum=Sum('amount'))['sum'] or Decimal('0.00')

        return render_to_response('admin/index.html',
                                  {'OVERLOAD_SITE': OVERLOAD_SITE,
                                   'MODULE_PREPAID': MODULE_PREPAID,
                                   'PREPAID_SETTINGS': PREPAID_SETTINGS,
                                   'MODULE_ACCOUNTING': MODULE_ACCOUNTING,
                                   'MODULE_FLATRATE': MODULE_FLATRATE,
                                   'MODULE_INVOICE': MODULE_INVOICE,
                                   'CANTEEN_MODE': CANTEEN_MODE,
                                   'MODULE_TERMINAL': MODULE_TERMINAL,
                                   'MODULE_REDUCTION': MODULE_REDUCTION,
                                   'MODULE_ABSENCE': MODULE_ABSENCE,
                                   'SepaDebits': dSepaDebits},
                                  context_instance = RequestContext(request))


@staff_member_required
def order_index(request):

    # Ajax request: order menu for single child
    if request.is_ajax():
        try:
            sChildId, sMenuId, sQuantity = request.GET.get('value').split('-')
            dReturnDict = order_menu(
                int(sMenuId), int(sChildId), int(sQuantity), request,
                reason=Order.REASON_STAFF
            )
            if dReturnDict['success']:
                return HttpResponse(simplejson.dumps({'error': False,
                                                      'quantity': dReturnDict['quantity']}),
                                    mimetype='application/javascript')
            else:
                return HttpResponse(simplejson.dumps({'error': True,
                                                      'quantity': dReturnDict['quantity'],
                                                      'message': dReturnDict['message']}),
                                    mimetype='application/javascript')

        except Exception as exc:
            #print exc.message
            return HttpResponse(simplejson.dumps({'error': True,
                                                  'message': 'Es ist ein Fehler aufgetreten!'}),
                                mimetype='application/javascript')

    # POST request: order menu for all selected children
    if request.POST and 'menu_id' in request.POST:
        iMenuId = int(request.POST.get('menu_id', 0))
        sChildIdsString = request.POST.get('child_ids', '')
        lChildIds = sChildIdsString.split('-')

        # order menus or cancel orders
        if iMenuId > 0 and len(lChildIds) > 0:
            if 'form_submit_order' in request.POST:
                for iChildId in lChildIds:
                    order_menu(iMenuId, iChildId, 1, request, reason=Order.REASON_STAFF)
            if 'form_submit_cancel' in request.POST:
                for iChildId in lChildIds:
                    order_menu(iMenuId, iChildId, 0, request, reason=Order.REASON_STAFF)

        # redirect for site refresh
        sRedirectUrl = '%s?%s' % (urlresolvers.reverse('kg.views_admin.order_index'), request.POST.get('query_string', ''))
        return HttpResponseRedirect(sRedirectUrl)

    oISOCalendar = datetime.now().isocalendar()
    iCurrentYear = oISOCalendar[0]
    iCurrentWeek = oISOCalendar[1]

    iWeek = int(request.GET.get('week') or 0) or iCurrentWeek
    iYear = int(request.GET.get('year') or 0) or iCurrentYear
    sChildQuery = request.GET.get("q", "")

    # facility and facility subunit filters
    all_facilities = Facility.ex_objects.values_list('id', 'name')
    iSelectedFacilityId = int(request.GET.get('facility', 0) or 0)
    all_facilities_selected = iSelectedFacilityId == 0
    # Not allow viewing all facility out of searching
    if not sChildQuery and iSelectedFacilityId == 0:
        iSelectedFacilityId = all_facilities and all_facilities[0][0]
        all_facilities_selected = False
    subs = FacilitySubunit.objects.filter(
        facility__id=iSelectedFacilityId).values_list('id', 'name')
    facilities = create_filter_list(
        request, ['facility'], [all_facilities, ], [iSelectedFacilityId],
        False, ['facility_subunit']
    )
    subunits = create_filter_list(
        request, ['facility_subunit'], [subs, ], [], True, [])

    # filter displayed childs
    iSelectedSubunit = request.GET.get('facility_subunit', '') or 0
    lWeekDays = DateHelper.getDaysInCalendarWeek(iYear, iWeek)

    # Get child query & filters
    lAllFilters = ["facility={}".format(iSelectedFacilityId)]
    qChildFilters = Q()
    if sChildQuery and facilities:
        # Enable finding in all facilities
        query = urlparse.parse_qs(
            urlparse.urlparse(facilities[0]['link']).query)
        for key, value in query.iteritems():
            try:
                query[key] = value[0]
            except IndexError:
                pass
        query.update({
            'facility': 0,
            'q': sChildQuery.encode('utf-8'),
        })
        facilities.insert(0, {
            'selected': all_facilities_selected,
            'link': u'?' + urlencode(query),
            'name': u'Alle'
        })
        lAllFilters.append(u"q={}".format(sChildQuery))
        qChildFilters = Q(name__icontains=sChildQuery) \
                        | Q(surname__icontains=sChildQuery) \
                        | Q(cust_nbr__icontains=sChildQuery)
    qsChildren = Child.objects.filter(qChildFilters)

    if iSelectedSubunit:
        lAllFilters.append("facility_subunit={}".format(iSelectedSubunit))
        # Get children, mealplans & meals for selected subunit.
        qsChildren = qsChildren.filter(
            facility_subunit__id=iSelectedSubunit) \
            .select_related('customer', 'facility_subunit')
        qsMealPlans = MealPlan.objects.filter(
            facilitysubunit__id=iSelectedSubunit)
        qsMeals = Meal.objects.getAvailableInRange(
            Q(periods__mealplan__facilitysubunit__id=iSelectedSubunit),
            lWeekDays[0], lWeekDays[-1]).filter(allow_order=True)
    elif not all_facilities_selected:
        # Get children, mealplans & meals for selected facility.
        qsChildren = qsChildren.filter(facility__id=iSelectedFacilityId) \
            .select_related('customer', 'facility_subunit')
        qsMealPlans = MealPlan.objects.filter(
            facilitysubunit__facility_id=iSelectedFacilityId)
        # meals in that facility that can be ordered
        qsMeals = Meal.objects.getAvailableInRange(
            Q(periods__mealplan__facilitysubunit__facility__id=iSelectedFacilityId),
            lWeekDays[0], lWeekDays[-1]).filter(allow_order=True)
    else:
        qsExcludedFacilities = Facility.objects.filter(type__name='Kundenparkplatz')
        # Get children, mealplans & meals for all facilities but 'Kundenparkplatz'
        qsChildren = qsChildren.exclude(facility__in=qsExcludedFacilities).select_related(
            'customer', 'facility_subunit')
        qsMealPlans = MealPlan.objects.exclude(facilitysubunit__facility__in=qsExcludedFacilities)
        # meals in that facility that can be ordered
        subunit_ids = qsChildren.values_list('facility_subunit_id', flat=True)
        qsMeals = Meal.objects.getAvailableInRange(
            Q(periods__mealplan__facilitysubunit__in=subunit_ids),
            lWeekDays[0], lWeekDays[-1]).filter(allow_order=True)

    # Build order filtering options
    sel_status = request.GET.get('ordered', CHOICE_ALL)
    if sel_status in [CHOICE_YES, CHOICE_NO]:
        lAllFilters.append("ordered={}".format(sel_status))
    base_link = '?facility={}'.format(iSelectedFacilityId)
    if 'facility_subunit' in request.GET:
        base_link += '&facility_subunit={}'.format(
            request.GET['facility_subunit'])
    status_choices = [
        {
            'link': base_link,
            'name': 'Alle',
            'selected': sel_status == CHOICE_ALL
        },
        {
            'link': base_link + '&ordered={}'.format(CHOICE_YES),
            'name': 'Ja',
            'selected': sel_status == CHOICE_YES
        },
        {
            'link': base_link + '&ordered={}'.format(CHOICE_NO),
            'name': 'Nein',
            'selected': sel_status == CHOICE_NO
        },
    ]

    # Get menus in given calendar week that can be ordered. The results are
    # ordered by mealplan__id, this is IMPORTANT because specific menus has
    # higher priority than general ones.
    qsMenus = Menu.objects.getMealsPerCalendarWeek(iYear, iWeek) \
        .filter(meal__allow_order=True) \
        .filter(Q(mealplan__isnull=True) | Q(mealplan__in=qsMealPlans)) \
        .order_by('mealplan__id')
    qsOrders = Order.objects.filter(menu__in=qsMenus).select_related('menu')
    menu_dates = qsMenus.dates('date', 'day')

    # Count number of days when each child has order. Class method
    # Order.count_ordered_weekdays() can be used upon each child, however it
    # would be inefficient in this case where we have orders of all week
    # collected.
    dChildOrders = {}
    # Make dict out of orders and menus queryset for better performance later on
    dOrders = {}
    dMenus = {}
    for oOrder in qsOrders:
        sOrderKey = '{}-{}'.format(oOrder.child_id, oOrder.menu.id)
        dOrders[sOrderKey] = oOrder.quantity
        if ORDER_EXCEED_WARNING:
            if oOrder.child_id in dChildOrders:
                dChildOrders[oOrder.child_id].add(oOrder.menu.date.day)
            else:
                dChildOrders[oOrder.child_id] = {oOrder.menu.date.day}

    for oMenu in qsMenus:
        sKey = oMenu.date.strftime("%Y%m%d") + "-" + str(oMenu.meal_id)
        if sKey not in dMenus:
            dMenus[sKey] = []
        dMenus[sKey].append(oMenu)

    # go through list of all children and create order lists
    lChildren = []
    lChildIdsAsString = []
    for oChild in qsChildren:
        availableMeals = Meal.objects.getAvailableInRange(
            Q(periods__mealplan__facilitysubunit=oChild.facility_subunit),
            lWeekDays[0], lWeekDays[-1]).filter(allow_order=True)
        lChildIdsAsString.append(str(oChild.id))
        lChildMeals = []
        ordered = False
        for oMeal in qsMeals:
            if all_facilities_selected and oMeal not in availableMeals:
                continue
            lChildOrders = []
            for date_item in menu_dates:
                sMenuKey = '{}-{}'.format(date_item.strftime("%Y%m%d"),
                                          oMeal.id)
                if sMenuKey in dMenus:
                    # There's at least one menu for the date & meal.
                    bHasOrderableMenu = False
                    bFoundMenu = False
                    for oMenu in dMenus[sMenuKey]:
                        # Find the first menu that is available for the child
                        if oMenu.mealplan_id == \
                                oChild.facility_subunit.mealplan_id or \
                                not oMenu.mealplan_id:
                            # Found a specific mealplan menu or a general one.
                            bHasOrderableMenu = oMenu.can_be_ordered
                            bFoundMenu = True
                        if bFoundMenu:
                            if bHasOrderableMenu:
                                sOrderKey = '{}-{}'.format(oChild.id, oMenu.id)
                                dChildOrder = {'exists': True, 'id': sOrderKey,
                                               'quantity': 0}
                                # Check if there's an existing order or not.
                                if sOrderKey in dOrders:
                                    dChildOrder['quantity'] = dOrders[sOrderKey]
                                    if dChildOrder['quantity'] and not ordered:
                                        ordered = True
                                lChildOrders.append(dChildOrder)
                            break
                    if not bHasOrderableMenu:
                        lChildOrders.append({'exists': False})
                else:
                    lChildOrders.append({'exists': False})
            lChildMeals.append({
                'name': oMeal.name,
                'name_short': oMeal.name_short,
                'orders': lChildOrders
            })
        if sel_status == CHOICE_ALL or ((sel_status == CHOICE_YES) == ordered):
            if ORDER_EXCEED_WARNING and oChild.id in dChildOrders:
                order_limit = 0
                try:
                    order_limit = int(oChild.extra_attribute)
                except ValueError:
                    pass
                ordered_day_count = len(dChildOrders[oChild.id])
                if ordered_day_count > order_limit:
                    oChild.ordered_day_count = ordered_day_count
            oChild.mealtimes_list = lChildMeals
            lChildren.append(oChild)

    # decide whether to show order for all childen row or not
    bShowOrderAll = False
    lOrderAll = []
    if OVERLOAD_SITE == 'leibundseele' and iSelectedFacilityId == 2:
        bShowOrderAll = True
    if OVERLOAD_SITE == 'oschatz' and iSelectedFacilityId == 5:
        bShowOrderAll = True

    if bShowOrderAll and len(qsChildren) > 0:
        sChildIdsString = '-'.join(lChildIdsAsString)
        for oMeal in qsMeals:
            dMeal = {
                'name': oMeal.name,
                'name_short': oMeal.name_short,
                'menus': []
            }
            for date_item in menu_dates:
                sMenuKey = '{}-{}'.format(date_item.strftime("%Y%m%d"),
                                          oMeal.id)
                if sMenuKey in dMenus:
                    dMeal['menus'].append({'exists': True,
                                           'can_be_ordered': dMenus[sMenuKey][0].can_be_ordered,
                                           'menu_id': dMenus[sMenuKey][0].id,
                                           'child_id_string': sChildIdsString})
                else:
                    dMeal['menus'].append({'exists': False})
            lOrderAll.append(dMeal)

    # Get list of last 3 previous and 8 next weeks
    dtFirstDay = DateHelper().getFirstDayInIsoweek(iYear, iWeek)
    lWeeksForLinks = DateHelper().getWeeks(dtFirstDay, -3)
    lWeeksForLinks += DateHelper().getWeeks(dtFirstDay, 8, False)

    return render_to_response('kg/admin/order_index.html',
                              {'facilities': facilities,
                               'subunits': subunits,
                               'status_choices': status_choices,
                               'dates': menu_dates,
                               'children': lChildren,
                               'current_week_num': iCurrentWeek,
                               'weeks_for_links': lWeeksForLinks,
                               'week_choice': iWeek,
                               'mealtimes': qsMeals,
                               'show_order_all': bShowOrderAll,
                               'order_all_list': lOrderAll,
                               'query': sChildQuery,
                               'all_filters': "&".join(lAllFilters),
                               'url_query_string': request.GET.urlencode()},
                              context_instance=RequestContext(request))


@staff_member_required
def flatrate_index(request):

    def process_ajax():
        with transaction.commit_manually():
            try:
                details = request.GET.get('flatrate')
                child_id, meal_id, weekday, kind = details.split('-')
                value = int(request.GET.get('value'))
                if meal_id and child_id and weekday:
                    try:
                        # retrieve flatrate object if it exists
                        flatrate = Flatrate.objects.get(
                            child__id=child_id,
                            meal__id=meal_id,
                            weekday=weekday)
                    except Flatrate.DoesNotExist:
                        flatrate = Flatrate(
                            child=Child.objects.get(pk=child_id),
                            meal=Meal.objects.get(pk=meal_id),
                            weekday=weekday,
                            active=False,
                            active_on_holidays=False,
                            created_by=request.user)
                    except:
                        raise
                    is_active = bool(value)

                    # kindergarten, holidays and no holidays is the same
                    if kind == 'k':
                        flatrate.active = is_active
                        flatrate.active_on_holidays = is_active
                    # school, no holidays
                    if kind == 'n':
                        flatrate.active = is_active
                    # school, holidays
                    if kind == 'h':
                        flatrate.active_on_holidays = is_active

                    flatrate.modified_by = request.user
                    flatrate.save()
            except:
                transaction.rollback()
                logger.exception('Error when processing flatrate update.')
                return HttpResponse(
                    simplejson.dumps({'error': True}),
                    mimetype='application/javascript'
                )
            else:
                transaction.commit()
                return HttpResponse(
                    simplejson.dumps({'status': is_active, 'error': False}),
                    mimetype='application/javascript'
                )

    if request.is_ajax():
        return process_ajax()

    search_query = request.GET.get("q", "")
    all_facilities = Facility.ex_objects.values_list('id', 'name')
    choosen_facility_id = int(request.GET.get('facility', 0) or 0)
    choosen_subunit_id = int(request.GET.get('facility_subunit', 0) or 0)
    all_facilities_selected = choosen_facility_id == 0

    # Not allow viewing all facility out of searching
    if not search_query and choosen_facility_id == 0:
        choosen_facility_id = all_facilities and all_facilities[0][0]
        all_facilities_selected = False

    subs = FacilitySubunit.objects.filter(
        facility__id=choosen_facility_id).values_list('id', 'name')
    facilities = create_filter_list(
        request, ['facility'], [all_facilities],
        [choosen_facility_id], False, ['facility_subunit']
    )
    subunits = create_filter_list(
        request, ['facility_subunit'], [subs, ], [], True, [])
    base_link = '?facility={}'.format(choosen_facility_id)

    # filter displayed childs
    qsChildren = Child.objects
    if search_query and facilities:
        # Enable finding in all facilities
        query = urlparse.parse_qs(
            urlparse.urlparse(facilities[0]['link']).query)
        query = {k: v[0] for k, v in query.iteritems()}
        query.update({
            'facility': 0,
            'q': search_query.encode('utf-8'),
        })
        facilities.insert(0, {
            'selected': all_facilities_selected,
            'link': u'?' + urlencode(query),
            'name': u'Alle'
        })
        qsChildren = qsChildren.filter(
            Q(name__icontains=search_query) |
            Q(surname__icontains=search_query) |
            Q(cust_nbr__icontains=search_query)
        )
        base_link += '&q=' + search_query

    if choosen_subunit_id:
        qsChildren = qsChildren.filter(facility_subunit__id=choosen_subunit_id)
        qsMeals = Meal.objects.filterAvailable(
            Q(periods__mealplan__facilitysubunit=choosen_subunit_id)
        ).filter(allow_order=True)
    elif not all_facilities_selected:
        qsChildren = qsChildren.filter(facility__id=choosen_facility_id)
        qsMeals = Meal.objects.filterAvailable(
            Q(periods__mealplan__facilitysubunit__facility=choosen_facility_id)
        ).filter(allow_order=True)
    else:
        # Get children, mealplans & meals for all facilities,
        # except: 'Kundenparkplatz'
        qsChildren = qsChildren.exclude(facility__type__name='Kundenparkplatz')
        # meals in that facility that can be ordered
        subunit_ids = qsChildren.values_list('facility_subunit_id', flat=True)
        qsMeals = Meal.objects.filterAvailable(
            Q(periods__mealplan__facilitysubunit__in=subunit_ids)
        ).filter(allow_order=True)

    meal_ids = qsMeals.values_list('id', flat=True)

    # Build presence filtering options
    selected_presence = request.GET.get('flatrate', CHOICE_ALL)
    if selected_presence in [CHOICE_YES, CHOICE_NO]:
        children_with_flatrate = qsChildren.filter(
            flat__meal__in=meal_ids).filter(
            Q(flat__active=True) | Q(flat__active_on_holidays=True)
        ).distinct()
        if selected_presence == CHOICE_YES:
            qsChildren = children_with_flatrate
        elif selected_presence == CHOICE_NO:
            qsChildren = qsChildren.exclude(
                pk__in=children_with_flatrate.values_list('id', flat=True)
            )
    if choosen_subunit_id:
        base_link += '&facility_subunit={}'.format(choosen_subunit_id)
    presence_choices = [
        {
            'link': base_link,
            'name': 'Alle',
            'selected': selected_presence == CHOICE_ALL
        },
        {
            'link': base_link + '&flatrate={}'.format(CHOICE_YES),
            'name': 'Ja',
            'selected': selected_presence == CHOICE_YES
        },
        {
            'link': base_link + '&flatrate={}'.format(CHOICE_NO),
            'name': 'Nein',
            'selected': selected_presence == CHOICE_NO
        },
    ]

    # check if school is selected
    if choosen_facility_id:
        facility = Facility.objects.get(pk=choosen_facility_id)
        is_school = facility.is_school
    else:
        is_school = False

    # select only normal active flatrates
    flatrate_queryset = Flatrate.objects.select_related()
    flatrates_n = flatrate_queryset.active().filter(
        child__in=qsChildren)

    # make dict out of flatrates queryset for faster handling later on
    flatrates_n_dict = dict()
    if not all_facilities_selected:
        for flatrate in flatrates_n:
            # mark school flatrates with n (no holidays)
            postfix = '-n' if is_school else '-k'
            flatrates_n_dict[flatrate.key+postfix] = flatrate
    else:
        for flatrate in flatrates_n:
            # mark school flatrates with n (no holidays)
            postfix = '-n' if flatrate.child.facility.is_school else '-k'
            flatrates_n_dict[flatrate.key+postfix] = flatrate

    # Only find holiday records on schools
    if is_school or all_facilities_selected:
        flatrates_h = flatrate_queryset.active_on_holidays().filter(
            Q(child__in=qsChildren) &
            Q(child__facility__type__id=FacilityType.SCHOOL)
        )
        flatrates_h_dict = dict()
        for flatrate in flatrates_h:
            flatrates_h_dict[flatrate.key+'-h'] = flatrate

    # go through list of all childs and create flatrate lists
    lChildren = []
    for child in qsChildren:
        if all_facilities_selected:
            available_meals = Meal.objects.filterAvailable(
                Q(periods__mealplan__facilitysubunit=child.facility_subunit)
            ).filter(allow_order=True)
        lMeals = []
        if all_facilities_selected:
            is_school = child.facility.is_school
        for oMeal in qsMeals:
            if all_facilities_selected and oMeal not in available_meals:
                continue
            flatrate_n_list = list()
            flatrate_h_list = list()
            for weekday in WEEKDAYS:
                # normal flatrate
                key_prefix = Flatrate.make_key(child.id, oMeal.id, weekday[0])
                value = key_prefix + ('-n' if is_school else '-k')
                flatrate_n_list.append({
                    'checked': value in flatrates_n_dict,
                    'value': value
                })
                # holiday flatrate
                if is_school:
                    value = key_prefix + '-h'
                    flatrate_h_list.append({
                        'checked': value in flatrates_h_dict,
                        'value': value
                    })

            lMeals.append({
                'name': oMeal.name,
                'name_short': oMeal.name_short,
                'flatrates': flatrate_n_list
            })
            if is_school:
                lMeals.append({
                    'name': oMeal.name,
                    'name_short': oMeal.name_short + u' (F)',
                    'flatrates': flatrate_h_list
                })

        child.mealtimes_list = lMeals
        lChildren.append(child)

    return render_to_response(
        'kg/admin/flatrate_index.html',
        {
            'facilities': facilities,
            'subunits': subunits,
            'presence_choices': presence_choices,
            'childs': lChildren,
            'mealtimes': qsMeals,
            'weekdays': WEEKDAYS
        },
        context_instance=RequestContext(request)
    )


@staff_member_required
def sepa_debit_download(request, download_id, sequence_type='none'):

    download_id = int(download_id)

    if sequence_type.lower() not in ['rcur', 'frst', 'none']:
        messages.error(request, u"Es ist ein Fehler aufgetreten. Anfrage ungültig!")
        return redirect(urlresolvers.reverse('admin:kg_sepadebitdownload_changelist'))

    with transaction.commit_manually():
        sSepaSeqType = ''
        sFilenameSeq = ''
        sPayUntilDate = ''
        sSepaMessageId = None
        oSepaDebitDownload = None

        try:
            if download_id == 0:
                if sequence_type.lower() == 'frst':
                    qsDebits = SepaDebit.objects.new().frst()
                elif sequence_type.lower() == 'rcur':
                    qsDebits = SepaDebit.objects.new().rcur()
                else:
                    qsDebits = None
            elif download_id > 0:
                oSepaDebitDownload = SepaDebitDownload.objects.get(pk=download_id)
                sequence_type = oSepaDebitDownload.sequence_type
                sSepaMessageId = oSepaDebitDownload.message_id
                qsDebits = SepaDebit.objects.filter(download=oSepaDebitDownload)
            else:
                qsDebits = None

            # abort if there are no SEPA Debits
            if not qsDebits:
                transaction.rollback()
                messages.warning(request, u"Zu Ihrer Anfrage wurden keine Lastschriften gefunden.")
                return redirect(urlresolvers.reverse('admin:kg_sepadebitdownload_changelist'))

            if sequence_type.lower() == 'frst':
                sPayUntilDate = (date.today() + timedelta(days=int(SEPA['SEPA_DEBIT_FRST_DAYS']))).isoformat()
                sSepaSeqType = "FRST"
                sFilenameSeq = "Erst"
            elif sequence_type.lower() == 'rcur':
                sPayUntilDate = (date.today() + timedelta(days=int(SEPA['SEPA_DEBIT_RCUR_DAYS']))).isoformat()
                sSepaSeqType = "RCUR"
                sFilenameSeq = "Folge"

            oSepaConfig = SEPAConfig(SEPA['SEPA_NAME'], SEPA['SEPA_EREF_PREFIX'], SEPA['SEPA_IBAN'], SEPA['SEPA_BIC'],
                                     'false', SEPA['SEPA_CREDITOR_ID'], 'EUR', bIbanOnly=True)
            oSepaDD = SepaDD(oSepaConfig, sSepaMessageId)

            # create and save new SepaDebitDownload if it not already exists
            if not oSepaDebitDownload:
                oSepaDebitDownload = SepaDebitDownload(first_downloaded=datetime.now(),
                                                       message_id=oSepaDD.getMessageId(),
                                                       sequence_type=sSepaSeqType)
                oSepaDebitDownload.save()

            # create Payments and save reference to SepaDebitDownload if necessary
            for oSepaDebit in qsDebits:
                oPayment = Payment(oSepaDebit.bank_account_owner, oSepaDebit.bank_account_iban, oSepaDebit.bank_account_bic,
                                   str(oSepaDebit.amount), sSepaSeqType, sPayUntilDate,
                                   oSepaDebit.mandate_reference, oSepaDebit.mandate_date.isoformat(),
                                   oSepaDebit.description, oSepaDebit.endtoend_id, oSepaConfig)
                oSepaDD.addPayment(oPayment)

                # save the reference to the SepaDebitDownload if it is the first download
                if not oSepaDebit.download:
                    oSepaDebit.download = oSepaDebitDownload
                    oSepaDebit.save()

            transaction.commit()

        except Exception as e:
            transaction.rollback()
            error_message = u"Es ist ein Fehler aufgetreten. Hinweis: %s" % e.message
            messages.error(request, error_message)
            return redirect(urlresolvers.reverse('admin:kg_sepadebitdownload_changelist'))

    filename = "SEPA_%s_%s.xml" % (oSepaDebitDownload.first_downloaded.strftime("%Y-%m-%d_%H-%M") ,sFilenameSeq)
    response = HttpResponse(mimetype='application/xml')
    response['Content-Disposition'] = 'attachment; filename=' + filename
    response.write(oSepaDD.save())
    return response


@staff_member_required
@permission_required('kg.view_invoices')
def rechnung_index(request):

    error = ''

    if request.method == 'POST':

        invoice_form = InvoiceSelectionForm(request.POST)
        sepa_form = SepaSelectionForm(request.POST)
        taxcsv_form = TaxCsvSelectionForm(request.POST)


        if invoice_form.is_valid():
            facility = Facility.ex_objects.get(pk=invoice_form.cleaned_data['facility'])
            invoices = InvoiceLog.objects.filter(invoice_date__year=int(invoice_form.cleaned_data['year']), invoice_date__month=int(invoice_form.cleaned_data['month']), child__facility=facility)

            filename = "Rechnungen_" + invoice_form.cleaned_data['year'] + "_%02d_" % int(invoice_form.cleaned_data['month']) + clean_not_ascii_chars(facility.name).replace(" ", "_")

            if invoice_form.cleaned_data['paper_only']:
                invoices = invoices.filter(child__customer__paper_invoice=True)
                filename = filename + "_nur_Postversand.pdf"
            else:
                filename = filename + "_alle.pdf"

            # order invoices
            # TODO: make order changable (leibundseele should be standard)
            if OVERLOAD_SITE == 'leibundseele' or OVERLOAD_SITE == 'tommys' or OVERLOAD_SITE == 'abraxas':
                invoices = invoices.order_by('child__name', 'child__surname')
            else:
                invoices = invoices.order_by('child__facility__name', 'child__facility_subunit__name', 'child__name', 'child__surname')

            # merge all invoices
            startTime = datetime.now()
            merger = PdfFileMerger(strict=False)
            if invoices:
                for iv in invoices:
                    merger.append(INVOICE_ROOT + "/" + iv.invoice_file)

            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=' + filename
            oOutputStream = cStringIO.StringIO()
            merger.write(oOutputStream)
            response.write(oOutputStream.getvalue())
            oOutputStream.close()

            return response

        if sepa_form.is_valid():

            sSepaSeqType = ''
            sFilenameSeq = ''
            if 'FRST' in request.POST:
                sSepaSeqType = "FRST"
                sFilenameSeq = "Erst"
            elif 'RCUR' in request.POST:
                sSepaSeqType = "RCUR"
                sFilenameSeq = "Folge"

            iFacilityId = int(sepa_form.cleaned_data['sepa_facility'])
            # hack for Leibundseele: second bank account for two facilities
            if OVERLOAD_SITE == 'leibundseele' and  iFacilityId in (3,5):
                SEPA['SEPA_IBAN'] = 'DE32810520000901027367'
                SEPA['SEPA_BIC'] = 'NOLADE21HRZ'

            oSepaConfig = SEPAConfig(SEPA['SEPA_NAME'], SEPA['SEPA_EREF_PREFIX'], SEPA['SEPA_IBAN'], SEPA['SEPA_BIC'],
                                     sepa_form.cleaned_data['sepa_batch'], SEPA['SEPA_CREDITOR_ID'], 'EUR',
                                     sepa_form.cleaned_data['sepa_version'], bIbanOnly=True)
            oSepaDD = SepaDD(oSepaConfig)

            facility = Facility.ex_objects.get(pk=sepa_form.cleaned_data['sepa_facility'])
            oInvoices = (InvoiceLog.objects.filter(invoice_date__year=sepa_form.cleaned_data['sepa_year'],
                                                  invoice_date__month=sepa_form.cleaned_data['sepa_month'],
                                                  pay_type_id=PayType.DEBIT,
                                                  sepa_seq_type=sSepaSeqType,
                                                  invoice_total__gt=0,
                                                  facility=facility)
                                           .order_by('child__name', 'child__surname')
                         )

            for oInvoiceLog in oInvoices:
                oPayment = Payment(oInvoiceLog.bank_account_owner, oInvoiceLog.bank_account_iban, oInvoiceLog.bank_account_bic,
                                   str(oInvoiceLog.invoice_total), sSepaSeqType, oInvoiceLog.pay_until.isoformat(),
                                   oInvoiceLog.sepa_mandate_reference, oInvoiceLog.sepa_mandate_date.isoformat(),
                                   oInvoiceLog.sepa_description, oInvoiceLog.sepa_transaction_id, oSepaConfig)
                oSepaDD.addPayment(oPayment)

                if MODULE_ACCOUNTING:
                    # check if CustomerAccountTransaction for this debit was already created, if it was not then create it
                    if not oInvoiceLog.cat_created:
                        with transaction.commit_manually():
                            try:
                                # create new CustomerAccountTransaction
                                oNewCat = CustomerAccountTransaction(customer=oInvoiceLog.child.customer)
                                oNewCat.transaction_id = oInvoiceLog.sepa_transaction_id
                                oNewCat.transaction_date = oInvoiceLog.pay_until
                                oNewCat.amount = oInvoiceLog.invoice_total
                                oNewCat.description = 'EREF+ %s MREF+%s SVWZ+%s' % (oInvoiceLog.sepa_transaction_id, oInvoiceLog.sepa_mandate_reference, oInvoiceLog.sepa_description)
                                oNewCat.type = CustomerAccountTransaction.TYPE_DEBIT
                                oNewCat.save()

                                # connect invoice CAT and debit CAT
                                try:
                                    oInvoiceCat = CustomerAccountTransaction.objects.get(transaction_id=oInvoiceLog.invoice_number)
                                    connectCustAccTrans(oInvoiceCat, oNewCat)
                                except Exception:
                                    pass

                                oInvoiceLog.cat_created = True
                                oInvoiceLog.save()

                                transaction.commit()

                            except Exception:
                                transaction.rollback()

            filename = "SEPA_%s_%02d_%s_%s.xml" % (sepa_form.cleaned_data['sepa_year'],
                                                   int(sepa_form.cleaned_data['sepa_month']),
                                                   clean_not_ascii_chars(facility.name).replace(" ", "_"),
                                                   sFilenameSeq)
            response = HttpResponse(mimetype='application/xml')
            response['Content-Disposition'] = 'attachment; filename=' + filename
            response.write(oSepaDD.save())
            return response

        if taxcsv_form.is_valid():
            invoices = InvoiceLog.objects.select_related('child').filter(invoice_date__year=int(taxcsv_form.cleaned_data['taxcsv_year']),
                                                                         invoice_date__month=int(taxcsv_form.cleaned_data['taxcsv_month']))
            if taxcsv_form.cleaned_data['taxcsv_facility'] != 'all':
                invoices = invoices.filter(facility__id=taxcsv_form.cleaned_data['taxcsv_facility'])
                facility = Facility.objects.get(id=taxcsv_form.cleaned_data['taxcsv_facility'])
                s_facility_name = facility.name.replace(' ', '')
            else:
                s_facility_name = 'alle'

            if taxcsv_form.cleaned_data['taxcsv_paytype'] != 'all':
                invoices = invoices.filter(pay_type_id=taxcsv_form.cleaned_data['taxcsv_paytype'])
                d_helper = dict(PayType.PAYTYPE_CHOICES)
                s_paytype_name = d_helper[int(taxcsv_form.cleaned_data['taxcsv_paytype'])]
            else:
                s_paytype_name = 'alle'

            filename = "Steuerberater_%s_%02d_%s_%s.csv" % (taxcsv_form.cleaned_data['taxcsv_year'],
                                                             int(taxcsv_form.cleaned_data['taxcsv_month']),
                                                             clean_not_ascii_chars(s_facility_name).replace(" ", "_"),
                                                             clean_not_ascii_chars(s_paytype_name).replace(" ", "_"))

            file_content = render_to_string('kg/utils_tax_csv.txt', {'invoices': invoices})
            file_content = file_content.encode("latin-1", 'ignore')
            response = HttpResponse(mimetype='text/plain')
            response['Content-Disposition'] = 'attachment; filename=' + filename
            response.write(file_content)
            return response

    form_invoice = InvoiceSelectionForm()
    if MODULE_PREPAID:
        form_sepa = None
    else:
        form_sepa   = SepaSelectionForm()
    form_fibu_bookings = None
    form_fibu_debtors = None
    form_taxcsv  = TaxCsvSelectionForm()

    # DATEV
    if OVERLOAD_SITE in ['abraxas', 'beinrode', 'oschatz', 'schmalkalden']:
        form_fibu_bookings = FibuBuchForm()
        form_fibu_bookings.action = urlresolvers.reverse('reports:datev_export_buch')
        form_fibu_debtors = FibuStammForm()
        form_fibu_debtors.action = urlresolvers.reverse('reports:datev_export_stamm')
        form_taxcsv = TaxCsvSelectionForm()

    # DIAMANT3
    if OVERLOAD_SITE in ['awessen', 'rudolstadt']:
        form_fibu_bookings = FibuBuchForm()
        form_fibu_bookings.action = urlresolvers.reverse('reports:diamant3_export_buch')
        form_fibu_debtors = FibuStammForm()
        form_fibu_debtors.action = urlresolvers.reverse('reports:diamant3_export_stamm')
        form_taxcsv = TaxCsvSelectionForm()

    # ESL
    if OVERLOAD_SITE == 'esl':
        form_fibu_bookings = FibuBuchForm()
        form_fibu_bookings.action = urlresolvers.reverse('kg.views_admin.fibuexport_esl')

    months   = range(1, 13)
    years    = InvoiceLog.objects.dates('invoice_date', 'year')
    children = Child.objects.all()

    return render_to_response('kg/admin/rechnung_index.html', {'form_invoice': form_invoice,
                                                               'form_sepa': form_sepa,
                                                               'form_fibu_bookings': form_fibu_bookings,
                                                               'form_fibu_debtors': form_fibu_debtors,
                                                               'form_taxcsv': form_taxcsv,
                                                               'months': months,
                                                               'years': years,
                                                               'children': children,
                                                               'last_of_last_month': DateHelper.getLastDayOfLastMonth(date.today()),
                                                               'error': error,
                                                               'MODULE_PREPAID': MODULE_PREPAID,}
                                                            , context_instance = RequestContext(request))


@staff_member_required
@permission_required('kg.create_invoices')
def rechnung_abrechnung(request):

    # Ajax request
    if request.is_ajax():
        value = request.GET.get('value')
        action = request.GET.get('action')
        value_list = value.split('-')
        btype = value_list[0]
        year = int(value_list[1])
        month = int(value_list[2])
        bid = int(value_list[3])

        params_valid = year and month and btype and bid
        if action == 'billing' and params_valid:
            return perform_billing(year, month, btype, bid, request)
        elif action == 'release' and params_valid:
            return perform_release(year, month, btype, bid, request)

    # show last x months
    today = date.today()
    months = Order.objects.filter(
        menu__date__lt=date(today.year, today.month, 1)
        ).dates('menu__date', 'month', order='DESC')[:MODULE_INVOICE_BILLING_MONTHS]
    facilities = Facility.ex_objects.filter()

    month_list = list()

    for month in months:
        fac_list = list()
        for facility in facilities:
            # count number of children with orders
            qsChildren = Child.objects.distinct().filter(facility=facility,
                                                         order__quantity__gt=0,
                                                         order__menu__date__year=month.year,
                                                         order__menu__date__month=month.month )

            # Abraxas: do not count children with extra_attribute 'Hort' and 'GGB'
            if OVERLOAD_SITE == 'abraxas':
                qsChildren = qsChildren.exclude(extra_attribute__exact='Hort')
                qsChildren = qsChildren.exclude(extra_attribute__exact='GGB')
            # Sonnenblick: do not count customers that are interns
            if OVERLOAD_SITE == 'sonnenblick':
                qsChildren = qsChildren.exclude(customer__pay_type__id=PayType.OTHER)

            iChildrenCount = qsChildren.count()

            # count number of created invoices
            iInvoiceCount = InvoiceLog.objects.filter(
                child__facility=facility, invoice_date__year=month.year,
                invoice_date__month=month.month).count()
            # count number of released invoices
            iReleaseCount = InvoiceLog.objects.released().filter(
                child__facility=facility, invoice_date__year=month.year,
                invoice_date__month=month.month).count()

            if iInvoiceCount >= iChildrenCount:
                iBillingStatusCode = 1
                sBillingStatus = "OK"
            elif iInvoiceCount == 0:
                iBillingStatusCode = 0
                sBillingStatus = "Offen"
            else:
                iBillingStatusCode = 0
                sBillingStatus = "Unvollständig"

            if iReleaseCount >= iInvoiceCount:
                iReleaseStatusCode = 1
                sReleaseStatus = "OK"
            elif iReleaseCount == 0:
                iReleaseStatusCode = 0
                sReleaseStatus = "Offen"
            else:
                iReleaseStatusCode = 0
                sReleaseStatus = "Unvollständig"

            fac_list.append({
                'invoices': iInvoiceCount,
                'releases': iReleaseCount,
                'children': iChildrenCount,
                'facility': facility,
                'billing_status': sBillingStatus,
                'billing_status_code': iBillingStatusCode,
                'release_status': sReleaseStatus,
                'release_status_code': iReleaseStatusCode,
            })

        month_list.append({'month': month, 'fac_list': fac_list})

    children = Child.objects.all()

    return render_to_response(
        'kg/admin/rechnung_abrechnung.html',
        {
            'month_list': month_list,
            'children': children
        },
        context_instance=RequestContext(request)
    )


@staff_member_required
def rechnung_download(request):
    if request.method == 'GET':
        year = request.GET.get('year')
        month = request.GET.get('month')
        child_id = request.GET.get('child_id')
        try:
            invoice = InvoiceLog.objects.get(child__id=int(child_id), invoice_date__month=int(month), invoice_date__year=int(year))
            response = HttpResponse(file(INVOICE_ROOT + "/" + invoice.invoice_file).read())
            response['Content-Type'] = 'application/pdf'
            response['Content-Disposition'] = 'attachment; filename=' + invoice.invoice_file[8:]
            return response
        except:
            logger.exception(
                'Error when saving invoice (year: %s, month: %s, child: %s)' %
                (year, month, child_id))
            raise Http404
    raise Http404


@staff_member_required
def mailtest_index(request):

    if request.method == 'POST':

        DEBUG = True

        form = MailTestForm(request.POST)
        if form.is_valid():
            subject ="Mailtest"
            body = "Hallo,\n\nwenn Sie diese E-Mail erreicht, funktioniert der Mailversand."
            send_mail(subject, body, DEFAULT_FROM_EMAIL, [form.cleaned_data['mailaddress']])

            messages.success(request, 'Die Mail wurde erfolgreich verschickt. Bitte prüfen Sie Ihr Mailkonto.')

    else:
        form = MailTestForm()

    return render_to_response('kg/admin/mailtest_index.html',
                              {'form': form},
                              context_instance=RequestContext(request))


@staff_member_required
def statistik_index(request):
    from django.db.models import Count

    form = MonthSelectionForm(request.GET)
    if form.is_valid():
        from_date = date(int(form.cleaned_data['year']), int(form.cleaned_data['month']), 1)
        to_date = DateHelper.getLastDayOfMonth(from_date)
    else:
        to_date = DateHelper.getLastDayOfLastMonth(date.today())
        from_date = date(to_date.year, to_date.month, 1)
        form = MonthSelectionForm()

    csv_list = list()

    facilities = Facility.ex_objects.filter(is_active=True)

    qsMeals = Meal.objects.all()

    dAccumulus = {}
    for oMeal in qsMeals:
        dAccumulus[oMeal.id] = {'all': 0, 'picked_up': 0}

    for facility in facilities:
        row = []
        for oMeal in qsMeals:
            dAll = Order.objects.filter(child__facility=facility, menu__meal=oMeal, menu__date__range=(from_date, to_date)).aggregate(sum=Sum('quantity'))
            iAll = dAll['sum'] or 0
            dPickedUp = Order.objects.filter(child__facility=facility, menu__meal=oMeal, menu__date__range=(from_date, to_date), pickup_time__gt=0).aggregate(sum=Sum('quantity'))
            iPickedUp = dPickedUp['sum'] or 0;
            row.append({'mealtime' : oMeal,
                        'sum_all' : iAll,
                        'sum_picked_up' : iPickedUp})
            dAccumulus[oMeal.id]['all'] = dAccumulus[oMeal.id]['all'] + iAll
            dAccumulus[oMeal.id]['picked_up'] = dAccumulus[oMeal.id]['picked_up'] + iPickedUp
        csv_list.append({'mealtimes':row, 'facility':facility})

    # order accumulus for template use
    lAccumulus = list()
    for oMeal in qsMeals:
        lAccumulus.append(dAccumulus[oMeal.id])

    return render_to_response(
        'kg/admin/statistik_index.html',
        {
            'csv_list': csv_list,
            'date': to_date,
            'mealtimes': qsMeals,
            'form': form,
            'accumulus': lAccumulus,
            'facilities': facilities
        },
        context_instance=RequestContext(request))


@staff_member_required
def statistik_reduction(request):
    # TODO: change reduction statistics view so that the reduced orders are the start point to count from
    # all other grouping, sorting should done later based on this numbers

    form = MonthSelectionForm(request.GET)
    if form.is_valid():
        oDateFrom = date(int(form.cleaned_data['year']), int(form.cleaned_data['month']),1)
        oDateTo = DateHelper.getLastDayOfMonth(oDateFrom)
    else:
        oDateTo = DateHelper.getLastDayOfLastMonth(date.today())
        oDateFrom = date(oDateTo.year, oDateTo.month, 1)
        form = MonthSelectionForm()

    qsReductionsInMonth = Reduction.objects.distinct().filter(order__menu__date__range=(oDateFrom, oDateTo))
    lReductionData = []
    for oReduction in qsReductionsInMonth:
        qsFacilities = Facility.objects.distinct().filter(child__order__reduction=oReduction,
                                                          child__order__menu__date__range=(oDateFrom, oDateTo))
        lFacilityData = []
        for oFacility in qsFacilities:
            lMealTotalFacilityList = []
            qsMeals = Meal.objects.distinct().filter(menu__order__child__facility=oFacility,
                                                     menu__date__range=(oDateFrom, oDateTo))

            qsChildReductions = ChildReduction.objects.distinct().filter(child__facility=oFacility,
                                                                         child__order__reduction=oReduction,
                                                                         date_start__lte=oDateTo,
                                                                         date_end__gte=oDateFrom)

            lChildData = [{'child_name': 'Kind',
                           'child_cust_nbr': 'Kd.-Nr.',
                           'customer_extra_attribute': 'Debitor-Nr.',
                           'childreduction_reference_number': 'Aktenzeichen',
                           'menu_sums': [oMeal.name_short for oMeal in qsMeals] + ['Summe']}, ]
            for dChildReduction in qsChildReductions:
                iTotalPerChild = 0
                lMealTotalPerChild = []
                for oMeal in qsMeals:
                    dMealCount = (Order.objects.filter(child__id=dChildReduction.child.id,
                                                       menu__date__range=(oDateFrom, oDateTo),
                                                       reduction=oReduction,
                                                       menu__meal=oMeal)
                                               .aggregate(count=Sum('quantity')))
                    iMealCount = dMealCount['count'] or 0
                    lMealTotalPerChild.append(iMealCount)
                    iTotalPerChild += iMealCount
                lMealTotalPerChild.append(iTotalPerChild)
                if iTotalPerChild > 0:
                    lChildData.append({'child_name': '%s, %s' % (dChildReduction.child.name, dChildReduction.child.surname),
                                       'child_cust_nbr': dChildReduction.child.cust_nbr,
                                       'customer_extra_attribute': dChildReduction.child.customer.extra_attribute,
                                       'childreduction_reference_number': dChildReduction.reference_number,
                                       'menu_sums': lMealTotalPerChild})
                    lMealTotalFacilityList.append(lMealTotalPerChild)
            # add facility total
            lChildData.append({'child_name': 'Gesamt',
                               'child_cust_nbr': '',
                               'childreduction_reference_number': '',
                               'menu_sums': [ sum(x) for x in zip(*lMealTotalFacilityList) ] })
            if len(lChildData) > 1:
                lFacilityData.append({'facility_name': oFacility.name,
                                      'child_data': lChildData})
        if len(lFacilityData) > 0:
            lReductionData.append({'reduction_name': oReduction.name,
                                   'facility_data': lFacilityData})

    return render_to_response('kg/admin/statistik_reduction.html',
                              {'reduction_data': lReductionData,
                               'date': oDateFrom,
                               'form': form},
                              context_instance = RequestContext(request))


@staff_member_required
def csv_backup(request):

    from datetime import timedelta

    form = MonthSelectionForm(request.GET)

    csv_list = list()

    if form.is_valid():
        from_date = date(int(form.cleaned_data['year']), int(form.cleaned_data['month']),1)
        # in actual month
        to_date = DateHelper.getLastDayOfMonth(from_date)
        if date.today() < to_date:
            to_date = date.today()

        facilities = Facility.ex_objects.filter()

        #csv header
        csv_list.append(['Datum','Kundennummer','Kunde','Einrichtung','Klasse/Gruppe','Kind','Essen','Teilnahme'])

        #Einrichtung
        for oFacility in facilities:

            for oFacSubunit in FacilitySubunit.objects.filter(facility = oFacility):

                #Kind
                for oChild in Child.objects.filter(facility_subunit=oFacSubunit):

                    #Mahlzeiten
                    qsMeals = Meal.objects.filter(mealplan__facilitysubunit__facility=oFacility)

                    for oMeal in qsMeals:

                        #Alle Tage des Monats
                        dDate = from_date

                        while dDate <= to_date:

                            oOrder = Order.objects.filter(child=oChild, menu__meal=oMeal, menu__date=dDate)

                            if oOrder:
                                csv_list.append([dDate.strftime("%d.%m.%Y"),str(oChild.cust_nbr),oChild.customer.surname + ' ' + oChild.customer.name,oFacility.name_short,oFacSubunit.name_short,oChild.surname + ' ' + oChild.name,oMeal.name_short,'1'])

                            #next day
                            dDate = dDate + timedelta(days=1)

        filename = "Backup_" + form.cleaned_data['year'] + "_%02d" % int(form.cleaned_data['month']) + ".csv"

        response = HttpResponse(mimetype='text/plain')
        response['Content-Disposition'] = 'attachment; filename=' + filename

        writer = csv.writer(response,dialect='excel')

        #schreibe csv
        for sRow in csv_list:
            writer.writerow(sRow)

        return response

    #default to the current month
    else:
        to_date = DateHelper.getLastDayOfLastMonth(date.today())
        from_date = date(to_date.year, to_date.month, 1)
        form = MonthSelectionForm()

        return render_to_response('kg/admin/backup.html', {'csv_list' : csv_list,'form' : form }, context_instance = RequestContext(request))



@staff_member_required
def actionlog_short_list(request):
    """
    Renders template to fill action log frame on admin index site
    """

    oLogEntries = ActionLog.objects.all()[:10]
    lLogEntries = list()
    for oLogEntry in oLogEntries:
        sStyle = ''
        if oLogEntry.is_warning():
            sStyle = 'color: red;'
        dLogEntry = {'action_time':oLogEntry.action_time,
                     'change_message':oLogEntry.change_message,
                     'style':sStyle}
        lLogEntries.append(dLogEntry)
    return render_to_response('kg/admin/actionlog_short_list.html', {'logEntries' : lLogEntries }, context_instance = RequestContext(request))


@staff_member_required
def orderhistory(request, child_id, year, month):
    """
    Show menu order history for one month
    """
    oChild = Child.objects.get(pk=child_id)
    oDate = date(int(year), int(month), 1)
    oMenus = Menu.objects.filter(date__year=year, date__month=month).order_by('date', 'meal__sortKey')

    lHistory = []
    for oMenu in oMenus:
        lTempOrderHistory = get_order_history(oChild, oMenu)
        if lTempOrderHistory:
            dDay = {'menu': oMenu,
                    'order_history': get_order_history(oChild, oMenu)}
            lHistory.append(dDay)

    return render_to_response('kg/admin/orderhistory.html',
                              {'date': oDate,
                               'child': oChild,
                               'history': lHistory},
                              context_instance=RequestContext(request))


@staff_member_required
def orderhistory_popup(request, child_id, menu_id):
    """
    Renders template to fill order history popup
    """
    oChild = Child.objects.get(pk=child_id)
    oMenu = Menu.objects.get(pk=menu_id)
    lOrderHistory = get_order_history(oChild, oMenu)
    # TODO: consider using date format from localization instead
    data = {
        'title': '{} - {}'.format(oMenu.date.strftime('%d. %B %Y'), oChild),
        'content': render_to_string(
            'kg/admin/orderhistory_popover.html',
            {
                'orderHistory': lOrderHistory,
                'child': oChild,
                'menu': oMenu
            }
        )
    }
    return HttpResponse(json.dumps(data), content_type='application/json')


@staff_member_required
@permission_required('kg.add_bankaccounttransaction')
def bank_csv_upload(request):
    """
    Renders template to upload csv file and parse file if necessary
    """
    if request.method == 'POST':

        oForm = UploadFileForm(request.POST, request.FILES)

        if oForm.is_valid():

            #check for cancelation
            if 'cancel' in request.POST:
                return redirect(urlresolvers.reverse('admin:kg_bankaccounttransaction_changelist'))

            # prevent uploading files greater than about 2MB
            if request.FILES['file'].size > 2210000:
                messages.error(request, u'Fehler: Die Datei darf max. 2MB groß sein.')
                return redirect(urlresolvers.reverse('admin:kg_bankaccounttransaction_changelist'))

            if OVERLOAD_SITE == 'bodelessen':
                dResult = parse_and_save_bank_csv_upload_bodelschwinghhaus(request.FILES['file'])
            elif OVERLOAD_SITE == 'rudolstadt':
                dResult = parse_and_save_bank_xlsx_upload_rudolstadt(request.FILES['file'])
            else:
                dResult = parse_and_save_bank_csv_upload(request.FILES['file'])

            if dResult['success']:
                messages.info(request, dResult['message'])
            else:
                messages.error(request, dResult['message'])

        return redirect(urlresolvers.reverse('admin:kg_bankaccounttransaction_changelist'))

    else:
        form = UploadFileForm()

    return render_to_response('kg/admin/bank_csv_upload.html', {'form': form}, context_instance = RequestContext(request))


@staff_member_required
@permission_required('kg.change_bankaccounttransaction')
def bat_auto_matching(request):
    """
    Calls BankAccountTransaction automatic matching functions and redirects to BankAccountTransaction change_list view
    """
    iMatches = match_bank_account_transactions()
    messages.info(request, '%s Buchungen wurden automatisch zugeordnet.' % iMatches)

    return redirect(urlresolvers.reverse('admin:kg_bankaccounttransaction_changelist'))




@staff_member_required
@permission_required('kg.change_bankaccounttransaction')
def bat_manual_matching(request):
    """
    Create form for manual matching BankAccountTransactions to CustomerAccountTransactions
    Processes matching form input and redirects to BankAccountTransaction change_list view after that
    """

    if request.method == 'POST':

        if 'cancel' in request.POST:
            return redirect(urlresolvers.reverse('admin:kg_bankaccounttransaction_changelist'))

        iCounter = 0
        iMissCounter = 0
        lBatCatIds = request.POST.getlist('batCatIds')
        lBatCatIds.sort()

        if lBatCatIds:

            # prepare Input
            dPreparedInput = {}
            for sBatCatPair in lBatCatIds:
                lActualItems = sBatCatPair.split('-')
                iBatId = int(lActualItems[0])
                iCustomerId = int(lActualItems[1])
                iCatId = int(lActualItems[2])
                if iBatId > 0 and iCustomerId > 0 and iCatId >= 0:
                    if iBatId not in dPreparedInput:
                        dPreparedInput[iBatId] = {'custId': [], 'catIds': []}
                    if iCustomerId not in dPreparedInput[iBatId]['custId']:
                        dPreparedInput[iBatId]['custId'].append(iCustomerId)
                    if iCatId > 0 and iCatId not in dPreparedInput[iBatId]['catIds']:
                        dPreparedInput[iBatId]['catIds'].append(iCatId)

            # create CustomerAccountTransactions
            for iBatKey, dItem in dPreparedInput.iteritems():
                # filter out entrys with more than one Customer (wrong manual matching)
                if len(dItem['custId']) != 1:
                    iMissCounter += 1
                else:
                    try:
                        oBAT = BankAccountTransaction.objects.get(id=iBatKey)
                    except ObjectDoesNotExist:
                        iMissCounter += 1
                        continue

                    # create new CAT, ignore TYPE_DEBIT_RETURN
                    if oBAT.type is BankAccountTransaction.TYPE_DEBIT_RETURN:
                        iMissCounter += 1
                        continue
                    else:
                        oNewCustAccTrans = createCustAccTransOutOfBankAccTrans(dItem['custId'][0], oBAT)
                        iCounter += 1

                    # additional actions if not in prepaid mode
                    if not MODULE_PREPAID:
                        # retrieve sum of manual selected CustomerAccountTransactions
                        oResult = CustomerAccountTransaction.objects.filter(id__in=dItem['catIds']).aggregate(iTotal=Sum('amount'))
                        # if amount of new CAT is equal to all selected CATs, mark new CAT and selected CATs as booked
                        if oResult['iTotal'] == -oNewCustAccTrans.amount:
                            oNewCustAccTrans.setStatusBooked()
                            oNewCustAccTrans.save()
                            CustomerAccountTransaction.objects.filter(id__in=dItem['catIds']).update(status=CustomerAccountTransaction.STATUS_BOOKED,
                                                                                                     connected_to=str(oNewCustAccTrans.id))

        sMessage =  '%s Buchungen wurden manuell zugeordnet.' % iCounter
        if iMissCounter > 0:
            sMessage += ' %s Buchungen konnten nicht zugeordnet werden.' % iMissCounter
        messages.info(request, sMessage)

        return redirect(urlresolvers.reverse('admin:kg_bankaccounttransaction_changelist'))


    lIds = []
    sIds = request.GET.get('ids')
    if sIds:
        lIds = sIds.split(',')
    oBankAccTrans = BankAccountTransaction.objects.open().filter(pk__in=lIds)

    # if module prepaid show customers, show open CATs else
    if MODULE_PREPAID:
        qsCustomers = Customer.objects.all()
        qsCATs = []
    else:
        qsCustomers = []
        qsCATs = CustomerAccountTransaction.objects.select_related().open().filter(type=CustomerAccountTransaction.TYPE_INVOICE).order_by('customer__name', 'customer__surname')

    dContext = {'BankAccTrans': oBankAccTrans,
                'qsCATs': qsCATs,
                'qsCustomers': qsCustomers,
                'MODULE_PREPAID': MODULE_PREPAID}

    return render_to_response('kg/admin/bat_manual_matching.html', dContext, context_instance = RequestContext(request))


@staff_member_required
def email_export(request):
    """
    Renders template to select facility and show all email addresses of customers for that facility
    """

    oCustomersWithEmail = None
    oCustomersWithoutEmail = None
    sSeparator = ''
    oForm = EmailExportForm()

    if request.method == 'POST':

        oForm = EmailExportForm(request.POST)

        if oForm.is_valid():
            oCustomers = (Customer.objects.distinct()
                                          .filter(child__facility=oForm.cleaned_data['facility'])
                                          .order_by('name', 'surname'))

            if oForm.cleaned_data['properties'] == 'paper_yes':
                oCustomers = oCustomers.filter(paper_invoice=True)
            if oForm.cleaned_data['properties'] == 'paper_no':
                oCustomers = oCustomers.filter(paper_invoice=False)

            oCustomersWithEmail = oCustomers.exclude(email='')
            oCustomersWithoutEmail = oCustomers.filter(email='')

            sSeparator = oForm.cleaned_data['separator']
            if sSeparator == 'br':
                sSeparator = "\n"

    return render_to_response('kg/admin/email_export.html',
                              {'form': oForm,
                               'customers_with': oCustomersWithEmail,
                               'customers_without': oCustomersWithoutEmail,
                               'separator': sSeparator},
                              context_instance = RequestContext(request))


@staff_member_required
def import_pos_app_data(request):
    """
    Upload and import JSON data that was exported from the POS App
    """
    if request.method == 'POST':

        oForm = UploadFileForm(request.POST, request.FILES)

        if oForm.is_valid():

            #check for cancelation
            if 'cancel' in request.POST:
                messages.info(request, 'Aktion abgebrochen')
                return redirect(urlresolvers.reverse('kg.views_admin.import_pos_app_data'))

            if not '_Export_' in request.FILES['file'].name:
                messages.info(request, u'Bitte wählen Sie eine Datei mit _Export_ im Namen')
                return redirect(urlresolvers.reverse('kg.views_admin.import_pos_app_data'))
            else:
                try:
                    rawfile = request.FILES['file'].read()
                    data = json.loads(rawfile)
                    update_children_from_pos_data(data)
                    update_orders_from_pos_data(data)
                    messages.info(request, 'Die Daten wurden erfolgreich importiert')
                except Exception, message:
                    messages.error(request, 'Beim Importieren der Daten ist ein Fehler aufgetreten!')

        return redirect(urlresolvers.reverse('kg.views_admin.import_pos_app_data'))

    else:
        form = UploadFileForm()

    return render_to_response('kg/admin/import_pos_app_data.html', {'form': form}, context_instance=RequestContext(request))


@staff_member_required
def voucher_index(request):

    if request.method == 'POST':

        oFacilityForm = VoucherPerFacilityForm(request.POST)
        oChildForm = VoucherPerChildForm(request.POST)

        if oFacilityForm.is_valid() or oChildForm.is_valid():

            if oFacilityForm.is_valid():
                iMonth = int(oFacilityForm.cleaned_data['month'])
                iYear = int(oFacilityForm.cleaned_data['year'])
                oFacility = Facility.ex_objects.get(pk=oFacilityForm.cleaned_data['facility'])
                qsChildren = Child.objects.filter(facility=oFacility).order_by('customer__name')
                sFilenameEnding =  oFacility.name_short.replace(' ', '_').replace('.', '')

            if oChildForm.is_valid():
                iMonth = int(oChildForm.cleaned_data['month'])
                iYear = int(oChildForm.cleaned_data['year'])
                qsChildren = Child.objects.filter(pk=oChildForm.cleaned_data['child'])
                sFilenameEnding = '%s_%s' % (qsChildren[0].name, qsChildren[0].surname)
                sFilenameEnding = sFilenameEnding.replace(' ', '_')

            oAllPages = PdfFileMerger()
            for oChild in qsChildren:
                sContentHtml = get_voucher_html(request, oChild, iYear, iMonth)
                if sContentHtml != '':
                    pdf_file = generate_pdf(
                        'kg/pdf/voucher.html',
                        file_object=tempfile.TemporaryFile('w+b'),
                        context={
                            'body_content_html': sContentHtml,
                            'STATICFILES_DIR': SITE_OVERLOAD_STATICFILES_DIR
                        }
                    )
                    oAllPages.append(pdf_file)
                    pdf_file.close()

            response = HttpResponse(content_type='application/pdf')
            sFilename = 'Essenmarken_%d_%02d_%s' % (iYear, iMonth, sFilenameEnding)
            response['Content-Disposition'] = 'attachment; filename=%s.pdf' % sFilename.encode('ascii', 'ignore')
            oAllPages.write(response)
            return response

    oFacilityForm = VoucherPerFacilityForm()
    oChildForm = VoucherPerChildForm()

    months   = range(1, 13)
    years    = Menu.objects.dates('date', 'year')

    return render_to_response('kg/admin/voucher_index.html',
                              {'facility_form': oFacilityForm,
                               'child_form': oChildForm,},
                              context_instance = RequestContext(request))

@staff_member_required
def customer_login(request):
    oCustomer = Customer.objects.get(id=request.GET.get('id', None))

    # If customer login is set to inactive then show functionality for new login creation
    oCustomerUser = oCustomer.user
    if oCustomerUser:
        if not oCustomerUser.is_active:
            oCustomerUser = None

    if not oCustomerUser:
        sRandomPassword = ''
        if request.method == 'GET':
            sRandomPassword = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))

            return render_to_response('kg/admin/new_customer_login.html',
                                      {'customer': oCustomer, 'password': sRandomPassword},
                                      context_instance = RequestContext(request))
        elif request.method == 'POST':
            sUsername = request.POST.get('username', None)
            sPassword = request.POST.get('password', None)

            oUser = User.objects.filter(username=sUsername)
            if len(oUser) > 0:
                # username taken show error
                return render_to_response('kg/admin/new_customer_login.html',
                                          {'customer': oCustomer, 'password': sPassword, 'username_taken': sUsername},
                                          context_instance = RequestContext(request))
            elif len(sUsername.strip()) == 0:
                return render_to_response('kg/admin/new_customer_login.html',
                                          {'customer': oCustomer, 'password': sPassword, 'username_to_short': True},
                                          context_instance = RequestContext(request))
            else:
                # Username free save user
                oNewUser = User.objects.create_user(sUsername, None, sPassword)
                oNewUser.save()
                oCustomer.user = oNewUser
                oCustomer.save()

                sSendToEmail = request.POST.get('toemail', None)
                if sSendToEmail and sSendToEmail == '1':
                    sMailText = render_to_string('email/customer_login_and_password.html',
                                                 {'customer': oCustomer, 'password': sPassword})
                    sMailSubject = 'Ihre Zugangsdaten für %s' % SITE_CONTEXT['SITE_NAME']
                    send_mail(sMailSubject, sMailText, DEFAULT_FROM_EMAIL, [oCustomer.email])


                return render_to_response('kg/admin/customer_login_created.html',
                                          {},
                                          context_instance = RequestContext(request))

        return render_to_response('kg/admin/new_customer_login.html',
                                  {'customer': oCustomer, 'password': sRandomPassword},
                                  context_instance = RequestContext(request))
    else:
        return render_to_response('kg/admin/existing_customer_login.html',
                                  {'customer': oCustomer, 'visible': 'change_username'},
                                  context_instance = RequestContext(request))

# @permission_required('kg.add_bankaccounttransaction')
@staff_member_required
def change_customer_login(request):
    oCustomer = Customer.objects.get(id=request.GET.get('id', None))

    if oCustomer.user:
        if request.method == 'POST':
            sUsername = request.POST.get('username', None)
            oUser = User.objects.filter(username=sUsername)
            if len(oUser) > 0:
                # username taken show error
                return render_to_response('kg/admin/existing_customer_login.html',
                                          {'customer': oCustomer, 'username_taken': sUsername, 'visible': 'change_username'},
                                          context_instance = RequestContext(request))
            elif len(sUsername.strip()) == 0:
                return render_to_response('kg/admin/existing_customer_login.html',
                                          {'customer': oCustomer, 'username_to_short': True, 'visible': 'change_username'},
                                          context_instance = RequestContext(request))
            else:
                # Username free save user
                oOldUser = User.objects.get(username=oCustomer.user.username)
                oOldUser.username = sUsername
                oOldUser.save()
                # Reload customer to get latest changes
                oCustomer = Customer.objects.get(id=request.GET.get('id', None))

                return render_to_response('kg/admin/existing_customer_login.html',
                                          {'customer': oCustomer, 'visible': 'change_username', 'username_changed': True},
                                          context_instance = RequestContext(request))

@staff_member_required
def random_password(request):
    return HttpResponse(''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8)))

@staff_member_required
def change_customer_password(request):
    oCustomer = Customer.objects.get(id=request.GET.get('id', None))

    if oCustomer.user:
        if request.method == 'POST':
            sPassword = request.POST.get('password', None)

            if len(sPassword.strip()) == 0:
                return render_to_response('kg/admin/existing_customer_login.html',
                                          {'customer': oCustomer, 'password_to_short': True, 'visible': 'change_password'},
                                          context_instance = RequestContext(request))
            else:
                oOldUser = User.objects.get(username=oCustomer.user.username)
                oOldUser.set_password(sPassword)
                oOldUser.save()

                sSendToEmail = request.POST.get('toemail', None)
                if sSendToEmail and sSendToEmail == '1':
                    sMailText = render_to_string('email/changed_customer_password.html',
                                                 {'customer': oCustomer, 'password': sPassword})
                    sMailSubject = 'Ihr neues Passwort für %s ' % SITE_CONTEXT['SITE_NAME']
                    send_mail(sMailSubject, sMailText, DEFAULT_FROM_EMAIL, [oCustomer.email])

                return render_to_response('kg/admin/existing_customer_login.html',
                                          {'customer': oCustomer, 'visible': 'change_password', 'password_changed': True},
                                          context_instance = RequestContext(request))


@staff_member_required
def delete_customer_login(request):
    oCustomer = Customer.objects.get(id=request.GET.get('id', None))
    deactivate_user(oCustomer.user.username)
    return render_to_response('kg/admin/customer_login_deleted.html',
                              {'customer': oCustomer},
                              context_instance=RequestContext(request))


@staff_member_required
@permission_required('kg.create_reports')
def somic_metzgerei_index(request):

    if OVERLOAD_SITE != 'somic':
        raise Http404

    oToday = date.today()

    # totel orders per brotzeit
    lSumList = []
    iTotalSum = 0
    for oMeal in Meal.objects.filter(mealtime__id=MEALTIMES['breakfast']):
        qsOrders = Order.objects.filter(menu__date=oToday, menu__meal=oMeal)
        dOrders = qsOrders.aggregate(sum=Sum('quantity'))
        if dOrders['sum'] is not None:
            lSumList.append({'name': qsOrders[0].menu.description,
                             'total': dOrders['sum']})
            iTotalSum += dOrders['sum']

    # orders per employee per unit
    # the unit is saved as string in the Customer.extra_attribute2 field
    lUnitsList = []
    lRowList = []
    sLastUnit = ''
    iUnitTotal = 0
    iCounter = 0
    iRowCounter = 0
    qsChildren = Child.objects.all().order_by('customer__extra_attribute2', 'name', 'surname')
    for oChild in qsChildren:
        iCounter += 1
        qsOrders = (Order.objects.select_related('menu')
                                 .filter(child=oChild, menu__date=oToday, menu__meal__mealtime__id=MEALTIMES['breakfast'])
                                 .order_by('menu__meal__sortKey'))
        if qsOrders:
            iRowCounter += 1
            lRowList.append({'child': oChild, 'orders': qsOrders, 'counter': iRowCounter})
            for oOrder in qsOrders:
                iUnitTotal += oOrder.quantity

        if sLastUnit != oChild.customer.extra_attribute2 or iCounter == len(qsChildren):
            if len(lRowList) > 0:
                lUnitsList.append({'unit_name': sLastUnit,
                                   'row_list': lRowList,
                                   'unit_total': iUnitTotal})
            lRowList = []
            iUnitTotal = 0

        sLastUnit = oChild.customer.extra_attribute2

    sContentHtml = render_to_string('kg/utils_metzgerei.html',
                                    {'sum_list': lSumList,
                                     'total_sum': iTotalSum,
                                     'unit_list': lUnitsList})

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=Metzgerei_%s.pdf' % oToday.strftime('%Y-%m-%d')
    oPdf = generate_pdf(
        'kg/pdf/metzgerei.html',
        response=response,
        context={
            'list_content_html': sContentHtml,
            'list_date': oToday,
            'print_time': datetime.now(),
            'STATICFILES_DIR': SITE_OVERLOAD_STATICFILES_DIR
        })
    return oPdf


@staff_member_required
@permission_required('kg.create_reports')
def somic_fibu_index(request):
    """ Makes Excel Sheet of customers with orders sum for Brotzeit for SOMIC """

    if OVERLOAD_SITE != 'somic':
        raise Http404

    import xlsxwriter, StringIO

    oStartDate = date(2016, 6, 1)
    oEndDate = date(2016, 6, 15)

    # retrieve list of divisions
    lTempDivisions = Customer.objects.values_list('extra_attribute2').annotate(ecount=Count('extra_attribute2')).order_by('extra_attribute2')
    lDivisions = [sDivision for sDivision, iCount in lTempDivisions]

    # create workbook and cell formats
    oOutput = StringIO.StringIO()
    oWorkbook = xlsxwriter.Workbook(oOutput)
    oValueFormat = oWorkbook.add_format({'num_format': '0.00'})

    # create and fill totals worksheet
    dWorksheets = {}
    dWorksheets['totals'] = oWorkbook.add_worksheet(u'Übersicht')
    dWorksheets['totals'].write(0, 0, 'Metzgerei %s bis %s' % (oStartDate.strftime('%d.%m.%Y'), oEndDate.strftime('%d.%m.%Y')))
    dWorksheets['totals'].write(1, 0, '')
    dWorksheets['totals'].write_row(2, 0, ['Brotzeit', ] + lDivisions + ['Summe', ])

    qsMeals = Meal.objects.filter(mealtime__id=MEALTIMES['breakfast'])
    iRow = 3
    for oMeal in qsMeals:
        iMealTotal = 0
        dWorksheets['totals'].write(iRow, 0, oMeal.name)
        iCol = 1
        for sDivision in lDivisions:
            iMealsPerDivision = 0
            lMealCounter = (Order.objects.values('menu__meal')
                                         .filter(child__customer__extra_attribute2__exact=sDivision,
                                                 menu__meal=oMeal,
                                                 menu__date__gte=oStartDate,
                                                 menu__date__lte=oEndDate)
                                         .annotate(ocount=Sum('quantity')))
            if lMealCounter:
                iMealsPerDivision = lMealCounter[0]['ocount']
            dWorksheets['totals'].write(iRow, iCol, iMealsPerDivision)
            iMealTotal += iMealsPerDivision
            iCol += 1
        dWorksheets['totals'].write(iRow, iCol, iMealTotal)
        iRow += 1

    # create and fill employees per division worksheets
    for sDivision in lDivisions:
        dWorksheets['sDivision'] = oWorkbook.add_worksheet(sDivision)
        dWorksheets['sDivision'].write_row(0, 0, ['Personal-Nr.', 'Nachname', 'Vorname', 'Summe'])
        iRow = 1
        qsChildren = Child.objects.filter(customer__extra_attribute2__exact=sDivision).order_by('customer__extra_attribute')
        for oChild in qsChildren:
            oTotal = Decimal('0.00')
            qsOrders = Order.objects.filter(child=oChild,
                                            menu__meal__mealtime__id=MEALTIMES['breakfast'],
                                            menu__date__gte=oStartDate,
                                            menu__date__lte=oEndDate)
            for oOrder in qsOrders:
                oPrice = Price.objects.get(menu=oOrder.menu,
                                           price_group=oChild.facility_subunit.price_group)
                oTotal = oTotal + oOrder.quantity * oPrice.price
            if oTotal > 0:
                dWorksheets['sDivision'].write(iRow, 0, oChild.customer.extra_attribute)
                dWorksheets['sDivision'].write(iRow, 1, oChild.customer.name)
                dWorksheets['sDivision'].write(iRow, 2, oChild.customer.surname)
                dWorksheets['sDivision'].write_number(iRow, 3, oTotal, oValueFormat)
                iRow += 1

    oWorkbook.close()
    oOutput.seek(0)

    response = HttpResponse(oOutput.read(), content_type="application/octet-stream")
    response['Content-Disposition'] = 'attachment; filename=Metzgerei_%s_bis_%s.xlsx' % (oStartDate.strftime('%Y-%m-%d'), oEndDate.strftime('%Y-%m-%d'))

    return response


@staff_member_required
def query_task_result(request):
    """Queries and returns status and additional info of given task."""
    task_id = request.POST.get('task') or request.GET.get('task')
    result = AsyncResult(task_id)
    return HttpResponse(
        json.dumps([result.state, result.result]),
        content_type='application/json')


@staff_member_required
@permission_required('kg.create_reports')
def kobrow_monthly_report(request):
    """ Makes Excel Sheet of monthly orders for Kobrow """

    if OVERLOAD_SITE != 'kobrow':
        raise Http404

    import xlsxwriter, StringIO
    from xlsxwriter.utility import xl_rowcol_to_cell

    oDateFrom = date.today()
    oDateTo = date.today()
    if 'date_from' in request.POST:
        oDateFrom = datetime.strptime(request.POST['date_from'], "%d.%m.%Y")
    if 'date_to' in request.POST:
        oDateTo = datetime.strptime(request.POST['date_to'], "%d.%m.%Y")

    # create workbook and cell formats
    oOutput = StringIO.StringIO()
    oWorkbook = xlsxwriter.Workbook(oOutput)
    oBoldFormat = oWorkbook.add_format({'bold': True})

    qsMealtimes = Mealtime.objects.all()
    qsChildren = Child.objects.all()


    oWorksheet = oWorkbook.add_worksheet('%s bis %s' % (oDateFrom.strftime('%d.%m.%Y'), oDateTo.strftime('%d.%m.%Y')))
    oWorksheet.set_column(0, 1, 13)  # set width to 11 for first two columns
    oWorksheet.write_row(0, 0, ['Name', 'Vorname',] + [oMealtime.name for oMealtime in qsMealtimes], oBoldFormat)

    iRow = 1
    for oChild in qsChildren:
        oWorksheet.write(iRow, 0, oChild.name)
        oWorksheet.write(iRow, 1, oChild.surname)
        iCol = 2
        for oMealtime in qsMealtimes:
            lMealCounter = (Order.objects.values('menu__meal__mealtime')
                                         .filter(child=oChild,
                                                 menu__meal__mealtime=oMealtime,
                                                 menu__date__gte=oDateFrom,
                                                 menu__date__lte=oDateTo)
                                         .annotate(mealtime_sum=Sum('quantity')))
            iMealtimeSum = 0
            if len(lMealCounter) > 0:
                iMealtimeSum = lMealCounter[0]['mealtime_sum']
            oWorksheet.write_number(iRow, iCol, iMealtimeSum)
            iCol += 1
        iRow += 1

    #write SUM fields
    iCol = 2
    for oMealtime in qsMealtimes:
        oWorksheet.write_formula(iRow, iCol, 'SUM(%s:%s)' % (xl_rowcol_to_cell(1, iCol), xl_rowcol_to_cell(iRow-1, iCol)), oBoldFormat)
        iCol += 1

    oWorkbook.close()
    oOutput.seek(0)

    response = HttpResponse(oOutput.read(), content_type="application/octet-stream")
    response['Content-Disposition'] = 'attachment; filename=Abrechnung_%s_bis_%s.xlsx' % (oDateFrom.strftime('%Y-%m-%d'), oDateTo.strftime('%Y-%m-%d'))

    return response


@staff_member_required
def svmv_create_sepa_debits(request):
    """ Creates SEPA debits for all customers that balances the customer account """

    if OVERLOAD_SITE != 'svmv':
        raise Http404

    # TODO: same functionality is used in views_kunden.guthaben_aufladen(). Refactor code

    sStart = request.POST.get('start', None)

    if sStart == 'start':
        with transaction.commit_manually():
            try:
                iCounter = 0
                qsCustomers = Customer.objects.filter(account_balance__lt=0, pay_type=PayType.DEBIT)
                for oCustomer in qsCustomers:
                    sepa_mandate = SepaMandate.objects.get(customer=oCustomer, is_valid=True)
                    oSepaDebit = SepaDebit(customer=oCustomer,
                                           amount=oCustomer.account_balance.copy_negate(),
                                           description=u"Abschlag Essengeld %s" % datetime.now().strftime(
                                               "%d.%m.%Y"),
                                           bank_account_owner=sepa_mandate.name,
                                           bank_account_iban=sepa_mandate.iban,
                                           bank_account_bic=sepa_mandate.bic,
                                           mandate_reference=sepa_mandate.mandate_id,
                                           mandate_date=sepa_mandate.created_at,
                                           endtoend_id=SepaHelper().createEndToEndId(SEPA['SEPA_EREF_PREFIX']),
                                           sequence_type='RCUR'
                                           )
                    oSepaDebit.save()

                    oCAT = CustomerAccountTransaction(customer=oCustomer,
                                                      transaction_id="sepadebit_%d" % oSepaDebit.id,
                                                      type=CustomerAccountTransaction.TYPE_DEBIT,
                                                      status=CustomerAccountTransaction.STATUS_OPEN,
                                                      amount=oSepaDebit.amount,
                                                      description=u"Abschlag Essengeld %s" % datetime.now().strftime(
                                                          "%d.%m.%Y"),
                                                      transaction_date=oSepaDebit.submission_time)
                    oCAT.save()

                    iCounter += 1

                transaction.commit()
                messages.success(request, 'SEPA-Lastschriften erstellt: %d' % iCounter)
                return redirect('kg.views_admin.index')
            except Exception as exc:
                transaction.rollback()
                messages.error(request, 'Es ist ein Fehler aufgetreten!')
                return redirect('kg.views_admin.index')

    return redirect('kg.views_admin.index')


@staff_member_required
def fibuexport_esl(request):
    """ Creates Excel Sheet for ESL FiBu export """

    if OVERLOAD_SITE != 'esl':
        raise Http404

    import xlsxwriter, StringIO
    from xlsxwriter.utility import xl_rowcol_to_cell

    if request.method == 'POST':
        oFibuForm = FibuBuchForm(request.POST)

        if oFibuForm.is_valid():
            iFacilityId = int(oFibuForm.cleaned_data['facility'])
            iYear = int(oFibuForm.cleaned_data['year'])
            iMonth = int(oFibuForm.cleaned_data['month'])

            oDateFrom = date(iYear, iMonth, 1)
            oDateTo = DateHelper.getLastDayOfMonth(oDateFrom)

            oFacility = Facility.objects.get(pk=iFacilityId)
            sFilename = 'ELK_Schulmenueplaner_%d-%02d_%s.xlsx' % (iYear, iMonth, clean_not_ascii_chars(oFacility.name_short.replace(' ', '-').replace('"', '').replace("'", '')))

            # Umsatzsteuersatz
            if oFacility.tax_rate == oFacility.TAX19:
                sTaxRateCode = 'A1'
            elif oFacility.tax_rate == oFacility.TAX7:
                sTaxRateCode = 'A2'
            else:
                raise Exception('Steuerzeichen fuer Umsatzsteuersatz nicht konfiguriert!')

            # Kostenstelle
            if oFacility.id == 1:  # Elblandzwerge
                sKostenstelle = '80970701'
            elif oFacility.id == 2:  # Nassaumücken
                sKostenstelle = '80970702'
            elif oFacility.id == 3:  # Regenbogen
                sKostenstelle = '80970703'
            else:
                raise Exception('Kostenstelle fuer Einrichtung nicht konfiguriert!')

            # create workbook and cell formats
            oOutput = StringIO.StringIO()
            oWorkbook = xlsxwriter.Workbook(oOutput)
            oBoldFormat = oWorkbook.add_format({'bold': True})
            oDateFormat = oWorkbook.add_format({'num_format': 'dd.mm.yy'})

            oWorksheet = oWorkbook.add_worksheet('%s bis %s' % (oDateFrom.strftime('%d.%m.%Y'), oDateTo.strftime('%d.%m.%Y')))
            lRow1 = ['Informationen zum Belegkopf:',
                     '', '', '', '', '',
                     'Information zur 1. Belegposition',
                     '', '', '', '', '', '', '', '', '', '',
                     'Information zur 2. Belegposition',
                     '', '', '', '', '', '', '', '', '', '']
            lRow2 = ['0_BUKRS', '0_BUDAT', '0_BELDAT', '0_BLART', '0_XBLNR', '0_BKTEXT',
                     '1_NEWBS', '1_NEWKO', '1_WRBTR', '1_MWSKZ', '1_WMWST', '1_KOSTL',
                     '1_AUFNR', '1_ANBWA', '1_BZDAT', '1_ZUONR', '1_SGTXT',
                     '2_NEWBS', '2_NEWKO', '2_WRBTR', '2_MWSKZ', '2_WMWST', '2_KOSTL',
                     '2_AUFNR', '2_ANBWA', '2_BZDAT', '2_ZUONR', '2_SGTXT']

            oWorksheet.write_row(0, 0, lRow1)
            oWorksheet.write_row(1, 0, lRow2)

            qsInvoices = InvoiceLog.objects.filter(facility_id=iFacilityId, invoice_date__year=iYear, invoice_date__month=iMonth)

            iRowCounter = 2
            for oInvoice in qsInvoices:
                sInvoiceTotal = str(oInvoice.invoice_total).replace(',', '').replace('.', ',')

                oWorksheet.write(iRowCounter, 0, '8000')  # A
                oWorksheet.write_datetime(iRowCounter, 1, oInvoice.invoice_date, oDateFormat)  # B
                oWorksheet.write_datetime(iRowCounter, 2, oInvoice.invoice_date, oDateFormat) # C
                oWorksheet.write(iRowCounter, 3, 'DR') # D
                oWorksheet.write(iRowCounter, 4, '%s' % oInvoice.invoice_number) # E
                oWorksheet.write(iRowCounter, 5, '%s, %s' % (oInvoice.child.name, oInvoice.child.surname)) # F
                oWorksheet.write(iRowCounter, 6, '01') # G
                oWorksheet.write(iRowCounter, 7, '%s' % oInvoice.child.customer.extra_attribute) # H - debitor number
                oWorksheet.write(iRowCounter, 8, sInvoiceTotal) # I
                oWorksheet.write(iRowCounter, 9, '%s' %sTaxRateCode)  # J
                oWorksheet.write(iRowCounter, 10, '') # K
                oWorksheet.write(iRowCounter, 11, '') # L
                oWorksheet.write(iRowCounter, 12, '') # M
                oWorksheet.write(iRowCounter, 13, '') # N
                oWorksheet.write(iRowCounter, 14, '') # O
                oWorksheet.write(iRowCounter, 15, '%s' % oInvoice.invoice_number) # P
                oWorksheet.write(iRowCounter, 16, '%s' % oInvoice.child.customer.extra_attribute) # Q - debitor number
                oWorksheet.write(iRowCounter, 17, '50') # R
                oWorksheet.write(iRowCounter, 18, '442300') # S
                oWorksheet.write(iRowCounter, 19, sInvoiceTotal) # T
                oWorksheet.write(iRowCounter, 20, '%s' % sTaxRateCode)  # U
                oWorksheet.write(iRowCounter, 21, '') # V
                oWorksheet.write(iRowCounter, 22, sKostenstelle)  # W
                oWorksheet.write(iRowCounter, 23, '') # X
                oWorksheet.write(iRowCounter, 24, '') # Y
                oWorksheet.write(iRowCounter, 25, '') # Z
                oWorksheet.write(iRowCounter, 26, '%s' % oInvoice.invoice_number) # AA
                oWorksheet.write(iRowCounter, 27, '%s, %s' % (oInvoice.child.name, oInvoice.child.surname)) # AB

                iRowCounter += 1

            oWorkbook.close()
            oOutput.seek(0)

            response = HttpResponse(oOutput.read(), content_type="application/octet-stream")
            response['Content-Disposition'] = 'attachment; filename=%s' % sFilename

            return response
