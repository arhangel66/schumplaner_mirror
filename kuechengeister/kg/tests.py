# -*- coding: utf-8 -*-
import re
import time

from datetime import datetime, timedelta

from django.conf import settings
from django.utils import simplejson
from django.http import HttpRequest
from django.test import TestCase, LiveServerTestCase
from django.test.utils import override_settings
from django.core.urlresolvers import resolve
from django.contrib.admin.forms import AdminAuthenticationForm
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.models import User

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from kg_utils.menu_helper import MenuRenderer
from kg_utils.date_helper import DateHelper
from kg.models import Order, Child, Customer
from menus.models import Menu

from settings import MENU_WEEKS_HOMEPAGE
from .views_index import index, menue_print


__author__ = 'Kristaps Buliņš'


class IndexViewsTest(TestCase):
    """
    Test all views in views_index.py
    """

    def test_root_url_resolves_to_index_view(self):
        # Resolve root url
        found = resolve('/')
        # Test did resolver return our expected view
        self.assertEqual(found.func, index)

    def test_menu_print_url_resolves_to_menu_print_view(self):
        # Resolve menu print url
        found = resolve('/menueprint/')
        # Test did resolver return our expected view
        self.assertEqual(found.func, menue_print)

    def _test_index_view_return_correct_html(self):
        # Create context and populate it
        request = HttpRequest()
        oNow = MenuRenderer.getMenuStartDate()
        lMenus = []
        lWeeks = DateHelper.getWeeks(oNow, MENU_WEEKS_HOMEPAGE)
        for dWeek in lWeeks:
            menu_html, small_view_menu_html = MenuRenderer.getMenuHtml(
                dWeek['year'], dWeek['week'], request=request)

            item = {'menu_html': menu_html,
                    'small_view_menu_html': small_view_menu_html,
                    'week': dWeek['week']}
            lMenus.append(item)

        context = {'menus': lMenus, 'weeks': lWeeks,
                   'selected': oNow.isocalendar()[1]}

        # Load expected html
        expected_html = render_to_string('kg/index.html', context,
                                         context_instance=RequestContext(
                                             request))

        # Get view html
        response = self.client.get('/')

        # Encode/decode - make both html to strings
        expected_html = str(expected_html.encode('utf-8'))
        returned_html = response.content.decode('utf-8').encode('utf-8')

        # CSRF tokens don't get render_to_string, remove it from returned_html
        csrf_regex = r"<div style='display:none'><input[^>]+csrfmiddlewaretoken[^>]+></div>"
        returned_html = re.sub(csrf_regex, '', returned_html)

        # Test: is expected html equal with returned html
        self.assertEqual(expected_html, returned_html, msg='Incorrect HTML content')


class KundenTestCaseMixin(object):
    """Base class for kunden test cases."""

    fixtures = ['tests/user.json', 'tests/menus.json', 'tests/kg.json']
    username = 'kunde2'
    password = 'test123'
    view_url = ''
    overrided_settings = {}

    def test_anonymous_access(self):
        """Anonymous user doesn't have access to this page, login will be
        loaded."""
        res = self.client.get(self.view_url)
        self.assertRedirects(res, '/login/?next='+self.view_url)

    def test_customer_access(self):
        """Anonymous user doesn't have access to this page, login will be
        loaded."""
        self.login()
        with self.settings(**self.overrided_settings):
            res = self.client.get(self.view_url)
            self.assertEqual(res.status_code, 200)

    def login(self):
        self.client.login(username=self.username, password=self.password)


class KundenIndexTestCase(KundenTestCaseMixin, TestCase):
    view_url = '/kunden/'

    def test_customer_access(self):
        """Anonymous user doesn't have access to this page, login will be
        loaded."""
        self.login()
        res = self.client.get('/kunden/')
        self.assertIn(res.status_code, [301, 302])
        self.assertIn('/kunden/essen/', res.items()[2][1])


class CustomerOrderTestCase(KundenTestCaseMixin, TestCase):
    view_url = '/kunden/essen/'

    def test_customer_order_table_view(self):
        """Test when customer open /kunden/essen/ page."""
        # Adjust some menus so that it will be shown in the
        now = datetime.now()
        avail_weeks = DateHelper.getWeeks(now, settings.MENU_WEEKS_CUSTOMER)
        menu_date = avail_weeks[0]['days'][1]
        Menu.objects.filter(pk__in=[345, 346]).update(
            date=menu_date,
            order_timelimit=now + timedelta(days=10)
        )
        self.login()
        res = self.client.get("/kunden/essen/")
        self.assertEqual(res.status_code, 200)
        self.assertIn('weeks', res.context)
        self.assertEqual(len(res.context['weeks']),
                         settings.MENU_WEEKS_CUSTOMER)
        self.assertIn('menus', res.context)
        self.assertEqual(len(res.context['menus']), 3)
        self.assertEqual(set(res.context['menus'][0].keys()),
                         {'small_view_menu_html', 'menu_html', 'child'})
        self.assertIn('selected', res.context)
        self.assertEqual(res.context['selected'],
                         res.context['weeks'][0]['week'])

    def send_order_request(self, url):
        """Send ajax request to order a menu and return json response."""
        res = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(res.status_code, 200)
        try:
            json_resp = simplejson.loads(res.content)
        except:
            json_resp = {}
        self.assertIn('error', json_resp)
        return json_resp

    def test_customer_order_ajax_view(self):
        """Test when customer order a menu."""
        self.login()
        menu = Menu.objects.get(pk=283)
        # Test to order an unorderable menu.
        url = '/kunden/essen/?order=312-283&quantity=1&_=1468858234762'
        json_resp = self.send_order_request(url)
        self.assertEqual(json_resp['error'], True)
        # Adjust the menu with id 283 so that it could be ordered today.
        now = datetime.now()
        menu.date = now.date()
        menu.order_timelimit = now + timedelta(days=2)
        menu.cancel_timelimit = now + timedelta(days=3)
        menu.save()
        # Do order via ajax request
        json_resp = self.send_order_request(url)
        self.assertEqual(json_resp['error'], False)
        self.assertEqual(
            set(json_resp.keys()),
            {'error', 'quantity', 'order_possible', 'id', 'account_balance',
             'order_brief_html'})
        self.assertEqual(json_resp['quantity'], 1)
        # Check if order exists or not.
        order = Order.objects.get(child_id=312, menu_id=283)
        self.assertEqual(order.quantity, 1)
        self.assertEqual(order.order_reason, Order.REASON_USER)
        # Do cancel order
        cancel_url = '/kunden/essen/?order=312-283&quantity=0&_=1468858234762'
        json_resp = self.send_order_request(cancel_url)
        self.assertEqual(json_resp['error'], False)
        self.assertEqual(json_resp['quantity'], 0)
        self.assertFalse(
            Order.objects.filter(child_id=312, menu_id=283).exists())
        self.assertEqual(
            Order.all_objects.get(child_id=312, menu_id=283).quantity, 0
        )


class BrowserTestBase(LiveServerTestCase):

    fixtures = ['tests/user.json', 'tests/menus.json', 'tests/kg.json']
    username = 'kunde2'
    password = 'test123'

    @classmethod
    def setUpClass(cls):
        cls.selenium = webdriver.Chrome()
        super(BrowserTestBase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(BrowserTestBase, cls).tearDownClass()

    def element_by_id(self, id):
        return self.selenium.find_element_by_id(id)

    def element_by_css(self, css):
        try:
            return self.selenium.find_element_by_css_selector(css)
        except NoSuchElementException:
            pass

    def _login_customer(self):
        """Login with pre-filled credentials"""
        self.selenium.get('%s%s' % (self.live_server_url, '/login/'))
        self.element_by_id('id_username').send_keys(self.username)
        self.element_by_id('id_password').send_keys(self.password)
        self.element_by_css('.content form button[type=submit]').click()


class CustomerMenuTestCase(BrowserTestBase):

    def test_initial_state(self):
        now = datetime.now()
        avail_weeks = DateHelper.getWeeks(now, settings.MENU_WEEKS_CUSTOMER)
        menu_date = max(avail_weeks[0]['days'][4], now.date())
        Menu.objects.filter(pk=347).update(
            date=menu_date,
            order_timelimit=now+timedelta(days=10),
            cancel_timelimit=now+timedelta(days=10)
        )
        Menu.objects.filter(pk=344).update(
            date=menu_date,
            order_timelimit=now-timedelta(days=10),
            cancel_timelimit=now-timedelta(days=10)
        )
        Menu.objects.filter(pk=353).update(
            date=menu_date,
            order_timelimit=now+timedelta(days=10),
            cancel_timelimit=now+timedelta(days=10)
        )

        self._login_customer()

        # Menu only available to order
        button_minus = self.element_by_css('#mof-312-347 button[data-method=minus]')
        self.assertIn('disabled', button_minus.get_attribute('class'))
        button_plus = self.element_by_css('#mof-312-347 button[data-method=plus]')
        self.assertNotIn('disabled', button_plus.get_attribute('class'))
        input_quantity = self.element_by_css('#mof-312-347 input[name=quantity]')
        self.assertEqual(input_quantity.get_attribute('value'), u'0')

        # Menu available to both order & cancel
        self.assertEqual(self.element_by_css('#mof-449-344'), None)
        self.assertNotEqual(self.element_by_css('#mo-449-344'), None)

    def test_order(self):
        return
        """docstring for test_orderfname"""
        now = datetime.now()
        avail_weeks = DateHelper.getWeeks(now, settings.MENU_WEEKS_CUSTOMER)
        menu_date = max(avail_weeks[0]['days'][4], now.date())
        Menu.objects.filter(pk=347).update(
            date=menu_date,
            order_timelimit=now+timedelta(days=10),
            cancel_timelimit=now+timedelta(days=10)
        )

        self._login_customer()

        button_minus = self.element_by_css('#mof-312-347 button[data-method=minus]')
        button_plus = self.element_by_css('#mof-312-347 button[data-method=plus]')
        input_quantity = self.element_by_css('#mof-312-347 input[name=quantity]')

        # Test ordering, max quantity at 1
        button_plus.click()
        time.sleep(1)
        self.assertEqual(input_quantity.get_attribute('value'), u'1')
        self.assertNotIn('disabled', button_minus.get_attribute('class'))
        self.assertIn('disabled', button_plus.get_attribute('class'))

        # Test canceling
        self.selenium.implicitly_wait(1)
        button_minus.click()
        time.sleep(1)
        self.assertEqual(input_quantity.get_attribute('value'), u'0')
        self.assertIn('disabled', button_minus.get_attribute('class'))
        self.assertNotIn('disabled', button_plus.get_attribute('class'))


class CustomerInvoiceTestCase(KundenTestCaseMixin, TestCase):
    view_url = '/kunden/rechnungen/'

    # TODO: Add more specific test cases.


class CustomerChildrenTestCase(KundenTestCaseMixin, TestCase):
    view_url = '/kunden/kinder/'

    def test_customer_children_list(self):
        self.login()
        res = self.client.get(self.view_url)
        self.assertEqual(len(res.context['childs']), 3)
        self.assertEqual(set(res.context['childs'][0].keys()),
                         {'reductions', 'child'})
        self.assertEqual(len(res.context['childs'][0]['reductions']), 0)
        self.assertEqual(res.context['childs'][0]['child'],
                         Child.objects.get(pk=312))

    def test_add_new_child(self):
        self.login()
        add_url = '/kunden/kinder/add/'
        # Submit an empty form.
        res = self.client.post(add_url, {})
        self.assertIn('form', res.context)
        form = res.context['form']
        self.assertEqual(
            set(form.fields.keys()),
            {'surname', 'name', 'birthday', 'facility', 'facility_subunit'})
        self.assertEqual(
            set(form.errors.keys()),
            {'surname', 'name', 'facility_subunit', 'facility'})
        # Submit a valid form
        res = self.client.post(add_url, {'surname': 'Test',
                                         'name': 'Child',
                                         'birthday': '19.07.2006',
                                         'facility': '4',
                                         'facility_subunit': 4})
        self.assertEqual(res.status_code, 302)
        self.assertEqual(Child.objects.filter(customer_id=160).count(), 4)

    # TODO: Add more specific test cases.


class CustomerProfileTestCase(KundenTestCaseMixin, TestCase):
    view_url = '/kunden/profil/'

    def test_customer_profile(self):
        self.login()
        res = self.client.get(self.view_url)
        self.assertNotEqual(res.context['cust'], None)


class CustomerAboTestCase(KundenTestCaseMixin, TestCase):
    view_url = '/kunden/abo/'
    overrided_settings = {
        'MODULE_PREPAID': False
    }

    @override_settings(
        MODULE_PREPAID=True,
        MODULE_FLATRATE=True,
        PREPAID_SETTINGS={
            'SEPA_DEBIT': False,  # Offer SEPA debit payment to customers
            'BANK_TRANSFER': False,  # Offer bank transfer payment
            'FLATRATE': True,  # Customer can use flatrate orders
        }
    )
    def test_with_module_prepaid_enabled(self):
        """Anonymous user doesn't have access to this page, login will be
        loaded."""
        self.login()
        res = self.client.get(self.view_url)
        self.assertEqual(res.status_code, 200)

    # TODO: Add more specific test cases.


class AdminTestMixin(object):

    fixtures = ['tests/user.json', 'tests/menus.json', 'tests/kg.json']
    username = 'verwaltung'
    password = 'test123'
    url = ''

    def test_anonymous_access(self):
        """Anonymous user doesn't have access to this page, admin login will be
        loaded."""
        self.client.logout()
        res = self.client.get(self.url)
        self.assertEqual(res.status_code, 200)
        self.assertIn('form', res.context)
        self.assertIsInstance(res.context['form'], AdminAuthenticationForm)

    def setUp(self):
        super(AdminTestMixin, self).setUp()
        self.client.login(username=self.username, password=self.password)


class GeneralAdminViews(AdminTestMixin, TestCase):

    url = '/admin/kg/'

    def test_normal_loads(self):
        """Ensure at least all important admin views are not broken."""
        paths = [
            'order',
            'customer', 'child', 'childreduction', 'flatrate', 'emailexport',
            'reminder', 'bankaccounttransaction',
            'rechnung', 'rechnung/abrechnung',
            'statistik', 'statistik/reduction',
            'backup', 'mailtest'
            ]
        for path in paths:
            full_path = '%s%s/' % (self.url, path)
            print 'Probe view: ', full_path
            res = self.client.get(full_path)
            self.assertEqual(res.status_code, 200)


class CustomerAdminTestCase(TestCase):

    fixtures = ['tests/user.json', 'tests/menus.json', 'tests/kg.json']

    def test_delete_customer(self):
        """Check if the user account will be deactivated. """
        customer = Customer.objects.get(pk=160)
        user = customer.user
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.username, 'kunde2')
        customer.delete()
        self.assertEqual(Customer.objects.filter(pk=160).exists(), False)
        user = User.objects.get(pk=user.pk)
        self.assertEqual(user.is_active, False)
        self.assertIn('kunde2_old', user.username)
