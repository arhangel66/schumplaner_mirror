# -*- coding: utf-8 -*-
from decimal import Decimal

from django.db.models.signals import (
    pre_save, post_save, post_delete, pre_delete
    )
from django.dispatch import receiver
from django.conf import settings
from django.db.models import ObjectDoesNotExist
from django.db.models import Max, Sum, Count
from django.utils.log import getLogger

from kg_utils.admin_helper import deactivate_user

from kg.models import (
    Customer, CustomerAccountTransaction, BankAccountTransaction,
    SepaMandate, Order, OrderHistory, Child, Facility, FacilitySubunit
    )

logger = getLogger(__name__)


@receiver(post_save, sender=Customer)
def __setExtraAttribute(sender, **kwargs):
    """ Set customer extra attribute for certain instances """
    if kwargs['created']:
        if settings.OVERLOAD_SITE == 'awessen':
            oCustomer = kwargs['instance']
            oCustomer.extra_attribute = str(400000 + oCustomer.id)
            oCustomer.save()

        if settings.OVERLOAD_SITE == 'beinrode':
            oCustomer = kwargs['instance']
            oCustomer.extra_attribute = str(13000 + oCustomer.id)
            oCustomer.save()

        if settings.OVERLOAD_SITE == 'rudolstadt':
            oCustomer = kwargs['instance']
            oCustomer.extra_attribute = str(180000 + oCustomer.id)
            oCustomer.save()

        if settings.OVERLOAD_SITE == 'schmalkalden':
            oCustomer = kwargs['instance']
            oCustomer.extra_attribute = str(30000 + oCustomer.id)
            oCustomer.save()

        if settings.OVERLOAD_SITE == 'muehlenmenue':
            oCustomer = kwargs['instance']
            oCustomer.extra_attribute = str(20000 + oCustomer.id)
            oCustomer.save()

@receiver(post_save, sender=CustomerAccountTransaction)
def __trackCustomerAccountTransactionChange(sender, **kwargs):
    """
    Track CustomerAccountTransaction changes
    """
    # update Customer account balance
    oCustomer = Customer.objects.get(pk=kwargs['instance'].customer.id)
    new_balance = CustomerAccountTransaction.objects.filter(customer__id=oCustomer.id).aggregate(Sum('amount'))
    oCustomer.account_balance = new_balance['amount__sum']
    oCustomer.save()


@receiver(post_delete, sender=CustomerAccountTransaction)
def __trackCustomerAccountTransactionDeletion(sender, **kwargs):
    """ Track CustomerAccountTransaction deletion """
    # check customer: if it not exists the cutomer was deleted -> no action necessary
    try:
        oCustomer = Customer.objects.get(pk=kwargs['instance'].customer.id)
    except ObjectDoesNotExist:
        return
    except Exception:
        raise

    # set status of connected BankAccountTransaction to open
    if kwargs['instance'].transaction_id:
        (BankAccountTransaction.objects.booked()
                                       .filter(t_hash=kwargs['instance'].bat_hash)
                                       .update(status=BankAccountTransaction.STATUS_OPEN)
        )

    # set status of connected CustomerAccountTransactions to open
    (CustomerAccountTransaction.objects.filter(customer=oCustomer,
                                               connected_to=str(kwargs['instance'].id))
                                       .update(status=CustomerAccountTransaction.STATUS_OPEN)
    )

    # delete all CATs from the customer that have the same origin as the deleted CAT
    # example 1: on deletion of a debit return CAT also delete the corresponding debit return fee CAT
    # example 2: on deletion of a part of a splitted bankaccount transaction delete the other parts with it
    # TODO: rethink + rewrite
    '''
    if kwargs['instance'].transaction_id:
        for delCAT in (CustomerAccountTransaction.objects.filter(customer=kwargs['instance'].customer,
                                                                 transaction_id=kwargs['instance'].transaction_id)):
            delCAT.delete()
    '''

    # update Customer account balance
    new_balance = CustomerAccountTransaction.objects.filter(customer__id=oCustomer.id).aggregate(Sum('amount'))
    if new_balance['amount__sum'] is not None:
        oCustomer.account_balance = new_balance['amount__sum']
    else:
        oCustomer.account_balance = Decimal('0.00')
    oCustomer.save()


@receiver(post_save, sender=Customer)
def __updateChildOnCustomerChanges(sender, **kwargs):
    """ If CANTEEN_MODE: make sure customer changes are mirrored to the child """

    if getattr(settings, 'CANTEEN_MODE', False) == True:
        oChild, created = Child.objects.get_or_create(customer=kwargs['instance'])
        oChild.name=kwargs['instance'].name
        oChild.surname=kwargs['instance'].surname
        if created:
            oFacility = Facility.objects.all()[0]
            oChild.facility = oFacility
            oChild.facility_subunit = FacilitySubunit.objects.filter(facility=oFacility)[0]

        oChild.save()


@receiver(pre_save, sender=Child)
def __createNewCustomerNumber(sender, **kwargs):
    if kwargs['instance'].pk is None and kwargs['instance'].cust_nbr is None:
        last_child = Child.objects.all().aggregate(max_nbr=Max('cust_nbr'))
        if last_child['max_nbr']:
            new_cust_nbr = str(int(last_child['max_nbr']) + 1)
        else:
            new_cust_nbr = '10001'
        kwargs['instance'].cust_nbr = new_cust_nbr
    return None


@receiver(pre_save, sender=SepaMandate)
def __createNewMandateOnIbanChange(sender, **kwargs):
    """ If IBAN on existing sepa mandate was changed, it creates a new
    sepa_mandate instead if updating it """
    if kwargs['instance'].pk is not None:
        try:
            old_mandate = SepaMandate.objects.get(pk=kwargs['instance'].pk)
            if old_mandate.iban != kwargs['instance'].iban:
                kwargs['instance'].pk = None
                kwargs['instance'].mandate_id = ''
        except SepaMandate.DoesNotExist:
            # If there is no Sepa Mandate with same ID, just skip the function
            pass


@receiver(post_save, sender=SepaMandate)
def __createNewMandateId(sender, **kwargs):
    if kwargs['created']:
        oSepaMandate = kwargs['instance']
        # create only new if no input by user
        if oSepaMandate.mandate_id == 'MA' or oSepaMandate.mandate_id == '':
            if settings.OVERLOAD_SITE == 'awessen':
                qsMandates = SepaMandate.objects.filter(customer=oSepaMandate.customer)  # check for other mandates
                oSepaMandate.mandate_id = 'AW-B00020%s%02d' % (str(400000 + oSepaMandate.customer.id), len(qsMandates) - 1)
            else:
                oSepaMandate.mandate_id = 'MA' + str(30000 + oSepaMandate.id)

            oSepaMandate.save()


@receiver(post_save, sender=SepaMandate)
def __invalidateOtherSepaMandates(sender, **kwargs):
    oActualMandate = kwargs['instance']
    if oActualMandate.is_valid:
        SepaMandate.objects.filter(customer=oActualMandate.customer).exclude(id=oActualMandate.id).update(is_valid=False)


@receiver(pre_save, sender=Order)
def __saveOrderHistory(sender, instance, **kwargs):
    try:
        oOldOrder = Order.all_objects.get(pk=instance.pk)
        # only save if quantity changed
        if instance.quantity != oOldOrder.quantity:
            oOrderHistory = OrderHistory(
                child=oOldOrder.child,
                menu=oOldOrder.menu,
                quantity=oOldOrder.quantity,
                modified_by=oOldOrder.modified_by,
                last_modified=oOldOrder.order_time,
                reason=oOldOrder.order_reason,
            )
            oOrderHistory.save()
    except Order.DoesNotExist:
        pass
    except Exception as exc:
        logger.exception(exc)


@receiver(pre_delete, sender=Customer)
def __deactivateUser(sender, **kwargs):
    customer = kwargs['instance']
    if customer.user is not None:
        deactivate_user(customer.user.username)
