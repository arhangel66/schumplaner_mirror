# -*- coding: utf-8 -*-
from datetime import datetime, date, timedelta

from django.contrib.admin import SimpleListFilter, FieldListFilter
from django.db.models import Q

from settings import OVERLOAD_SITE

from kg.models import Facility, FacilitySubunit


class AccountBalanceListFilter(SimpleListFilter):
    title = 'Saldo'
    parameter_name = 'bal'

    def lookups(self, request, model_admin):
        return (
            ('neg', 'Negativ'),
            ('bal', 'Ausgeglichen'),
            ('pos', 'Positiv')
        )

    def queryset(self, request, queryset):
        if self.value() == 'neg':
            return queryset.filter(account_balance__lt=0)
        if self.value() == 'bal':
            return queryset.filter(account_balance=0)
        if self.value() == 'pos':
            return queryset.filter(account_balance__gt=0)


class ChildHasChipListFilter(SimpleListFilter):
    title = 'Chip zugeordnet'
    parameter_name = 'chip'

    def lookups(self, request, model_admin):
        return (
            ('1', 'Ja'),
            ('0', 'Nein'),
        )

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.exclude(rfid_tag='')
        if self.value() == '0':
            return queryset.filter(rfid_tag='')


class ChildFacilityFilter(SimpleListFilter):
    title = 'Einrichtung'
    parameter_name = 'facility'

    def lookups(self, request, model_admin):
        return Facility.objects.filter(is_active=True).values_list(
            'id', 'name')

    def queryset(self, request, queryset):
        if self.value():
            queryset = queryset.filter(facility=self.value())
        return queryset


class ChildSubunitFilter(SimpleListFilter):
    """ Custom filter which depends on the selected facility """
    title = 'Klasse/Gruppe'
    parameter_name = 'subunit'

    def lookups(self, request, model_admin):
        if 'facility' in request.GET:
            qs_subunits = FacilitySubunit.objects.filter(
                facility_id=request.GET['facility']).order_by('sortKey')
            return qs_subunits.values_list('id', 'name')
        return None

    def queryset(self, request, queryset):
        if self.value():
            # Reset selected subunit when switching facility
            if not FacilitySubunit.objects.filter(
                    id=self.value(),
                    facility_id=request.GET.get('facility')).exists():
                self.used_parameters[self.parameter_name] = None
            else:
                queryset = queryset.filter(facility_subunit=self.value())
        return queryset


class ReminderDownloadedListFilter(SimpleListFilter):
    title = 'Heruntergeladen'
    parameter_name = 'dl'

    def lookups(self, request, model_admin):
        return (
            ('1', 'Ja'),
            ('0', 'Nein'),
        )

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.filter(first_downloaded__isnull=False)
        if self.value() == '0':
            return queryset.filter(first_downloaded__isnull=True)


def CustomTitledFieldListFilter(title):
    """ Wrapper around FieldListFilter for using it with a custom title
    Usage example: list_filter = (('extra_attribute2', CustomTitledFieldListFilter('Firma')), 'is_suspended')
    """
    class Wrapper(FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return Wrapper


class ValidReductionFilter(SimpleListFilter):
    title = u'Noch gültig'
    parameter_name = 'valid'

    def lookups(self, request, model_admin):
        return (
            ('1', u'Ja'),
            ('0', u'Nein'),
        )

    def queryset(self, request, queryset):
        today = datetime.today()
        if self.value() == '1':
            return queryset.filter(date_end__gte=today)
        if self.value() == '0':
            return queryset.filter(date_end__lt=today)


class LastMonthValidReductionFilter(SimpleListFilter):
    """ Filter children with at least one valid day in the last month.
    """
    title = u'Letzten Monat gültig'
    parameter_name = 'valid_lm'

    def lookups(self, request, model_admin):
        return (
            ('1', u'Ja'),
            ('0', u'Nein'),
        )

    def queryset(self, request, queryset):
        # Get last month first and last days
        today = datetime.today()
        lm_end = today - timedelta(days=today.day)
        lm_start = date(lm_end.year, lm_end.month, 1)

        condition = \
            Q(date_start__gte=lm_start) & Q(date_start__lte=lm_end) | \
            Q(date_end__gte=lm_start) & Q(date_end__lte=lm_end) | \
            Q(date_start__lt=lm_start) & Q(date_end__gt=lm_end)
        if self.value() == '1':
            return queryset.filter(condition)
        if self.value() == '0':
            return queryset.exclude(condition)
