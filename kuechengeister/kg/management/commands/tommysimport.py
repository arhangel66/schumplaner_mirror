# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand
from django.db import transaction

from kg_utils import ucsv as csv
from kg.models import User, Customer, Child, Facility, FacilitySubunit, SepaMandate, PayType



class Command(BaseCommand):
    help = "Datenimport für Tommys Partyservice:\n"

    def handle(self, *args, **options):

        with open('Import-X.csv', 'rb') as csvfile:
            oReader = csv.DictReader(csvfile, delimiter=';')

            facility = Facility.objects.get(pk=8)
            cust_nbr_prefix = 80000

            facility_subunit = FacilitySubunit.objects.filter(facility=facility)[0]
            paytype_debit = PayType.objects.get(pk=PayType.DEBIT)
            paytype_invoice = PayType.objects.get(pk=PayType.INVOICE)

            iRowCounter = 0
            iUserCounter = 0
            iCustomerCounter = 0
            iOnlineInvoiceCounter = 0
            iSepaCounter = 0
            iChildrenCounter = 0
            for dRow in oReader:

                with transaction.commit_manually():
                    try:
                        iRowCounter += 1

                        # prüfen, ob leere Zeile
                        if dRow['Anrede'].strip() == '':
                            self.stdout.write("Leere Zeile: %s\n" % dRow['Kundennummer'])
                            continue

                        # customer number festlegen
                        cust_nbr = cust_nbr_prefix + int(dRow['Kundennummer'][2:5])
                        self.stdout.write("%d\n" % cust_nbr)

                        # einen Customer anlegen, falls noch nicht existiert
                        customer, created = Customer.objects.get_or_create(child__cust_nbr=cust_nbr)

                        customer.title = dRow['Anrede'].strip()
                        customer.name = dRow['Name Eltern'].strip()
                        customer.surname = dRow['Vorname Eltern'].strip()
                        customer.email = ''
                        customer.street = dRow[u'Straße'].strip()
                        if len(str(dRow['Postleitzahl']).strip()) < 5:
                            dRow['Postleitzahl'] = "0%s" % dRow['Postleitzahl']
                        customer.zip_code = dRow['Postleitzahl']
                        customer.city = dRow['Ort']
                        customer.phone = dRow['Telefon']
                        if dRow['Bankeinzug'].strip() == 'ja':
                            customer.pay_type = paytype_debit
                        else:
                            customer.pay_type = paytype_invoice
                        customer.save()
                        iCustomerCounter += 1

                        # SEPA Mandat eintragen -> nur wenn Bankdaten vorliegen
                        if str(dRow['IBAN']).strip() != '' and str(dRow['BIC']).strip() != '':
                            sepa_mandate = SepaMandate(customer=customer)
                            sepa_mandate.name = "%s %s" % (customer.surname, customer.name)
                            sepa_mandate.street = customer.street
                            sepa_mandate.city = "%s %s" % (customer.zip_code, customer.city)
                            sepa_mandate.mandate_id = ''
                            if 'DE' in str(dRow['IBAN']):
                                sepa_mandate.iban = str(dRow['IBAN']).replace(' ', '')
                                sepa_mandate.bic = str(dRow['BIC']).replace(' ', '')
                            else:
                                sepa_mandate.iban = str(dRow['BIC']).replace(' ', '')
                                sepa_mandate.bic = str(dRow['IBAN']).replace(' ', '')

                            if customer.pay_type == paytype_debit:
                                sepa_mandate.is_valid = True
                                sepa_mandate.on_paper = True
                            else:
                                sepa_mandate.is_valid = False
                                sepa_mandate.on_paper = False
                            sepa_mandate.last_debit_date = date(2016,1,1)
                            sepa_mandate.created_at = date(2016,1,1)
                            sepa_mandate.last_debit_sequence = 'RCUR'
                            sepa_mandate.save()
                            iSepaCounter += 1

                        # Kind anlegen und in richtige Einrichtung/Klasse speichern
                        child, created = Child.objects.get_or_create(customer=customer,
                                                                     cust_nbr=cust_nbr,
                                                                     facility=facility,
                                                                     facility_subunit = facility_subunit,
                                                                     name = dRow['Name Kind'],
                                                                     surname = dRow['Vorname Kind'])
                        iChildrenCounter += 1

                    except Exception as exc:
                        transaction.rollback()
                        import os, sys
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(dRow['Kundennummer'], exc.message, exc_type, fname, exc_tb.tb_lineno)
                        #self.stdout.write("Exception: %s\n" % e)
                    else:
                        transaction.commit()

        self.stdout.write("Anzahl Zeilen: %d\n" % iRowCounter)
        self.stdout.write("Angelegte Customer: %d\n" % iCustomerCounter)
        self.stdout.write("Angelegte SepaMandate: %d\n" % iSepaCounter)
        self.stdout.write("Angelegte Children: %d\n" % iChildrenCounter)