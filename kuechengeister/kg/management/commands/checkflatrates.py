from django.core.management.base import BaseCommand, CommandError
from django.db.models import F
from kg.models import Flatrate, FacilityType, WEEKDAYS


class Command(BaseCommand):
    help = ("Check flatrate problem in holidays for kindergarten!\n"
            "In kindergarten the active and the active_on_holidays fields should have the same value.\n")

    def handle(self, *args, **options):
        self.stdout.write("%s\n" % self.help)

        self.stdout.write("Kindergarten: active - active_on_holidays - Kind (Id des Kindes)"
                          "- Id der Flatrate - Mahlzeit, Wochentag\n")

        try:
            oFlatrates = (Flatrate.objects.filter(child__facility__type=FacilityType.KINDERGARTEN)
                                          .exclude(active__exact=F('active_on_holidays'))
                                          .order_by('child__facility__id', 'child__name', 'child__surname',
                                                    'mealtime__sortKey', 'weekday'))
            lChildren = []
            iCounter = 0
            iTmpChildId = -1
            for oFlat in oFlatrates:
                self.stdout.write("%s: %s - %s - %s (%d) - %s, %s\n" %
                                  (oFlat.child.facility, oFlat.active, oFlat.active_on_holidays, oFlat.child,
                                   oFlat.child.id, oFlat.meal, oFlat.weekday))
                if iTmpChildId != oFlat.child.id:
                    lChildren.append(oFlat.child)
                    iCounter += 1
                iTmpChildId = oFlat.child.id

            self.stdout.write("\nBetroffene Kinder:\n")
            for oChild in lChildren:
                self.stdout.write("%s\n" % oChild)

            self.stdout.write("\nAnzahl betroffene Kinder: %d\n\n" % iCounter)

        except Exception as e:
            raise CommandError(str(e))



