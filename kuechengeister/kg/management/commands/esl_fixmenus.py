from datetime import date

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Sum

from kg.models import Order, OrderHistory
from menus.models import Menu


class Command(BaseCommand):
    help = "Checks for menus that are twice in the table for same mealtime and mealplan and fixes that.\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            iCounter1 = 0
            iCounter2 = 0
            iCounter3 = 0

            try:
                lDates = Menu.objects.filter(date__gte=(date(2017, 10, 9))).dates('date', 'day')
                for oDate in lDates:
                    for iMealId in [1, 2, 3]:
                        qsMenus = Menu.objects.filter(date=oDate, meal__id=iMealId).order_by('date', 'meal', 'id')
                        if len(qsMenus) > 1:
                            dTempOrders = Order.objects.filter(menu=qsMenus[0]).aggregate(Sum('quantity'))
                            print 'M0 (%d): %d - %s' % (dTempOrders['quantity__sum'], qsMenus[0].id, qsMenus[0])
                            dTempOrders = Order.objects.filter(menu=qsMenus[1]).aggregate(Sum('quantity'))
                            print 'M1 (%d): %d - %s' % (dTempOrders['quantity__sum'], qsMenus[1].id, qsMenus[1])
                            if len(qsMenus) > 2:
                                print "Mehr als 2 identische Menus\n"
                                continue
                            if qsMenus[0].mealplan_id == qsMenus[1].mealplan_id:
                                for oOrderM0 in Order.all_objects.filter(menu=qsMenus[0]):
                                    # alle Eintraege sollen zu M0 werden
                                    oChild = oOrderM0.child
                                    bDebug = False
                                    #if oChild.id in [58, 334] :
                                    #    bDebug = True
                                    qsOrdersM1 = Order.all_objects.filter(menu=qsMenus[1],
                                                                          child=oChild)
                                    if len(qsOrdersM1) > 0:
                                        if qsOrdersM1[0].order_time <= oOrderM0.order_time:
                                            # loesche M1, M0 wird gueltig
                                            if bDebug:
                                                print 'M0 (%d) aktueller als M1 (%d)' % (oOrderM0.quantity, qsOrdersM1[0].quantity)
                                                print 'Loesche M1 child_id - menu_id: %d - %d:' % (oChild.id, qsMenus[1].id)
                                            (OrderHistory.objects.filter(child=oChild, menu=qsMenus[1])
                                                                 .delete())
                                            (Order.all_objects.filter(child=oChild, menu=qsMenus[1])
                                                              .delete())
                                        else:
                                            # loesche M0, setze M1 als neues M0
                                            if bDebug:
                                                print 'M1 (%d) aktueller als M0 (%d)'  % (qsOrdersM1[0].quantity, oOrderM0.quantity)
                                                print 'Loesche M0 child_id - menu_id: %d - %d:' % (oChild.id, qsMenus[0].id)
                                            (OrderHistory.objects.filter(child=oChild,
                                                                         menu=qsMenus[0])
                                                                 .delete())
                                            (Order.all_objects.filter(child=oChild, menu=qsMenus[0])
                                                              .delete())
                                            if bDebug:
                                                print 'Mache M1 zu M0: %d zu %d' % (qsMenus[1].id, qsMenus[0].id)
                                            (OrderHistory.objects.filter(child=oChild,
                                                                         menu=qsMenus[1])
                                                                  .update(menu=qsMenus[0]))
                                            (Order.all_objects.filter(child=oChild,
                                                                  menu=qsMenus[1])
                                                          .update(menu=qsMenus[0]))

                                qsMenus[1].delete()

                            dTempOrders = Order.objects.filter(menu=qsMenus[0]).aggregate(Sum('quantity'))
                            print 'Korrigiert: %d' % (dTempOrders['quantity__sum'],)

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)
