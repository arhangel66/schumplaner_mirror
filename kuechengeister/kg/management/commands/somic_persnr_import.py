# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand
from django.db import transaction

from kg_utils import ucsv as csv
from kg.models import User, Customer, Child, Facility, FacilitySubunit, SepaMandate, PayType



class Command(BaseCommand):
    help = "Import der Personalnummer der SOMIC Mitarbeiter:\n"

    def handle(self, *args, **options):

        with open('SOMIC_Personalnummern_Firmen.csv', 'rb') as csvfile:
            oReader = csv.DictReader(csvfile, delimiter=';')


            iRowCounter = 0
            iCustomerCounter = 0
            for dRow in oReader:

                with transaction.commit_manually():
                    try:
                        iRowCounter += 1

                        lCustomerName = dRow['Name'].split(' ')
                        qsCustomers = Customer.objects.filter(name=lCustomerName[0], surname__istartswith=lCustomerName[1])
                        if len(qsCustomers) == 0:
                            self.stdout.write("0 Treffer: %s\n" % dRow['Name'])
                        elif len(qsCustomers) == 1:
                            oCustomer = qsCustomers[0]
                            oCustomer.extra_attribute = dRow['Personal-Nr.']
                            oCustomer.note = dRow['Firma']
                            oCustomer.save()
                            iCustomerCounter += 1
                        else:
                            self.stdout.write("Mehrere Treffer:  %s\n" % dRow['Name'])


                    except Exception as exc:
                        transaction.rollback()
                        import os, sys
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        self.stdout.write("Exception: %s\n" % exc)
                    else:
                        transaction.commit()

        self.stdout.write("Anzahl Zeilen: %d\n" % iRowCounter)
        self.stdout.write("Eingetragene Personalnummern: %d\n" % iCustomerCounter)