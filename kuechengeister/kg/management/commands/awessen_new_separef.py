# -*- coding: utf-8 -*-

from __future__ import with_statement
from smtplib import SMTPException

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.core.mail import send_mail

from kg.models import SepaMandate


class Command(BaseCommand):
    help = "Change all SEPA mandate reference numbers to new ones\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            try:
                for oSepaMandate in SepaMandate.objects.all():
                    oSepaMandate.mandate_id = 'AW-B00020%s00' % str(400000 + oSepaMandate.customer.id)
                    oSepaMandate.save()

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)
