from datetime import date
from decimal import Decimal

from django.core.management.base import BaseCommand, CommandError

from kg.models import ChildReduction, Order


class Command(BaseCommand):
    help = "Berichtigt Ermaessigungen fuer alle Bestellungen im Monat 08/2015 ohne Rechnungen neu zu generieren.\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            iCounter = 0

            try:
                start_date = date(2015, 8, 1)
                end_date = date(2015, 8, 31)

                qsChildReductions = ChildReduction.objects.filter(date_start__lte=start_date, date_end__gte=end_date)
                for oChildReduction in qsChildReductions:
                    qsOrders = Order.objects.filter(menu__date__range=(start_date, end_date),
                                                    child__id=oChildReduction.child_id)
                    iOrderCounter = 0
                    for oOrder in qsOrders:
                        if oOrder.price != Decimal('1.00') or oOrder.reduction_id != oChildReduction.reduction_id:
                            oOrder.price = Decimal('1.00')
                            oOrder.reduction_id = oChildReduction.reduction_id
                            oOrder.save()
                            iOrderCounter += 1
                            iCounter += 1



                    if iOrderCounter > 0:
                        self.stdout.write("%s: %d\n" % (oChildReduction, iOrderCounter))

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)

            self.stdout.write("Geaenderte Bestellungen: %d\n" % iCounter)


