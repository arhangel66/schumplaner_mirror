# -*- coding: utf-8 -*-

from __future__ import with_statement
from smtplib import SMTPException

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.core.mail import send_mail

from kg.models import Customer, User, PayType, SepaMandate


class Command(BaseCommand):
    help = "Find customers with old Bankleitzahl\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            try:
                for oSepaMandate in SepaMandate.objects.filter(is_valid=True).order_by('customer__name', 'customer__surname'):
                    if oSepaMandate.iban == '' or 'DE' not in oSepaMandate.iban:
                        self.stdout.write('%s, %s\n' % (oSepaMandate.customer.name, oSepaMandate.customer.surname))

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)
