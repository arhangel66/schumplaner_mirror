# -*- coding: utf-8 -*-

import traceback
from datetime import datetime, date, timedelta
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from menus.models import Meal, Mealtime, Menu
from kg_utils.date_helper import DateHelper


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--noinput',
                    action='store_true',
                    dest='noinput',
                    default=False,
                    help='Do NOT prompt the user for input of any kind.'),
    )

    help = ("Create demo meals for the actual week and the next two weeks if not already created.\n"
            "Do NOT use on production Server!\n\n")

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)

        if options['noinput'] is True:
            choice = "YES"
        else:
            choice = None

        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            self.stdout.write("Create demo meals...\n")

            try:
                # TODO: write new to work with new menus app

                self.stdout.write("Created menus.\n")

            except Exception:
                traceback.print_exc()
                raise CommandError("Abortion through Exception\n")
