from datetime import date

from django.core.management.base import BaseCommand, CommandError
from django.utils import translation


from kg.models import InvoiceLog
from kg_utils.invoice_generator import InvoiceGenerator


class Command(BaseCommand):
    help = "Erstellt Rechnungen neu.\n"


    def handle(self, *args, **options):

        invoices = InvoiceLog.objects.filter(invoice_date=date(2014, 12, 31), facility__id__in=(9, 10, 11))
        self.stdout.write("Anzahl neu zu erstellender Rechnungen: %d\n" % len(invoices))

        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            translation.activate('de')

            for invoice in invoices:
                oInvoice = InvoiceGenerator(invoice.child, 2014, 12, None)
                oInvoice.save_invoice()

            translation.deactivate()


