from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from kg.models import Customer, PayType, SepaMandate
from kg_utils.validators import IbanValidator, BicValidator


class Command(BaseCommand):
    help = "Checks all customers wiht paytype debit and prints all with invalid IBAN.\n"

    def handle(self, *args, **options):
        self.stdout.write("Customers with paytype debit and invalid IBAN:\n")

        customers = Customer.objects.filter(pay_type__id=PayType.DEBIT)
        for cust in customers:
            try:
                sepa = SepaMandate.objects.get(customer=cust, is_valid=True)
            except ObjectDoesNotExist:
                print "%s, %s, %s: kein gueltiges SEPA Mandat" % (cust.id, cust.name, cust.surname)
            except MultipleObjectsReturned:
                print "%s, %s, %s: mehr als ein gueltiges SEPA Mandat" % (cust.id, cust.name, cust.surname)

            try:
                IbanValidator(sepa.iban)
            except:
                print "%s, %s, %s: IBAN fehlerhaft" % (cust.id, cust.name, cust.surname)

            try:
                BicValidator(sepa.bic)
            except:
                print "%s, %s, %s: BIC fehlerhaft" % (cust.id, cust.name, cust.surname)



