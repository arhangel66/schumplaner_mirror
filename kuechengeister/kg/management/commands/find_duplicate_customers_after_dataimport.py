from django.core.management.base import BaseCommand, CommandError
from django.db.models import Max
from kg.models import Customer, Child, SepaMandate


class Command(BaseCommand):
    help = "Find duplicate customers and manually join them.\n"

    def handle(self, *args, **options):
        self.stdout.write("%s\n" % self.help)
        iDuplicateCounter = 0
        lMatchHistory = []
        try:

            for oCustomer in Customer.objects.all():
                if oCustomer.id in lMatchHistory:
                    continue

                qsMatchCustomer = (Customer.objects.filter(name=oCustomer.name,
                                                           surname=oCustomer.surname)
                                                   .exclude(pk=oCustomer.id))
                if qsMatchCustomer:
                    lCustomerObjects = [oCustomer]
                    iCustomerCounter = 0
                    oSepa = SepaMandate.objects.filter(customer=oCustomer)
                    if oSepa:
                        sIban = oSepa[0].iban
                    else:
                        sIban = ''
                    self.stdout.write("%d: %s, %s, %s (%s) - %s, %s - %s\n" % (iCustomerCounter + 1,
                                                                               oCustomer.name,
                                                                               oCustomer.surname,
                                                                               oCustomer.email,
                                                                               oCustomer.id,
                                                                               oCustomer.zip_code,
                                                                               oCustomer.street,
                                                                               sIban))
                    for oMatchCustomer in qsMatchCustomer:
                        iCustomerCounter += 1
                        lMatchHistory.append(oMatchCustomer.id)
                        lCustomerObjects.append(oMatchCustomer)
                        oSepa = SepaMandate.objects.filter(customer=oMatchCustomer)
                        if oSepa:
                            sIban = oSepa[0].iban
                        else:
                            sIban = ''
                        self.stdout.write("%d: %s, %s, %s (%s) - %s, %s - %s\n" % (iCustomerCounter + 1,
                                                                                   oMatchCustomer.name,
                                                                                   oMatchCustomer.surname,
                                                                                   oMatchCustomer.email,
                                                                                   oMatchCustomer.id,
                                                                                   oMatchCustomer.zip_code,
                                                                                   oMatchCustomer.street,
                                                                                   sIban))
                    self.stdout.write("---------------------------------------\n")
                    cust_choice = None
                    while not cust_choice:
                        val = raw_input("Enter number of customer to retain: ")
                        cust_choice = str(val)

                    try:
                        iCustChoice = int(cust_choice) - 1
                        if 0 <= iCustChoice < len(lCustomerObjects):
                            try:
                                for oCustomer in lCustomerObjects:
                                    if oCustomer is not lCustomerObjects[iCustChoice]:
                                        Child.objects.filter(customer=oCustomer).update(customer=lCustomerObjects[iCustChoice])
                                        self.stdout.write("Delete: %s, %s, %s (%s)\n" % (oCustomer.name,
                                                                                           oCustomer.surname,
                                                                                           oCustomer.email,
                                                                                           oCustomer.id))
                                        oCustomer.delete()
                            except Exception as exc:
                                self.stdout.write('Error: %s\nNo changes made\n\n' % exc)
                            else:
                                self.stdout.write("Retained: %s, %s, %s (%s)\n\n" % (lCustomerObjects[iCustChoice].name,
                                                                                     lCustomerObjects[iCustChoice].surname,
                                                                                     lCustomerObjects[iCustChoice].email,
                                                                                     lCustomerObjects[iCustChoice].id))

                        else:
                            self.stdout.write('Invalid input: No changes made\n\n')
                            continue
                    except:
                        continue

        except Exception as e:

            raise CommandError("Error\n%s\n" % e.message)

        self.stdout.write("Gefundene Duplikate: %d\n" % iDuplicateCounter)


