from optparse import make_option

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.hashers import make_password
from kg.models import User


class Command(BaseCommand):
    help = "Set the password 'test' for all user accounts.\nATTENTION: Don't use on production server!\n"

    option_list = BaseCommand.option_list + (
        make_option('--noinput',
                    action='store_true',
                    dest='noinput',
                    default=False,
                    help='Do NOT prompt the user for input of any kind.'),
        )

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)

        if options['noinput'] is True:
            choice = "YES"
        else:
            choice = None

        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:

            iCustCounter = 1
            try:
                oUsers = User.objects.all()
                sNewPassword = make_password('test', '', 'sha1')

                for oUser in oUsers:
                    oUser.password = sNewPassword
                    oUser.save()
                    iCustCounter += 1

                self.stdout.write("All user passwords reset to 'test'\n")

            except:
                raise CommandError('Anonymization aborted')




