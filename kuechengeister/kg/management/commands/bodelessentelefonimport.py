# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from kg_utils import ucsv as csv
from kg.models import Customer


class Command(BaseCommand):
    help = "Import der vergessenen Telefonnummern für Bodelschwinghaus:\n"

    def handle(self, *args, **options):

        for file_name in ['BodelKunden34.csv', 'BodelKunden37.csv']:
            with open(file_name, 'rb') as csvfile:
                oReader = csv.DictReader(csvfile, delimiter=';')

                iRowCounter = 0
                for dRow in oReader:

                    try:
                        try:
                            # Customer holen
                            customer = Customer.objects.get(child__cust_nbr=dRow['Nummer'])
                            if customer.phone:
                                self.stdout.write("Nummer vorhanden: %s, %s - %s\n" % (customer.name, customer.surname, customer.phone))
                            else:
                                if dRow['Telefon'] != '':
                                    customer.phone = dRow['Telefon']
                                    customer.save()
                                    iRowCounter += 1

                        except Exception:
                            pass

                    except Exception as e:
                        self.stdout.write("Exception: %s\n" % e)

            self.stdout.write("Telefonnummern eingefuegt: %d\n" % iRowCounter)
