# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand
from django.db import transaction

from kg_utils import ucsv as csv
from kg.models import User, Customer, Child, Facility, FacilitySubunit, SepaMandate, PayType



class Command(BaseCommand):
    help = "Datenimport für Mensa am LFG:\n"

    def handle(self, *args, **options):

        with open('LFG_Import.csv', 'rb') as csvfile:
            oReader = csv.DictReader(csvfile, delimiter=';')

            facility = Facility.objects.get(pk=1)
            facility_subunit = FacilitySubunit.objects.filter(facility=facility)[0]

            paytype_debit = PayType.objects.get(pk=PayType.DEBIT)
            paytype_invoice = PayType.objects.get(pk=PayType.INVOICE)

            iRowCounter = 0
            iUserCounter = 0
            iCustomerCounter = 0
            iOnlineInvoiceCounter = 0
            iSepaCounter = 0
            iChildrenCounter = 0
            sLastCustomerName = ''
            oLastCustomer = None
            for dRow in oReader:
                #print dRow

                with transaction.commit_manually():
                    try:
                        iRowCounter += 1

                        # prüfen, ob leere Zeile
                        if dRow['Name'].strip() == '':
                            self.stdout.write("Leere Zeile: %s\n" % dRow['Kundennummer'])
                            continue

                        # Customer
                        # check if it is the same Customer
                        if sLastCustomerName ==  dRow['Kunde'].strip():
                            oCustomer = oLastCustomer
                        else:
                            oCustomer = Customer()

                            sLastCustomerName = dRow['Kunde'].strip()
                            oCustomer.title = 'Familie'
                            lNamen = dRow['Kunde'].strip().split(' ')
                            oCustomer.name = lNamen[0]
                            oCustomer.surname = ' '.join(lNamen[1:])
                            oCustomer.email = dRow['Email'].strip()
                            oCustomer.street = dRow[u'Strasse'].strip()
                            oCustomer.zip_code = dRow['PLZ']
                            oCustomer.city = 'München'
                            oCustomer.phone = dRow['Telefon'].strip()
                            if dRow['IBAN'].strip() != '':
                                oCustomer.pay_type = paytype_debit
                            else:
                                oCustomer.pay_type = paytype_invoice
                            oCustomer.save()
                            oLastCustomer = oCustomer
                            iCustomerCounter += 1

                            # SEPA Mandat eintragen -> nur wenn Bankdaten vorliegen
                            if str(dRow['IBAN']).strip() != '':
                                sepa_mandate = SepaMandate(customer=oCustomer)
                                sepa_mandate.name = dRow['Kunde'].strip()
                                sepa_mandate.street = oCustomer.street
                                sepa_mandate.city = u'%s München' % (oCustomer.zip_code)
                                sepa_mandate.mandate_id = dRow['Mandat'].strip()
                                sepa_mandate.iban = dRow['IBAN'].strip().replace(' ', '')
                                sepa_mandate.bic = dRow['BIC'].strip().replace(' ', '')
                                sepa_mandate.bank = dRow['Bank'].strip()
                                sepa_mandate.is_valid = True
                                sepa_mandate.last_debit_date = date(2016,6,1)
                                sepa_mandate.created_at = date(2016,1,1)
                                sepa_mandate.last_debit_sequence = 'RCUR'
                                sepa_mandate.save()
                                iSepaCounter += 1

                        # Kind anlegen und in richtige Einrichtung/Klasse speichern
                        oChild, created = Child.objects.get_or_create(customer=oCustomer,
                                                                      facility=facility,
                                                                      facility_subunit = facility_subunit,
                                                                      name = dRow['Name'].strip(),
                                                                      surname = dRow['Vorname'].strip())
                        iChildrenCounter += 1

                    except Exception as exc:
                        transaction.rollback()
                        import os, sys
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(dRow['Kunde'], exc.message, exc_type, fname, exc_tb.tb_lineno)
                        #self.stdout.write("Exception: %s\n" % exc)
                    else:
                        transaction.commit()

        self.stdout.write("Anzahl Zeilen: %d\n" % iRowCounter)
        self.stdout.write("Angelegte Customer: %d\n" % iCustomerCounter)
        self.stdout.write("Angelegte SepaMandate: %d\n" % iSepaCounter)
        self.stdout.write("Angelegte Children: %d\n" % iChildrenCounter)