from django.core.management.base import BaseCommand
from django.db.models import Q
from django.contrib.admin.models import LogEntry
from django.contrib.contenttypes.models import ContentType
from django.utils.timezone import now

from simple_history.models import HistoricalRecords

# from menus.models import MealPlan, Meal, PriceGroup, MealDefaultPrice
from kg.models import (AdvancedHistory, CustomerAccountTransaction,
                       HistoricalCustomerAccountTransaction)


ACTIONS = {
    1: '+',
    2: '~',
    3: '-'
}


class NotHistorical(TypeError):
    pass


def get_history_model(model):
    """Find the history model for a given app model."""
    try:
        manager_name = model._meta.simple_history_manager_attribute
    except AttributeError:
        raise NotHistorical("Cannot find a historical model for "
                            "{model}.".format(model=model))
    return getattr(model, manager_name).model


def bulk_history_create(model, history_model):
    """Save a copy of all instances to the historical model."""
    historical_instances = [
        history_model(
            history_date=getattr(instance, '_history_date', now()),
            history_user=getattr(instance, '_history_user', None),
            **dict((field.attname, getattr(instance, field.attname))
                   for field in instance._meta.fields)
        ) for instance in model.objects.all()]
    history_model.objects.bulk_create(historical_instances)


def create_history(instance, history_type, user=None, action_time=None,
                   history_message=None, **kwargs):
    target_model = kwargs.get('model', None) or instance.__class__
    history_model = get_history_model(target_model)
    if instance is not None:
        field_values = dict(
            (field.attname, getattr(instance, field.attname))
            for field in instance._meta.fields)
    else:
        field_values = {'id': kwargs['id']}
    record = history_model(
        history_date=action_time or getattr(instance, '_history_date', now()),
        history_user=user or getattr(instance, '_history_user', None),
        history_type=history_type,
        history_message=history_message,
        **field_values
    )
    record.save()


class Command(BaseCommand):
    """Create historical records based on existing admin log entries."""

    def handle(self, *args, **options):
        # Only log entries related to CustomerAccountTransaction objects
        # will be ported for now.
        target_model = CustomerAccountTransaction

        ctype = ContentType.objects.get_for_model(target_model)
        entries = LogEntry.objects.filter(content_type=ctype).order_by(
            'object_id', 'action_time')
        action_count = 0
        for entry in entries:
            try:
                # Only accept custom message with change actions
                message = (entry.action_flag == 2) and entry.change_message
                cat_object = target_model.objects.get(id=entry.object_id)
                create_history(
                    cat_object,
                    history_type=ACTIONS[entry.action_flag],
                    user=entry.user,
                    action_time=entry.action_time,
                    history_message=None or message
                )
                action_count += 1
            except target_model.DoesNotExist:
                # We cannot retrieve details from deleted objects. Therefore,
                # all related records will be skipped.
                pass
        print 'History records created: {}'.format(action_count)
