from django.core.management.base import BaseCommand, CommandError

from kg.models import CustomerAccountTransaction



class Command(BaseCommand):
    help = "Traegt fuer alle CAT, die aus BankAccountTransactions hervorgegangen sind, die transaction_id in Feld bat_hash um.\n"

    def handle(self, *args, **options):

        iCounter = 0

        try:
            all_cats = CustomerAccountTransaction.objects.all()

            for cat in all_cats:
                if (cat.type == CustomerAccountTransaction.TYPE_DEBIT or
                    cat.type == CustomerAccountTransaction.TYPE_DEBIT_RETURN or
                    cat.type == CustomerAccountTransaction.TYPE_BANK_TRANSFER_IN):

                    if len(cat.transaction_id) == 40:

                        cat.bat_hash = cat.transaction_id
                        cat.save()
                        self.stdout.write("%s\n" % (cat.bat_hash))
                        iCounter += 1

        except Exception as e:
            raise CommandError("Error: %s\n" % e.message)

        self.stdout.write("Geaenderte CATs: %d\n" % iCounter)

