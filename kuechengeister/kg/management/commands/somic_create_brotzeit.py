# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand
from django.db import transaction

from kg_utils.date_helper import DateHelper
from menus.helpers import save_menu_prices
from menus.models import Menu, Meal, Mealtime, MealDefaultPrice

from settings import MEALTIMES


class Command(BaseCommand):
    help = "Legt Brotzeiten für diese und naechste Woche an.\n"

    def handle(self, *args, **options):
        dWeeks = DateHelper.getWeeks(date.today(), 2)
        qsBrotzeitMeals = Meal.objects.filter(mealtime__id=MEALTIMES['breakfast'])

        iCounter = 0
        try:
            for dWeek in dWeeks:
                for oDay in dWeek['days'][:5]:
                    for oMeal in qsBrotzeitMeals:

                        # check for existing menu
                        qsExistingMenus = Menu.objects.filter(meal=oMeal, date=oDay)
                        if not qsExistingMenus:
                            oNewMenu = Menu(description=oMeal.name,
                                            meal=oMeal,
                                            date=oDay,
                                            can_be_ordered = True,
                                            order_timelimit = DateHelper.calculateOrderTimelimit(
                                                                    oDay,
                                                                    oMeal.order_day_limit_days,
                                                                    oMeal.order_day_limit,
                                                                    oMeal.order_time_limit),
                                            cancel_timelimit = DateHelper.calculateOrderTimelimit(
                                                                    oDay,
                                                                    oMeal.cancel_day_limit_days,
                                                                    oMeal.cancel_day_limit,
                                                                    oMeal.cancel_time_limit)
                                            )
                            oNewMenu.save()
                            iCounter += 1

                            qsDefaultPrices = MealDefaultPrice.objects.filter(meal=oMeal, price_group__id=1)
                            if qsDefaultPrices:
                                save_menu_prices(oNewMenu, [str(qsDefaultPrices[0].price)], [1])

            self.stdout.write("%s - Essen angelegt: %d\n" % (datetime.now(), iCounter))

        except Exception as exc:
            self.stdout.write("Fehler: %s\n" % exc)


