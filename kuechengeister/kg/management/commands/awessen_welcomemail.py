# -*- coding: utf-8 -*-

from __future__ import with_statement
from smtplib import SMTPException

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.core.mail import send_mail

from kg.models import Customer, User


class Command(BaseCommand):
    help = "Create user and send welcome message to all customers without user\n"

    sFrom = "info@aw-essen.de"
    sSubject = u"Augustinuswerk e.V. - Neues Online-Bestellsystem"
    sMailtextRaw=u"""Liebe Eltern, liebe Kinder,

unser neues Online-Bestellsystem ist ab sofort einsatzbereit!

Alle Bestellungen ab 01.04.2016 machen Sie bitte im neuen Online-Bestellsystem, die ersten Bestellungen für den Monat April sind bitte bis zum 20.03.2016 durchzuführen.

Sie finden das Online-Bestellsystem unter: https://www.aw-essen.de/

Ihr Benutzername: ___benutzername___
Ihr Passwort: ___passwort___


Mit dem in dieser E-Mail enthaltenem Passwort und Ihrer eigenen E-Mail-Adresse können Sie sich jetzt jederzeit im Internet einloggen, den neuesten Speiseplan ansehen, Essen bestellen und Essen abbestellen. Zu jeder Zeit ganz einfach von zu Hause aus.
Ihre Rechnungen erhalten Sie zukünftig online. Sie werden per E-Mail informiert, wenn eine neue Rechnung vorliegt.


Eine Hilfe finden Sie unter: https://www.aw-essen.de/hilfe/


Mit freundlichen Grüßen

Augustinuswerk e.V. Lutherstadt Wittenberg"""


    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            try:
                for oCustomer in Customer.objects.all().order_by('name', 'surname'):
                    if not oCustomer.user and oCustomer.email != '':
                        with transaction.commit_manually():
                            try:
                                sPasswd = User.objects.make_random_password(length=8)
                                oNewUser = User.objects.create_user(oCustomer.email.lower(), oCustomer.email, sPasswd)
                                oNewUser.last_name = oCustomer.name
                                oNewUser.first_name = oCustomer.surname
                                oNewUser.save()

                                oCustomer.user = oNewUser
                                oCustomer.save()

                                sMailtext = self.sMailtextRaw.replace('___benutzername___', oCustomer.user.username)
                                sMailtext = sMailtext.replace('___passwort___', sPasswd)

                                send_mail(self.sSubject, sMailtext, self.sFrom, [oCustomer.email])

                            except Exception as exc:
                                transaction.rollback()
                                self.stdout.write('%s\n' % exc)
                            else:
                                transaction.commit()
                                self.stdout.write('%s, %s\n' % (oCustomer.name, oCustomer.surname))

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)
