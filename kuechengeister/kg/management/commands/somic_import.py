# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand
from django.db import transaction

from kg_utils import ucsv as csv
from kg.models import User, Customer, Child, Facility, FacilitySubunit, SepaMandate, PayType



class Command(BaseCommand):
    help = "Datenimport der Mitarbeiter fuer SOMIC:\n"

    def handle(self, *args, **options):

        with open('SOMIC_Mitarbeiter.csv', 'rb') as csvfile:
            oReader = csv.DictReader(csvfile, delimiter=';')

            facility = Facility.objects.get(pk=1)

            facility_subunit = FacilitySubunit.objects.filter(facility=facility)[0]
            paytype_invoice = PayType.objects.get(pk=PayType.INVOICE)

            iRowCounter = 0
            iUserCounter = 0
            iCustomerCounter = 0
            iOnlineInvoiceCounter = 0
            iSepaCounter = 0
            iChildrenCounter = 0
            for dRow in oReader:

                with transaction.commit_manually():
                    try:
                        iRowCounter += 1

                        # user anlegen
                        new_user = User.objects.create_user(dRow['Chipnr'], '', 'start123')

                        # neuen Customer anlegen
                        customer, was_created = Customer.objects.get_or_create(user=new_user)
                        customer.title = 'Herr'
                        customer.name = dRow['Nachname'].strip()
                        customer.surname = dRow['Vorname'].strip()
                        customer.email = ''
                        customer.street = ' '
                        customer.zip_code = ' '
                        customer.city = ' '
                        customer.phone = ''
                        customer.paytype = paytype_invoice
                        customer.save()
                        iCustomerCounter += 1


                        # Kind anlegen und in richtige Einrichtung/Klasse speichern
                        child, created = Child.objects.get_or_create(customer=customer,
                                                                     cust_nbr=dRow['Chipnr'],
                                                                     facility=facility,
                                                                     facility_subunit = facility_subunit,
                                                                     name = customer.name,
                                                                     surname = customer.surname)
                        iChildrenCounter += 1

                    except Exception as exc:
                        transaction.rollback()
                        import os, sys
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(dRow['Chipnr'], exc.message, exc_type, fname, exc_tb.tb_lineno)
                        #self.stdout.write("Exception: %s\n" % e)
                    else:
                        transaction.commit()

        self.stdout.write("Anzahl Zeilen: %d\n" % iRowCounter)
        self.stdout.write("Angelegte Customer: %d\n" % iCustomerCounter)
        self.stdout.write("Angelegte Children: %d\n" % iChildrenCounter)