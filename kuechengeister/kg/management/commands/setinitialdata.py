# -*- coding: utf-8 -*-

import traceback

from django.core.management.base import BaseCommand, CommandError
from django.contrib.flatpages.models import FlatPage, Site

from kg.models import FacilityType, PayType
from menus.models import Mealtime

from settings import MEALTIMES


class Command(BaseCommand):
    help = ("Imports the flatpage data for a fresh set up site into the DB.\n"
            "ATTENTION: Use only on new customers sites!\n\n")

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:

            try:
                qsSites = Site.objects.all()  # normally only the example site is installed

                oFlatpage, created = FlatPage.objects.get_or_create(url='/')
                oFlatpage.sites = qsSites
                oFlatpage.title = "Startseite"
                oFlatpage.content = """<div class="row" style="padding-top: 30px;">
<div class="col-md-9"><!-- Änderungen ab hier --> <span class="h3">Willkommen beim Online-Bestellsystem f&uuml;r Kinderg&auml;rten und Schulen</span> <br /><br />
<p>Liebe Eltern, liebe Kinder,<br /> <br /> ab sofort steht Ihnen unser neues Online-Bestellsystem zur Verf&uuml;gung.</p>
<p>So k&ouml;nnen Sie komfortabel Essen bestellen und abbestellen. Zu jeder Zeit, ganz einfach von zu Hause aus.</p>
<br /><br /> <br /><br /> <br /><br /> <!-- Änderungen bis hier --></div>
<div class="col-md-3">&nbsp;</div>
</div>"""
                oFlatpage.save()

                oFlatpage, created = FlatPage.objects.get_or_create(url='/datenschutzerklaerung/')
                oFlatpage.title = "Datenschutzerklärung"
                oFlatpage.sites = qsSites
                oFlatpage.content = """<h1>Datenschutz</h1>
<br />
<p>Wir, die Firma Demo Gmbh, Musterstraße 11, 1234 Musterstadt nehmen den Schutz Ihrer persönlichen Daten sehr ernst und halten uns strikt an die Regeln der Datenschutzgesetze. Personenbezogene Daten werden auf dieser Webseite nur im technisch notwendigen Umfang erhoben. Wir erfassen und speichern alle Informationen, die Sie auf unserer Website eingeben oder uns in anderer Weise übermitteln. In keinem Fall werden die erhobenen Daten verkauft oder aus anderen Gründen an Dritte weitergegeben.</p>

<p>Die nachfolgende Erklärung gibt Ihnen einen Überblick darüber, wie wir diesen Schutz gewährleisten und welche Art von Daten zu welchem Zweck erhoben werden.</p>

<h4>Datenverarbeitung auf dieser Internetseite</h4>

<p>Wir erheben und speichern neben den Daten, die Sie auf unserer Webseite eingeben, automatisch in unseren Server-Logfiles Informationen, die Ihr Browser an uns übermittelt. Dies sind:</p>

<ul>
<li>Browsertyp/ -version</li>
<li>verwendetes Betriebssystem</li>
<li>Referrer URL (die zuvor besuchte Seite)</li>
<li>Hostname des zugreifenden Rechners (IP Adresse)</li>
<li>Uhrzeit der Serveranfrage.</li>
</ul>

<p>Diese Daten sind für uns nicht bestimmten Personen zuordenbar. Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen, die Daten werden zudem nach einer statistischen Auswertung gelöscht.</p>

<h4>Cookies</h4>

<p>Die Internetseiten verwenden an mehreren Stellen so genannte Cookies. Sie dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die Ihr Browser speichert. Die von uns verwendeten Cookies sind so genannte "Session-Cookies". Sie werden nach Ende Ihres Besuchs automatisch gelöscht. Cookies richten auf Ihrem Rechner keinen Schaden an und enthalten keine Viren.</p>



<h4>Auskunftsrecht</h4>

<p>Sie haben jederzeit das Recht auf Auskunft über die bezüglich Ihrer Person gespeicherten Daten, deren Herkunft und Empfänger sowie den Zweck der Speicherung. Auskunft über die gespeicherten Daten erhalten Sie unter unten angegebener E-Mail-Adresse bzw. Telefonnummer.</p>

<h4>Weitere Informationen</h4>

<p>Ihr Vertrauen ist uns wichtig. Daher möchten wir Ihnen jederzeit Rede und Antwort bezüglich der Verarbeitung Ihrer personenbezogenen Daten stehen. Wenn Sie Fragen haben, die Ihnen diese Datenschutzerklärung nicht beantworten konnte oder wenn Sie zu einem Punkt vertiefte Informationen wünschen, wenden Sie sich bitte jederzeit an uns.</p>"""

                oFlatpage.save()

                oFlatpage, created = FlatPage.objects.get_or_create(url='/hilfe/')
                oFlatpage.title = "Hilfe"
                oFlatpage.sites = qsSites
                oFlatpage.content = """<h1>Hilfe</h1>
<br />
<p>Wofür benötigen Sie Hilfe?</p>
<p>
<a href="#neukunde">Anmelden als Neukunde</a><br />
<br />
<a href="#kunde">Kundenbereich</a>:
<ul>
<li><a href="#kunde_essen">Essen bestellen</a></li>
<li><a href="#kunde_kinder">Die Daten Ihrer Kinder verwalten</a></li>
<li><a href="#kunde_kinder_neu">Ein Kind hinzufügen</a></li>
<li><a href="#kunde_profil">Ihre Daten verwalten</a></li>
<li><a href="#kunde_passwort">Ihr Passwort ändern</a></li>
<li><a href="#kunde_abo">Das Essen Ihrer Kinder im Abonnement bestellen</a></li>
</ul>

</p>
<br />
<a name="neukunde"></a>
<h2>Anmelden als Neukunde</h2>
<p>Als Neukunde melden Sie sich bitte auf der <a href="/registrierung/neukunde/">Registrierungsseite für Neukunden</a> an.</p>
<p>Bitte prüfen Sie als erstes, ob die Schule/der Kindergarten Ihres Kindes auswählbar ist.</p>
<p>Füllen Sie danach das Formular vollständig aus. Mit * markierte Felder sind Pflichtfelder.</p>
<p><img src="/static/img/hilfe/neukunden_anmeldung.png" alt="Formular Anmeldung Neukunden" border="0" /></p>
<p>Nach Absenden des Formulars wird Ihnen umgehend Ihr Passwort per E-Mail zugeschickt.<br />
Damit können Sie sich auf unserer Webseite einloggen und gelangen in Ihren persönlichen Kundenbereich.<br />
Dort können Sie als erstes die Daten Ihres Kindes eingeben und anschließend das Essen für Ihr Kind bestellen.</p>
<br /><br /><br />

<a name="kunde"></a>
<h2>Der Kundenbereich</h2>
<p>Nachdem Sie sich mit Ihrer Mailadresse und Ihrem Passwort eingeloggt haben, gelangen Sie in Ihren persönlichen Kundenbereich.<br />
Im oberen Bereich der Webseite finden Sie das Menü:<br />
<img src="/static/img/hilfe/kunden_menue.png" alt="Menü Kundenbereich" border="0" /></p>
<p>Sie können folgendes in Ihrem Kundenbereich tun:
<ul>
<li><a href="#kunde_essen">Essen bestellen</a></li>
<li><a href="#kunde_kinder">Die Daten Ihrer Kinder verwalten</a></li>
<li><a href="#kunde_kinder_neu">Ein Kind hinzufügen</a></li>
<li><a href="#kunde_profil">Ihre Daten verwalten</a></li>
<li><a href="#kunde_passwort">Ihr Passwort ändern</a></li>
<li><a href="#kunde_abo">Das Essen Ihrer Kinder im Abonnement bestellen</a></li>
</ul></p>

<a name="kunde_essen"></a>
<h3>Essen bestellen</h3>
<p>Diese Seite wird immer als erstes nach dem Login angezeigt. Alternativ können Sie auch auf den Punkt "Essenbestellung" im Kundenmenü klicken.</p>
<br />

<a name="kunde_profil"></a>
<h3>Persönliche Daten verwalten</h3>
<p>Klicken Sie hierzu auf den Punkt "Meine Daten" im Kundenmenü.</p>
<p>Sie sehen eine Übersicht Ihrer gespeicherten Daten.<br />
Um Ihre Daten zu ändern, klicken Sie oben rechts in der Tabelle auf den Link "bearbeiten". Daraufhin gelangen Sie zu einem Formular, mit dem Sie bequem Ihre Daten ändern können. Wenn Sie Ihre Änderungen eingegeben haben, klicken Sie auf "Senden". Ihre Änderungen sind nun gespeichert und werden Ihnen angezeigt.</p>
<br />

<a name="kunde_passwort"></a>
<h3>Passwort ändern</h3>
<p>Klicken Sie hierzu auf den Punkt "Meine Daten" im Kundenmenü.</p>
<p>Oben rechts auf der Seite finden Sie den Link "Passwort ändern".<br />
Nach Klick auf den Link bekommen Sie ein Formular angezeigt, in das Sie Ihr neues Passwort eingeben können. Zum Schutz vor Tippfehlern müssen Sie Ihr neues Passwort zwei mal eingeben.<br />
Nach Absenden des Formulars wird Ihr Passwort umgehend geändert.</p>
<p><strong>Achtung:</strong> Beim nächsten Login müssen Sie Ihr neues Passwort benutzen.</p>
<br />

<a name="kunde_kinder"></a>
<h3>Daten der Kinder verwalten</h3>
<p>Klicken Sie hierzu auf den Punkt "Meine Kinder" im Kundenmenü.</p>
<p>Sie sehen eine Übersicht aller Ihrer bei uns gespeicherten Kinder. Bei jedem Kind finden Sie oben rechts einen "bearbeiten" Link. Nach Klick auf diesen Link gelangen Sie zu einem Formular, um die Daten des Kindes zu ändern.</p>
<p>Nach Absenden des Formulars werden die Änderungen gespeichert und die geänderten Daten werden in der Übersicht angezeigt.</p>
<br />


<a name="kunde_kinder_neu"></a>
<h3>Ein Kind hinzufügen</h3>
<p>Klicken Sie hierzu auf den Punkt "Meine Kinder" im Kundenmenü.</p>
<p>Oben rechts auf der Seite finden Sie den Link "Kind hinzufügen".<br />
Nach Klick auf den Link, bekommen Sie ein Formular angezeigt, mit dem Sie ein Kind hinzufügen können.<br />
Bitte füllen Sie das Formular aus. Mit * markierte Felder sind Pflichtfelder.<br />
Nach Absenden des Formulars werden die Daten des Kindes gespeichert und das Kind wird in der Übersicht der Kinder angezeigt.</p>
<br />

<a name="kunde_abo"></a>
<h3>Abonnement einrichten</h3>
<p>Klicken Sie hierzu auf den Punkt "Abonnement" im Kundenmenü.</p>
<p>Nach Aktivierung des Abonnements ist Ihr Kind in Zukunft jeden Tag für das Essen angemeldet, ohne dass Sie das Essen explizit bestellen müssen.</p>
<p>Selbstverständlich können Sie trotzdem einzelne Essen stornieren, falls Ihr Kind mal krank sein sollte oder auf Klassenfahrt ist. Nutzen Sie dazu bitte die Bestellmaske unter dem Punkt "<a href="#kunde_essen">Essenbestellung</a>".</p>
"""

                oFlatpage.save()

                oFlatpage, created = FlatPage.objects.get_or_create(url='/impressum/')
                oFlatpage.title = "Impressum"
                oFlatpage.sites = qsSites
                oFlatpage.content = """<h1>Impressum</h1>
<br />
<p>Alle auf dieser Website veröffentlichten Texte, Abbildungen und graphischen Elemente sind urheberrechtlich geschützt. Die Inhalte externer Links werden von uns nicht geprüft. Sie unterliegen der Haftung der jeweiligen Anbieter. </p>
<p>Verantwortlich für den Inhalt dieser Webseite nach § 5 TMG ist:</p>
<p>Demo GmbH<br />Geschäftsführer</p>

<p>Musterstraße 11<br />
1234 Musterstadt</p>
<p>Telefon: +49 <br />
E-Mail: (at)
</p>
<br />
Steuer-Nr.: """
                oFlatpage.save()

                oFlatpage, created = FlatPage.objects.get_or_create(url='/neuigkeiten/')
                oFlatpage.title = "Information für unsere Kunden"
                oFlatpage.sites = qsSites
                oFlatpage.content = """<div class="subcolumns">
 <div class="c75l">
         <div class="subcl">
<div class="info highlight">


<!--  AB HIER AENDERN -->

An dieser Stelle können Sie Ihre Kunden mit aktuellen Informationen versorgen.

<!--  BIS HIER AENDERN -->
</div>"""
                oFlatpage.save()

                oFlatpage, created = FlatPage.objects.get_or_create(url='/agb/')
                oFlatpage.title = "AGB"
                oFlatpage.sites = qsSites
                oFlatpage.content = "<h1>Allgemeine Geschäftsbedingungen</h1>"
                oFlatpage.save()


                # FacilityType
                oFacilityType, created = FacilityType.objects.get_or_create(id=FacilityType.SCHOOL, name='Schule', name_short='S')
                oFacilityType, created = FacilityType.objects.get_or_create(id=FacilityType.KINDERGARTEN, name='Kindergarten', name_short='KG')
                oFacilityType, created = FacilityType.objects.get_or_create(id=FacilityType.KUNDENPARKPLATZ, name='Kundenparkplatz', name_short='KP')

                # PayType
                oPayType, created = PayType.objects.get_or_create(id=PayType.DEBIT, new_customers=True)
                oPayType, created = PayType.objects.get_or_create(id=PayType.INVOICE, new_customers=False)

                # Mealtime
                oMealtime, created = Mealtime.objects.get_or_create(id=MEALTIMES['breakfast'], name=u'Frühstück', name_short='F', sortKey=1)
                oMealtime, created = Mealtime.objects.get_or_create(id=MEALTIMES['lunch'], name=u'Mittagessen', name_short='M', sortKey=2)
                oMealtime, created = Mealtime.objects.get_or_create(id=MEALTIMES['snack'], name=u'Vesper', name_short='V', sortKey=3)

                self.stdout.write("Data successfully imported.\n")

            except Exception:
                traceback.print_exc()
                raise CommandError("Abortion through Exception\n")




