# -*- coding: utf-8 -*-

from __future__ import with_statement
from smtplib import SMTPException

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.core.mail import send_mail

from kg.models import Customer, User


class Command(BaseCommand):
    help = "Create user and send welcome message to all customers without user\n"

    sFrom = "info@tommys-partyservice.de"
    sSubject = u"Tommy's Menü- und Partyservice - Neues Online-Bestellsystem"
    sMailtextRaw=u"""Liebe Eltern, liebe Lehrer, liebe Erzieher und liebe Kinder,

unser neues Online-Bestellsystem ist ab sofort einsatzbereit!
Alle Bestellungen machen Sie bitte im Online-Bestellsystem. Der 10. des laufenden Monats ist der letzte Bestelltag für den Folgemonat.

Sie finden das Online-Bestellsystem unter: www.tommys-partyservice.de

Ihr Benutzername: ___benutzername___
Ihr Passwort: ___passwort___


Mit dem in dieser E-Mail enthaltenem Passwort und Ihrer eigenen E-Mail Adresse können Sie sich jetzt jederzeit im Internet einloggen, den neuesten Speisenplan ansehen, Essen bestellen und Essen abbestellen. Zu jeder Zeit ganz einfach von zu Hause aus.
Unsere allgemeinen Geschäftsbestimmungen können Sie im Internet nachlesen. Hier sind noch einmal Termine, Uhrzeiten und Bestellung bzw. Abbestellung erläutert.
Die Rechnungen werden am letzten des Monats erstellt. Lastschriften werden zum 10. des jeweiligen Monats eingezogen, wir bitten um ausreichende Kontodeckung (Gebühr für Rücklastschriften 7,50 €).
Rechnungen werden per E-Mail versandt oder wenn gewünscht per Post (gebührenpflichtig), Essenmarken mit Kurierdienst.

Für weitere Fragen stehen wir Ihnen gerne telefonisch unter 0 34 94 / 50 35 93 zur Verfügung.


Mit freundlichen Grüßen

Tommy's Menü- und Partyservice"""


    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            try:
                for oCustomer in Customer.objects.all().order_by('name', 'surname'):
                    if not oCustomer.user and oCustomer.email != '':
                        with transaction.commit_manually():
                            try:
                                sPasswd = User.objects.make_random_password(length=8)
                                oNewUser = User.objects.create_user(oCustomer.email.lower(), oCustomer.email, sPasswd)
                                oNewUser.last_name = oCustomer.name
                                oNewUser.first_name = oCustomer.surname
                                oNewUser.save()

                                oCustomer.user = oNewUser
                                oCustomer.save()

                                sMailtext = self.sMailtextRaw.replace('___benutzername___', oCustomer.user.username)
                                sMailtext = sMailtext.replace('___passwort___', sPasswd)

                                send_mail(self.sSubject, sMailtext, self.sFrom, [oCustomer.email])

                            except Exception as exc:
                                transaction.rollback()
                                self.stdout.write('%s\n' % exc)
                            else:
                                transaction.commit()
                                self.stdout.write('%s, %s\n' % (oCustomer.name, oCustomer.surname))

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)
