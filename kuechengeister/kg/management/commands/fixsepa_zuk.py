from datetime import date
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Max
from kg.models import SepaMandate, InvoiceLog, PayType


class Command(BaseCommand):
    help = "Fixes SEPA for Zipfel and KG\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:

            try:
                counter = 0
                invs = InvoiceLog.objects.filter(pay_type_id=PayType.DEBIT,
                                                 invoice_date=date(2014, 2, 28),
                                                 sepa_seq_type='FRST')
                for inv in invs:
                    test = InvoiceLog.objects.filter(sepa_mandate_reference=inv.sepa_mandate_reference)
                    if len(test) > 1:
                        inv.sepa_seq_type = 'RCUR'
                        inv.save()
                        counter += 1

                print ("%d Eintraege gefixt\n" % counter)

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)




