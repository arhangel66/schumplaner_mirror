from django.core.management.base import BaseCommand, CommandError

from kg.models import InvoiceLog, PayType, CustomerAccountTransaction
from kg_utils.accounting_helper import connectCustAccTrans


class Command(BaseCommand):
    help = "Creates a corresponding CustomerAccountTransaction for all invoices that were payed by SEPA\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:

            try:
                for oInvoiceLog in InvoiceLog.objects.filter(pay_type_id=PayType.DEBIT, cat_created=False):

                    # create new CustomerAccountTransaction
                    oNewCat = CustomerAccountTransaction(customer=oInvoiceLog.child.customer)

                    oNewCat.transaction_id = oInvoiceLog.sepa_transaction_id
                    oNewCat.transaction_date = oInvoiceLog.pay_until
                    oNewCat.amount = oInvoiceLog.invoice_total
                    oNewCat.description = 'EREF+ %s MREF+%s SVWZ+%s' % (oInvoiceLog.sepa_transaction_id, oInvoiceLog.sepa_mandate_reference, oInvoiceLog.sepa_description)
                    oNewCat.type = CustomerAccountTransaction.TYPE_DEBIT
                    oNewCat.save()

                    # connect invoice CAT and debit CAT
                    try:
                        oInvoiceCat = CustomerAccountTransaction.objects.get(transaction_id=oInvoiceLog.invoice_number)

                        connectCustAccTrans(oInvoiceCat, oNewCat)

                        oInvoiceLog.cat_created = True
                        oInvoiceLog.save()

                    except Exception as e:
                        print("Exception\n%s\nKunde: %s - %s\n" % (e.message, oInvoiceLog.child.customer, oInvoiceLog.child.customer.id ))

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)




