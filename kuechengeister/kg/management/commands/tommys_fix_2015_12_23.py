# -*- coding: utf-8 -*-

import traceback
from datetime import datetime, date, timedelta

from django.core.management.base import BaseCommand, CommandError

from kg.models import Meal, Mealtime, MealtimeCategory, Menu, PriceGroup
from kg_utils.date_helper import DateHelper


class Command(BaseCommand):
    help = ("Set menu order times for 01/2016 and 02/2016\n\n")

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)

        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            self.stdout.write("Change order times..\n")

            try:
                Menu.objects.filter(date__month=1, date__year=2016).update(order_timelimit=datetime(2015,12,12,23,59,59))
                Menu.objects.filter(date__month=2, date__year=2016).update(order_timelimit=datetime(2016,1,12,23,59,59))

                (Menu.objects.exclude(mealtime__in=[1,2,3])
                             .update(order_timelimit=datetime(2015,12,12,23,59,59)))

                for oMenu in Menu.objects.filter(mealtime__category__id=MealtimeCategory.BREAKFAST):
                    oMenu.cancel_timelimit=datetime(oMenu.date.year, oMenu.date.month, oMenu.date.day, 6, 0)
                    oMenu.save()

                self.stdout.write("Done\n")
            except Exception:
                traceback.print_exc()
                raise CommandError("Abortion through Exception\n")
