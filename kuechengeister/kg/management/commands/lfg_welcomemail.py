# -*- coding: utf-8 -*-

from __future__ import with_statement
from smtplib import SMTPException

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.core.mail import send_mail

from kg.models import Customer, User


class Command(BaseCommand):
    help = "Create user and send welcome message to all customers without user\n"

    sFrom = "info@mensa-lfg.de"
    sSubject = u"Mensa am LFG - Neues Online-Bestellsystem"
    sMailtextRaw=u"""Liebe Eltern, liebe Lehrer und liebe Kinder,

wir haben ab nächster Woche ein neues Bestellsystem. Das System wird noch vor den großen Sommerferien eingesetzt, damit wir und auch Sie als Nutzer sich ohne den Hektik und Stress des neuen Schuljahres auf dieses System einstellen können und evtl. anfallende Schwierigkeiten noch behoben werden können. Die Abrechnung bleibt weiterhin auf Lastschrifteinzug.

Die Abholung des Essens erfolgt mit Hilfe eines Chips (Schlüsselanhänger), den wir diesen Freitag gegen einen Pfand von 2,00 Euro (Geld bitte in bar mitbringen) am Kiosk ausgeben.

Alle Bestellungen ab dem 20. Juni 2016 machen Sie bitte im neuen Online-Bestellsystem. Die Bestellung für die Folgewoche ist bis jeweils Freitag der laufenden Woche möglich.

Sie finden das neue Online-Bestellsystem unter: www.mensa-lfg.de

Ihr Benutzername: ___benutzername___
Ihr Passwort: ___passwort___


Mit dem in dieser E-Mail enthaltenem Passwort und Ihrer E-Mail Adresse können Sie sich ab sofort im Internet einloggen, den neuesten Speisenplan ansehen, Essen bestellen und Essen abbestellen.
Bitte prüfen Sie Ihre persönlichen Daten auf Aktualität und Vollständigkeit und ergänzen Sie diese falls notwendig.

Ihre Bestellungen für die Woche ab dem 20. Juni 2016 machen Sie bitte bis spätestens Freitag, 17. Juni 2016.


Mit freundlichen Grüßen

Petra Aigner und das Mensa Team"""


    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            try:
                #for oCustomer in Customer.objects.all().order_by('name', 'surname'):
                for oCustomer in Customer.objects.filter(name__exact='Weitz').order_by('name', 'surname'):
                    if not oCustomer.user and oCustomer.email != '':
                        with transaction.commit_manually():
                            try:
                                sPasswd = User.objects.make_random_password(length=8)
                                oNewUser = User.objects.create_user(oCustomer.email.lower(), oCustomer.email, sPasswd)
                                oNewUser.last_name = oCustomer.name
                                oNewUser.first_name = oCustomer.surname
                                oNewUser.save()

                                oCustomer.user = oNewUser
                                oCustomer.save()

                                sMailtext = self.sMailtextRaw.replace('___benutzername___', oCustomer.user.username)
                                sMailtext = sMailtext.replace('___passwort___', sPasswd)

                                send_mail(self.sSubject, sMailtext, self.sFrom, [oCustomer.email])

                            except Exception as exc:
                                transaction.rollback()
                                self.stdout.write('%s\n' % exc)
                            else:
                                transaction.commit()
                                self.stdout.write('%s, %s\n' % (oCustomer.name, oCustomer.surname))

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)
