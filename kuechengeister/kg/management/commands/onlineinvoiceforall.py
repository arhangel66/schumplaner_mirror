from django.core.management.base import BaseCommand, CommandError
from django.db.models import Max
from kg.models import Customer, OnlineInvoice


class Command(BaseCommand):
    help = "Activate online invoice for all customers\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:

            try:
                for customer in Customer.objects.all():

                    obj, created = OnlineInvoice.objects.get_or_create(customer=customer)

                    if created:
                        obj.save()

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)




