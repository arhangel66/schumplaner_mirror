from django.core.management.base import BaseCommand, CommandError
from django.db.models import Max
from kg.models import SepaMandate, InvoiceLog


class Command(BaseCommand):
    help = "Fixes SEPA\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:

            try:
                for sepa in SepaMandate.objects.all():

                    if sepa.last_debit_date is not None:
                        inv_dates = InvoiceLog.objects.filter(sepa_mandate_reference=sepa.mandate_id).dates('pay_until', 'month')

                        if len(inv_dates) > 1:
                            sepa.last_debit_sequence = 'RCUR'
                        else:
                            sepa.last_debit_sequence = 'FRST'
                    sepa.save()


            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)




