# -*- coding: utf-8 -*-
from datetime import datetime, date, timedelta

from django import forms
from django.core.validators import ValidationError, RegexValidator
from django.forms.util import ErrorList

from form_utils.forms import BetterForm
import nested_admin

from settings import CANTEEN_MODE, OVERLOAD_SITE, MODULE_PREPAID
from kg.models import (
    Facility, InvoiceLog, User, Customer, CustomerAccountTransaction, PayType,
    Child, Reduction, BankAccountTransaction
)
from menus.models import Menu
from kg_utils.date_helper import DateHelper


class LocalizedModelForm(forms.ModelForm):
    def __new__(cls, *args, **kwargs):
        new_class = super(LocalizedModelForm, cls).__new__(
            cls, *args, **kwargs)
        for field in new_class.base_fields.values():
            if isinstance(field, forms.DecimalField):
                field.localize = True
                field.widget.is_localized = True
        return new_class


class LocalizedModelFormSet(nested_admin.formsets.NestedInlineFormSet):
    def __new__(cls, *args, **kwargs):
        new_class = super(LocalizedModelFormSet, cls).__new__(
            cls, *args, **kwargs)
        for field in new_class.form.base_fields.values():
            if isinstance(field, forms.DecimalField):
                field.localize = True
                field.widget.is_localized = True
        return new_class


class InvoiceSelectionForm(BetterForm):

    paper_only = forms.BooleanField(label="Nur Rechnungen mit Versand per Post  ", required=False)

    def __init__(self, *args, **kwargs):
        super(InvoiceSelectionForm, self).__init__(*args, **kwargs)

        last_of_last_month = DateHelper.getLastDayOfLastMonth(date.today())

        year_choices = []
        for year in InvoiceLog.objects.dates('invoice_date', 'year'):
            year_choices.append((year.year, str(year.year)))
        self.fields['year'] = forms.ChoiceField(choices=year_choices, required=True,
                                                initial=last_of_last_month.year, label="Jahr")

        fac_choices = []
        for fac in Facility.ex_objects.filter():  # .exclude(type__name='Kundenparkplatz'):
            fac_choices.append((str(fac.id), fac.name))
        self.fields['facility'] = forms.ChoiceField(choices=fac_choices, required=True, label="Einrichtung")

        month_choices = []
        for month in range(1, 13):
            month_choices.append((month, "%02d" % month))
        self.fields['month'] = forms.ChoiceField(choices=month_choices, required=True,
                                                 initial=last_of_last_month.month, label="Monat")

    class Meta:
        fieldsets = [('Rechnungen herunterladen',
                      {'fields': ['month', 'year', 'facility'],
                      #'description': '',
                       'classes': ['module aligned'],
                       }),
                     ('Optionen',
                      {'fields': ['paper_only'],
                      #'description': '',
                       'classes': ['module aligned']
                       }), ]

        row_attrs = {'month': {'class': 'form-row'}, }


class SepaSelectionForm(BetterForm):

    def __init__(self, *args, **kwargs):
        super(SepaSelectionForm, self).__init__(*args, **kwargs)

        last_of_last_month = DateHelper.getLastDayOfLastMonth(date.today())

        month_choices = []
        for month in range(1, 13):
            month_choices.append((month, "%02d" % month))
        self.fields['sepa_month'] = forms.ChoiceField(choices=month_choices, required=True,
                                                      initial=last_of_last_month.month, label="Monat")

        year_choices = []
        for year in InvoiceLog.objects.dates('invoice_date', 'year'):
            year_choices.append((year.year, str(year.year)))
        self.fields['sepa_year'] = forms.ChoiceField(choices=year_choices, required=True,
                                                     initial=last_of_last_month.year, label="Jahr")

        fac_choices = []
        for fac in Facility.ex_objects.filter():   # .exclude(type__name='Kundenparkplatz'):
            fac_choices.append((str(fac.id), fac.name))
        self.fields['sepa_facility'] = forms.ChoiceField(choices=fac_choices, required=True, label="Einrichtung")

    version_choices = [('pain.008.003.02', 'Version 2.7'), ('pain.008.001.02', 'Version 3.0')]
    batch_choices = [('false', 'Einzelbuchung'), ('true', 'Sammelbuchung')]

    sepa_batch = forms.ChoiceField(choices=batch_choices, required=True, label="Buchungstyp", initial='false')
    sepa_version = forms.ChoiceField(choices=version_choices, required=True, label="SEPA Version",
                                     initial='pain.008.001.02')

    class Meta:
        fieldsets = [('SEPA-Lastschrift Datei erstellen',
                      {'fields': ['sepa_month', 'sepa_year', 'sepa_facility', 'sepa_batch', 'sepa_version'],
                      #'description': '',
                       'classes': ['module aligned'],
                       }),
                     ]

        row_attrs = {'sepa_month': {'class': 'form-row'}, }


class TaxCsvSelectionForm(BetterForm):

    def __init__(self, *args, **kwargs):
        super(TaxCsvSelectionForm, self).__init__(*args, **kwargs)

        last_of_last_month = DateHelper.getLastDayOfLastMonth(date.today())

        month_choices = []
        for month in range(1, 13):
            month_choices.append((month, "%02d" % month))
        self.fields['taxcsv_month'] = forms.ChoiceField(choices=month_choices, required=True,
                                                        initial=last_of_last_month.month, label="Monat")
        year_choices = []
        for year in InvoiceLog.objects.dates('invoice_date', 'year'):
            year_choices.append((year.year, str(year.year)))
        self.fields['taxcsv_year'] = forms.ChoiceField(choices=year_choices, required=True,
                                                       initial=last_of_last_month.year, label="Jahr")
        fac_choices = [('all', 'Alle')]
        for fac in Facility.ex_objects.filter():  # .exclude(type__name='Kundenparkplatz'):
            fac_choices.append((str(fac.id), fac.name))
        self.fields['taxcsv_facility'] = forms.ChoiceField(choices=fac_choices, required=True, label="Einrichtung")
        paytype_choices = [('all', 'Alle')]
        for paytype in PayType.PAYTYPE_CHOICES:
            paytype_choices.append(paytype)
        self.fields['taxcsv_paytype'] = forms.ChoiceField(choices=paytype_choices, required=True,
                                                          initial='all', label="Zahlungsart")

    class Meta:
        fieldsets = [('CSV-Datei für Steuerberater erstellen',
                      {'fields': ['taxcsv_month', 'taxcsv_year', 'taxcsv_facility', 'taxcsv_paytype'],
                      #'description': '',
                       'classes': ['module aligned'],
                       }),
                     ]

        row_attrs = {'taxcsv_month': {'class': 'form-row'}, }


class MailTestForm(BetterForm):

    mailaddress = forms.EmailField(label="Mailadresse:")

    class Meta:
        fieldsets = [('Testmail senden', {'fields': ['mailaddress', ]}), ]
        row_attrs = {'mailaddress': {'class': 'form-row'}}


class MonthSelectionForm(BetterForm):

    def __init__(self, *args, **kwargs):
        super(MonthSelectionForm, self).__init__(*args, **kwargs)

        initial_date = DateHelper.getLastDayOfLastMonth(date.today())

        month_choices = []
        for month in range(1, 13):
            month_choices.append((month, "%02d" % month))
        self.fields['month'] = forms.ChoiceField(choices=month_choices, initial=initial_date.month, label="")

        year_choices = []
        for year in Menu.objects.dates('date', 'year'):
            year_choices.append((year.year, str(year.year)))
        self.fields['year'] = forms.ChoiceField(choices=year_choices, initial=initial_date.year, label="")


class WeekSelectionForm(BetterForm):

    def __init__(self, *args, **kwargs):
        super(WeekSelectionForm, self).__init__(*args, **kwargs)

        initial_isoyear, initial_isoweek, initial_isoweekday = date.today().isocalendar()

        week_choices = []
        for week in range(1, 53):
            week_choices.append((week, "%02d" % week))
        self.fields['week'] = forms.ChoiceField(choices=week_choices, initial=initial_isoweek, label="Woche")

        year_choices = []
        for year in Menu.objects.dates('date', 'year'):
            year_choices.append((year.year, str(year.year)))
        self.fields['year'] = forms.ChoiceField(choices=year_choices, initial=initial_isoyear, label="Jahr")


class CustomerAdminForm(forms.ModelForm):
    """
    Own CustomerAdminForm to order the user in CustomerAdmin
    Overwrites the user field and sets extra attributes fields for certain instances
    All other fields are as configured in CustomerAdmin
    """
    user = forms.ModelChoiceField(queryset=User.objects.order_by('username'), required=False)

    class Meta:
        model = Customer

    def __init__(self, *args, **kwargs):
        super(CustomerAdminForm, self).__init__(*args, **kwargs)

        # set object id to use in validation
        if 'instance' in kwargs:
            self.id = kwargs['instance'].id

        if CANTEEN_MODE:
            self.fields['surname'].required = True
        else:
            self.fields['street'].required = True
            self.fields['zip_code'].required = True
            self.fields['city'].required = True

        # Instance specific configuration
        if OVERLOAD_SITE in ['hytera', 'somic']:
            self.fields['extra_attribute'].label = 'Personalnr.'
            self.fields['extra_attribute'].required = True

        if OVERLOAD_SITE == 'awessen':
            self.fields['extra_attribute'].label = 'Debitor-Nr.'
            self.fields['extra_attribute'].help_text = 'Nicht änderbar. Wird vom System automatisch vergeben.'
            self.fields['extra_attribute'].widget.attrs['readonly'] = True

        if OVERLOAD_SITE == 'bossow':
            self.fields['extra_attribute'].label = 'Debitor-Nr.'

        if OVERLOAD_SITE == 'abraxas':
            self.fields['extra_attribute'].validators = [
                RegexValidator(
                    regex='^[0-9]{6}$',
                    message='Bitte 6 stellige Zahl eingeben.',
                    code=''
                ),
            ]

        # extra_attribute2
        if OVERLOAD_SITE == 'somic':
            SOMIC_EXTRA2_CHOICES = [
                ('Components - Teilefertigung', 'Components - Teilefertigung'),
                ('Components - Montage', 'Components - Montage'),
                ('Somicon', 'Somicon'),
                ('Verpackung', 'Verpackung'),
                ('Verwaltung', 'Verwaltung'),
            ]
            self.fields['extra_attribute2'] = forms.ChoiceField(choices=SOMIC_EXTRA2_CHOICES)
            self.fields['extra_attribute2'].label = 'Firma'

        if OVERLOAD_SITE == 'hytera':
            HYTERA_EXTRA2_CHOICES = [
                ('Intern', 'Intern'),
                ('Extern', 'Extern'),
                ('Gast', 'Gast'),
            ]
            self.fields['extra_attribute2'] = forms.ChoiceField(choices=HYTERA_EXTRA2_CHOICES)
            self.fields['extra_attribute2'].label = 'Zugehörigkeit'

    def clean_extra_attribute(self):
        extra_attribute = self.cleaned_data['extra_attribute']
        extra_attribute = extra_attribute.strip()

        # unique Debitor-Nr. or blank field check
        if OVERLOAD_SITE in ['abraxas', 'bossow', 'esl']:
            if extra_attribute != '':
                qsCustomers = Customer.objects.filter(extra_attribute__exact=extra_attribute)
                if self.id:
                    qsCustomers = qsCustomers.exclude(pk=self.id)
                if qsCustomers.count() > 0:
                    raise forms.ValidationError('Diese Debitor-Nr. ist bereits vergeben.')

        return extra_attribute


class CustomerAccountTransactionAdminForm(LocalizedModelForm):
    """
    Own CustomerAccountTransactionForm to preselect the right customer in the form if given per GET
    """
    def __init__(self, *args, **kwargs):
        super(CustomerAccountTransactionAdminForm, self).__init__(*args, **kwargs)

        # read out all choices without the empty choice
        my_choices = []
        for choice in (self.fields['customer'].widget).choices:
            if choice[0] != '':
                my_choices.append(choice)
        # set the created choices to the form field if there is only one choice left
        # this is for the cases when there is only one choice for the customer selection box
        if len(my_choices) == 1:
            self.fields['customer']._set_choices(my_choices)

    class Meta:
        model = CustomerAccountTransaction


class BankAccountTransactionForm(LocalizedModelForm):

    class Meta:
        model = BankAccountTransaction


class UploadFileForm(BetterForm):
    file = forms.FileField(label="Datei")

    class Meta:
        fieldsets = [('Datei hochladen',
                      {'fields': ['file'],
                       'classes': ['module aligned'], }),
                     ]


class EmailExportForm(BetterForm):

    def __init__(self, *args, **kwargs):
        super(EmailExportForm, self).__init__(*args, **kwargs)

        l_facility_choices = []
        for oFac in Facility.ex_objects.filter():
            l_facility_choices.append((str(oFac.id), oFac.name))
        self.fields['facility'] = forms.ChoiceField(choices=l_facility_choices, required=True, label="Einrichtung")

    l_separator_choices = [(';', 'Semikolon'), (',', 'Komma'), ('br', 'Zeilenumbruch')]
    l_customer_choices = [('all', 'Alle'), ('paper_yes', 'Mit Rechnung per Post'), ('paper_no', 'Ohne Rechnung per Post')]


    properties = forms.ChoiceField(choices=l_customer_choices, required=True, label="Kunden")
    separator = forms.ChoiceField(choices=l_separator_choices, required=True, label="Trennzeichen")

    class Meta:
        fieldsets = [('Einstellungen',
                      {'fields': ['facility', 'properties', 'separator'],
                       'description': 'Wenn Sie Thunderbird als Mail-Client verwenden, wählen Sie als Trennzeichen bitte den Zeilenumbruch.'
                                      '<br />Bei den meisten anderen Mail-Clients wird das Semikolon als Trennzeichen genutzt. Weitere Hinweise finden Sie in der Bedienungsanleitung Ihres Mail-Clients.',
                       'classes': ['module aligned'], }),
                     ]


class VoucherPerFacilityForm(BetterForm):

    def __init__(self,  *args, **kwargs):
        super(VoucherPerFacilityForm, self).__init__(*args, **kwargs)

        initial_date = DateHelper.getLastDayOfMonth() + timedelta(days=1)

        year_choices = []
        for year in Menu.objects.dates('date', 'year'):
            year_choices.append((year.year, str(year.year)))
        self.fields['year'] = forms.ChoiceField(choices=year_choices, required=True,
                                                initial=initial_date.year, label="Jahr")

        fac_choices = []
        for fac in Facility.ex_objects.filter():  # .exclude(type__name='Kundenparkplatz'):
            fac_choices.append((str(fac.id), fac.name))
        self.fields['facility'] = forms.ChoiceField(choices=fac_choices, required=True, label="Einrichtung")

        month_choices = []
        for month in range(1, 13):
            month_choices.append((month, "%02d" % month))
        self.fields['month'] = forms.ChoiceField(choices=month_choices, required=True,
                                                 initial=initial_date.month, label="Monat")

    class Meta:
        fieldsets = [('Essenmarken gesamte Einrichtung',
                      {'fields': ['month', 'year', 'facility'],
                       #'description': '',
                       'classes': ['module aligned'],
                       }), ]

        row_attrs = {'month': {'class': 'form-row'}, }


class VoucherPerChildForm(BetterForm):

    def __init__(self,  *args, **kwargs):
        super(VoucherPerChildForm, self).__init__(*args, **kwargs)

        initial_date = DateHelper.getLastDayOfMonth() + timedelta(days=1)

        year_choices = []
        for year in Menu.objects.dates('date', 'year'):
            year_choices.append((year.year, str(year.year)))
        self.fields['year'] = forms.ChoiceField(choices=year_choices, required=True,
                                                initial=initial_date.year, label="Jahr")

        month_choices = []
        for month in range(1, 13):
            month_choices.append((month, "%02d" % month))
        self.fields['month'] = forms.ChoiceField(choices=month_choices, required=True,
                                                 initial=initial_date.month, label="Monat")

        child_choices = []
        for oChild in Child.objects.filter():
            child_choices.append((str(oChild.id), '%s, %s' % (oChild.name, oChild.surname)))
        self.fields['child'] = forms.ChoiceField(choices=child_choices, required=True, label="Kind")

    class Meta:
        fieldsets = [('Essenmarken einzelnes Kind',
                      {'fields': ['month', 'year', 'child'],
                       #'description': '',
                       'classes': ['module aligned'],
                       }), ]

        row_attrs = {'month': {'class': 'form-row'}, }


class ReductionForm(LocalizedModelForm):

    def clean_per_menu(self):
        if not self.cleaned_data['per_menu'] and MODULE_PREPAID:
            raise ValidationError(u'Einmal Ermäßigungen werden im Prepaid '
                                  u'Mode aktuell nicht unterstützt')
        return self.cleaned_data['per_menu']

    class Meta:
        model = Reduction


class ChildAdminForm(forms.ModelForm):
    """Customized form class for preventing saving duplication RFID value
       when running as CANTEEN.
    """
    def clean(self):
        # There is not rfid_tag value in non canteen mode
        if 'rfid_tag' in self.cleaned_data:
            rfid_tag = self.cleaned_data['rfid_tag'].strip()
            if rfid_tag:
                rfid_exists = Child.objects.exclude(pk=self.instance.pk).filter(
                    rfid_tag__iexact=rfid_tag).exists()
                if rfid_exists:
                    self.errors['rfid_tag'] = ErrorList([u'Chip-ID bereits vergeben'])
            # Save the cleaned value
            self.cleaned_data['rfid_tag'] = rfid_tag
        return self.cleaned_data

    def clean_cust_nbr(self):
        customer_number = self.cleaned_data['cust_nbr']
        if customer_number:
            child_queryset = Child.objects.exclude(pk=self.instance.pk)
            if child_queryset.filter(cust_nbr=customer_number).exists():
                raise ValidationError(u'Kundennummer existiert bereits.')
        elif self.instance.pk:
            raise ValidationError(u'Dieses Feld ist zwingend erforderlich.')
        return customer_number

    class Meta:
        model = Child
        exclude = []


class FibuStammForm(BetterForm):

    def __init__(self, *args, **kwargs):
        super(FibuStammForm, self).__init__(*args, **kwargs)

        fac_choices = []
        for fac in Facility.ex_objects.filter():  # .exclude(type__name='Kundenparkplatz'):
            fac_choices.append((str(fac.id), fac.name))
        self.fields['facility'] = forms.ChoiceField(choices=fac_choices, required=True, label="Einrichtung")

    class Meta:
        fieldsets = [('FiBu Export - Debitoren herunterladen',
                      {'fields': ['facility'],
                      #'description': '',
                       'classes': ['module aligned'],
                       }),]

        row_attrs = {'month': {'class': 'form-row'}, }


class FibuBuchForm(BetterForm):

    def __init__(self, *args, **kwargs):
        super(FibuBuchForm, self).__init__(*args, **kwargs)

        last_of_last_month = DateHelper.getLastDayOfLastMonth(date.today())

        year_choices = []
        for year in InvoiceLog.objects.dates('invoice_date', 'year'):
            year_choices.append((year.year, str(year.year)))
        self.fields['year'] = forms.ChoiceField(choices=year_choices, required=True,
                                                initial=last_of_last_month.year, label="Jahr")

        month_choices = []
        for month in range(1, 13):
            month_choices.append((month, "%02d" % month))
        self.fields['month'] = forms.ChoiceField(choices=month_choices, required=True,
                                                 initial=last_of_last_month.month, label="Monat")

        fac_choices = []
        for fac in Facility.ex_objects.filter():  # .exclude(type__name='Kundenparkplatz'):
            fac_choices.append((str(fac.id), fac.name))
        self.fields['facility'] = forms.ChoiceField(choices=fac_choices, required=True, label="Einrichtung")

    class Meta:
        fieldsets = [('FiBu Export - Buchungsstapel herunterladen',
                      {'fields': ['month', 'year', 'facility'],
                      #'description': '',
                       'classes': ['module aligned'],
                       }),]

        row_attrs = {'month': {'class': 'form-row'}, }

