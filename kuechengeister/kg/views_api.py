# -*- coding: utf-8 -*-

from __future__ import with_statement
import json
from datetime import date, datetime
from itertools import chain

from django.http import HttpResponse, HttpResponseServerError
from django.core import serializers
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.log import getLogger

from settings import API_KEY, OVERLOAD_SITE, MEALTIMES
from kg.models import Facility, FacilitySubunit, Child, Order
from menus.models import Menu, Mealtime
from kg_utils.admin_helper import update_children_from_pos_data, update_orders_from_pos_data, update_orders_from_pos_data_v2

logger = getLogger(__name__)

@csrf_exempt
def pull_pos_data(request):
    """
    Retrieves all data necessary for the point of sale app and returns it in json format
    Data that is returned:
     facility_is = all: all data (used for USB-Stick sync)
     facility_id not set: active facilities, facility subunits
     facility_id set: like before + extended and flated version of menus, children with allergies, active orders
    """
    try:
        __check_api_key(request)

        facilities = Facility.ex_objects.filter(is_active=True)
        facility_subunits = FacilitySubunit.objects.filter(facility__in=facilities)

        # TODO: remove hack
        # Hack for Mensa Koechin: Show all mealtimes
        if OVERLOAD_SITE == 'mensakoechin' or OVERLOAD_SITE == 'sonnenblick':
            menus = Menu.objects.select_related('mealtimes').filter(date=date.today())
        else:
            menus = Menu.objects.select_related('mealtimes').filter(date=date.today(), meal__mealtime__id=MEALTIMES['lunch'])
        if OVERLOAD_SITE == 'schulrestaurant':
            menus = menus.filter(meal_id__in=[1,2])

        menus_extended = []
        for menu in menus:
            menus_extended.append({'pk': menu.id,
                                   'model': 'kg.menu_extended',
                                   'fields': {'date': menu.date,
                                              'meal': menu.description,
                                              'mealtime': menu.meal.name,
                                              'mealtime_short': menu.meal.name_pos_app,
                                              'mealtime_sort_key': menu.meal.sortKey,
                                              'mealtimecategory': menu.meal.mealtime.id}
                                   })

        # retrieve facility_id
        facility_id = ''
        if request.method == 'GET' and 'facility_id' in request.GET:
            facility_id = request.GET['facility_id']
        if request.method == 'POST' and 'facility_id' in request.POST:
            facility_id = request.POST['facility_id']

        # if facility_id is set, retrieve children and orders for selected facility
        if facility_id == 'all' or (facility_id.isdigit() and int(facility_id) > 0):
            # select all children (used for export to USB stick)
            children = Child.objects.select_related('Allergen')

            # if concrete facility_id is given, select only children from that facility
            # used for app requests per internet
            if facility_id != 'all':
                selected_facilities = facilities.filter(id=int(facility_id))
            else:
                selected_facilities = facilities
            children = children.filter(facility__in=selected_facilities)

            children_extended = []
            for child in children:
                qsChildAllergens = child.allergen.all()
                sAllergens = ''
                if child.customer.is_suspended == True:
                    sAllergens += 'GESPERRT'
                    if qsChildAllergens:
                        sAllergens += ' - '
                for oAllergen in qsChildAllergens:
                    sAllergens += '%s, ' % oAllergen.name
                if qsChildAllergens:
                    sAllergens = sAllergens[:-2]
                children_extended.append({'pk': child.id,
                                          'model': 'kg.child_extended',
                                          'fields': {'name': child.name,
                                                     'surname': child.surname,
                                                     'rfid_tag': child.rfid_tag,
                                                     'rfid_tag_update_time': child.rfid_tag_update_time,
                                                     'chip_number': child.chip_number,
                                                     'chip_number_update_time': child.chip_number_update_time,
                                                     'chip_deposit': child.chip_deposit,
                                                     'chip_deposit_update_time': child.chip_deposit_update_time,
                                                     'facility': child.facility.id,
                                                     'facility_subunit': child.facility_subunit.id,
                                                     'allergens': sAllergens}
                                          })

            order_result = Order.objects.filter(child__in=children, menu__date=date.today())
            orders = []
            for order in order_result:
                orders.append({'pk': order.id,
                               'model': 'kg.order',
                               'fields': {'menu': order.menu.id,
                                          'child': order.child.id,
                                          'quantity': order.quantity,
                                          'pickup_time': order.pickup_time}
                               })

            model_data = list(chain(facilities, facility_subunits))

            json_data = serializers.serialize('json', model_data)
            if children_extended:
                json_data = json_data[:-1] + ', ' + json.dumps(children_extended, cls=DjangoJSONEncoder)[1:]
            if orders:
                json_data = json_data[:-1] + ', ' + json.dumps(orders, cls=DjangoJSONEncoder)[1:]
            if menus_extended:
                json_data = json_data[:-1] + ', ' + json.dumps(menus_extended, cls=DjangoJSONEncoder)[1:]

        else:
            model_data = list(chain(facilities, facility_subunits))
            json_data = serializers.serialize('json', model_data)

        response = HttpResponse(json_data)
        response['Content-Type'] = 'application/json'
        if ((request.method == 'GET' and 'download' in request.GET and request.GET['download'] == '1') or
           (request.method == 'POST' and 'download' in request.POST and request.POST['download'] == '1')):
            response['Content-Disposition'] = ('attachment; '
                                               'filename=%s_Import_Essenausgabe.json' % date.today().strftime('%Y-%m-%d'))
        return response

    except Exception, message:
        logger.exception('Exception in kg.views_api.pull_pos_data()')
        return HttpResponseServerError()

@csrf_exempt
def push_pos_data(request):
    """
    Expects the updated children (updated rfid_tag) and updated orders data from the point of sale app
    Returns a list of all successful updated children and orders in json format
    """
    __check_api_key(request)

    if 'data' in request.POST:
        data = json.loads(request.POST['data'])
    else:
        data = {}

    try:
        updated_children = update_children_from_pos_data(data)
    except Exception, message:
        logger.exception('Exception when trying to update children after receiving terminal data via APIv2.')
        return HttpResponseServerError()

    try:
        updated_orders = update_orders_from_pos_data(data)
    except Exception, message:
        logger.exception('Exception when trying to update orders after receiving terminal data via APIv1.')
        return HttpResponseServerError()

    response_data = {'updated_children': updated_children,
                     'updated_orders': updated_orders}

    return HttpResponse(json.dumps(response_data), mimetype='application/json')


@csrf_exempt
def push_pos_data_v2(request):
    """
    Expects the updated children (updated rfid_tag) and updated orders data from the point of sale app
    Returns a list of all successful updated children and orders in json format
    Difference to v1: works with order.child_id, order.menu_id pair instead of order.id
    """
    __check_api_key(request)

    if 'data' in request.POST:
        data = json.loads(request.POST['data'])
    else:
        data = {}

    try:
        updated_children = update_children_from_pos_data(data)
    except Exception, message:
        logger.exception('Exception when trying to update children after receiving terminal data via APIv2.')
        return HttpResponseServerError()

    try:
        updated_orders = update_orders_from_pos_data_v2(data)
    except Exception, message:
        logger.exception('Exception when trying to update orders after receiving terminal data via APIv2.')
        return HttpResponseServerError()

    response_data = {'updated_children': updated_children,
                     'updated_orders': updated_orders}

    return HttpResponse(json.dumps(response_data), mimetype='application/json')


def __check_api_key(request):
    if request.user.is_staff:
        return True
    elif request.method == 'POST' and 'API_KEY' in request.POST and request.POST['API_KEY'] == API_KEY:
        return True
    else:
        raise PermissionDenied