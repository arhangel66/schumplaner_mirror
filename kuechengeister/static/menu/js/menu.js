var description_rules = {minlength: 3,
                         required: true};

var price_rules = {number: true,
                   required: true,
                   min: 0};

(function($) {
    function create_price_form(price, type){
        if(type == 'fixed'){
            var price_html = $('#divFixedPrices');
        }
        else if(type == 'variable'){
            var price_html = $('#divVariablePrices');
        }

        var form_group = $(document.createElement('div'));
        var label = $(document.createElement('label'));
        var price_input = $(document.createElement('input'));
        var price_group_id_input = $(document.createElement('input'));

        form_group.addClass('form-group col-sm-4');

        label.attr('for', 'inputPrice' + price.object_id);
        label.text(price.price_group);

        price_input.attr('type', 'text');
        price_input.attr('id', 'inputPrice' + price.object_id);
        price_input.attr('name', 'price_' + type + '_' + price.object_id);
        price_input.addClass('form-control');
        price_input.val(price.price);
        if(type == 'fixed'){
            price_input.attr('readonly', 'readonly');
            price_input.dblclick(function() {
                if ( $(this).attr('readonly') ) {
                    $(this).removeAttr('readonly');
                    $(this).rules('add', price_rules);
                }
                else {
                    $(this).attr('readonly', 'readonly');
                    $(this).rules('remove');
                }
            });
        }


        price_group_id_input.attr('type', 'hidden');
        price_group_id_input.attr('name', 'price_group_id_' + type);
        price_group_id_input.val(price.price_group_id);

        form_group.append(label);
        form_group.append(price_input);
        form_group.append(price_group_id_input);

        price_html.append(form_group);

    }
    function clear_modal_form(ignore_prices, ignore_can_be_ordered){
        $('#textareaDescription').val('');
        //$('#inputImportMeal').val('');
        $('#inputImportMeal').typeahead('val', '');

        var can_be_order = $('#checkboxCanBeOrdered').prop('checked');

        var $form = $('#mealModal form');
        $form.find('input[type="checkbox"]').prop('checked', false);
        $form.find('.form-group').removeClass('has-error');
        $form.find('.help-block').remove();

        if (ignore_can_be_ordered){
            $('#checkboxCanBeOrdered').prop('checked', can_be_order);
        } else {
            // If menu is imported from another, is_preparation will not be cleared.
            $('#inputMenuPreparation').val('');
            // Hide Reset and Delete buttons
            $('#buttonDeleteMenu, #buttonResetMenu').hide();
            $('#buttonDeleteMenu').data("inherited", false);
            $('#read-only-message').hide();
        }

        $("#checkboxDisabled").prop("checked", false);

        $('#divOrderCancelTimeLimit').collapse('hide');

        if(!ignore_prices){
            $('#divFixedPrices').html('').hide();
            $('#divVariablePrices').html('').hide();
            $form.find('.prices-wrapper').hide();
        }

        $('#mealModal #required-menu option:first-child').prop('selected', 1);
        //$('#mealModal form')[0].reset();
    }
    function fill_modal_form(item, fill_order_cancel_timelimit, ignore_prices, ignore_can_be_ordered){
        clear_modal_form(ignore_prices, ignore_can_be_ordered);

        //<div class="form-group">
        //<label for="inputVariablePrice">Price group</label>
        //<input type="text" name="variable_price" class="form-control" value="" />
        //<input type="hidden" name="variable_price_id" value="" />
        //</div>

        //Mark the item is menu prepration or not
        if (!item.id || item.is_preparation) {
            $('#inputMenuPreparation').val("1");
        }

        if(!ignore_prices) {
            $('.prices-wrapper').show();
            if (item.variable_prices.length > 0) {
                for (c = 0; c < item.variable_prices.length; c++) {
                    create_price_form(item.variable_prices[c], 'variable');
                }
                $('#divVariablePrices').show();
            }
            if (item.fixed_prices.length > 0) {
                for (c = 0; c < item.fixed_prices.length; c++) {
                    create_price_form(item.fixed_prices[c], 'fixed');
                }
                $('#divFixedPrices').show();
            }
        }

        if(item.description !== null){
            $('#textareaDescription').val(item.description);
        }

        if(!ignore_can_be_ordered){
            $('#checkboxCanBeOrdered').prop('checked', item.can_be_ordered);
            if (item.inherited) {
                // Show Reset button with the specific mealplan menu which is
                // inherited from a general one.
                $("#buttonResetMenu").show();
                $('#buttonDeleteMenu').data("inherited", true);
            }
            if (item.id && (!item.inherited || !item.disabled)) {
                // Show Delete button if item exists and deletable.
                $('#buttonDeleteMenu').show();
            }
            $('#buttonMealSave').show();
            if (item.in_past) {
                $('#buttonDeleteMenu, #buttonResetMenu').hide();
                $('#read-only-message').show();
            }
        }

        $("#checkboxDisabled").unbind("change").change(function () {
            if ($(this).prop("checked")) {
                $('#checkboxCanBeOrdered').prop("checked", false);
            }
        }).prop("checked", item.disabled);

        $('#checkboxCanBeOrdered').unbind("change").change(function() {
            if ($(this).prop("checked")) {
                $('#checkboxDisabled').prop("checked", false);
            }
        });

        if(fill_order_cancel_timelimit) {
            $('#inputOrderTimeLimit').data("DateTimePicker").date(item.order_timelimit.replace(/-/g, '.'));
            $('#inputCancelTimeLimit').data("DateTimePicker").date(item.cancel_timelimit.replace(/-/g, '.'));
        }

        for(c = 0; c < item.allergens.length; c++){
            $('#al'+item.allergens[c]).prop('checked', true);
        }

        for(c = 0; c < item.additionals.length; c++){
            $('#ad'+item.additionals[c]).prop('checked', true);
        }

        if (item.required_menu) {
            $('#mealModal #required-menu option[value="'+item.required_menu+'"]').prop('selected', 1);
        }
    }


    function handleMenuDropEvent( event, ui ) {
        var targetMenuId = $(this).find('a').data( 'id' );
        var targetMealId = $(this).find('a').data( 'mealid' );
        var targetDate = $(this).find('a').data( 'date' );
        var mealplanId = $(this).find('a').data('mealplanid');
        var isPreparation = ui.draggable.data('menupreparation');

        var sourceMenuId = ui.draggable.data( 'id' );

        if ( sourceMenuId && targetMenuId != sourceMenuId ) {
            $.get(window.location.href.split('?')[0] + 'save_droppable/', {'source_menu_id': sourceMenuId,
                                                                        'target_menu_id': targetMenuId,
                                                                        'target_meal_id': targetMealId,
                                                                        'target_date': targetDate,
                                                                        'mealplan_id': mealplanId,
                                                                        'is_preparation': isPreparation})
            .done(function() {
                location.reload(true);
            })
        }
    }


    function delete_menu(reset, orders_action) {
        var menu_id = $("#inputMenuId").val();
        var is_preparation = $("#inputMenuPreparation").val();
        var url = '/admin/menus/menu/delete/';
        if (reset) {
            url = '/admin/menus/menu/reset/';
        }
        var post_data = $("#menuForm").serialize();
        if (orders_action == "relink") {
            post_data += '&relink_orders=1';
        }
        $.post(url, post_data, function(data) {
            // Find the table cell to focus after reloading.
            var $td = $('#' + (is_preparation ? 'menu-preparation' : 'menu' ) + '-' + menu_id).closest("td");
            window.location.href = window.location.href.split('#')[0] + '#' + $td.attr("id");
            location.reload()
        });
    }

    // This will process transfered list of MenuImport to only keep which
    // match with current mealtime.
    function filterMenuImport(data) {
        var modalMealId = parseInt($('#inputMealId').data('mealtimeid'));
        var filteredData = []
        if (modalMealId != NaN && data.length > 0) {
            for (var i=0; i<data.length; i++) {
                if (data[i]['mealtimes'].indexOf(modalMealId) >= 0)
                    filteredData.push(data[i]);
            }
        }
        return filteredData;
    }

    $(document).ready(function () {
        // Setup datetime picker for order and cancel time limits
        var dtConf = {format: 'DD.MM.YYYY HH:mm', locale: 'de'}
        $('#inputOrderTimeLimit').datetimepicker(dtConf);
        $('#inputCancelTimeLimit').datetimepicker(dtConf);

        $("#mealModal").on('show.bs.modal', function (event) {
            clear_modal_form(false);
            // Get button that triggered the modal
            var button = $(event.relatedTarget);

            // Extract value from data-* attributes
            var titleData = button.data('title');
            var idData = button.data('id');
            var mealIdData = button.data('mealid');
            var mealTimeIdData = button.data('mealtimeid');
            var dateData = button.data('date');

            // Only show potential required menus of the same day
            var $requiredMenu = $(this).find('#required-menu');
            $requiredMenu.find('option:not(:first-child)').removeClass('visible');
            $requiredMenu.find('option[data-date='+dateData+']:not([value="'+idData+'"])').addClass('visible');

            $(this).find('.modal-title').text(titleData);

            // Always call server side data loading - new menu need calculated order and cancel time limits.
            $.ajax({
                url: '/admin/menus/menu/load-menu/?menu_id=' + idData + '&meal_id=' + mealIdData + '&menu_date=' + dateData,
                success: function (data) {
                    fill_modal_form(data, true, false, false);
                },
                dataType: 'json'
            });

            $('#inputMenuId').val(idData);
            $('#inputMealId').val(mealIdData).data('mealtimeid', mealTimeIdData);
            $('#inputMenuDate').val(dateData);
            $('#inputRedirectUrl').val(window.location.href)

            // Setup Typeahead remote source
            var searchMenusSource = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '/admin/menus/menu/search/?phrase=%QUERY&mealtime='+mealTimeIdData,
                    wildcard: '%QUERY'
                    // Not using transform as data has been filtered from back-end
                    // transform: filterMenuImport
                }
            });

            var inputImportMeal = $('#inputImportMeal').text('');
            inputImportMeal.typeahead('destroy');
            inputImportMeal.typeahead(
                {
                    hint: true,
                    highlight: true,
                    minLength: 0
                },
                {
                    name: 'menus',
                    source: searchMenusSource,
                    displayKey: 'description',
                    limit: 9999
                }
            );
        });

        //    Typeahead item selected event
        $('#inputImportMeal').on('typeahead:selected', function (evt, item) {
            if (!item.id) {
                item.id = $('#inputMenuId').val();
            }
            fill_modal_form(item, false, true, true);
        });

        // Menu modal form save button click event
        $("#buttonMealSave").on('click', function (event) {
            $('#menuForm input').each(function(){
                $(this).rules('remove');
            });

            if($('#checkboxCanBeOrdered').prop('checked')){
                $('#textareaDescription').rules('add', description_rules);

                $('#divVariablePrices input[type="text"], #divFixedPrices input[type="text"]').each(function(){
                    $(this).rules('add', price_rules);
                });
            }

            if(!$('#checkboxCanBeOrdered').prop('checked') || $('#menuForm').valid()){
                // Before serialize form change price variable and price fixed field names to equal names
                //   this is work around jquery validate plugin - it can't validate fields with equal names

                $('#divVariablePrices input[type="text"]').each(function(){
                    $(this).attr('name', 'price_variable');
                });

                $('#divFixedPrices input[type="text"]').each(function(){
                    $(this).attr('name', 'price_fixed');
                });

                var formSerialized = $("#menuForm").serialize();
                $.post("/admin/menus/menu/save/", formSerialized,
                    function(data) {
                        if (!data['success']) {
                            var msg = data['message'] || 'Fehler beim Speichern des Menüs';
                            alert(msg);
                        }
                        window.location.href = window.location.href.split('#')[0] + '#meal' + $('#inputMealId').val();
                        location.reload()
                    }
                ).fail(function(data) {
                        alert('Fehler beim Speichern des Menüs');
                    }
                );
            }
        });

        // Handle click event on reset button
        $("#buttonResetMenu").click(function() {
            bootbox.dialog({
                message: 'Sie sind dabei, das ausgewählte Menü zu löschen. ' +
                'Was wollen Sie mit den bestehenden Bestellungen tun?:' +
                '<div class="row"><div class="col-md-12">' +
                    '<div class="form-group">' +
                        '<div class="radio">' +
                            '<label for="relink-orders">' +
                                '<input type="radio" name="orders_action" value="relink" id="relink-orders" checked/>' +
                                'Alle Bestellungen in Globalen Speiseplan übernehmen.</label>' +
                        '</div>' +
                        '<div class="radio">' +
                            '<label for="delete-orders">' +
                                '<input type="radio" name="orders_action" value="delete" id="delete-orders"/>' +
                                'Bestehende Bestellungen löschen.</label>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>',
                title: "Menü zurücksetzen",
                buttons: {
                    close: {
                        label: "Abbrechen",
                        className: "btn-default",
                        callback: function () {}
                    },
                    confirm: {
                        label: "Bestätigen",
                        className: "btn-primary",
                        callback: function () {
                            // Get selected option.
                            var answer = $("input[name='orders_action']:checked").val()
                            delete_menu(true, answer);
                        }
                    }
                }
            });
        });

        // Handle delete event on reset button
        $("#buttonDeleteMenu").click(function() {
            var message = "Dieses Menü und alle dazugehörigen Bestellungen werden gelöscht. Fortfahren?";
            if ($(this).data("inherited")) {
                message = "Dieses Menü und alle dazugehörigen Bestellungen werden gelöscht. Fortfahren?"
            }
            bootbox.confirm(message, function (result) {
                if (result) {
                    delete_menu(false);
                }
            });
        });

        // Collapsable - change text on show
        $('#divOrderCancelTimeLimit').on('hidden.bs.collapse', function () {
            $('#aCollapseOrderCancel').html('Bestellfristen &#9662;');
        });
        $('#divOrderCancelTimeLimit').on('shown.bs.collapse', function () {
            $('#aCollapseOrderCancel').html('Bestellfristen &#9652;');
        });

        $('#menuForm').validate({
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('.draggable-menu-item').draggable( {
            containment: '#menu-table',
            cursor: 'move',
            snap: '.droppable-menu-item',
            helper: 'clone',
            opacity: 0.9,
            zIndex: 1000,
        } );
        $('.droppable-menu-item').droppable( {
            drop: handleMenuDropEvent,
            hoverClass: 'hovered',
        } );

        // Handle Release Meal Plan confirmation.
        $('button[name="release-btn"]').click(function() {
            var $btn = $(this);
            var mp_options = [];
            for (var i=0; i<prep_mealplans.length; i++) {
                mp_options.push({
                    value: prep_mealplans[i][0].toString(),
                    text: prep_mealplans[i][1],
                });
            }
            bootbox.prompt({
                title: "Wollen Sie folgende Speisepläne freigeben?",
                inputType: "checkbox",
                inputOptions: mp_options,
                callback: function (result) {
                    if (result == null || result.length == 0)
                        return;
                    $btn.siblings('input[name="release"]').val(1);
                    $btn.siblings('input[name="mealplans"]').val(result);
                    $btn.closest("form").submit();
                }
            }).on("shown.bs.modal", function(e) {
                $('.bootbox-form .bootbox-input-checkbox').prop('checked', true);
            });
            return false;
        });

        //Handle mealplan selecting
        $("#id_mealplan").change(function() {
            //Append mealplan param to url's query string.
            $(this).closest("form").submit();
        });

        //Show tooltips
        $('[data-toggle="tooltip"]').tooltip();
    });
})(django.jQuery);
