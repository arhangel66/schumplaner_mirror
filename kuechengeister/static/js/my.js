$(function() {
	
	
	//define populate function for meal <select>
	//val is the id with the meal category
	$.fn.populateMeals = function(val,mealtime) {
		
		var object = $(this);
		var options = [];
		$.getJSON('/api/meal', function(data) {
			  $.each(data, function(i, item) { 
			   if (item.category==val && item.mealtime == mealtime) {
				   //add the item to the meal selector
				   //$(this).append(new Option(item.description,item.id));
				   options.push('<option value="'+ item.id +'">'+ item.description +'</option>');
				  // alert(options[0]);
			   }
			  });
			  object.html(options.join(''));
			});
    }; 
	//define populate function for meal <select>
	//val is the id with the meal category
	$.fn.populateFSVE = function(mealtime) {
		
		var object = $(this);
		var options = [];
		$.getJSON('/api/meal', function(data) {
			  $.each(data, function(i, item) { 
			   if (item.mealtime == mealtime) {
				   //add the item to the meal selector
				   //alert(item.description);
				   //$(this).append(new Option(item.description,item.id));
				   options.push('<option value="'+ item.id +'">'+ item.description +'</option>');
				  // alert(options[0]);
			   }
			  });
			  object.html(options.join(''));
			});
    }; 
    
    $.fn.populateMealCategories = function(val) {
		var object = $(this);
		var options = [];
		$.getJSON('/api/mealcategory', function(data) {
			  $.each(data, function(i, item) { 
			   if (item.category==val) {
				   //add the item to the meal selector
				  // alert(item.description);
				   //alert(item.name)
				   //$(this).append(new Option(item.description,item.id));
				   options.push('<option value="'+ item.id +'">'+ item.name +'</option>');
				  // alert(options[0]);
			   }
			  });
			  object.html(options.join(''));
			});
    }; 
    
	//when mealcategory selector is changed, populate meal selector with relevant meals
    $('.mealcategory').change(function() {
    	$("#absenden").show();
    	$(this).siblings('.meal').show();
    	$(this).siblings('.meal').populateMeals(this.value,'MI');
    });
    
  //when mealcategory selector is changed, populate meal selector with relevant meals
    $('.editmeal').click(function() {
    	$(this).siblings('.mealcategory').show();
    	$(this).siblings('.mealcategory').populateMealCategories(this.value);
    });
    
    //when mealcategory selector is changed, populate meal selector with relevant meals
    $('.editfs').click(function() {
    	$("#absenden").show();
    	$(this).siblings('.meal').show();
    	$(this).siblings('.meal').populateFSVE('FS');
    });
  //when mealcategory selector is changed, populate meal selector with relevant meals
    $('.editve').click(function() {
    	$("#absenden").show();
    	$(this).siblings('.meal').show();
    	$(this).siblings('.meal').populateFSVE('VE');
    });
    
    //planadd: hide breakfast and vesper when facility type is S (school)
    $('#facility').change(function() {
    	
    	var facilityType = $('#facility option:selected').attr('facilityType');
    	if (facilityType == 'S') {
    		$('#frueh').hide(500);
    		$('#vesper').hide(500);
    	} else {
    		$('#frueh').show(500);
    		$('#vesper').show(500);
    		
    	}
    });
    
    
    
});

function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}