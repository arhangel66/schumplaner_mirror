/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: DE
 */
jQuery.extend(jQuery.validator.messages, {
    required: "Das Feld darf nicht leer sein.",
    remote: "Bitte korrigieren Sie Ihre Angaben",
    email: "Bitte geben Sie eine gültige E-Mail-Adresse ein.",
    url: "Bitte geben Sie eine gültige URL ein.",
    date: "Bitte geben Sie ein gültiges Datum ein.",
    dateISO: "Bitte geben Sie ein gültiges Datum (ISO) ein.",
    number: "Bitte geben Sie eine Zahl ein.",
    digits: "Bitte geben Sie nur Zahlen ein.",
    creditcard: "Bitte geben Sie eine gültige Kreditkartennummer ein.",
    equalTo: "Bitte gleichen Wert noch einmal eingeben.",
    maxlength: $.validator.format( "Bitte mehr als {0} Zeichen eingeben." ),
    minlength: $.validator.format( "Bitte höchstens {0} Zeichen eingeben." ),
    rangelength: $.validator.format( "Bitte geben Sie zwischen {0} und {1} Zeichen ein." ),
    range: $.validator.format( "Bitte geben Sie einen Wert zwischen {0} und {1} ein." ),
    max: $.validator.format( "Bitte Wert kleiner gleich {0} eingeben." ),
    min: $.validator.format( "Bitte Wert größer gleich {0} eingeben.")
});
