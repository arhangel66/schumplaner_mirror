var css_mperiods_selects = '.dynamic-meal_periods .field-meal > select';
var css_mdprices_selects = '.dynamic-form-mealdefaultprice select';

(function($) {
    $(document).ready(function() {
        disable_selects(true);

        // Ensure new pricegroup will have enough meals
        $('.add-handler.pricegroup').click(function() {
            var present_meals = get_present_meals();
            $pricegroup = $('.dynamic-form-pricegroup').last();
            // Populate data to each new default price
            $.each(present_meals, function(key, mealprice) {
                $pricegroup.find('.add-handler.mealdefaultprice').last().click();
            });
            // The new inline fields should be there, hopefully
            $pricegroup.find('.dynamic-form-mealdefaultprice').each(function(key, mealprice) {
                $(mealprice).find('select').val(present_meals[key]);
            });
        });

        // Re-enable the select inputs right before submitting form.
        // Otherwise, the disabled input won't be included inside POST data
        $('form#mealplan_form').submit(function(event) {
            disable_selects(false);
        });


        // Work-around to have this performed as last.
        setTimeout(function() {
            $('#meal_periods-group > div > fieldset > table > tbody > tr.add-row > td > a').click(function(e){
                $(css_mperiods_selects).unbind('change', handle_meal_change);
                $(css_mperiods_selects).change(handle_meal_change);
            });

            $('.dynamic-form-pricegroup').each(function(key, node) {
                var present_meals = get_present_meals();
                var $pricegroup = $(node);
                // Filter for missing meals
                var saved_meals = [];
                $pricegroup.find(css_mdprices_selects).each(function(key, meal_node){
                    saved_meals.push(parseInt($(meal_node).val()));
                });
                for (var i=0; i<present_meals.length; i++) {
                    if (saved_meals.indexOf(present_meals[i]) < 0 ) {
                        $pricegroup.find('.grp-add-item .add-handler.mealdefaultprice').click();
                        $pricegroup.find(css_mdprices_selects).last().val(present_meals[i]);
                    }
                }
            });
            $(css_mperiods_selects).change(handle_meal_change);
        }, 100);
    });

})(django.jQuery);

function handle_meal_change(e) {
    var mealperiod_id = e.target.id.split('-')[1];
    var mealperiod_meal = e.target.value;
    django.jQuery('.dynamic-form-pricegroup').each(function(key, node) {
        var pricegroup_id = node.id.split('_set')[1];
        var meal_select_id = '#id_pricegroup_set-'+pricegroup_id+'-mealdefaultprice_set-'+mealperiod_id+'-meal';
        var $meal_select = django.jQuery(meal_select_id);
        if ($meal_select.length == 0) {
            // Add for all pricegroups at once
            django.jQuery('.dynamic-form-pricegroup .inline-group .grp-add-item > a').click();
            var $meal_select = django.jQuery(meal_select_id);
        }
        $meal_select.val(mealperiod_meal);
    });
}

function get_present_meals() {
    var present_meals = [];
    django.jQuery(css_mperiods_selects).each(function(key, node) {
        var mid = parseInt(django.jQuery(node).val());
        if (mid != NaN && mid > 0 && present_meals.indexOf(mid) < 0) {
            present_meals.push(mid);
        }
    });
    return present_meals;
}

function disable_selects(state) {
    // The form will not valid until selects are enabled.
    django.jQuery('.meal.field-meal > select').attr('disabled', state);
    django.jQuery('#meal_periods-group .has_original .field-meal > select').attr('disabled', state);
}


