from django.conf.urls.defaults import patterns, url


urlpatterns = patterns('reports.views',
    url(r'^$', 'index', name='index'),
    url(r'^day-report/$', 'day_report', name='report_day'),
    url(r'^week-report/$', 'week_report', name='report_week'),
    url(r'^week-compact-report/$', 'week_report_compact',
        name='report_week_compact'),
    url(r'^month-report-stats/$', 'month_report_stats',
        name='report_month_stats'),
    url(r'^somic/$', 'somic'),
    url(r'^hytera/$', 'hytera'),
    url(r'^diamant3-buch/$', 'diamant3_export_buch', name='diamant3_export_buch'),
    url(r'^diamant3-stamm/$', 'diamant3_export_stamm', name='diamant3_export_stamm'),
    url(r'^datev-buch/$', 'datev_export_buch', name='datev_export_buch'),
    url(r'^datev-stamm/$', 'datev_export_stamm', name='datev_export_stamm'),
)
