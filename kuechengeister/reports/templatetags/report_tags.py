from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def insert_page_break():
    return getattr(settings, 'PDF_PAGE_BREAK', '')
