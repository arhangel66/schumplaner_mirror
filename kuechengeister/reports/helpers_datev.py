# -*- coding: utf-8 -*-
from datetime import date, datetime
import StringIO

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from kg_utils import ucsv as csv
from kg.models import Facility, InvoiceLog, Customer, Child, PayType
from settings import OVERLOAD_SITE


def generate_datev_export_buch(oDateFrom, oDateTo, iFacilityId):
    """ Generate CSV with invoices data in DATEV format version 510 """

    oOutput = StringIO.StringIO()
    oCsvWriter = csv.writer(oOutput, delimiter=';', encoding='iso-8859-1')

    qsInvoices = InvoiceLog.objects.filter(invoice_date__gte=oDateFrom, invoice_date__lte=oDateTo)
    if iFacilityId > 0:
        qsInvoices = qsInvoices.filter(facility_id=iFacilityId)
        
    # default values
    sBerater = u''
    sMandant = u''
    sSachkontennummernlaenge = '4'  # Standard
    sBUSchluessel = u''
    sKonto = u''
    sKOST1 = u''
    sKOST2 = u''
    sFestschreibung = u''
    sGegenkontoSource = 'oInvoice.child.customer.extra_attribute'
    bHeaderStandard = True  # Header for Schmalkalden is different

    # Abraxas
    if OVERLOAD_SITE == 'abraxas':
        sBerater = u'391241'
        sMandant = u'20945'
        sSachkontennummernlaenge = '5'
        sBUSchluessel = u''
        sKonto = u'43000'
        # configure facility specific values
        if iFacilityId == 1:
            sKOST1 = u'11'
        elif iFacilityId == 2:
            sKOST1 = u'20'
        else:
            raise Exception('Einrichtung unbekannt. Bitte konfigurieren!')

    # Beinrode
    if OVERLOAD_SITE == 'beinrode':
        sBerater = u'446520'
        sMandant = u'39227'
        sBUSchluessel = u''
        sKonto = u'8403'

    # Lebenshilfe Oschatz
    if OVERLOAD_SITE == 'oschatz':
        sBerater = u'92287'
        sMandant = u'12'
        sBUSchluessel = u''
        sKonto = u' 8302'
        sKOST1 = u'130'
        sGegenkontoSource = 'oInvoice.child.cust_nbr'

    # Schmalkalden
    if OVERLOAD_SITE == 'schmalkalden':
        sBerater = u'317461'
        sMandant = u'10000'
        sBUSchluessel = u''
        sKonto = u'4212'
        sKOST1 = u'300'
        sFestschreibung = u'0'  # keine Festschreibung
        bHeaderStandard = False


    # Feldbeschreibung
    lFeldbeschreibung = [
          u'EXTF',  # 1 DATEV-Format-KZ (EXTF = für Dateiformate, die von externen Programmen erstellt wurden, DTVF = für DATEV reserviert)
          u'510',  # 2 Versionsnummer
          u'21',  # 3 Datenkategorie (21 = Buchungsstapel, 16 = Debitoren-/Kreditoren)
          u'Buchungsstapel',  # 4 Formatname
          u'7',  # 5 Formatversion (Buchungsstapel = 7, Debitoren/Kreditoren = 4)
          u'%s' % datetime.now().strftime('%Y%m%d%H%M%S'),  # 6 Erzeugt am
          u'',  # 7 Importiert (Das Header-Feld darf nicht gefüllt werden.)
          u'SV',  # 8 Herkunft (Beim Import wird das Herkunfts-Kennzeichen durch „SV“ (= Stapelverarbeitung) ersetzt.)
          u'',  # 9 Exportiert von
          u'',  # 10 Importiert von
          u'%s' % sBerater,  # 11 Berater
          u'%s' % sMandant,  # 12 Mandant
          u'%d0101' % oDateFrom.year,  # 13 WJ-Beginn (Wirtschaftsjahresbeginn Format: JJJJMMTT)
          u'%s' % sSachkontennummernlaenge,  # 14 Sachkontennummernlänge
          # Die nachfolgenden Header-Felder sind ausschließlich für Buchungsstapel relevant, jedoch:
          # Der Aufbau des Headers ist über alle Datenkategorien identisch, die Anzahl der Felder ist immer gleich.
          u'%s' % oDateFrom.strftime('%Y%m%d'),  # 15 Datum von
          u'%s' % oDateTo.strftime('%Y%m%d'),  # 16 Datum bis
          u'',  # 17 Bezeichnung (Bezeichnung des Buchungsstapels)
          u'',  # 18 Diktatkürzel
          u'1',  # 19 Buchungstyp (1 = Finanzbuchführung, 2 = Jahresabschluss)
          u'',  # 20 Rechnungslegungszweck (0 oder leer = vom Rechnungslegungszweck unabhängig)
          u'',  # 21 Festschreibung (leer = nicht definiert; wird ab Jahreswechselversion 2016/2017 automatisch festgeschrieben; 0 = keine Festschreibung, 1 = Festschreibung)
          u'EUR',  # 22 WKZ (Währungskennzeichen)
          u'',  # 23 reserviert
          u'',  # 24 Derivats- kennzeichen
          u'',  # 25 reserviert
          u'',  # 26 reserviert
          u'',  # 27 SKR
          u'',  # 28 Branchenlösung-Id
          u'',  # 29 reserviert
          u'',  # 30 reserviert
          u'',  # 31 Anwendungsinformation
          ]
    oCsvWriter.writerow(lFeldbeschreibung)

    # Kopfzeile
    if bHeaderStandard:
        lKopfzeile = [
            u'Beleginfo',  # 1
            u'Soll/-Haben-Kennzeichen',  # 2
            u'WKZ Umsatz',  # 3
            u'Kurs',  # 4
            u'Basisumsatz',  # 5
            u'WKZ Basisumsatz',  # 6
            u'Konto',  # 7
            u'Gegenkonto (ohne BU-Schlüssel)',  # 8
            u'BU-Schlüssel',  # 9
            u'Belegdatum',  # 10
            u'Belegfeld 1',  # 11
            u'Belegfeld 2',  # 12
            u'Skonto',  # 13
            u'Buchungstext',  # 14
            u'Postensperre',  # 15
            u'Diverse Adressnummer',  # 16
            u'Geschäftspartnerbank',  # 17
            u'Sachverhalt',  # 18
            u'Zinssperre',  # 19
            u'Beleglink',  # 20
            u'Beleginfo - Art 1',  # 21
            u'Beleginfo - Inhalt 1',  # 22
            u'Beleginfo - Art 2',  # 23
            u'Beleginfo - Inhalt 2',  # 24
            u'Beleginfo - Art 3',  # 25
            u'Beleginfo - Inhalt 3',  # 26
            u'Beleginfo - Art 4',  # 27
            u'Beleginfo - Inhalt 4',  # 28
            u'Beleginfo - Art 5',  # 29
            u'Beleginfo - Inhalt 5',  # 30
            u'Beleginfo - Art 6',  # 31
            u'Beleginfo - Inhalt 6',  # 32
            u'Beleginfo - Art 7',  # 33
            u'Beleginfo - Inhalt 7',  # 34
            u'Beleginfo - Art 8',  # 35
            u'Beleginfo - Inhalt 8',  # 36
            u'KOST1 - Kostenstelle',  # 37
            u'KOST2 - Kostenstelle',  # 38
            u'KOST-Menge',  # 39
            u'EU-Mitgliedstaat u.USt-IdNr.',  # 40
            u'EU-Steuersatz',  # 41
            u'Abw. Versteuerungsart',  # 42
            u'Sachverhalt L+L',  # 43
            u'Funktionsergänzung L+L',  # 44
            u'BU 49 Hauptfunktionstyp',  # 45
            u'BU 49 Hauptfunktionsnummer',  # 46
            u'BU 49 Funktionsergänzung',  # 47
            u'Zusatzinformation - Art 1',  # 48
            u'Zusatzinformation - Inhalt 1',  # 49
            u'Zusatzinformation - Art 2',  # 50
            u'Zusatzinformation - Inhalt 2',  # 51
            u'Zusatzinformation - Art 3',  # 52
            u'Zusatzinformation - Inhalt 3',  # 53
            u'Zusatzinformation - Art 4',  # 54
            u'Zusatzinformation - Inhalt 4',  # 55
            u'Zusatzinformation - Art 5',  # 56
            u'Zusatzinformation - Inhalt 5',  # 57
            u'Zusatzinformation - Art 6',  # 58
            u'Zusatzinformation - Inhalt 6',  # 59
            u'Zusatzinformation - Art 7',  # 60
            u'Zusatzinformation - Inhalt 7',  # 61
            u'Zusatzinformation - Art 8',  # 62
            u'Zusatzinformation - Inhalt 8',  # 63
            u'Zusatzinformation - Art 9',  # 64
            u'Zusatzinformation - Inhalt 9',  # 65
            u'Zusatzinformation - Art 10',  # 66
            u'Zusatzinformation - Inhalt 10',  # 67
            u'Zusatzinformation - Art 11',  # 68
            u'Zusatzinformation - Inhalt 11',  # 69
            u'Zusatzinformation - Art 12',  # 70
            u'Zusatzinformation - Inhalt 12',  # 71
            u'Zusatzinformation - Art 13',  # 72
            u'Zusatzinformation - Inhalt 13',  # 73
            u'Zusatzinformation - Art 14',  # 74
            u'Zusatzinformation - Inhalt 14',  # 75
            u'Zusatzinformation - Art 15',  # 76
            u'Zusatzinformation - Inhalt 15',  # 77
            u'Zusatzinformation - Art 16',  # 78
            u'Zusatzinformation - Inhalt 16',  # 79
            u'Zusatzinformation - Art 17',  # 80
            u'Zusatzinformation - Inhalt 17',  # 81
            u'Zusatzinformation - Art 18',  # 82
            u'Zusatzinformation - Inhalt 18',  # 83
            u'Zusatzinformation - Art 19',  # 84
            u'Zusatzinformation - Inhalt 19',  # 85
            u'Zusatzinformation - Art 20',  # 86
            u'Zusatzinformation - Inhalt 20',  # 87
            u'Stück',  # 88
            u'Gewicht',  # 89
            u'Zahlweise',  # 90 (OPOS-Informationen kommunal 1 = Lastschrift, 2 = Mahnung, 3 = Zahlung)
            u'Forderungsart',  # 91
            u'Veranlagungsjahr',  # 92
            u'Zugeordnete Fälligkeit',  # 93
            u'Skontotyp',  # 94
            u'Auftragsnummer',  # 95
            u'Buchungstyp',  # 96
            u'USt-Schlüssel (Anzahlungen)',  # 97
            u'EU-Mitgliedstaat (Anzahlungen)',  # 98
            u'Sachverhalt L+L (Anzahlungen)',  # 99
            u'EU-Steuersatz (Anzahlungen)',  # 100
            u'Erlöskonto (Anzahlungen)',  # 101
            u'Herkunft-Kz',  # 102
            u'Leerfeld',  # 103
            u'KOST-Datum',  # 104
            u'SEPA - Mandatsreferenz',  # 105
            u'Skontosperre',  # 106
            u'Gesellschaftername',  # 107
            u'Beteiligtennummer',  # 108
            u'Identifikationsnummer',  # 109
            u'Zeichnernummer',  # 110
            u'Postensperre bis',  # 111
            u'Bezeichnung SoBil-Sachverhalt',  # 112
            u'Kennzeichen SoBil-Buchung',  # 113
            u'Festschreibung',  # 114
            u'Leistungsdatum',  # 115
            u'Datum Zuord. Steuerperiode',  # 116
             ]
    else:
        # alternative header for Schmalkalden (maybe older/newer version of format)
        lKopfzeile = [
            u'Umsatz (ohne Soll/Haben-Kz)',  # 1
            u'Soll/Haben-Kennzeichen',
            u'WKZ Umsatz'
            ,u'Kurs',
            u'Basis-Umsatz',
            u'WKZ Basis-Umsatz',
            u'Konto',
            u'Gegenkonto (ohne BU-Schlüssel)',
            u'BU-Schlüssel',
            u'Belegdatum',  # 10
            u'Belegfeld 1',
            u'Belegfeld 2',
            u'Skonto',
            u'Buchungstext',
            u'Postensperre',
            u'Diverse Adressnummer',
            u'Geschäftspartnerbank',
            u'Sachverhalt',
            u'Zinssperre',
            u'Beleglink',  # 20
            u'Beleginfo - Art 1',
            u'Beleginfo - Inhalt 1',
            u'Beleginfo - Art 2',
            u'Beleginfo - Inhalt 2',
            u'Beleginfo - Art 3',
            u'Beleginfo - Inhalt 3',
            u'Beleginfo - Art 4',
            u'Beleginfo - Inhalt 4',
            u'Beleginfo - Art 5',
            u'Beleginfo - Inhalt 5',  # 30
            u'Beleginfo - Art 6',
            u'Beleginfo - Inhalt 6',
            u'Beleginfo - Art 7',
            u'Beleginfo - Inhalt 7',
            u'Beleginfo - Art 8',
            u'Beleginfo - Inhalt 8',
            u'KOST1 - Kostenstelle',
            u'KOST2 - Kostenstelle',
            u'Kost-Menge',
            u'EU-Land u. UStID',  # 40
            u'EU-Steuersatz',
            u'Abw. Versteuerungsart',
            u'Sachverhalt L+L',
            u'Funktionsergänzung L+L',
            u'BU 49 Hauptfunktionstyp',
            u'BU 49 Hauptfunktionsnummer',
            u'BU 49 Funktionsergänzung',
            u'Zusatzinformation - Art 1',
            u'Zusatzinformation- Inhalt 1',
            u'Zusatzinformation - Art 2',  # 50
            u'Zusatzinformation- Inhalt 2',
            u'Zusatzinformation - Art 3',
            u'Zusatzinformation- Inhalt 3',
            u'Zusatzinformation - Art 4',
            u'Zusatzinformation- Inhalt 4',
            u'Zusatzinformation - Art 5',
            u'Zusatzinformation- Inhalt 5',
            u'Zusatzinformation - Art 6',
            u'Zusatzinformation- Inhalt 6',
            u'Zusatzinformation - Art 7',  # 60
            u'Zusatzinformation- Inhalt 7',
            u'Zusatzinformation - Art 8',
            u'Zusatzinformation- Inhalt 8',
            u'Zusatzinformation - Art 9',
            u'Zusatzinformation- Inhalt 9',
            u'Zusatzinformation - Art 10',
            u'Zusatzinformation- Inhalt 10',
            u'Zusatzinformation - Art 11',
            u'Zusatzinformation- Inhalt 11',
            u'Zusatzinformation - Art 12',  # 70
            u'Zusatzinformation- Inhalt 12',
            u'Zusatzinformation - Art 13',
            u'Zusatzinformation- Inhalt 13',
            u'Zusatzinformation - Art 14',
            u'Zusatzinformation- Inhalt 14',
            u'Zusatzinformation - Art 15',
            u'Zusatzinformation- Inhalt 15',
            u'Zusatzinformation - Art 16',
            u'Zusatzinformation- Inhalt 16',
            u'Zusatzinformation - Art 17',  # 80
            u'Zusatzinformation- Inhalt 17',
            u'Zusatzinformation - Art 18',
            u'Zusatzinformation- Inhalt 18',
            u'Zusatzinformation - Art 19',
            u'Zusatzinformation- Inhalt 19',
            u'Zusatzinformation - Art 20',
            u'Zusatzinformation- Inhalt 20',
            u'Stück',
            u'Gewicht',
            u'Zahlweise',  # 90
            u'Forderungsart',
            u'Veranlagungsjahr',
            u'Zugeordnete Fälligkeit',
            u'Skontotyp',
            u'Auftragsnummer',
            u'Buchungstyp',
            u'USt-Schlüssel (Anzahlungen)',
            u'EU-Land (Anzahlungen)',
            u'Sachverhalt L+L (Anzahlungen)',
            u'EU-Steuersatz (Anzahlungen)',  # 100
            u'Erlöskonto (Anzahlungen)',
            u'Herkunft-Kz',
            u'Buchungs GUID',
            u'KOST-Datum',
            u'SEPA-Mandatsreferenz',
            u'Skontosperre',
            u'Gesellschaftername',
            u'Beteiligtennummer',
            u'Identifikationsnummer',
            u'Zeichnernummer',  # 110
            u'Postensperre bis',
            u'Bezeichnung SoBil-Sachverhalt',
            u'Kennzeichen SoBil-Buchung',
            u'Festschreibung',
            u'Leistungsdatum',
            u'Datum Zuord. Steuerperiode' # 116
            ]
    oCsvWriter.writerow(lKopfzeile)

    for oInvoice in qsInvoices:

        sInvoiceTotal = str(oInvoice.invoice_total).replace(',', '').replace('.', ',')

        # Belegfeld1
        sBelegfeld1 = u'%s' % oInvoice.invoice_number
        if OVERLOAD_SITE == 'abraxas':
            sBelegfeld1 = u'%s' % oInvoice.invoice_date.strftime('%Y-%m')

        # Buchungstext
        sBuchungstext = u'Rechnung Nr. %s %s, %s %02d/%d' % (
            oInvoice.invoice_number, oInvoice.child.customer.name,
            oInvoice.child.customer.surname, oInvoice.invoice_date.month, oInvoice.invoice_date.year)
        if OVERLOAD_SITE == 'abraxas':
            sBuchungstext = u'%s, %s' % (oInvoice.child.name, oInvoice.child.surname)

        # Buchungen
        lRow = [
            u'%s' % str(oInvoice.invoice_total).replace(u'.', u','),  # 1 Beleginfo - Betrag 10
            u'H', # 2 Soll/-Haben-Kennzeichen - Text 1
            u'',  # 3 WKZ Umsatz - Text 3
            u'',  # 4 Kurs - Zahl 4
            u'',  # 5 Basisumsatz - Betrag 10
            u'',  # 6 WKZ Basisumsatz - Text 3
            u'%s' % sKonto,  # 7 Konto - Konto 9
            u'%s' % eval(sGegenkontoSource),  # 8 Gegenkonto (ohne BU-Schlüssel)
            u'%s' % sBUSchluessel,  # 9 BU-Schlüssel - Text 2 (Steuerschlüssel und/oder Berichtigungsschlüssel)
            u'%s' % oInvoice.invoice_date.strftime('%d%m'),  # 10 Belegdatum - Datum 4 (Belegdatum (Format: TTMM))
            u'%s' % sBelegfeld1,  # 11 Belegfeld 1 - Text 12
            u'',  # 12 Belegfeld 2 - Text 12
            u'',  # 13 Skonto - Betrag 8
            u'%s' % sBuchungstext[:60],  # 14 Buchungstext - Text 60
            u'',  # Postensperre',  # 15
            u'',  # Diverse Adressnummer',  # 16
            u'',  # Geschäftspartnerbank',  # 17
            u'',  # Sachverhalt',  # 18
            u'',  # Zinssperre',  # 19
            u'',  # Beleglink',  # 20
            u'',  # Beleginfo - Art 1',  # 21
            u'',  # Beleginfo - Inhalt 1',  # 22
            u'',  # Beleginfo - Art 2',  # 23
            u'',  # Beleginfo - Inhalt 2',  # 24
            u'',  # Beleginfo - Art 3',  # 25
            u'',  # Beleginfo - Inhalt 3',  # 26
            u'',  # Beleginfo - Art 4',  # 27
            u'',  # Beleginfo - Inhalt 4',  # 28
            u'',  # Beleginfo - Art 5',  # 29
            u'',  # Beleginfo - Inhalt 5',  # 30
            u'',  # Beleginfo - Art 6',  # 31
            u'',  # Beleginfo - Inhalt 6',  # 32
            u'',  # Beleginfo - Art 7',  # 33
            u'',  # Beleginfo - Inhalt 7',  # 34
            u'',  # Beleginfo - Art 8',  # 35
            u'',  # Beleginfo - Inhalt 8',  # 36
            u'%s' % sKOST1,  # 37 KOST1 - Kostenstelle - Text 8
            u'%s' % sKOST2,  # 38 KOST2 - Kostenstelle - Text 8
            u'',  # 39 KOST-Menge
            u'',  # 40 EU-Mitgliedstaat u.USt-IdNr.
            u'',  # EU-Steuersatz',  # 41
            u'',  # Abw. Versteuerungsart',  # 42
            u'',  # Sachverhalt L+L',  # 43
            u'',  # Funktionsergänzung L+L',  # 44
            u'',  # BU 49 Hauptfunktionstyp',  # 45
            u'',  # BU 49 Hauptfunktionsnummer',  # 46
            u'',  # BU 49 Funktionsergänzung',  # 47
            u'',  # Zusatzinformation - Art 1',  # 48
            u'',  # Zusatzinformation - Inhalt 1',  # 49
            u'',  # Zusatzinformation - Art 2',  # 50
            u'',  # Zusatzinformation - Inhalt 2',  # 51
            u'',  # Zusatzinformation - Art 3',  # 52
            u'',  # Zusatzinformation - Inhalt 3',  # 53
            u'',  # Zusatzinformation - Art 4',  # 54
            u'',  # Zusatzinformation - Inhalt 4',  # 55
            u'',  # Zusatzinformation - Art 5',  # 56
            u'',  # Zusatzinformation - Inhalt 5',  # 57
            u'',  # Zusatzinformation - Art 6',  # 58
            u'',  # Zusatzinformation - Inhalt 6',  # 59
            u'',  # Zusatzinformation - Art 7',  # 60
            u'',  # Zusatzinformation - Inhalt 7',  # 61
            u'',  # Zusatzinformation - Art 8',  # 62
            u'',  # Zusatzinformation - Inhalt 8',  # 63
            u'',  # Zusatzinformation - Art 9',  # 64
            u'',  # Zusatzinformation - Inhalt 9',  # 65
            u'',  # Zusatzinformation - Art 10',  # 66
            u'',  # Zusatzinformation - Inhalt 10',  # 67
            u'',  # Zusatzinformation - Art 11',  # 68
            u'',  # Zusatzinformation - Inhalt 11',  # 69
            u'',  # Zusatzinformation - Art 12',  # 70
            u'',  # Zusatzinformation - Inhalt 12',  # 71
            u'',  # Zusatzinformation - Art 13',  # 72
            u'',  # Zusatzinformation - Inhalt 13',  # 73
            u'',  # Zusatzinformation - Art 14',  # 74
            u'',  # Zusatzinformation - Inhalt 14',  # 75
            u'',  # Zusatzinformation - Art 15',  # 76
            u'',  # Zusatzinformation - Inhalt 15',  # 77
            u'',  # Zusatzinformation - Art 16',  # 78
            u'',  # Zusatzinformation - Inhalt 16',  # 79
            u'',  # Zusatzinformation - Art 17',  # 80
            u'',  # Zusatzinformation - Inhalt 17',  # 81
            u'',  # Zusatzinformation - Art 18',  # 82
            u'',  # Zusatzinformation - Inhalt 18',  # 83
            u'',  # Zusatzinformation - Art 19',  # 84
            u'',  # Zusatzinformation - Inhalt 19',  # 85
            u'',  # Zusatzinformation - Art 20',  # 86
            u'',  # Zusatzinformation - Inhalt 20',  # 87
            u'',  # Stück',  # 88
            u'',  # Gewicht',  # 89
            u'', # 90 Zahlweise - Zahl 2 (OPOS-Informationen kommunal 1 = Lastschrift, 2 = Mahnung, 3 = Zahlung)
            u'',  # Forderungsart',  # 91
            u'',  # Veranlagungsjahr',  # 92
            u'%s' % oInvoice.pay_until.strftime('%d%m%Y'),  # Zugeordnete Fälligkeit',  # 93
            u'',  # Skontotyp',  # 94
            u'',  # Auftragsnummer',  # 95
            u'',  # Buchungstyp',  # 96
            u'',  # USt-Schlüssel (Anzahlungen)',  # 97
            u'',  # EU-Mitgliedstaat (Anzahlungen)',  # 98
            u'',  # Sachverhalt L+L (Anzahlungen)',  # 99
            u'',  # EU-Steuersatz (Anzahlungen)',  # 100
            u'',  # Erlöskonto (Anzahlungen)',  # 101
            u'',  # Herkunft-Kz',  # 102
            u'',  # Leerfeld',  # 103
            u'',  # KOST-Datum',  # 104
            u'',  # SEPA - Mandatsreferenz',  # 105
            u'',  # Skontosperre',  # 106
            u'',  # Gesellschaftername',  # 107
            u'',  # Beteiligtennummer',  # 108
            u'',  # Identifikationsnummer',  # 109
            u'',  # Zeichnernummer',  # 110
            u'',  # Postensperre bis',  # 111
            u'',  # Bezeichnung SoBil-Sachverhalt',  # 112
            u'',  # Kennzeichen SoBil-Buchung',  # 113
            u'%s' % sFestschreibung,  # Festschreibung',  # 114
            u'',  # Leistungsdatum',  # 115
            u''  # Datum Zuord. Steuerperiode - 116
            ]
        oCsvWriter.writerow(lRow)

    oOutput.seek(0)
    return oOutput


def generate_datev_export_stamm(iFacilityId):
    """ Generate CSV with customer/debtor data in DATEV format v510"""

    oOutput = StringIO.StringIO()
    oCsvWriter = csv.writer(oOutput, delimiter=';', encoding='iso-8859-1')

    oFacility = Facility.objects.get(pk=iFacilityId)

    # default
    sBerater = u''
    sMandant = u''
    sSachkontennummernlaenge = '4'
    sExportType = 'CUSTOMER'  # the type of export: 'CUSTOMER' or 'CHILD'

    # Abraxas
    if OVERLOAD_SITE == 'abraxas':
        sBerater = u'391241'
        sMandant = u'20945'
        sSachkontennummernlaenge = '5'

    # Beinrode
    if OVERLOAD_SITE == 'beinrode':
        sBerater = u'446520'
        sMandant = u'39227'

    # Lebenshilfe Oschatz
    if OVERLOAD_SITE == 'oschatz':
        sBerater = u'92287'
        sMandant = u'12'
        sExportType = 'CHILD'

    # Schmalkalden
    if OVERLOAD_SITE == 'schmalkalden':
        sBerater = u'317461'
        sMandant = u'10000'

    sContent = u''

    # Feldbeschreibung
    lFeldbeschreibung = [
        u'EXTF',  # 1 DATEV-Format-KZ (EXTF = für Dateiformate, die von externen Programmen erstellt wurden, DTVF = für DATEV reserviert)
        u'510',  # 2 Versionsnummer
        u'16',  # 3 Datenkategorie (21 = Buchungsstapel, 16 = Debitoren-/Kreditoren)
        u'Debitoren/Kreditoren',  # 4 Formatname
        u'4',  # 5 Formatversion (Buchungsstapel = 7, Debitoren/Kreditoren = 4)
        u'%s' % datetime.now().strftime('%Y%m%d%H%M%S'),  # 6 Erzeugt am
        u'',  # 7 Importiert (Das Header-Feld darf nicht gefüllt werden.)
        u'SV',  # 8 Herkunft (Beim Import wird das Herkunfts-Kennzeichen durch „SV“ (= Stapelverarbeitung) ersetzt.)
        u'',  # 9 Exportiert von
        u'',  # 10 Importiert von
        u'%s' % sBerater,  # 11 Berater
        u'%s' % sMandant,  # 12 Mandant
        u'%d0101' % date.today().year,  # 13 WJ-Beginn (Wirtschaftsjahresbeginn Format: JJJJMMTT)
        u'%s' % sSachkontennummernlaenge,  # 14 Sachkontennummernlänge
        # Die nachfolgenden Header-Felder sind ausschließlich für Buchungsstapel relevant, jedoch:
        # Der Aufbau des Headers ist über alle Datenkategorien identisch, die Anzahl der Felder ist immer gleich.
        u'',  # 15 Datum von
        u'',  # 16 Datum bis
        u'',  # 17 Bezeichnung (Bezeichnung des Buchungsstapels)
        u'',  # 18 Diktatkürzel
        u'',  # 19 Buchungstyp (1 = Finanzbuchführung, 2 = Jahresabschluss)
        u'',  # 20 Rechnungslegungszweck (0 oder leer = vom Rechnungslegungszweck unabhängig)
        u'',  # 21 Festschreibung (leer = nicht definiert; wird ab Jahreswechselversion 2016/2017 automatisch festgeschrieben; 0 = keine Festschreibung, 1 = Festschreibung)
        u'',  # 22 WKZ (Währungskennzeichen)
        u'',  # 23 reserviert
        u'',  # 24 Derivats- kennzeichen
        u'',  # 25 reserviert
        u'',  # 26 reserviert
        u'',  # 27 SKR
        u'',  # 28 Branchenlösung-Id
        u'',  # 29 reserviert
        u'',  # 30 reserviert
        u''  # 31 Anwendungsinformation
        ]
    oCsvWriter.writerow(lFeldbeschreibung)

    # Kopfzeile
    lKopfzeile = [
        u'Konto',  # 1
        u'Name (Adressatentyp Unternehmen)',  # 2
        u'Unternehmensgegenstand',  # 3
        u'Name (Adressatentyp natürl.Person)',  # 4  - Text 30
        u'Vorname (Adressatentyp natürl.Person)',  # 5  - Text 30
        u'Name (Adressatentyp keine Angabe)',  # 6 Name (Adressatentyp keine Angabe) - Text 50
        u'Adressatentyp',  # 7  - Text 1 (0 = keine Angabe, 1 = natürliche Person, 2 = Unternehmen)
        u'Kurzbezeichnung',  # 8  - Text 15
        u'EU-Mitgliedstaat',  # 9  - Text 2
        u'EU-USt-IdNr.',  # 10  - Text 13
        u'Anrede',  # 11  - Text 30
        u'Titel/Akad. Grad',  # 12  - Text 25
        u'Adelstitel',  # 13  - Text 15
        u'Namensvorsatz',  # 14  - Text 14
        u'Adressart',  # 15  - Text 3 (STR = Straße, PF = Postfach, GK = Großkunde)
        u'Straße',  # 16  - Text 36
        u'Postfach',  # 17  - Text 10
        u'Postleitzahl',  # 18  - Text 10
        u'Ort',  # 19  - Text 30
        u'Land',  # 20  - Text 2
        u'Versandzusatz',  # 21  - Text 50
        u'Adresszusatz',  # 22  - Text 36
        u'Abweichende Anrede',  # 23  - Text 30
        u'Abw. Zustellbezeichnung 1',  # 24  - Text 50
        u'Abw. Zustellbezeichnung 2',  # 25  - Text 36
        u'Kennz. Korrespondenzadresse',  # 26  - Zahl 1 (1 = Kennzeichnung Korrespondenzadresse)
        u'Adresse Gültig von',  # 27  - Datum 8 TTMMJJJJ
        u'Adresse Gültig bis',  # 28  - Datum 8 TTMMJJJJ
        u'Telefon',  # 29  - Text 80
        u'Bemerkung (Telefon)',  # 30  - Text 40
        u'Telefon Geschäftsleitung',  # 31  - Text 60
        u'Bemerkung (Telefon GL)',  # 32  - Text 40
        u'E-Mail',  # 33  - Text 60
        u'Bemerkung (E-Mail)',  # 34  - Text 40
        u'Internet',  # 35  - Text 60
        u'Bemerkung (Internet)',  # 36  - Text 40
        u'Fax',  # 37  - Text 60
        u'Bemerkung (Fax)',  # 38  - Text 40
        u'Sonstige',  # 39  - Text 60
        u'Bemerkung (Sonstige)',  # 40  - Text 40
        u'Bankleitzahl 1',  # 41  - Text 8
        u'Bankbezeichnung 1',  # 42  - Text 30
        u'Bankkonto-Nummer 1',  # 43  - Text 10
        u'Länderkennzeichen 1',  # 44  - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
        u'IBAN-Nr. 1',  # 45  - Text 34
        u'Leerfeld',  # 46  - Text 1
        u'SWIFT-Code 1',  # 47  - Text 11 (Beachten Sie, dass für Zahlungen und Lastschrif- ten bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
        u'Abw. Kontoinhaber 1',  # 48  - Text 70
        u'Kennz. Hauptbankverb. 1',  # 49  - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1 = Ja, 0 = Nein, Nur eine Bankverbindung eines Debitoren/Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
        u'Bankverb. 1 Gültig von',  # 50  - Datum 8 (Format: TTMMJJJJ)
        u'Bankverb. 1 Gültig bis',  # 51  - Datum 8 (Format: TTMMJJJJ)
        u'Bankleitzahl 2',  # 52  - Text 8
        u'Bankbezeichnung 2',  # 53  - Text 30
        u'Bankkonto-Nummer 2',  # 54  - Text 10
        u'Länderkennzeichen 2',  # 55  - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
        u'IBAN-Nr. 2',  # 56  - Text 34
        u'Leerfeld',  # 57  - Text 1
        u'SWIFT-Code 2',  # 58  - Text 11 (Beachten Sie, dass für Zahlungen und Lastschrif- ten bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
        u'Abw. Kontoinhaber 2',  # 59  - Text 70
        u'Kennz. HauptBankverb. 2',  # 60  - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1 = Ja, 0 = Nein, Nur eine Bankverbindung eines Debitoren/ Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
        u'Bankverb. 2 Gültig von',  # 61  - Datum 8 (Format: TTMMJJJJ)
        u'Bankverb.2 Gültig bis',  # 62  - Datum 8 (Format: TTMMJJJJ)
        u'Bankleitzahl 3',  # 63  - Text 8
        u'Bankbezeichnung 3',  # 64  - Text 30
        u'Bankkonto-Nummer 3',  # 65  - Text 10
        u'Länderkennzeichen 3',  # 66  - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
        u'IBAN-Nr. 3',  # 67  - Text 34
        u'Leerfeld',  # 68  - Text 1
        u'SWIFT-Code 3',  # 69  - Text 11 (Beachten Sie, dass für Zahlungen und Lastschriften bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
        u'Abw. Kontoinhaber 3',  # 70  - Text 70
        u'Kennz. Haupt-Bankverb. 3',  # 71  - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1 = Ja, 0 = Nein, Nur eine Bankverbindung eines Debitoren/ Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
        u'Bankverb. 3 Gültig von',  # 72  - Datum 8 (Format: TTMMJJJJ)
        u'Bankverb. 3 Gültig bis',  # 73  - Datum 8 (Format: TTMMJJJJ)
        u'Bankleitzahl 4',  # 74  - Text 8
        u'Bankbezeichnung 4',  # 75  - Text 30
        u'Bankonto-Nummer 4',  # 76  - Text 10
        u'Länderkennzeichen 4',  # 77  - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
        u'IBAN-Nr. 4',  # 78  - Text 34
        u'Leerfeld',  # 79  - Text 1
        u'SWIFT-Code 4 ',  # 80 - Text 11 (Beachten Sie, dass für Zahlungen und Lastschriften bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
        u'Abw. Kontoinhaber 4',  # 81  - Text 70
        u'Kennz. Haupt-Bankverb. 4',  # 82  - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1= Ja, 0= Nein, Nur eine Bankverbindung eines Debitoren/ Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
        u'Bankverb. 4 Gültig von',  # 83  - Datum 8 (Format: TTMMJJJJ)
        u'Bankverb. 4 Gültig bis',  # 84  - Datum 8 (Format: TTMMJJJJ)
        u'Bankleitzahl 5',  # 85  - Text 8
        u'Bankbezeichnung 5',  # 86  - Text 30
        u'Bankkonto-Nummer 5',  # 87  - Text 10
        u'Länderkennzeichen 5',  # 88  - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
        u'IBAN-Nr. 5',  # 89  - Text 34
        u'Leerfeld',  # 90  - Text 1
        u'SWIFT-Code 5',  # 91  - Text 11 (Beachten Sie, dass für Zahlungen und Lastschrif- ten bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
        u'Abw. Kontoinhaber 5',  # 92  - Text 70
        u'Kennz. Haupt-Bankverb. 5',  # 93  - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1 = Ja, 0 = Nein, Nur eine Bankverbindung eines Debitoren/ Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
        u'Bankverb. 5 Gültig von',  # 94  - Datum 8 (Format: TTMMJJJJ)
        u'Bankverb. 5 Gültig bis',  # 95  - Datum 8 (Format: TTMMJJJJ)
        u'Leerfeld',  # 96  - Zahl 3
        u'Briefanrede',  # 97  - Text 100
        u'Grußformel',  # 98  - Text 50
        u'Kundennummer',  # 99  - Text 15 (Kann nicht geändert werden, wenn zentralisierte Geschäftspartner verwendet werden.)
        u'Steuernummer',  # 100  - Text 20
        u'Sprache',  # 101  - Zahl 2 (1 = deutsch, 4 = französisch, 5 = englisch, 10 = spanisch, 19 = italienisch)
        u'Ansprechpartner',  # 102  - Text 40
        u'Vertreter',  # 103  - Text 40
        u'Sachbearbeiter',  # 104  - Text 40
        u'Diverse-Konto',  # 105  - Zahl 1 (0 = Nein, 1 = Ja)
        u'Ausgabeziel',  # 106  - Zahl 1 (1 = Druck, 2 = Telefax, 3 = E-Mail)
        u'Währungssteuerung',  # 107  - Zahl 1 (0 = Zahlungen in Eingabewährung, 2 = Ausgabe in EUR)
        u'Kreditlimit (Debitor)',  # 108  - Betrag 10 (Nur für Debitoren gültig Beispiel: 1.123.123.123)
        u'Zahlungsbedingung',  # 109  - Zahl 3 (Eine gespeicherte Zahlungsbedingung kann hier einem Geschäftspartner zugeordnet werden.)
        u'Fälligkeit in Tagen (Debitor)',  # 110  - Zahl 3 (Nur für Debitoren gültig)
        u'Skonto in Prozent (Debitor)',  # 111  - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
        u'Kreditoren-Ziel 1 (Tage)',  # 112  - Zahl 2 (Nur für Kreditoren gültig)
        u'Kreditoren-Skonto 1 (%)',  # 113  - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
        u'Kreditoren-Ziel 2 (Tage)',  # 114  - Zahl 2 (Nur für Kreditoren gültig)
        u'Kreditoren-Skonto 2 (%)',  # 115  - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
        u'Kreditoren-Ziel 3 Brutto (Tage)',  # 116  - Zahl 3 (Nur für Kreditoren gültig)
        u'Kreditoren-Ziel 4 (Tage)',  # 117  - Zahl 2 (Nur für Kreditoren gültig)
        u'Kreditoren-Skonto 4 (%)',  # 118  - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
        u'Kreditoren-Ziel 5 (Tage)',  # 119  - Zahl 2 (Nur für Kreditoren gültig)
        u'Kreditoren-Skonto 5 (%)',  # 120  - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
        u'Mahnung',  # 121  - Zahl 1 (0 = Keine Angaben, 1 = 1. Mahnung, 2 = 2. Mahnung, 3 = 1. + 2. Mahnung, 4 = 3. Mahnung, 5 = (nicht vergeben), 6 = 2. + 3. Mahnung, 7 = 1., 2. + 3. Mahnung, 9 = keine Mahnung)
        u'Kontoauszug',  # 122  - Zahl 1 (1 = Kontoauszug für alle Posten, 2 = Auszug nur dann, wenn ein Posten mahnfähig ist, 3 = Auszug für alle mahnfälligen Posten, 9 = kein Kontoauszug)
        u'Mahntext 1',  # 123  - Zahl 1 (Leer = keinen Mahntext ausgewählt, 1 = Textgruppe 1, ..., 9 = Textgruppe 9)
        u'Mahntext 2',  # 124  - Zahl 1 (Leer = keinen Mahntext ausgewählt, 1 = Textgruppe 1, ..., 9 = Textgruppe 9)
        u'Mahntext 3',  # 125  - Zahl 1 (Leer = keinen Mahntext ausgewählt, 1 = Textgruppe 1, ..., 9 = Textgruppe 9)
        u'Kontoauszugstext',  # 126  - Zahl 1 (Leer = kein Kontoauszugstext ausgewählt, 1 = Kontoauszugstext 1, ..., 8 = Kontoauszugstext 8, 9 = Kein Kontoauszugstext)
        u'Mahnlimit Betrag',  # 127  - Betrag 5 (Beispiel: 12.123,12)
        u'Mahnlimit %',  # 128 Mahnlimit % - Zahl 2
        u'Zinsberechnung',  # 129  - Zahl 1
        u'Mahnzinssatz 1',  # 130  - Zahl 2
        u'Mahnzinssatz 2',  # 131  - Zahl 2
        u'Mahnzinssatz 3',  # 132  - Zahl 2
        u'Lastschrift',  # 133  - Text 1
        u'Verfahren',  # 134  - Text 1
        u'Mandantenbank',  # 135  - Zahl 4
        u'Zahlungsträger',  # 136  - Text 1
        u'Indiv. Feld 1',  # 137  - Text 40
        u'Indiv. Feld 2',  # 138  - Text 40
        u'Indiv. Feld 3',  # 139  - Text 40
        u'Indiv. Feld 4',  # 140  - Text 40
        u'Indiv. Feld 5',  # 141  - Text 40
        u'Indiv. Feld 6',  # 142  - Text 40
        u'Indiv. Feld 7',  # 143  - Text 40
        u'Indiv. Feld 8',  # 144  - Text 40
        u'Indiv. Feld 9',  # 145  - Text 40
        u'Indiv. Feld 10',  # 146 - Text 40
        u'Indiv. Feld 11',  # 147  - Text 40
        u'Indiv. Feld 12',  # 148  - Text 40
        u'Indiv. Feld 13',  # 149  - Text 40
        u'Indiv. Feld 14',  # 150  - Text 40
        u'Indiv. Feld 15',  # 151  - Text 40
        u'Abweichende Anrede (Rechnungsadresse)',  # 152  - Text 30
        u'Adressart (Rechnungsadresse)',  # 153  - Text 3
        u'Straße (Rechnungsadresse)',  # 154  - Text 36
        u'Postfach (Rechnungsadresse)',  # 155  - Text 10
        u'Postleitzahl (Rechnungsadresse)',  # 156  - Text 10
        u'Ort (Rechnungsadresse)',  # 157  - Text 30
        u'Land (Rechnungsadresse)',  # 158  - Text 2
        u'Versandzusatz (Rechnungsadresse)',  # 159  - Text 50
        u'Adresszusatz (Rechnungsadresse)',  # 160  - Text 36
        u'Abw. Zustellbezeichnung 1 (Rechnungsadresse)',  # 161  - Text 50
        u'Abw. Zustell- bezeichnung 2 (Rechnungsadresse)',  # 162  - Text 36
        u'Adresse Gültig von (Rechnungsadresse)',  # 163  - Datum 8
        u'Adresse Gültig bis (Rechnungsadresse)',  # 164  - Datum 8
        u'Bankleitzahl 6',  # 165  - Text 8
        u'Bankbezeichnung 6',  # 166  - Text 30
        u'Bankkonto-Nummer 6',  # 167  - Text 10
        u'Länderkennzeichen 6',  # 168  - Text 2
        u'IBAN-Nr. 6',  # 169  - Text 34
        u'Leerfeld',  # 170  - Text 1
        u'SWIFT-Code 6',  # 171  - Text 11
        u'Abw. Kontoinhaber 6',  # 172  - Text 70
        u'Kennz. Haupt-Bankverb. 6',  # 173  - Zahl 1
        u'Bankverb. 6 Gültig von',  # 174  - Datum 8
        u'Bankverb. 6 Gültig bis',  # 175  - Datum 8
        u'Bankleitzahl 7',  # 176  - Text 8
        u'Bankbezeichnung 7',  # 177  - Text 30
        u'Bankkonto-Nummer 7',  # 178  - Text 10
        u'Länderkennzeichen 7',  # 179  - Text 2
        u'IBAN-Nr. 7',  # 180  - Text 34
        u'Leerfeld',  # 181  - Text 1
        u'SWIFT-Code 7',  # 182  - Text 11
        u'Abw. Kontoinhaber 7',  # 183  - Text 70
        u'Kennz. Haupt-Bankverb. 7',  # 184  - Zahl 1
        u'Bankverb. 7 Gültig von',  # 185  - Datum 8
        u'Bankverb. 7 Gültig bis',  # 186  - Datum 8
        u'Bankleitzahl 8',  # 187  - Text 8
        u'Bankbezeichnung 8',  # 188  - Text 30
        u'Bankkonto-Nummer 8',  # 189  - Text 10
        u'Länderkennzeichen 8',  # 190  - Text 2
        u'IBAN-Nr. 8',  # 191  - Text 34
        u'Leerfeld',  # 192  - Text 1
        u'SWIFT-Code 8',  # 193  - Text 11
        u'Abw. Kontoinhaber 8',  # 194  - Text 70
        u'Kennz. Haupt-Bankverb. 8',  # 195  - Zahl 1
        u'Bankverb. 8 Gültig von',  # 196  - Datum 8
        u'Bankverb. 8 Gültig bis',  # 197  - Datum 8
        u'Bankleitzahl 9',  # 198  - Text 8
        u'Bankbezeichnung 9',  # 199  - Text 30
        u'Bankkonto-Nummer 9',  # 200  - Text 10
        u'Länderkennzeichen 9',  # 201  - Text 2
        u'IBAN-Nr. 9',  # 202  - Text 34
        u'Leerfeld',  # 203  - Text 1
        u'SWIFT-Code 9',  # 204  - Text 11
        u'Abw. Kontoinhaber 9',  # 205  - Text 70
        u'Kennz. Haupt-Bankverb. 9',  # 206  - Zahl1
        u'Bankverb. 9 Gültig von',  # 207  - Datum 8
        u'Bankverb. 9 Gültig bis',  # 208  - Datum 8
        u'Bankleitzahl 10',  # 209  - Text 8
        u'Bankbezeichnung 10',  # 210  - Text 30
        u'Bankkonto-Nummer 10',  # 211  - Text 10
        u'Länderkennzeichen 10',  # 212  - Text 2
        u'IBAN-Nr. 10',  # 213  - Text 34
        u'Leerfeld',  # 214  - Text 1
        u'SWIFT-Code 10',  # 215  - Text 11
        u'Abw. Kontoinhaber 10',  # 216  - Text 70
        u'Kennz. Haupt-Bankverb. 10',  # 217  - Zahl 1
        u'Bankverb. 10 Gültig von',  # 218  - Datum 8
        u'Bankverb. 10 Gültig von',  # 219  - Datum 8
        u'Nummer Fremdsystem',  # 220  - Text 15
        u'Insolvent',  # 221  - Zahl 1
        u'SEPA-Mandatsreferenz 1',  # 222  - Text 35
        u'SEPA-Mandatsreferenz 2',  # 223  - Text 35
        u'SEPA-Mandatsreferenz 3',  # 224  - Text 35
        u'SEPA-Mandatsreferenz 4',  # 225  - Text 35
        u'SEPA-Mandatsreferenz 5',  # 226  - Text 35
        u'SEPA-Mandatsreferenz 6',  # 227  - Text 35
        u'SEPA-Mandatsreferenz 7',  # 228  - Text 35
        u'SEPA-Mandatsreferenz 8',  # 229  - Text 35
        u'SEPA-Mandatsreferenz 9',  # 230  - Text 35
        u'SEPA-Mandatsreferenz 10',  # 231  - Text 35
        u'Verknüpftes OPOS-Konto',  # 232  - Konto 9
        u'Mahnsperre bis',  # 233  - Datum 8
        u'Lastschriftsperre bis',  # 234  - Datum 8
        u'Zahlungssperre bis',  # 235  - Datum 8
        u'Gebührenberechnung',  # 236  - Zahl 1
        u'Mahngebühr 1',  # 237  - Zahl 2
        u'Mahngebühr 2',  # 238  - Zahl 2
        u'Mahngebühr 3',  # 239  - Zahl 2
        u'Pauschalenberechnung',  # 240  - Zahl 1
        u'Verzugspauschale 1',  # 241  - Zahl 3
        u'Verzugspauschale 2',  # 242  - Zahl 3
        u'Verzugspauschale 3'  # 243  - Zahl 3
        ]
    oCsvWriter.writerow(lKopfzeile)

    if sExportType == 'CHILD':
        qsExportItems = Child.objects.distinct().filter(facility__id=iFacilityId).order_by('cust_nbr')
    else:
        qsExportItems = Customer.objects.distinct().filter(child__facility__id=iFacilityId).order_by('extra_attribute')

    for oItem in qsExportItems:
        oCustomer = oItem
        sGegenkonto = oCustomer.extra_attribute
        sAnrede = oCustomer.title
        if sExportType == 'CHILD':
            oCustomer = Customer.objects.get(child=oItem)
            sGegenkonto = oItem.cust_nbr
            sAnrede = u''
        sSepaMandateId = ''
        sBlz = ''
        sBankKontoNr = ''
        sKontoInhaber = ''
        sBic = ''
        sIban = ''
        sBankbezeichnung = ''
        sLaenderkennzeichen = ''
        sSepaCreatedAt = ''
        sKontoTyp = '1'  # Überweisungsbank
        if oCustomer.pay_type.id == PayType.DEBIT:
            try:
                oSepaMan = oCustomer.sepamandate_set.get(is_valid=True)
                sSepaMandateId = oSepaMan.mandate_id
                sBlz = oSepaMan.iban[4:12]
                sBankKontoNr = oSepaMan.iban[12:]
                sKontoInhaber = oSepaMan.name
                sBic = oSepaMan.bic
                sIban = oSepaMan.iban
                sBankbezeichnung = oSepaMan.bank
                sLaenderkennzeichen = oSepaMan.iban[0:2]
                sSepaCreatedAt = oSepaMan.created_at.strftime('%d%m%Y')
            except (ObjectDoesNotExist, MultipleObjectsReturned):
                pass

        lRow = [
            # 1 Konto
            u'%s' % sGegenkonto,
            # 2 Name (Adressatentyp Unternehmen) - Text
            u'',
            # 3 Unternehmensgegenstand - Text
            u'',
            # 4 Name (Adressatentyp natürl.Person) - Text 30
            u'%s' % oItem.name[:30].strip(),
            # 5 Vorname (Adressatentyp natürl.Person) - Text 30
            u'%s' % oItem.surname[:30].strip(),
            # 6 Name (Adressatentyp keine Angabe) - Text 50
            u'',
            # 7 Adressatentyp - Text 1 (0 = keine Angabe, 1 = natürliche Person, 2 = Unternehmen)
            u'1',
            # 8 Kurzbezeichnung - Text 15
            u'',
            # 9 EU-Mitgliedstaat - Text 2
            u'',
            # 10 EU-USt-IdNr. - Text 13
            u'',
            # 11 Anrede - Text 30
            u'%s' % sAnrede,
            # 12 Titel/Akad. Grad - Text 25
            u'',
            # 13 Adelstitel - Text 15
            u'',
            # 14 Namensvorsatz - Text 14
            u'',
            # 15 Adressart - Text 3 (STR = Straße, PF = Postfach, GK = Großkunde)
            u'STR',
            # 16 Straße - Text 36
            u'%s' % oCustomer.street[:36],
            # 17 Postfach - Text 10
            u'',
            # 18 Postleitzahl - Text 10
            u'%s' % oCustomer.zip_code,
            # 19 Ort - Text 30
            u'%s' % oCustomer.city[:30],
            # 20 Land - Text 2
            u'DE',
            # 21 Versandzusatz - Text 50
            u'',
            # 22 Adresszusatz - Text 36
            u'',
            # 23 Abweichende Anrede - Text 30
            u'',
            # 24 Abw. Zustellbezeichnung 1 - Text 50
            u'',
            # 25 Abw. Zustellbezeichnung 2 - Text 36
            u'',
            # 26 Kennz. Korrespondenzadresse - Zahl 1 (1 = Kennzeichnung Korrespondenzadresse)
            u'1',
            # 27 Adresse Gültig von - Datum 8 TTMMJJJJ
            u'',
            # 28 Adresse Gültig bis - Datum 8 TTMMJJJJ
            u'',
            # 29 Telefon - Text 80
            u'%s' % oCustomer.phone,
            # 30 Bemerkung (Telefon) - Text 40
            u'',
            # 31 Telefon Geschäftsleitung - Text 60
            u'',
            # 32 Bemerkung (Telefon GL) - Text 40
            u'',
            # 33 E-Mail - Text 60
            u'%s' % oCustomer.email,
            # 34 Bemerkung (E-Mail) - Text 40
            u'',
            # 35 Internet - Text 60
            u'',
            # 36 Bemerkung (Internet) - Text 40
            u'',
            # 37 Fax - Text 60
            u'',
            # 38 Bemerkung (Fax) - Text 40
            u'',
            # 39 Sonstige - Text 60
            u'',
            # 40 Bemerkung (Sonstige) - Text 40
            u'',
            # 41 Bankleitzahl 1 - Text 8
            u'',
            # 42 Bankbezeichnung 1 - Text 30
            u'%s' % sBankbezeichnung[0:30].strip(),
            # 43 Bankkonto-Nummer 1 - Text 10
            u'',
            # 44 Länderkennzeichen 1 - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
            u'%s' % sLaenderkennzeichen,
            # 45 IBAN-Nr. 1 - Text 34
            u'%s' % sIban,
            # 46 Leerfeld - Text 1
            u'',
            # 47 SWIFT-Code 1 - Text 11 (Beachten Sie, dass für Zahlungen und Lastschriften bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
            u'%s' % sBic,
            # 48 Abw. Kontoinhaber 1 - Text 70
            u'%s' % sKontoInhaber,
            # 49 Kennz. Hauptbankverb. 1 - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1 = Ja, 0 = Nein, Nur eine Bankverbindung eines Debitoren/Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
            u'%s' % ('' if sIban == '' else '1'),
            # 50 Bankverb. 1 Gültig von - Datum 8 (Format: TTMMJJJJ)
            u'%s' % sSepaCreatedAt,
            # 51 Bankverb. 1 Gültig bis - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 52 Bankleitzahl 2 - Text 8
            u'',
            # 53 Bankbezeichnung 2 - Text 30
            u'',
            # 54 Bankkonto-Nummer 2 - Text 10
            u'',
            # 55 Länderkennzeichen 2 - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
            u'',
            # 56 IBAN-Nr. 2 - Text 34
            u'',
            # 57 Leerfeld - Text 1
            u'',
            # 58 SWIFT-Code 2 - Text 11 (Beachten Sie, dass für Zahlungen und Lastschrif- ten bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
            u'',
            # 59 Abw. Kontoinhaber 2 - Text 70
            u'',
            # 60 Kennz. HauptBankverb. 2 - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1 = Ja, 0 = Nein, Nur eine Bankverbindung eines Debitoren/ Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
            u'',
            # 61 Bankverb. 2 Gültig von - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 62 Bankverb.2 Gültig bis - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 63 Bankleitzahl 3 - Text 8
            u'',
            # 64 Bankbezeichnung 3 - Text 30
            u'',
            # 65 Bankkonto-Nummer 3 - Text 10
            u'',
            # 66 Länderkennzeichen 3 - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
            u'',
            # 67 IBAN-Nr. 3 - Text 34
            u'',
            # 68 Leerfeld - Text 1
            u'',
            # 69 SWIFT-Code 3 - Text 11 (Beachten Sie, dass für Zahlungen und Lastschriften bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
            u'',
            # 70 Abw. Kontoinhaber 3 - Text 70
            u'',
            # 71 Kennz. Haupt-Bankverb. 3 - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1 = Ja, 0 = Nein, Nur eine Bankverbindung eines Debitoren/ Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
            u'',
            # 72 Bankverb. 3 Gültig von - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 73 Bankverb. 3 Gültig bis - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 74 Bankleitzahl 4 - Text 8
            u'',
            # 75 Bankbezeichnung 4 - Text 30
            u'',
            # 76 Bankonto-Nummer 4 - Text 10
            u'',
            # 77 Länderkennzeichen 4 - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
            u'',
            # 78 IBAN-Nr. 4 - Text 34
            u'',
            # 79 Leerfeld - Text 1
            u'',
            # 80 SWIFT-Code 4 - Text 11 (Beachten Sie, dass für Zahlungen und Lastschriften bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
            u'',
            # 81 Abw. Kontoinhaber 4 - Text 70
            u'',
            # 82 Kennz. Haupt-Bankverb. 4 - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1= Ja, 0= Nein, Nur eine Bankverbindung eines Debitoren/ Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
            u'',
            # 83 Bankverb. 4 Gültig von - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 84 Bankverb. 4 Gültig bis - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 85 Bankleitzahl 5 - Text 8
            u'',
            # 86 Bankbezeichnung 5 - Text 30
            u'',
            # 87 Bankkonto-Nummer 5 - Text 10
            u'',
            # 88 Länderkennzeichen 5 - Text 2 (ISO-Code beachten, (siehe Info-Datenbank, Dokument Nr. 1080169))
            u'',
            # 89 IBAN-Nr. 5 - Text 34
            u'',
            # 90 Leerfeld - Text 1
            u'',
            # 91 SWIFT-Code 5 - Text 11 (Beachten Sie, dass für Zahlungen und Lastschrif- ten bis zur Installation der Programm-DVD 8.3 (Auslieferung vorauss. Januar 2015) BLZ und/ oder BIC noch erforderlich ist.)
            u'',
            # 92 Abw. Kontoinhaber 5 - Text 70
            u'',
            # 93 Kennz. Haupt-Bankverb. 5 - Zahl 1 (Kennzeichnung als Haupt-Bankverbindung 1 = Ja, 0 = Nein, Nur eine Bankverbindung eines Debitoren/ Kreditoren kann als Haupt-Bankverbindung gekennzeichnet werden.)
            u'',
            # 94 Bankverb. 5 Gültig von - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 95 Bankverb. 5 Gültig bis - Datum 8 (Format: TTMMJJJJ)
            u'',
            # 96 Leerfeld - Zahl 3
            u'',
            # 97 Briefanrede - Text 100
            u'',
            # 98 Grußformel - Text 50
            u'',
            # 99 Kundennummer - Text 15 (Kann nicht geändert werden, wenn zentralisierte Geschäftspartner verwendet werden.)
            u'',
            # 100 Steuernummer - Text 20
            u'',
            # 101 Sprache - Zahl 2 (1 = deutsch, 4 = französisch, 5 = englisch, 10 = spanisch, 19 = italienisch)
            u'',
            # 102 Ansprechpartner - Text 40
            u'',
            # 103 Vertreter - Text 40
            u'',
            # 104 Sachbearbeiter - Text 40
            u'',
            # 105 Diverse-Konto - Zahl 1 (0 = Nein, 1 = Ja)
            u'',
            # 106 Ausgabeziel - Zahl 1 (1 = Druck, 2 = Telefax, 3 = E-Mail)
            u'',
            # 107 Währungssteuerung - Zahl 1 (0 = Zahlungen in Eingabewährung, 2 = Ausgabe in EUR)
            u'',
            # 108 Kreditlimit (Debitor) - Betrag 10 (Nur für Debitoren gültig Beispiel: 1.123.123.123)
            u'',
            # 109 Zahlungsbedingung - Zahl 3 (Eine gespeicherte Zahlungsbedingung kann hier einem Geschäftspartner zugeordnet werden.)
            u'',
            # 110 Fälligkeit in Tagen (Debitor) - Zahl 3 (Nur für Debitoren gültig)
            u'',
            # 111 Skonto in Prozent (Debitor) - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
            u'',
            # 112 Kreditoren-Ziel 1 (Tage) - Zahl 2 (Nur für Kreditoren gültig)
            u'',
            # 113 Kreditoren-Skonto 1 (%) - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
            u'',
            # 114 Kreditoren-Ziel 2 (Tage) - Zahl 2 (Nur für Kreditoren gültig)
            u'',
            # 115 Kreditoren-Skonto 2 (%) - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
            u'',
            # 116 Kreditoren-Ziel 3 Brutto (Tage) - Zahl 3 (Nur für Kreditoren gültig)
            u'',
            # 117 Kreditoren-Ziel 4 (Tage) - Zahl 2 (Nur für Kreditoren gültig)
            u'',
            # 118 Kreditoren-Skonto 4 (%) - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
            u'',
            # 119 Kreditoren-Ziel 5 (Tage) - Zahl 2 (Nur für Kreditoren gültig)
            u'',
            # 120 Kreditoren-Skonto 5 (%) - Zahl 2 (Nur für Debitoren gültig, Beispiel: 12,12)
            u'',
            # 121 Mahnung - Zahl 1 (0 = Keine Angaben, 1 = 1. Mahnung, 2 = 2. Mahnung, 3 = 1. + 2. Mahnung, 4 = 3. Mahnung, 5 = (nicht vergeben), 6 = 2. + 3. Mahnung, 7 = 1., 2. + 3. Mahnung, 9 = keine Mahnung)
            u'',
            # 122 Kontoauszug - Zahl 1 (1 = Kontoauszug für alle Posten, 2 = Auszug nur dann, wenn ein Posten mahnfähig ist, 3 = Auszug für alle mahnfälligen Posten, 9 = kein Kontoauszug)
            u'',
            # 123 Mahntext 1 - Zahl 1 (Leer = keinen Mahntext ausgewählt, 1 = Textgruppe 1, ..., 9 = Textgruppe 9)
            u'',
            # 124 Mahntext 2 - Zahl 1 (Leer = keinen Mahntext ausgewählt, 1 = Textgruppe 1, ..., 9 = Textgruppe 9)
            u'',
            # 125 Mahntext 3 - Zahl 1 (Leer = keinen Mahntext ausgewählt, 1 = Textgruppe 1, ..., 9 = Textgruppe 9)
            u'',
            # 126 Kontoauszugstext - Zahl 1 (Leer = kein Kontoauszugstext ausgewählt, 1 = Kontoauszugstext 1, ..., 8 = Kontoauszugstext 8, 9 = Kein Kontoauszugstext)
            u'',
            # 127 Mahnlimit Betrag - Betrag 5 (Beispiel: 12.123,12)
            u'',
            # 128 Mahnlimit % - Zahl 2
            u'',
            # 129 Zinsberechnung - Zahl 1
            u'',
            # 130 Mahnzinssatz 1 - Zahl 2
            u'',
            # 131 Mahnzinssatz 2 - Zahl 2
            u'',
            # 132 Mahnzinssatz 3 - Zahl 2
            u'',
            # 133 Lastschrift - Text 1
            u'',
            # 134 Verfahren - Text 1
            u'',
            # 135 Mandantenbank - Zahl 4
            u'',
            # 136 Zahlungsträger - Text 1
            u'',
            # 137 Indiv. Feld 1 - Text 40
            u'',
            # 138 Indiv. Feld 2 - Text 40
            u'',
            # 139 Indiv. Feld 3 - Text 40
            u'',
            # 140 Indiv. Feld 4 - Text 40
            u'',
            # 141 Indiv. Feld 5 - Text 40
            u'',
            # 142 Indiv. Feld 6 - Text 40
            u'',
            # 143 Indiv. Feld 7 - Text 40
            u'',
            # 144 Indiv. Feld 8 - Text 40
            u'',
            # 145 Indiv. Feld 9 - Text 40
            u'',
            # 146 Indiv. Feld 10 - Text 40
            u'',
            # 147 Indiv. Feld 11 - Text 40
            u'',
            # 148 Indiv. Feld 12 - Text 40
            u'',
            # 149 Indiv. Feld 13 - Text 40
            u'',
            # 150 Indiv. Feld 14 - Text 40
            u'',
            # 151 Indiv. Feld 15 - Text 40
            u'',
            # 152 Abweichende Anrede (Rechnungsadresse) - Text 30
            u'',
            # 153 Adressart (Rechnungsadresse) - Text 3
            u'',
            # 154 Straße (Rechnungsadresse) - Text 36
            u'',
            # 155 Postfach (Rechnungsadresse) - Text 10
            u'',
            # 156 Postleitzahl (Rechnungsadresse) - Text 10
            u'',
            # 157 Ort (Rechnungsadresse) - Text 30
            u'',
            # 158 Land (Rechnungsadresse) - Text 2
            u'',
            # 159 Versandzusatz (Rechnungsadresse) - Text 50
            u'',
            # 160 Adresszusatz (Rechnungsadresse) - Text 36
            u'',
            # 161 Abw. Zustellbezeichnung 1 (Rechnungsadresse) - Text 50
            u'',
            # 162 Abw. Zustell- bezeichnung 2 (Rechnungsadresse) - Text 36
            u'',
            # 163 Adresse Gültig von (Rechnungsadresse) - Datum 8
            u'',
            # 164 Adresse Gültig bis (Rechnungsadresse) - Datum 8
            u'',
            # 165 Bankleitzahl 6 - Text 8
            u'',
            # 166 Bankbezeichnung 6 - Text 30
            u'',
            # 167 Bankkonto-Nummer 6 - Text 10
            u'',
            # 168 Länderkennzeichen 6 - Text 2
            u'',
            # 169 IBAN-Nr. 6 - Text 34
            u'',
            # 170 Leerfeld - Text 1
            u'',
            # 171 SWIFT-Code 6 - Text 11
            u'',
            # 172 Abw. Kontoinhaber 6 - Text 70
            u'',
            # 173 Kennz. Haupt-Bankverb. 6 - Zahl 1
            u'',
            # 174 Bankverb. 6 Gültig von - Datum 8
            u'',
            # 175 Bankverb. 6 Gültig bis - Datum 8
            u'',
            # 176 Bankleitzahl 7 - Text 8
            u'',
            # 177 Bankbezeichnung 7 - Text 30
            u'',
            # 178 Bankkonto-Nummer 7 - Text 10
            u'',
            # 179 Länderkennzeichen 7 - Text 2
            u'',
            # 180 IBAN-Nr. 7 - Text 34
            u'',
            # 181 Leerfeld - Text 1
            u'',
            # 182 SWIFT-Code 7 - Text 11
            u'',
            # 183 Abw. Kontoinhaber 7 - Text 70
            u'',
            # 184 Kennz. Haupt-Bankverb. 7 - Zahl 1
            u'',
            # 185 Bankverb. 7 Gültig von - Datum 8
            u'',
            # 186 Bankverb. 7 Gültig bis - Datum 8
            u'',
            # 187 Bankleitzahl 8 - Text 8
            u'',
            # 188 Bankbezeichnung 8 - Text 30
            u'',
            # 189 Bankkonto-Nummer 8 - Text 10
            u'',
            # 190 Länderkennzeichen 8 - Text 2
            u'',
            # 191 IBAN-Nr. 8 - Text 34
            u'',
            # 192 Leerfeld - Text 1
            u'',
            # 193 SWIFT-Code 8 - Text 11
            u'',
            # 194 Abw. Kontoinhaber 8 - Text 70
            u'',
            # 195 Kennz. Haupt-Bankverb. 8 - Zahl 1
            u'',
            # 196 Bankverb. 8 Gültig von - Datum 8
            u'',
            # 197 Bankverb. 8 Gültig bis - Datum 8
            u'',
            # 198 Bankleitzahl 9 - Text 8
            u'',
            # 199 Bankbezeichnung 9 - Text 30
            u'',
            # 200 Bankkonto-Nummer 9 - Text 10
            u'',
            # 201 Länderkennzeichen 9 - Text 2
            u'',
            # 202 IBAN-Nr. 9 - Text 34
            u'',
            # 203 Leerfeld - Text 1
            u'',
            # 204 SWIFT-Code 9 - Text 11
            u'',
            # 205 Abw. Kontoinhaber 9 - Text 70
            u'',
            # 206 Kennz. Haupt-Bankverb. 9 - Zahl 1
            u'',
            # 207 Bankverb. 9 Gültig von - Datum 8
            u'',
            # 208 Bankverb. 9 Gültig bis - Datum 8
            u'',
            # 209 Bankleitzahl 10 - Text 8
            u'',
            # 210 Bankbezeichnung 10 - Text 30
            u'',
            # 211 Bankkonto-Nummer 10 - Text 10
            u'',
            # 212 Länderkennzeichen 10 - Text 2
            u'',
            # 213 IBAN-Nr. 10 - Text 34
            u'',
            # 214 Leerfeld - Text 1
            u'',
            # 215 SWIFT-Code 10 - Text 11
            u'',
            # 216 Abw. Kontoinhaber 10 - Text 70
            u'',
            # 217 Kennz. Haupt-Bankverb. 10 - Zahl 1
            u'',
            # 218 Bankverb. 10 Gültig von - Datum 8
            u'',
            # 219 Bankverb. 10 Gültig von - Datum 8
            u'',
            # 220 Nummer Fremdsystem - Text 15
            u'',
            # 221 Insolvent - Zahl 1
            u'',
            # 222 SEPA-Mandatsreferenz 1 - Text 35
            u'%s' % sSepaMandateId,
            # 223 SEPA-Mandatsreferenz 2 - Text 35
            u'',
            # 224 SEPA-Mandatsreferenz 3 - Text 35
            u'',
            # 225 SEPA-Mandatsreferenz 4 - Text 35
            u'',
            # 226 SEPA-Mandatsreferenz 5 - Text 35
            u'',
            # 227 SEPA-Mandatsreferenz 6 - Text 35
            u'',
            # 228 SEPA-Mandatsreferenz 7 - Text 35
            u'',
            # 229 SEPA-Mandatsreferenz 8 - Text 35
            u'',
            # 230 SEPA-Mandatsreferenz 9 - Text 35
            u'',
            # 231 SEPA-Mandatsreferenz 10 - Text 35
            u'',
            # 232 Verknüpftes OPOS-Konto - Konto 9
            u'',
            # 233 Mahnsperre bis - Datum 8
            u'',
            # 234 Lastschriftsperre bis - Datum 8
            u'',
            # 235 Zahlungssperre bis - Datum 8
            u'',
            # 236 Gebührenberechnung - Zahl 1
            u'',
            # 237 Mahngebühr 1 - Zahl 2
            u'',
            # 238 Mahngebühr 2 - Zahl 2
            u'',
            # 239 Mahngebühr 3 - Zahl 2
            u'',
            # 240 Pauschalenberechnung - Zahl 1
            u'',
            # 241 Verzugspauschale 1 - Zahl 3
            u'',
            # 242 Verzugspauschale 2 - Zahl 3
            u'',
            # 243 Verzugspauschale 3 - Zahl 3
            u''
            ]
        oCsvWriter.writerow(lRow)

    oOutput.seek(0)
    return oOutput