# -*- coding: utf-8 -*-

import settings

def zipfelmuetze(request):
    return { 'SITE_HTML_TITLE'                      : 'Zipfelmuetze-Chemnitz',
             'SITE_HTML_ADMIN_TITLE'                : 'Zipfelmütze - Verwaltung',
             'INVOICE_COMPANY_NAME'                 : 'Fa. Ronny Koblischeck',
             'INVOICE_COMPANY_NAME_1'               : 'Zipfelmütze',
             'INVOICE_COMPANY_NAME_2'               : '',
             'INVOICE_COMPANY_ADDRESS_STREET'       : 'Tschaikowskistraße 9',
             'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '09130 Chemnitz',
             'INVOICE_COMPANY_TELEFONE'             : '(0371) 25620156',
             'INVOICE_COMPANY_EMAIL'                : 'zipfelmuetze-chemnitz@web.de',
             'INVOICE_COMPANY_TAXID'                : '215/240/05732',
             'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '111778700',
             'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '87040000',
             'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : 'Commerzbank',
             'STATICFILES_DIR'                      : settings.STATIC_DIR,
             'PRINTMENUE_COLORSTRONG'               : '6fa700',
             'PRINTMENUE_COLORLIGHT'                : 'cce5cc',
             'MENU_FOOTER_LINE_1'                   : 'Änderungen vorbehalten',
             'MENU_ADDITIVES'                       : '1 mit Farbstoff, 2 mit Konservierungsstoff, 3 mit Süßungsmittel, 4 mit Geschmacksverstärker',
             'SITE_HEADER_BACKGROUND'               : 'a1e3aa',
             'SITE_NAME'                            : 'zipfelmuetze-chemnitz.de',
             'SITE_URL'                             : 'http://zipfelmuetze.schulmenueplaner.de',
             'SITE_REGARDS'                         : 'Ihr Zipfelmuetzen Team',
             'SITE_HEADER_SUBTITLE'                 : '<em>fresh</em> | <em>and</em> | <em>tasty</em>'
                       
            }

def kuechengeister(request):
    return { 'SITE_HTML_TITLE'                      : 'Kuechengeister', 
             'SITE_HTML_ADMIN_TITLE'                : 'Küchengeister - Verwaltung',
             'INVOICE_COMPANY_NAME'                 : 'Mauersberger-Schuster GbR',
             'INVOICE_COMPANY_NAME_1'               : 'Anett Mauersberger',
             'INVOICE_COMPANY_NAME_2'               : 'Timo Schuster',
             'INVOICE_COMPANY_ADDRESS_STREET'       : 'Michaelstraße 58',
             'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '09116 Chemnitz',
             'INVOICE_COMPANY_TELEFONE'             : '0371 2353944',
             'INVOICE_COMPANY_EMAIL'                : 'kuechengeister@gmx.de',
             'INVOICE_COMPANY_TAXID'                : '215/159/23907',
             'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '3140008758',
             'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '87050000',
             'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : 'Sparkasse Chemnitz',
             'STATICFILES_DIR'                      : settings.STATIC_DIR,
             'PRINTMENUE_COLORSTRONG'               : 'ee1d23',
             'PRINTMENUE_COLORLIGHT'                : 'cce5cc',
             'MENU_FOOTER_LINE_1'                   : '',
             'MENU_ADDITIVES'                       : '',
             'SITE_HEADER_BACKGROUND'               : 'ee1d23',
             'SITE_NAME'                            : 'www.kuechengeister.net',
             'SITE_URL'                             : 'http://www.kuechengeister.net',
             'SITE_REGARDS'                         : 'Ihr Küchengeister Team',
             'SITE_HEADER_SUBTITLE'                 : '<em>gesund</em> | <em>frisch</em> | <em>regional</em> - einfach gutes Essen für Ihr Kind in Kindergarten und Schule'
            }
