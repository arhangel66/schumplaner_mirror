from datetime import date, timedelta

from django.core.management.base import BaseCommand, CommandError
from django.contrib.admin.util import NestedObjects
from django.contrib.auth.models import User

from menus.models import Menu, MealDefaultPrice, Price

class Command(BaseCommand):

    def handle(self, *args, **options):
        self.stdout.write("Setzt Preise fuer alle Menus fuer gewaehltes Jahr/Monat.\n")
        iYear = None
        while not iYear:
            val = raw_input("Jahr waehlen (YYYY): ")
            iYear = int(val)
        if iYear not in range(2016, 2020):
            raise Exception("Auswahl ungueltig!\n")
        iMonth = None
        while not iMonth:
            val = raw_input("Monat waehlen (1-12): ")
            iMonth = int(val)
        if iMonth not in range (1,13):
            self.stdout.write("Auswahl ungueltig!\n")
        else:
            oStartDate = date(iYear, iMonth, 1)
            if iMonth == 12:
                oEndDate = date(iYear, 12, 31)
            else:
                oEndDate = date(iYear, iMonth + 1, 1) - timedelta(days=1)

            self.stdout.write("Setze Preise fuer alle Menus von %s bis %s\n" % (oStartDate, oEndDate))
            choice = None
            while not choice:

                val = raw_input("Enter YES if you want to proceed: ")
                choice = str(val)

            if choice != "YES":
                self.stdout.write("Aborted by user!\n")
            else:
                iCounterNew = 0
                iCounterChanged = 0

                try:
                    qsMenus = Menu.objects.filter(date__range=(oStartDate, oEndDate)).order_by('date')
                    self.stdout.write("Menus gesamt: %s\n" % len(qsMenus))

                    for oMenu in qsMenus:
                        sOutput = ''
                        qsDefaultPrices = MealDefaultPrice.objects.filter(meal=oMenu.meal, price_is_variable=False)
                        for oDefaultPrice in qsDefaultPrices:
                            qsPrices = Price.objects.filter(menu=oMenu, price_group=oDefaultPrice.price_group)

                            if len(qsPrices) == 0:
                                oPrice = Price(menu=oMenu,
                                               price_group=oDefaultPrice.price_group,
                                               price=oDefaultPrice.price)
                                oPrice.save()
                                sOutput += ("Neu: %s - %s - %s\n" % (oDefaultPrice.meal, oDefaultPrice.price_group, oPrice.price))
                                iCounterNew += 1
                            elif len(qsPrices) == 1 and qsPrices[0].price != oDefaultPrice.price:
                                qsPrices[0].price = oDefaultPrice.price
                                qsPrices[0].save()
                                sOutput += ("Geaendert: %s - %s - %s\n" % (oDefaultPrice.meal, oDefaultPrice.price_group, qsPrices[0].price))
                                iCounterChanged += 1

                        if len(sOutput) > 0:
                            self.stdout.write("%s\n" % oMenu)
                            self.stdout.write(sOutput)

                    self.stdout.write("Preise neu angelegt: %d\n" % iCounterNew)
                    self.stdout.write("Preise korrigiert: %d\n" % iCounterChanged)

                except Exception as exc:
                    self.stdout.write("Error: %s\n" % exc.message)
