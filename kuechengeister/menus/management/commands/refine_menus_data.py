from datetime import datetime

from django.core.management.base import BaseCommand
from django.db.models import Count
from django.db import transaction

from menus.models import Menu, MenuPreparation, MealPlan, Meal, PriceGroup, MealDefaultPrice
from menus.helpers import initialize_menu_prices


class Command(BaseCommand):
    """Maintains integrity of data between MealPlan, PriceGroup and MealDefaultPrice.
    """
    def handle(self, *args, **options):
        # Remove all default prices of meals not properly bound to pricegroup
        for mealplan in MealPlan.objects.all():
            meals = Meal.objects.filterAvailable(mealplan=mealplan).values_list('id', flat=True)
            pricegroups = PriceGroup.objects.filter(mealplan=mealplan).values_list('id', flat=True)
            MealDefaultPrice.objects.filter(
                price_group__id__in=pricegroups
            ).exclude(meal__id__in=meals).delete()

        # Generate missing price objects. This happened from an issue of auto
        # populating week menus, affected to both Menu and MenuPreparation.
        # This only targets to menus in the future and currently doesn't have
        # any price objects at all.
        with transaction.commit_on_success():
            menu_count = 0
            for MenuClass in (Menu, MenuPreparation):
                future_menus = MenuClass.objects.filter(date__gte=datetime.now())
                for menu in future_menus.annotate(prices_count=Count('prices')).filter(prices_count=0):
                    initialize_menu_prices(menu)
                    menu_count += 1
            print u'Menu number have price(s) added: {}'.format(menu_count)
