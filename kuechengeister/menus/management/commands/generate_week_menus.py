from datetime import datetime
from random import randint
from decimal import Decimal

from django.core.management.base import BaseCommand

from menus.models import (
    Menu, Meal, MenuImport, Mealtime, MealPlan, PriceGroup, Price,
    MealDefaultPrice
)
from kg_utils.date_helper import DateHelper


class Command(BaseCommand):
    help = ("Generate random menus for given week(s). Just for testing purpose"
            " in developing environment.")

    def add_arguments(self, parser):
        parser.add_argument('week', default=None, type=int)
        parser.add_argument('week-count', default=1, type=int)

    def handle(self, *args, **options):
        year, week, day = datetime.now().isocalendar()
        start_week = args[0] if args else week
        week_count = args[1] if len(args) > 1 else 3
        mealtime_menus = {}
        price_groups = []
        all_meals = []

        for mid in Mealtime.objects.values_list('id', flat=True):
            mealtime_menus[mid] = [_[0] for _ in MenuImport.objects.filter(
                mealtimes__id=mid).values_list('id')]

        unsaved_default_prices = []
        for mealplan in MealPlan.objects.all():
            meals = mealplan.meals()
            all_meals.extend(meals)
            for price_group in mealplan.pricegroup_set.all():
                price_groups.append(price_group)
                # Create MealDefaultPrice for all pricegroup first
                for meal in meals:
                    if not MealDefaultPrice.objects.filter(
                            meal=meal,
                            price_group=price_group
                            ).exists():
                        unsaved_default_prices.append(
                            MealDefaultPrice(
                                meal=meal,
                                price_group=price_group,
                                price=Decimal('2.1'),
                                price_is_variable=False
                            )
                        )
        MealDefaultPrice.objects.bulk_create(unsaved_default_prices)

        gen_count = 0
        unsaved_prices = []
        for week in [start_week+i for i in range(week_count)]:
            for day in DateHelper().getDaysInCalendarWeek(year, week):
                for meal in all_meals:
                    if Menu.objects.filter(meal=meal, date=day).exists():
                        continue
                    avail_menus = mealtime_menus[meal.mealtime.id]
                    if not avail_menus:
                        continue
                    mid = avail_menus[randint(0, len(avail_menus)-1)]
                    menuimport = MenuImport.objects.get(id=mid)
                    menu = menuimport.prepare_menu(
                        mealplan=None, meal=meal, date=day, preparation=False)
                    for price_group in price_groups:
                        try:
                            default_price = MealDefaultPrice.objects.get(
                                meal=meal, price_group=price_group)
                            unsaved_prices.append(
                                Price(
                                    menu=menu,
                                    price_group=price_group,
                                    price=default_price.price
                                )
                            )
                        except MealDefaultPrice.DoesNotExist:
                            pass
                    gen_count += 1
        Price.objects.bulk_create(unsaved_prices)

        self.stdout.write('Total menu(s) generated: %s\n' % gen_count)
