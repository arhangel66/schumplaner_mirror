import json

from django import template
from django.db.models import Q
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from kg_utils.date_helper import DateHelper

from kg.models import Child
from menus.models import Menu, MenuPreparation, Order

register = template.Library()


def date_menu(meal, date, mealplan=None):
    """Call Meal object function date_menu with date parameter.
      With this filter is possible to get menu from meal in given date.
      Useful when draw week view table - is possible to save loop iterations"""
    return meal.date_menu(date, mealplan)

register.assignment_tag(date_menu, name='date_menu')


@register.filter
def is_past_date(date):
    """Returns True if given date is in the past."""
    return DateHelper().is_past_date(date)


@register.assignment_tag
def check_meal_available(meal, mealplan, day):
    return meal.is_available(mealplan=mealplan, day=day)


@register.assignment_tag(takes_context=True)
def get_preparing_mealplans(context, json_format=False):
    """
    Returns a list of preparing meal plans.

    :param json_format: if True, returns a json string instead of a list.
    """
    prep_mealplans = []
    # This enables releasing from any mealplan being browsed
    # sel_mealplan = context.get('selected_mealplan')
    # if sel_mealplan is not None:
    #     if sel_mealplan.has_preparation_menu:
    #         prep_mealplans.append((sel_mealplan.id, sel_mealplan.name))
    # else:
    for item in MenuPreparation.objects.all().values(
            'mealplan__id', 'mealplan__name').distinct():
        prep_mealplans.append((
            item['mealplan__id'] or 0,
            item['mealplan__name'] or force_text(_("Global")),
        ))
    if json_format:
        return mark_safe(json.dumps(prep_mealplans))
    return prep_mealplans


@register.assignment_tag
def get_day_menus(date, mealplan=None):
    """Returns all available menus belong to a given date and mealplan if
    provided."""
    return Menu.objects.filter(date=date).filter(Q(mealplan__isnull=True) | Q(mealplan=mealplan))


@register.assignment_tag
def get_menu_price(menu, pricegroup, child=None):
    if pricegroup:
        return menu.get_price(pricegroup, child=child)
    else:
        return None


@register.assignment_tag
def get_order(menu, child=None):
    try:
        return Order.objects.get(menu=menu, child=child)
    except (ValueError, Order.DoesNotExist):
        pass
