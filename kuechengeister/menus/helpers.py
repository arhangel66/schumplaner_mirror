import locale

from datetime import datetime

from django.conf import settings
from django.db import transaction
from django.db.models import Q
from django.utils.log import getLogger
from django.http import HttpResponse

from kg_utils.date_helper import DateHelper

from menus.models import Meal
from menus.templatetags.admin_extras import is_past_date
from .models import (
    MealDefaultPrice, MenuPreparation, PricePreparation, Menu,
    Price, PriceGroup, MealPlan
)

logger = getLogger(__name__)
default_locale = (settings.LANGUAGE_CODE.replace('-', '_'), 'UTF-8')


def is_prices_correct(lPrices):
    for iIdx, sPrice in enumerate(lPrices):
        if len(sPrice.strip()) == 0:
            return False
        else:
            try:
                fPrice = locale.atof(sPrice)
                if fPrice < 0.0:
                    return False
            except:
                return False
    return True


def save_menu_prices(oMenu, lPrices, lPriceGroups):
    cPriceClass = isinstance(oMenu, Menu) and Price or PricePreparation
    for iIdx, iPrice in enumerate(lPrices):
        if isinstance(iPrice, basestring):
            iPrice = locale.atof(iPrice)
        oPrice = cPriceClass()
        oPrice.menu = oMenu
        oPrice.price = iPrice
        oPrice.price_group = PriceGroup.objects.get(pk=lPriceGroups[iIdx])
        oPrice.save()


def build_price_list(lPrices, price_empty=False):
    lOut = list()
    for oPrice in lPrices:
        fPrice = '' if price_empty and oPrice.__class__.__name__ == 'MealDefaultPrice' else float(oPrice.price)

        if isinstance(fPrice, float):
            locale.setlocale(locale.LC_ALL, default_locale)
            sPrice = locale.format("%.2f", fPrice, grouping=True)
        else:
            sPrice = fPrice

        lOut.append({'id': oPrice.id,
                     'object_id': '%d%d' % (oPrice.id, oPrice.price_group.id),
                     'price_group_id': oPrice.price_group.id,
                     'price_group': oPrice.price_group.name,
                     'price': sPrice})

    return lOut


def prepare_menu_for_json(oMenu, meal=None, date=None):
    """
    Build json object from Menu python object. This json object is used in Week view to fill form and in search view.
    If function is called with oMenu = None then it returns json object but with empty properties.

    If meal and date is provided then order time limit and cancel time limit is calculated for empty oMenu object.

    :param oMenu:
    :return: json
    """
    dOut = {'id': None,
            'description': None,
            'allergens': [],
            'additionals': [],
            'order_timelimit': None,
            'cancel_timelimit': None,
            'can_be_ordered': True,
            'disabled': False,
            'variable_prices': [],
            'fixed_prices': [],
            'mealplan': None,
            'inherited': False,
            'in_past': False,
            'required_menu': None
            }

    if meal and date:
        dOut['in_past'] = is_past_date(date)
        dOut['order_timelimit'] = DateHelper.calculateOrderTimelimit(
            date,
            meal.order_day_limit_days,
            meal.order_day_limit,
            meal.order_time_limit)
        dOut['cancel_timelimit'] = DateHelper.calculateOrderTimelimit(
            date,
            meal.cancel_day_limit_days,
            meal.cancel_day_limit,
            meal.cancel_time_limit)

    if not meal and oMenu:
        meal = oMenu.meal

    lFixedPrices = MealDefaultPrice.objects.filter(meal=meal.id, price_is_variable=False,)
    lVariablePrices = MealDefaultPrice.objects.filter(meal=meal.id, price_is_variable=True)
    if oMenu and oMenu.mealplan_id:
        # If menu is a specific mealplan menu, returns only prices for that
        # mealplan.
        lFixedPrices = lFixedPrices.filter(
            price_group__facilitysubunit__mealplan_id=oMenu.mealplan_id).distinct()
        lVariablePrices = lVariablePrices.filter(
            price_group__facilitysubunit__mealplan_id=oMenu.mealplan_id).distinct()

    dOut['can_be_ordered'] = meal.allow_order

    if oMenu:
        dOut['id'] = oMenu.id
        if isinstance(oMenu, MenuPreparation):
            dOut['is_preparation'] = True
        dOut['description'] = oMenu.description

        lAllergens = oMenu.allergens.all()
        if lAllergens:
            for oAllergen in lAllergens:
                dOut['allergens'].append(oAllergen.id)

        lAdditionals = oMenu.additionals.all()

        if lAdditionals:
            for oAdditional in lAdditionals:
                dOut['additionals'].append(oAdditional.id)

        dOut['order_timelimit'] = oMenu.order_timelimit
        dOut['cancel_timelimit'] = oMenu.cancel_timelimit

        dOut['can_be_ordered'] = oMenu.can_be_ordered
        dOut['disabled'] = oMenu.disabled
        dOut['in_past'] = is_past_date(oMenu.date)

        # Check if menu is inherited from a general one or not
        if oMenu.mealplan_id:
            dOut['mealplan'] = oMenu.mealplan_id
            dOut['inherited'] = Menu.objects.filter(
                meal=oMenu.meal, date=oMenu.date, mealplan__isnull=True
            ).exists()

        lTmpPrices = list()
        for oFixedPrice in lFixedPrices:
            lPrices = oMenu.prices.filter(price_group=oFixedPrice.price_group)
            if len(lPrices) > 0:
                lTmpPrices.append(lPrices[0])
            else:
                lTmpPrices.append(oFixedPrice)

        dOut['fixed_prices'] = build_price_list(lTmpPrices)

        lTmpPrices = list()
        for oVariablePrice in lVariablePrices:
            lPrices = oMenu.prices.filter(price_group=oVariablePrice.price_group)
            if len(lPrices) > 0:
                lTmpPrices.append(lPrices[0])
            else:
                lTmpPrices.append(oVariablePrice)

        dOut['variable_prices'] = build_price_list(lTmpPrices, True)

        if oMenu.required_menu is not None:
            dOut['required_menu'] = oMenu.required_menu.pk
    else:
        dOut['fixed_prices'] = build_price_list(lFixedPrices)
        dOut['variable_prices'] = build_price_list(lVariablePrices, True)

    if dOut['cancel_timelimit'] is not None:
        dOut['cancel_timelimit'] = dOut['cancel_timelimit'].strftime('%d-%m-%Y %H:%M')

    if dOut['order_timelimit'] is not None:
        dOut['order_timelimit'] = dOut['order_timelimit'].strftime('%d-%m-%Y %H:%M')

    return dOut


def prepare_menuimport_for_json(oMenuImport):
    """
    Compact version of prepare_menu_for_json() for MenuImport model.
    :param oMenuImport:
    :return: json
    """
    oMI = oMenuImport
    return {
        'id': None,
        'description': oMI.description,
        'allergens': [alg.id for alg in oMI.allergens.all()],
        'additionals': [adt.id for adt in oMI.additionals.all()],
        'order_timelimit': None,
        'cancel_timelimit': None,
        'can_be_ordered': True,
        'disabled': False,
        'variable_prices': [],
        'fixed_prices': [],
        'mealplan': None,
        'mealtimes': [tme.id for tme in oMI.mealtimes.all()],
        'inherited': False,
        'in_past': False
    }


def get_preparing_weeks(pdStart=None, pdEnd=None, poMealPlan=None):
    """Returns a set of weeks that have menu preparation. Changes are reported
    within the provided mealplan or all meal plans in case of None (~general
    meal plan.)"""
    filter_params = Q(mealplan=poMealPlan) | Q(mealplan=None) if poMealPlan else Q()
    if pdStart:
        filter_params &= Q(date__gte=pdStart)
    if pdEnd:
        filter_params &= Q(date__lte=pdEnd)

    # Find all dates of visible weeks that have menu preparation.
    lPreparationDates = MenuPreparation.objects.filter(
        filter_params).values('date').distinct()

    # Get set of weeks from dates above.
    stWeeks = {item['date'].isocalendar()[1] for item in lPreparationDates}
    return stWeeks


def getList(source, key):
    """Return list value extracted from either dictionary or QueryDict
    object."""
    func = source.getlist if hasattr(source, 'getlist') else source.get
    return func(key, [])


def fill_default_menus(mealplan, week, year):
    """Fill default menus to available slots of given mealplan.
    Week and year will be set as current date if not provided."""
    # Browse all present meals and get default menu
    if not week or not year:
        year, week, day = datetime.now().isocalendar()

    week_days = DateHelper().getDaysInCalendarWeek(year, week)
    mealplan_day_count = mealplan.weekdays if mealplan else MealPlan.get_max_weekdays()
    week_days = week_days[:mealplan_day_count]
    wd_set = set(week_days)
    wd_count = len(week_days)
    meals = Meal.objects.filterAvailable(mealplan=mealplan)

    with transaction.commit_manually():
        try:
            lMenus = []
            for meal in meals:
                if meal.default_menu is None:
                    continue
                # Search for filled slot (with Menu or MenuPreparation)
                taken_set = set()
                for cls in (Menu, MenuPreparation):
                    mealplan_condition = Q(mealplan=mealplan)
                    if cls == MenuPreparation:
                        mealplan_condition |= Q(mealplan=None)
                    qsMenu = cls.objects.filter(mealplan_condition, meal=meal)
                    dates = qsMenu.filter(
                        Q(date__gte=week_days[0]) &
                        Q(date__lte=week_days[-1])
                    ).values_list('date', flat=True)
                    taken_set.update(dates)
                    # All week days are taken, stop the search early
                    if len(taken_set) == wd_count:
                        break
                avail_set = wd_set.difference(taken_set)

                # Create MenuPreparation for empty slots
                for day in avail_set:
                    lMenus.append(meal.default_menu.prepare_menu(
                        mealplan, meal, day)
                    )
            transaction.commit()
            return lMenus
        except Exception as exc:
            transaction.rollback()
            logger.exception(
                'Errror when creating default menus for {}.'.format(mealplan))
            raise exc


def get_mealplan_pdf(year=None, week_num=None, mealplan_id=None,
                     pricegroup_id=None, **kwargs):
    """ Download a given mealplan as PDF.

        If `child` is present in kwargs, the order info will be included within
        the exported PDF.
    """
    from reports.helpers import generate_pdf
    try:
        mealplan = MealPlan.objects.get(pk=mealplan_id)
    except (MealPlan.DoesNotExist, ValueError):
        mealplan = None
    try:
        pricegroup = PriceGroup.objects.get(pk=pricegroup_id)
    except (PriceGroup.DoesNotExist, ValueError):
        pricegroup = None

    cur_year, cur_week_num, cur_week_day = datetime.now().isocalendar()
    week_num = int(week_num) if week_num else cur_week_num
    year = int(year) if year else cur_year
    first_day = DateHelper().getFirstDayInIsoweek(year, week_num)
    lWeeksForLinks = DateHelper().getWeeks(first_day, -3)
    lWeeksForLinks += DateHelper().getWeeks(first_day, 8, False)

    week_days = DateHelper().getDaysInCalendarWeek(year, week_num)
    if mealplan is not None:
        week_days = week_days[:mealplan.weekdays]
    else:
        week_days = week_days[:MealPlan.get_max_weekdays()]

    # Get all present meals, exclude one with no menus
    meals = []
    for meal in Meal.objects.filterAvailable(
            mealplan=mealplan).order_by('sortKey'):
        for day in week_days:
            menu = meal.date_menu(day, mealplan)
            if menu is None or menu.is_preparation or menu.disabled:
                continue
            meals.append(meal)
            break

    context = {
        'mealplan': mealplan,
        'pricegroup': pricegroup,
        'week_num': week_num,
        'weeks_for_links': lWeeksForLinks,
        'week_days': week_days,
        'meals': meals,
        'day_cell_width': (100-15)/len(week_days),
        'child': kwargs.get('child')
    }

    mealplan_name = mealplan.name if mealplan else 'Alle'
    file_name = u'Speiseplan {}-{}-{}.pdf'.format(
        mealplan_name, week_num, year)
    file_name = file_name.encode('ascii', 'ignore')
    resp = HttpResponse(content_type='application/pdf')
    resp['Content-Disposition'] = 'attachment; filename=' + file_name
    resp = generate_pdf(
        'menus/pdf/mealplan.html', response=resp, context=context)
    return resp


def cleanup_menu_preparations():
    """ Find and remove all obsolete menu preparations. The prevention is
        added to save() method, but there is still bulk_create could avoid it.
    """
    menu_preparations = MenuPreparation.objects.all().values(
        'id', 'date', 'mealplan_id', 'meal_id')
    obsolete_ids = []
    for preparation in menu_preparations:
        if not Menu.objects.filter(
            date=preparation['date'],
            mealplan=preparation['mealplan_id'],
            meal=preparation['meal_id']
        ).exists():
            continue
        obsolete_ids.append(preparation['id'])
    if obsolete_ids:
        MenuPreparation.objects.filter(id__in=obsolete_ids).delete()


def initialize_menu_prices(menu):
    """ Ensuring about menu prices, make it similar to saving via menu dialog.
    Notice: Only running on menu without any prices.
    """
    if not menu.prices.exists():
        meal_prices = []
        price_groups = []
        for default_price in MealDefaultPrice.objects.filter(meal=menu.meal):
            meal_prices.append(default_price.price)
            price_groups.append(default_price.price_group.pk)
        save_menu_prices(menu, meal_prices, price_groups)
