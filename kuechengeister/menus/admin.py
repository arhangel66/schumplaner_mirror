# -*- coding: utf-8 -*-

import json
import socket
import math
import locale
from datetime import datetime


from django.db import transaction
from django.template import Context
from django.template.loader import get_template
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core import urlresolvers
from django import template
from django.conf import settings
from django.conf.urls.defaults import patterns
from django.contrib import admin
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.utils.encoding import force_unicode
from django.forms.models import BaseInlineFormSet
from django.forms import ValidationError
from django.utils.log import getLogger

import nested_admin

from kg.models import Order, OrderHistory
from kg.forms_admin import LocalizedModelFormSet
from kg_utils.date_helper import DateHelper
from reports.helpers import generate_pdf

from menus.helpers import (
    prepare_menu_for_json, prepare_menuimport_for_json, save_menu_prices,
    is_prices_correct, get_preparing_weeks, getList, fill_default_menus,
    get_mealplan_pdf
    )
from menus.models import (
    Meal, Mealtime, Menu, MealPlan, MealPeriod, MealDefaultPrice, Price,
    Allergen, Additional, MenuPreparation, PricePreparation, MenuImport,
    PriceGroup,
    )
from menus.templatetags.admin_extras import is_past_date, \
    get_preparing_mealplans
from menus.tasks import releaseMealPlan


logger = getLogger(__name__)

MENU_DATE_FORMAT = '%d-%m-%Y'

JSON_CONTENT_TYPE = 'application/json'

makeDate = lambda s: datetime.strptime(s, MENU_DATE_FORMAT)
makeDateTime = lambda s: datetime.strptime(s, '%d-%m-%Y %H:%M')
makeDETime = lambda s: datetime.strptime(s, '%d.%m.%Y %H:%M')

locale.setlocale(
    locale.LC_ALL, (settings.LANGUAGE_CODE.replace('-', '_'), 'UTF-8'))


class MealtimeAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'name_short', 'sortKey')


class MealAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'name_short', 'sortKey', 'mealtime', 'allow_order',
                    'order_day_limit_days', 'order_day_limit','order_time_limit',
                    'cancel_day_limit_days', 'cancel_day_limit', 'cancel_time_limit')

    def get_form(self, request, obj=None, **kwargs):
        """Show MenuImport items having same meal time only."""
        form = super(MealAdmin, self).get_form(request, obj, **kwargs)
        qsMenuImport = MenuImport.objects
        if obj is not None:
            qsMenuImport = qsMenuImport.filter(mealtimes=obj.mealtime)
        else:
            qsMenuImport = qsMenuImport.none()
        form.base_fields['default_menu'].queryset = qsMenuImport
        return form


class MealPeriodInline(admin.TabularInline):
    model = MealPeriod
    extra = 1


class DefaultPriceFormSet(LocalizedModelFormSet):
    """Ensure that all present default prices are valid, all prices are
    required."""

    def __init__(self, *args, **kwargs):
        super(DefaultPriceFormSet, self).__init__(*args, **kwargs)
        self.can_delete = False


class DefaultPriceNestedInline(nested_admin.NestedTabularInline):
    model = MealDefaultPrice
    formset = DefaultPriceFormSet
    extra = 0
    template = 'admin/inlines/tabular_defaultprice.html'

    def has_delete_permission(self, request, obj=None):
        return True


class PriceGroupNestedInline(nested_admin.NestedTabularInline):
    model = PriceGroup
    inlines = [DefaultPriceNestedInline]
    extra = 0
    template = 'admin/inlines/tabular_pricegroup.html'


class MealPlanAdmin(nested_admin.NestedModelAdmin):
    list_display = ('name', 'list_meals', 'duplicate_order_mealtime',
                    'max_order_quantity')
    fieldsets = (
        ('Allgemeine Angaben', {
            'fields': ('name', 'weekdays', 'show_price', 'show_price_public',
                       'duplicate_order_mealtime', 'max_order_quantity'),
        }),
    )
    inlines = [MealPeriodInline, PriceGroupNestedInline]

    class Media:
        js = [
            'js/mealplan.js',
        ]
        css = {
            'all': ('css/mealplan.css',)
        }

    def list_meals(self, obj):
        """List all the included meals in mealplan list."""
        return ', '.join(obj.meal_periods.values_list('meal__name', flat=True))
    list_meals.short_description = "Essensangebote"


class AllergenAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_short', 'use_for_menu')


class AdditionalAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_short', 'sortKey')


class MenuAdmin(admin.ModelAdmin):
    list_display = ('description', )
    search_fields = ['description']

    def response(self, data='', code=200):
        return HttpResponse(
            json.dumps(data), JSON_CONTENT_TYPE, code, JSON_CONTENT_TYPE)

    def get_urls(self):
        """
        Add extra urls to default Django admin url list

        :return: list
        """
        lUrls = super(MenuAdmin, self).get_urls()
        lMenuAdminUrls = patterns((r'^/$', self.changelist_view),
                                  (r'^save/$', self.save_view),
                                  (r'^reset/$', self.reset_view),
                                  (r'^delete/$', self.delete_view),
                                  (r'^save_droppable/$', self.save_droppable_view),
                                  (r'^load-menu/$', self.load_menu_view),
                                  (r'^search/$', self.search_menu_view),)
        return lMenuAdminUrls + lUrls

    def load_menu_view(self, request):
        """
        Return Menu object in json if it is found in db by given id.
        If not object found return empty json structure.

        :param request:
        :return: json
        """

        if not self.has_change_permission(request, None):
            raise PermissionDenied

        sMenuId = request.GET.get('menu_id', None)
        sMealId = request.GET.get('meal_id', None)
        sDate = request.GET.get('menu_date', None)
        dDate = makeDate(sDate)

        oMeal = None
        if sMealId:
            oMeal = Meal.objects.get(pk=sMealId)

        oMenu = None
        if sMenuId:
            # Get the menu or menu preparation with given id
            try:
                oMenu = Menu.objects.get(pk=sMenuId, meal=oMeal, date=dDate)
            except Menu.DoesNotExist:
                try:
                    oMenu = MenuPreparation.objects.get(pk=sMenuId, meal=oMeal,
                                                        date=dDate)
                except MenuPreparation.DoesNotExist:
                    pass
        dOut = prepare_menu_for_json(oMenu, meal=oMeal, date=dDate)
        return self.response(dOut)

    def search_menu_view(self, request):
        """
        Search menus. Look for input phrase in menu description if found then return it.
        Return list of dictionaries with only needed attributes

        :param request:
        :return: json
        """
        if not self.has_change_permission(request, None):
            raise PermissionDenied

        sPhrase = request.GET.get('phrase', None)
        dMealTimeId = request.GET.get('mealtime', None)
        qsMenus = MenuImport.objects.all()
        if sPhrase:
            # Use basis sql search capability - use like with %phrase% syntax
            qsMenus = qsMenus.filter(description__icontains=sPhrase)
        if dMealTimeId:
            qsMenus = qsMenus.filter(mealtimes__pk=dMealTimeId)

        lOut = []
        for oMenu in qsMenus[:settings.MENU_IMPORT_SUGGESTION]:
            # Return only needed attributes
            lOut.append(prepare_menuimport_for_json(oMenu))
        return self.response(lOut)

    def save_view(self, request, data=None):
        """
        Menu save function. Used to save menu/menu preparation with Ajax.

        :param request:
        :param data: dict Inject data to use instead POST data
        :return: json
        """

        def save_response(success=True, message='', code=200):
            data = {
                'success': success
            }
            if message:
                data['message'] = message
            return self.response(data, code=code)

        if not self.has_change_permission(request, None):
            raise PermissionDenied
        dValues = data or request.POST
        # get menu information from injected input data
        sDescription = dValues.get('description', None)
        sDate = dValues.get('menu_date', None)
        sOrderTimeLimit = dValues.get('order_timelimit', None)
        sCancelTimeLimit = dValues.get('cancel_timelimit', None)
        sMenuId = dValues.get('menu_id', None)
        sMealId = dValues.get('meal_id', None)
        bCanBeOrdered = bool(dValues.get('can_be_ordered', False))
        isPreparation = bool(dValues.get("is_preparation", False))
        sMealPlanId = dValues.get("mealplan_id") or '0'
        bDisabled = bool(dValues.get("disabled", False))
        iRequiredMenu = dValues.get("required_menu", None)

        lAllergens = getList(dValues, 'allergens')
        lAdditionals = getList(dValues, 'additionals')
        lVariablePrices = getList(dValues, 'price_variable')
        lVariablePriceGroups = getList(dValues, 'price_group_id_variable')
        lFixedPrices = getList(dValues, 'price_fixed')
        lFixedPriceGroups = getList(dValues, 'price_group_id_fixed')

        # Terminate the current (preparation) saving if already having menu
        # or menu preparation at the same slot.
        if isPreparation and not sMenuId:
            menu_present = Menu.objects.filter(
                meal=sMealId, mealplan=sMealPlanId, date=makeDate(sDate)
            ).exists()
            menu_present |= MenuPreparation.objects.filter(
                meal=sMealId, mealplan=sMealPlanId, date=makeDate(sDate)
            ).exists()
            if menu_present:
                return save_response(False, u'Menü existiert bereits')


        # Disabled menu can not be ordered.
        if bDisabled:
            bCanBeOrdered = False

        bSaveMenu = False
        if bCanBeOrdered:
            if sDescription \
                and len(sDescription.strip()) > 0\
                and is_prices_correct(lVariablePrices)\
                and is_prices_correct(lFixedPrices):
                bSaveMenu = True
        else:
            if sDescription and len(sDescription.strip()) > 0:
                bSaveMenu = True

        cMenuClass = isPreparation and MenuPreparation or Menu

        # Get selected mealplan, empty means General mealplan.
        qsMealPlan = MealPlan.objects.filter(pk=sMealPlanId)
        oMealPlan = qsMealPlan.exists() and qsMealPlan[0] or None

        saving_successful = True
        if bSaveMenu:
            with transaction.commit_manually():
                try:
                    # Initialize and load Menu and Meal objects if exist in
                    # database
                    oMenu = None
                    oMeal = None
                    oExistingMenu = None
                    bMenuExists = False
                    bExistingMenuCanBeOrdered = None
                    bCreatedMealPlanMenu = False
                    if sMenuId:
                        oMenu = cMenuClass.objects.get(pk=sMenuId)
                        # If menu is in General mealplan and mealplan is given,
                        # create a new menu for that mealplan
                        # ~ General menu is customized in specific mealplan.
                        bMenuExists = True
                        oExistingMenu = oMenu
                        bExistingMenuCanBeOrdered = oMenu.can_be_ordered
                        # Only create specific preparation when general
                        # preparation does not exist.
                        if oMealPlan and not oMenu.mealplan_id and not isPreparation:
                            oMenu = Menu(mealplan=oMealPlan)
                            bCreatedMealPlanMenu = True
                    if sMealId:
                        oMeal = Meal.objects.get(pk=sMealId)

                    # No menu means we need to create new Menu
                    if not oMenu:
                        oMenu = cMenuClass(mealplan=oMealPlan)

                    fnMakeTime = makeDateTime if data else makeDETime
                    # Load new data into Menu object
                    oMenu.description = sDescription
                    oMenu.meal_id = oMeal.id
                    oMenu.date = makeDate(sDate)
                    oMenu.order_timelimit = fnMakeTime(sOrderTimeLimit)
                    oMenu.cancel_timelimit = fnMakeTime(sCancelTimeLimit)
                    oMenu.can_be_ordered = bCanBeOrdered
                    oMenu.disabled = bDisabled
                    if iRequiredMenu != oMenu.pk:
                        oMenu.required_menu_id = iRequiredMenu
                    oMenu.save()

                    # Save allergens
                    #  before save clear all previous allergens
                    oMenu.allergens.clear()
                    if lAllergens:
                        for iAllergenId in lAllergens:
                            oAllergen = Allergen.objects.get(pk=iAllergenId)
                            if oAllergen:
                                oMenu.allergens.add(oAllergen)

                    # Save additionals
                    #  before save clear all previous additionals
                    oMenu.additionals.clear()
                    if lAdditionals:
                        for iAdditionalId in lAdditionals:
                            oAdditional = Additional.objects.get(
                                pk=iAdditionalId)
                            if oAdditional:
                                oMenu.additionals.add(oAdditional)
                    oMenu.save()

                    if bMenuExists and not isPreparation:
                        qsOrders = Order.objects.filter(menu=oExistingMenu)
                        if bCreatedMealPlanMenu:
                            # If new menu is created for specific mealplan,
                            # only cancel/update orders related to that
                            # mealplan.
                            qsOrders = qsOrders.filter(
                                child__facility_subunit__mealplan=oMealPlan)
                        if bExistingMenuCanBeOrdered and \
                                not oMenu.can_be_ordered:
                            # If menu status changed from can be ordered to can
                            # not be ordered: select existing orders and cancel
                            # them
                            for oOrder in qsOrders:
                                oOrder.menu = oMenu
                                oOrder.quantity = 0
                                oOrder.modified_by = None
                                oOrder.save()
                        if oMenu.can_be_ordered and bCreatedMealPlanMenu:
                            # If newly created menu of specific mealplan is
                            # orderable, link related existing Order,
                            # OrderHistory
                            # objects to the new menu.
                            for oOrder in qsOrders:
                                oOrder.menu = oMenu
                                oOrder.save()
                            qsOrderHistories = OrderHistory.objects.filter(
                                child__facility_subunit__mealplan=oMealPlan,
                                menu=oExistingMenu
                            )
                            for oOrderHistory in qsOrderHistories:
                                oOrderHistory.menu = oMenu
                                oOrderHistory.save()

                    # Remove old prices and save new
                    oMenu.prices.all().delete()
                    save_menu_prices(oMenu, lVariablePrices, lVariablePriceGroups)
                    save_menu_prices(oMenu, lFixedPrices, lFixedPriceGroups)
                    transaction.commit()
                except:
                    transaction.rollback()
                    logger.exception('Error when saving menu/menu preparation')
                    saving_successful = False

        return save_response(saving_successful)

    def reset_view(self, request):
        """Handle reset menu action."""
        sMenuId = request.POST.get("menu_id", "")
        bRelink = request.POST.get("relink_orders", False)
        oMenu = Menu.objects.get(pk=sMenuId)
        qsGeneralMenu = Menu.objects.filter(meal=oMenu.meal, date=oMenu.date,
                                            mealplan__isnull=True)
        if oMenu.mealplan_id and qsGeneralMenu.exists():
            with transaction.commit_manually():
                try:
                    if bRelink:
                        oGeneralMenu = qsGeneralMenu[0]
                        # Link all Order & OrderHistory objects to general menu
                        for cObjClass in [Order, OrderHistory]:
                            cObjClass.objects.filter(menu=oMenu).update(
                                menu=oGeneralMenu)
                    # Delete the menu
                    oMenu.delete()
                    messages.success(
                        request, _(u"Menü {} wurde von Speiseplan {} entfernt.").format(
                            oMenu.description, oMenu.mealplan.name))
                    transaction.commit()
                except Exception:
                    messages.error(request,
                                   _(u"Fehler beim Entfernen von Menü {} von Speiseplan {}.")
                                   .format(oMenu.description,
                                           oMenu.mealplan.name))
                    transaction.rollback()
        return HttpResponse('1', status=200)

    def delete_view(self, request):
        """Handle delete menu action."""
        sMenuId = request.POST.get("menu_id", "")
        bIsPreparation = request.POST.get("is_preparation", False)
        if not bIsPreparation:
            oMenu = Menu.objects.get(pk=sMenuId)
        else:
            oMenu = MenuPreparation.objects.get(pk=sMenuId)
        bInherited = Menu.objects.filter(meal=oMenu.meal, date=oMenu.date,
                                         mealplan__isnull=True).exists()
        with transaction.commit_manually():
            try:
                if oMenu.mealplan_id and bInherited and not bIsPreparation:
                    # Disable the menu instead of deleting it.
                    oMenu.disabled = True
                    oMenu.can_be_ordered = False
                    # Cancel menu's orders
                    for oOrder in Order.objects.filter(menu=oMenu):
                        oOrder.quantity = 0
                        oOrder.modified_by = None
                        oOrder.save()
                    oMenu.save()
                    messages.success(request,
                                     _(u"Menü {} wurde auf nicht bestellbar gesetzt.")
                                     .format(oMenu.description))
                else:
                    # Delete the menu and its related objects.
                    oMenu.delete()
                    messages.success(request,
                                     _(u"Menü {} erfolgreich gelöscht.")
                                     .format(oMenu.description))
                transaction.commit()
            except Exception:
                transaction.rollback()
                messages.error(request, _(u"Menü {} konnte nicht gelöscht werden.").format(
                    oMenu.description))
        return HttpResponse('1', status=200)

    def save_droppable_view(self, request):
        """
        Addition to normal menu save function . Used to save menu with information that comes from
        dragging another menu and dropping on this one.

        :param request:
        :return: json
        """
        if not self.has_change_permission(request, None):
            raise PermissionDenied

        sSourceMenuId = request.GET.get('source_menu_id', 0)
        sTargetMenuId = request.GET.get('target_menu_id', None)
        sTargetMealId = request.GET.get('target_meal_id', None)
        sTargetDate = request.GET.get('target_date', None)
        is_preparation = bool(request.GET.get('is_preparation', False))

        # source menu has to be valid
        if not is_preparation:
            oSourceMenu = Menu.objects.get(pk=sSourceMenuId)
        else:
            oSourceMenu = MenuPreparation.objects.get(pk=sSourceMenuId)

        if not oSourceMenu:
            return HttpResponse('1', status=500)

        dSourceMenuData = prepare_menu_for_json(oSourceMenu)

        dPrices = {}
        # prepare target menu data
        if sTargetMenuId:
            oTargetMenu = Menu.objects.get(pk=sTargetMenuId)
            oTargetMeal = oTargetMenu.meal
            dTargetMenuData = prepare_menu_for_json(oTargetMenu)
            del dTargetMenuData['id']
            dTargetMenuData['menu_id'] = sTargetMenuId
            dTargetMenuData['meal_id'] = oTargetMenu.meal.id
            dTargetMenuData['menu_date'] = oTargetMenu.date.strftime(MENU_DATE_FORMAT)
            # Collect menu's current prices.
            for oPrice in oTargetMenu.prices.all():
                dPrices[oPrice.price_group_id] = locale.str(oPrice.price)
        elif sTargetMealId and sTargetDate:
            oTargetDate = datetime.strptime(sTargetDate, MENU_DATE_FORMAT)
            oTargetMeal = Meal.objects.get(pk=sTargetMealId)
            dTargetMenuData = prepare_menu_for_json(None, oTargetMeal, oTargetDate)
            del dTargetMenuData['id']
            dTargetMenuData['meal_id'] = sTargetMealId
            dTargetMenuData['menu_date'] = sTargetDate
            # Collect meal's default prices
            for oDefaultPrice in oTargetMeal.mealdefaultprice_set.all():
                if not oDefaultPrice.price_is_variable:
                    dPrices[oDefaultPrice.price_group_id] = \
                        locale.str(oDefaultPrice.price)
                else:
                    dPrices[oDefaultPrice.price_group_id] = '0'
            # Mark target menu is a preparation
            dTargetMenuData["is_preparation"] = True
        else:
            return HttpResponse('1', status=500)

        # updated target Menu data with source menu data
        dTargetMenuData['description'] = dSourceMenuData['description']
        dTargetMenuData['additionals'] = dSourceMenuData['additionals']
        dTargetMenuData['allergens'] = dSourceMenuData['allergens']

        # Copy prices from source menu to target menu if possible.
        for oPrice in oSourceMenu.prices.all():
            if oPrice.price_group_id in dPrices:
                dPrices[oPrice.price_group_id] = locale.str(oPrice.price)

        # Add prices to target Menu data. Since fixed prices and variable
        # prices are saved in the same way, so we only need to set all prices
        # as fixed prices here.
        dTargetMenuData["price_group_id_fixed"] = dPrices.keys()
        dTargetMenuData["price_fixed"] = dPrices.values()

        # Update target Menu data with given mealplan id
        dTargetMenuData["mealplan_id"] = request.GET.get("mealplan_id", None)

        # save menu and return http response
        return self.save_view(request, dTargetMenuData)

    def __release_mealplan(self, request):
        """Publish changes (MenuPreparation) of all mealplans."""
        bNoRunningWorkers = False

        # lPreparingMealplans = get_preparing_mealplans()
        mealplan_ids = request.POST.get('mealplans').split(',')
        mealplan_names = list(MealPlan.objects.filter(
            id__in=mealplan_ids).values_list('name', flat=True))
        if '0' in mealplan_ids:
            mealplan_names.insert(0, 'Global')
            mealplan_ids.remove('0')
            mealplan_ids.insert(0, None)

        dMsgContext = {
            'queue_disabled': settings.QUEUE_DISABLED,
            'mealplans': mealplan_names,
        }
        tMessageTemplate = get_template(
            "admin/partials/release_mealplan_message.html")
        sMessage = tMessageTemplate.render(Context(dMsgContext))

        params = {
            'year': request.POST.get('year'),
            'week': request.POST.get('week'),
            'mealplan_ids': mealplan_ids
        }
        if not settings.QUEUE_DISABLED:
            # Call the celery task if queue is enabled.
            try:
                releaseMealPlan.delay(**params)
                messages.success(request, sMessage, extra_tags="safe")
            except socket.error:
                # There's no running workers. Call release function without
                # queue.
                bNoRunningWorkers = True
                logger.warning('There is no queueing app found. Task will be '
                               'run synchronously.')

        if settings.QUEUE_DISABLED or bNoRunningWorkers:
            dMsgContext['queue_disabled'] = True
            sMessage = tMessageTemplate.render(Context(dMsgContext))
            try:
                releaseMealPlan(**params)
                messages.success(request, sMessage, extra_tags="safe")
            except Exception as ex:
                logger.exception('Fehler beim Freigeben der Speisepläne.')
                messages.error(
                    request, _("Speiseplan konnte nicht freigegeben werden."))
        # Redirect back to the menus admin after releasing meal plan.
        return redirect(request.get_full_path())

    def __fill_default_menus(self, request):
        """Fill default menus (if found) to available slots of current week."""
        # Browse all present meals and get default menu
        iWeekNum = request.GET.get('week_num', None)
        iYear = request.GET.get('year', None)
        if not iWeekNum or not iYear:
            iYear, iWeekNum, iWeekDay = datetime.now().isocalendar()
        try:
            oMealPlan = MealPlan.objects.get(
                pk=request.GET.get('mealplan') or 0)
        except MealPlan.DoesNotExist:
            oMealPlan = None

        fill_default_menus(oMealPlan, iWeekNum, iYear)

        # Redirect back to the menus admin after releasing meal plan.
        return redirect(request.get_full_path())

    def changelist_view(self, request, extra_context=None):
        """
        Override default Django admin changelist_view with custom.
        Week view for menu items. Show Menu objects in week view table with possibility to add/edit them in one screen.

        :param request:
        :param extra_context:
        :return: html
        """
        oOpts = self.model._meta
        sAppLabel = oOpts.app_label
        if not self.has_change_permission(request, None):
            raise PermissionDenied

        # Get selected mealplan, empty means General mealplan.
        qsMealPlan = MealPlan.objects.filter(
            pk=request.GET.get("mealplan", 0) or 0)
        oMealPlan = qsMealPlan.exists() and qsMealPlan[0] or None

        # Get MenuPreparation queryset base on selected meal plan
        qsMenuPreparation = MenuPreparation.objects
        # Release button status reflects existence of menu preparation in all meaplans.
        # if oMealPlan is not None:
        #     qsMenuPreparation = qsMenuPreparation.filter(mealplan=oMealPlan)

        # Handle different actions performing on current meal plan
        if request.method == "POST":
            if request.POST.get("release") and request.POST.get('mealplans'):
                return self.__release_mealplan(request)
            elif request.POST.get("add-default"):
                return self.__fill_default_menus(request)
            elif request.POST.get("download-pdf"):
                return get_mealplan_pdf(
                    year=request.GET.get('year'),
                    week_num=request.GET.get('week_num'),
                    mealplan_id=request.GET.get('mealplan'),
                    pricegroup_id=request.POST.get('pricegroup') or None
                )

        # Calculate year and week number to show
        iWeekNum = request.GET.get('week_num', None)
        iYear = request.GET.get('year', None)
        iWeekDay = None
        bLoadCurrentDate = True
        if iWeekNum and iYear:
            try:
                # If is not possible to get integers form input values then load current year and week
                iYear = int(iYear)
                iWeekNum = int(iWeekNum)
                bLoadCurrentDate = False
            except ValueError:
                pass

        # No input week and year or invalid input for year and week number
        if bLoadCurrentDate:
            iYear, iWeekNum, iWeekDay = datetime.now().isocalendar()

        lWeekDays = DateHelper().getDaysInCalendarWeek(iYear, iWeekNum)
        lMeals = Meal.objects.filterAvailable(mealplan=oMealPlan).order_by(
            'sortKey')
        lAllergens = Allergen.objects.filter(use_for_menu=True).order_by(
            'name_short')
        lAdditionals = Additional.objects.all()

        fColumnCount = 3.0

        tAllergenSets = tuple()
        if len(lAllergens) > 0:
            fCount = float(len(lAllergens))
            iItemsInCol = int(math.ceil(fCount/fColumnCount))
            tAllergenSets = (lAllergens[0:iItemsInCol],
                             lAllergens[iItemsInCol:iItemsInCol*2],
                             lAllergens[iItemsInCol*2:],)

        tAdditionalSets = tuple()
        if len(lAdditionals) > 0:
            fCount = float(len(lAdditionals))
            iItemsInCol = int(math.ceil(fCount/fColumnCount))
            tAdditionalSets = (lAdditionals[0:iItemsInCol],
                               lAdditionals[iItemsInCol:iItemsInCol*2],
                               lAdditionals[iItemsInCol*2:],)

        lWeeksForLinks = DateHelper().getWeeks(DateHelper().getFirstDayInIsoweek(iYear, iWeekNum), -3)
        lWeeksForLinks += DateHelper().getWeeks(DateHelper().getFirstDayInIsoweek(iYear, iWeekNum), 8, False)
        iCurrentYear, iCurrentWeekNum, iCurrentWeekDay = datetime.now().isocalendar()

        # Get set of preparing weeks
        stWeeks = get_preparing_weeks(
            lWeeksForLinks[0]['days'][0], lWeeksForLinks[-1]['days'][-1],
            poMealPlan=oMealPlan)
        # Add has_preparation field to each week that has menu preparation
        for week in lWeeksForLinks:
            if week['week'] in stWeeks:
                week['has_preparation'] = True

        if oMealPlan is not None:
            pricegroups = oMealPlan.pricegroup_set.values_list('id', 'name')
            lWeekDays = lWeekDays[:oMealPlan.weekdays]
        else:
            pricegroups = []
            lWeekDays = lWeekDays[:MealPlan.get_max_weekdays()]

        dContext = {
            'module_name': force_unicode(oOpts.verbose_name_plural),
            #'title': 'Wochenansicht',
            'has_add_permission': self.has_add_permission(request),
            'app_label': sAppLabel,
            'admin_url': urlresolvers.reverse("admin:menus_menu_changelist"),
            'week_days': lWeekDays,
            'year': iYear,
            'week_num': iWeekNum,
            'weeks_for_links': lWeeksForLinks,
            'current_week_num': iCurrentWeekNum,
            'meals': lMeals,
            'allergens': tAllergenSets,
            'additionals': tAdditionalSets,
            'has_preparation': qsMenuPreparation.exists(),
            'avail_mealplans': MealPlan.objects.all(),
            'selected_mealplan': oMealPlan,
            'media': self.media,
            'pricegroups': pricegroups,
        }
        dContext.update(extra_context or {})
        context_instance = template.RequestContext(request, current_app=self.admin_site.name)
        return render_to_response('admin/week_view.html', dContext, context_instance=context_instance)


admin.site.register(Mealtime, MealtimeAdmin)
admin.site.register(Meal, MealAdmin)
admin.site.register(MealPlan, MealPlanAdmin)
admin.site.register(Menu, MenuAdmin)
admin.site.register(Price)
admin.site.register(Allergen, AllergenAdmin)
admin.site.register(Additional, AdditionalAdmin)
admin.site.register(MenuPreparation)
admin.site.register(PricePreparation)
admin.site.register(MenuImport)
