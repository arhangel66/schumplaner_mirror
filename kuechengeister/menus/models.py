# -*- coding: utf-8 -*-
from random import randint
from datetime import date, datetime
from decimal import Decimal

from django.db import models, transaction
from django.db.models import Count, Q, Max
from django.db.models.signals import pre_save, post_save, pre_delete
from django.db.utils import IntegrityError
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.log import getLogger
from django.core.validators import MinValueValidator, MaxValueValidator

from kg.models import Flatrate, Holiday, Order
from kg_utils.date_helper import DateHelper

from settings import MEALTIMES

logger = getLogger(__name__)

ORDER_TIMES = (
    (10, 'Tage vor Tag des Essens'),
    (11, 'Tage vor Tag des Essens oder Freitag, falls Essen am Montag'),
    (20, 'Tage vor Wochenanfang'),
    (30, 'Tag des Vormonats (31 = letzter Tag des Monats)'),
)


class Allergen(models.Model):
    name = models.CharField(max_length=100, verbose_name=u"Bezeichnung",
                            help_text=u"Geben Sie hier die vollständige Bezeichnung des Allergens ein.<br />"
                                      u"Allergene finden im Speiseplan Verwendung, können aber auch einzelnen Kindern zugeordnet werden.")
    name_short = models.CharField(max_length=20, blank=True, verbose_name=u"Kennzeichnung auf Speiseplan",
                                  help_text=u"Die Kennzeichnung wird nur für Allergene benötigt, die auf dem Speiseplan ausgewiesen werden.<br>"
                                            u"Dort wird die Kennzeichnung unter der Essensbeschreibung angezeigt. Meist werden kleine Buchstaben benutzt. Beispiel: a, j, k")
    use_for_menu = models.BooleanField(default=False, verbose_name=u"Auf Speiseplan ausweisen",
                                       help_text=u"Aktivieren Sie die Checkbox, wenn Sie dieses Allergen auf dem Speiseplan ausweisen möchten.<br />"
                                                 u"Das Allergen kann dann bei der Speiseplaneingabe ausgewählt werden und wird später auf dem Speiseplan ausgewiesen.")

    class Meta:
        ordering = ['name', 'name_short']
        verbose_name = "Allergen"
        verbose_name_plural = "Allergene"

    def __unicode__(self):
        return self.name


class Additional(models.Model):
    name = models.CharField(max_length=100, verbose_name=u"Bezeichnung",
                            help_text=u"Geben Sie hier die vollständige Bezeichnung des Zusatzstoffes ein. Zusatzstoffe finden im Speiseplan Verwendung.")
    name_short = models.CharField(max_length=20, blank=True, verbose_name=u"Kennzeichnung auf Speiseplan",
                                  help_text=u"Die Kennzeichnung wird auf dem Speiseplan unter der Essensbeschreibung angezeigt.<br />"
                                            u"Meist werden Zahlen benutzt. Beispiel: 1, 3, 4")
    sortKey = models.PositiveSmallIntegerField(verbose_name="Sortierung", default=0,
                                               help_text="Geben Sie hier bitte eine Zahl ein. Die Zusatzstoffe werden nach dieser Zahl in aufsteigender Reihenfolge sortiert.")

    class Meta:
        ordering = ['sortKey']
        verbose_name = "Zusatzstoffe"
        verbose_name_plural = "Zusatzstoffe"

    def __unicode__(self):
        return self.name


class Mealtime(models.Model):
    """ The mealtime of the day (breakfast, lunch, snack, dinner)
    The Ids are fix
    """

    MEALTIME_CHOICES = (
        (MEALTIMES['breakfast'], u'Frühstück'),
        (MEALTIMES['lunch'], u'Mittagessen'),
        (MEALTIMES['snack'], u'Vesper'),
        (MEALTIMES['dinner'], u'Abendessen')
    )

    id = models.PositiveSmallIntegerField(primary_key=True, choices=MEALTIME_CHOICES, verbose_name="Id")
    name = models.CharField(max_length=50, verbose_name="Bezeichnung")
    name_short = models.CharField(max_length=20,
                                  blank=True,
                                  verbose_name="Kurzbezeichnung",
                                  help_text=u"Die Kurzbezeichnung wird nur im internen Bereich und auf Listen verwendet. Im Kundenbereich wird immer die vollständige Bezeichnung angezeigt.")
    sortKey = models.PositiveSmallIntegerField(verbose_name="Sortierung",
                                               help_text="Geben Sie hier bitte eine Zahl ein. Die Mahlzeiten werden nach dieser Zahl in aufsteigender Reihenfolge sortiert.")

    class Meta:
        ordering = ['sortKey']
        verbose_name = "Mahlzeit"
        verbose_name_plural = "Mahlzeiten"

    @property
    def is_breakfast(self):
        return self.id == MEALTIMES['breakfast']

    @property
    def is_lunch(self):
        return self.id == MEALTIMES['lunch']

    def __unicode__(self):
        return self.name


class MealManager(models.Manager):

    def filterAvailable(self, mealplan=None, day=None, allergy=True):
        """Returns only meals available to specific mealplan, and at a given
        date if provided."""
        return self.getAvailableInRange(mealplan=mealplan, from_date=day,
                                        to_date=day, allergy=allergy)

    def getAvailableInRange(self, mealplan, from_date, to_date, allergy=True):
        """
        Return only meals available for at least one day within range of
        dates.

        :param mealplan: can be a mealplan or a query on mealplan object
        :param from_date: meals from starting date..
        :param to_date: ..to to_date
        :param allergy: Include or not allergy meals
        :return: a meal queryset.
        """
        qsMeals = self.get_query_set()
        qFilters = []

        if not allergy:
            qsMeals = qsMeals.exclude(allergy_meal=True)

        if mealplan is not None:
            qMealPlan = mealplan if isinstance(mealplan, Q) else \
                Q(periods__mealplan=mealplan)
            qFilters.append(qMealPlan)
        if to_date:
            qFilters.append(Q(periods__from_date__lte=to_date) | \
                            Q(periods__from_date=None))
        if from_date:
            qFilters.append(Q(periods__to_date__gte=from_date) | \
                            Q(periods__to_date=None))

        return qsMeals.filter(*qFilters).distinct()


class Meal(models.Model):
    name = models.CharField(max_length=50, verbose_name="Bezeichnung",
                            help_text=u"Die Bezeichnung wird auf den Speiseplänen im Kundenbereich und im internen Bereich angezeigt.")
    name_short = models.CharField(max_length=20, blank=True, verbose_name="Kurzbezeichnung",
                                  help_text=u"Die Kurzbezeichnung wird nur im internen Bereich und auf Listen verwendet. Im Kundenbereich wird immer die vollständige Bezeichnung angezeigt.")
    name_public_menu = models.CharField(max_length=100, blank=True, verbose_name=u"Bezeichnung auf Speiseplan Startseite",
                                        help_text=u"Geben Sie hier an, wie das Essensangebot auf dem Speiseplan auf der Startseite heißen soll.")
    name_pos_app = models.CharField(max_length=15, blank=True, verbose_name="Bezeichnung Ausgabeterminal",
                                    help_text=u"Geben Sie hier an, wie das Essensangebot auf dem Essenausgabeterminal heißen soll.")
    mealtime = models.ForeignKey(Mealtime, verbose_name="Mahlzeit",
                                 help_text=u"Wählen Sie aus, zu welcher Mahlzeit das Essensangebot gehört.")
    default_menu = models.ForeignKey('MenuImport', verbose_name=u"Standard Angebot",
                                     blank=True, null=True,
                                     help_text=u'Wählen Sie hier ein Essen aus, falls dieses Essen jeden Tag identisch angeboten wird (z.B. "Frühstück vom Buffet").')
    allow_order = models.BooleanField(default=True, verbose_name="Essensangebot ist bestellbar",
                                      help_text=u"Ein nicht bestellbares Essensangebot könnte z.B. ein Nachtisch sein, den es zu jedem Essen dazu gibt.")
    allergy_meal = models.BooleanField(
        default=False, verbose_name=u"Nur für Allergiker bestellbar",
        help_text=u"Aktivieren Sie die Checkbox, wenn das Essensangebot nur von Essensteilnehmern mit einer Allergie bestellt werden darf.")
    order_day_limit_days = models.PositiveSmallIntegerField(default=0, verbose_name="Bestellfrist Tage",
                                                            help_text="Geben Sie hier bitte eine Zahl ein.")
    order_day_limit = models.PositiveSmallIntegerField(default=0, choices=ORDER_TIMES,
                                                       verbose_name="Bestellfrist")
    order_time_limit = models.TimeField(default='00:00:00', verbose_name="Bestellfrist Uhrzeit")
    cancel_day_limit_days = models.PositiveSmallIntegerField(default=0, verbose_name="Abbestellfrist Tage",
                                                             help_text="Geben Sie hier bitte eine Zahl ein.")
    cancel_day_limit = models.PositiveSmallIntegerField(default=0, choices=ORDER_TIMES,
                                                        verbose_name="Abbestellfrist")
    cancel_time_limit = models.TimeField(default='00:00:00', verbose_name="Abbestellfrist Uhrzeit")
    sortKey = models.PositiveSmallIntegerField(verbose_name="Sortierung",
                                               help_text="Geben Sie hier bitte eine Zahl ein. Die Essensangebote werden nach dieser Zahl in aufsteigender Reihenfolge sortiert.")
    deleted = models.BooleanField(default=False, verbose_name=u"Gelöscht")

    objects = MealManager()

    class Meta:
        ordering = ['sortKey']
        verbose_name = u"Essensangebot"
        verbose_name_plural = u"Essensangebote"

    def __unicode__(self):
        return self.name

    def date_menu(self, date, mealplan=None):
        """
        Return first menu in meal in given date. If menu is not available, find
        MenuPreparation instead.

        :param date: datetime
        :return: None | Menu
        """
        for cClass in [Menu, MenuPreparation]:
            qsMenu = cClass.objects.filter(meal=self.id, date=date)

            # Returns specific mealplan menu instead of general one if exists
            qsMenu = qsMenu.filter(
                Q(mealplan__isnull=True) | Q(mealplan=mealplan)) \
                .order_by('meal__id', 'date', 'mealplan__id') \
                .distinct('meal__id', 'date')

            if qsMenu.exists():
                return qsMenu.select_related()[0]
        return None

    def is_available(self, mealplan, day):
        """Determine whether a meal if available for the given mealplan at
        a specific date."""
        queryset = self.periods.all()
        if mealplan:
            queryset = queryset.filter(mealplan=mealplan)
        valid_day = isinstance(day, date)
        if valid_day:
            queryset = queryset.filter(
                (Q(from_date__lte=day) | Q(from_date=None)) &
                (Q(to_date__gte=day) | Q(to_date=None))
                )
        exists = queryset.exists()
        # Check if current weekday belong to any mealplan
        max_weekday = 1
        if exists and valid_day:
            # for mealplan in queryset.values_list('mealplan', flat=True)
            mealplan_ids = queryset.values_list(
                'mealplan', flat=True).distinct()
            qs_max = MealPlan.objects.filter(id__in=mealplan_ids).aggregate(
                Max('weekdays'))
            max_weekday = qs_max['weekdays__max'] or max_weekday
            exists = day.isoweekday() <= max_weekday

        return exists

    @property
    def mealplan_total(self):
        """Returns number of mealplan using this meal."""
        return MealPeriod.objects.filter(meal=self).distinct('mealplan').count()


class MealPeriod(models.Model):
    """Defines effective time of a meal within a specific meal plan."""
    meal = models.ForeignKey('Meal', related_name='periods')
    mealplan = models.ForeignKey('MealPlan', related_name='meal_periods')
    from_date = models.DateField(verbose_name=u'Gültig ab',
                                 blank=True, null=True)
    to_date = models.DateField(verbose_name=u'Gültig bis',
                               blank=True, null=True)

    def __unicode__(self):
        return self.meal.name

    class Meta:
        unique_together = (('meal', 'mealplan', 'from_date', 'to_date'),)
        verbose_name = "Essensangebot"
        verbose_name_plural = "Essensangebote"


class MealPlan(models.Model):
    name = models.CharField(
        max_length=50, verbose_name="Bezeichnung",
        help_text=u"Die Bezeichnung wird nur im internen Bereich verwendet.")
    duplicate_order_mealtime = models.BooleanField(
        default=False, verbose_name=u"Verschiedene pro Mahlzeit",
        help_text=u"Pro Mahlzeit kann mehr als ein Menü gewählt werden.")
    max_order_quantity = models.PositiveIntegerField(
        default=1, verbose_name=u"Maximale Bestellmenge",
        validators=[MinValueValidator(1)],
        help_text=u"Wie viele Essen dürfen max. pro Mahlzeit bestellt werden.")
    show_price = models.BooleanField(
        default=True, verbose_name=u"Preise im Kundenbereich anzeigen")
    show_price_public = models.BooleanField(
        default=False, verbose_name=u"Preise im öffentlichen Bereich anzeigen, z.B. auf Speiseplan Startseite.")
    weekdays = models.PositiveSmallIntegerField(
        choices=((wd, wd) for wd in range(1, 8)), default=5,
        verbose_name=u"Wochentage",
        validators=[MinValueValidator(1), MaxValueValidator(7)],
        help_text=u"Geben Sie hier bitte an, wieviel Wochentage der Speiseplan haben soll.")

    @classmethod
    def get_max_weekdays(cls):
        # TODO: not good when having this queried at every call
        max_wd = cls.objects.all().aggregate(Max('weekdays'))['weekdays__max']
        return max_wd or 7

    def meals(self, day=None):
        """Return all effective meals of current meal plan at the given
        time."""
        day = day or datetime.today()
        return Meal.objects.filterAvailable(mealplan=self, day=day)

    @property
    def has_preparation_menu(self):
        return MenuPreparation.objects.filter(mealplan=self).exists()

    class Meta:
        ordering = ['name']
        verbose_name = u"Speiseplan"
        verbose_name_plural = u"Speisepläne"

    def __unicode__(self):
        return self.name


class MenuManager(models.Manager):
    def getMealsPerCalendarWeek(self, year, calendar_week):
        day_list = DateHelper.getDaysInCalendarWeek(year, calendar_week)
        return super(MenuManager, self).get_query_set().filter(date__range=(day_list[0], day_list[6])).order_by('date')

    def getRandomMenuDescriptionPerMealtime(self, mealtime):
        count = self.filter(meal__mealtime=mealtime).aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        menu = self.filter(meal__mealtime=mealtime)[random_index]
        return menu.description


class AbstractMenu(models.Model):
    """An abstract model that holds information of a menu."""

    description = models.TextField(verbose_name="Beschreibung", blank=True)
    meal = models.ForeignKey(Meal, verbose_name="Essensangebot")
    date = models.DateField(verbose_name="Datum")
    allergens = models.ManyToManyField(Allergen, verbose_name="Allergene")
    additionals = models.ManyToManyField(Additional, verbose_name="Zusatzstoffe")
    order_timelimit = models.DateTimeField(verbose_name="Bestellbar bis")
    cancel_timelimit = models.DateTimeField(verbose_name="Abbestellbar bis")
    can_be_ordered = models.BooleanField(default=True, verbose_name="Bestellbar ja/nein")
    mealplan = models.ForeignKey(
        MealPlan, verbose_name=u"Speiseplan", related_name="%(class)s_set",
        null=True, blank=True)
    disabled = models.BooleanField(
        verbose_name=_("disabled"), default=False,
        help_text=_("Disabled menu will not be visible to customer and can "
                    "not be ordered."))
    required_menu = models.ForeignKey(
        'self', verbose_name=u'Erforderlich Menü',
        related_name='dependent_menus', null=True, blank=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        name = u"%s - %s - %s" % (
            str(self.date), self.meal.name, self.description)
        return name

    @property
    def is_preparation(self):
        return isinstance(self, MenuPreparation)

    def save(self, *args, **kwargs):
        # When mealplan property is None, the unique constraint will not
        # take effect. This is the prevention for duplication.
        if not self.mealplan:
            queryset = self.__class__.objects.exclude(pk=self.pk)
            if queryset.filter(meal_id=self.meal_id,
                               date=self.date,
                               mealplan=None).exists():
                raise IntegrityError('General mealplan: menu duplicated')
        super(AbstractMenu, self).save(*args, **kwargs)

    def get_price(self, price_group, child=None):
        """ Returns price value of current menu in given pricegroup. If child is
            provided, reduction will be included if present.
        :return dict {'price', 'reduction_id'}
        """
        dPrice = {'price': None, 'reduction_id': 0}
        # show_price = self.mealplan.show_price if self.mealplan else True
        # if show_price and self.can_be_ordered:
        if self.can_be_ordered:
            # Get price from Price object
            qsPrice = Price.objects.filter(menu=self, price_group=price_group)
            dPrice['price'] = qsPrice[0].price if qsPrice else dPrice['price']
            if child is None:
                return dPrice['price']
            # Further calculation with reduction
            oChildReduction = child.get_reduction_for_date(self.date)
            if oChildReduction is not None:
                oReduction = oChildReduction.reduction
                if oReduction.per_menu:
                    # Always valid in holiday with kindergartens
                    valid_on_holiday = (oReduction.valid_on_holiday or
                                        child.facility.is_kindergarten)
                    is_holiday = Holiday.objects.filter(
                        date_start__lte=self.date, date_end__gte=self.date
                        ).exists()
                    if (valid_on_holiday or not is_holiday)\
                            and oReduction.reduction < dPrice['price'] \
                            and self.meal.mealtime in oReduction.mealtimes.all():
                        dPrice['price'] = oReduction.reduction
                        dPrice['reduction_id'] = oReduction.id
                else:
                    dPrice['price'] = Decimal('0.0')
                    dPrice['reduction_id'] = oReduction.id

        return dPrice


class Menu(AbstractMenu):

    objects = MenuManager()

    class Meta:
        unique_together = ("meal", "date", "mealplan")
        ordering = ['-date', 'meal']
        verbose_name = "Essen"
        verbose_name_plural = "Essen"

    def is_on_holiday(self):
        holiday = Holiday.objects.filter(date_start__lte=self.date, date_end__gte=self.date)
        if holiday:
            return True
        else:
            return False

    def create_menuimport(self):
        """This ensures that MenuImport will be create or update if needed
        in order to maintain list of menu import uniqueness."""
        desc = self.description
        try:
            menu_import = MenuImport.objects.get(description__iexact=desc)
            menu_import.allergens.clear()
            menu_import.additionals.clear()
        except MenuImport.DoesNotExist:
            menu_import = MenuImport(description=desc)
            menu_import.save()

        # All allergens and additionals info will be overrided by the latest
        # data. However mealtimes will be appended to keep it importable by
        # other meal as well.
        for allergen in self.allergens.values_list('pk', flat=True):
            menu_import.allergens.add(allergen)
        for additional in self.additionals.values_list('pk', flat=True):
            menu_import.additionals.add(additional)
        menu_import.mealtimes.add(self.meal.mealtime)

        return menu_import

    def transfer_orders(self, from_general=True):
        """ This will transfer all orders from or to corresponding general menu.
        """
        # Only executing from special mealplan
        if self.mealplan is None:
            return
        try:
            general_menu = Menu.objects.get(
                date=self.date, mealplan=None, meal=self.meal
            )
            if from_general:
                orders = general_menu.order_set.filter(
                    child__facility_subunit__mealplan=self.mealplan)
                orders.update(menu=self)
            else:
                # This should be better resolved. Even after  the order_set
                # is updated to point to general_menu. Method delete() still
                # clear all those orders.
                for order in self.order_set.all():
                    order.menu = general_menu
                    order.id = None
                    order.save()
        except Menu.DoesNotExist:
            pass

    def save(self, *args, **kwargs):
        """ If special menu is created, orders from general menu will be
            transfer to this one.
        """
        transfer_orders_needed = not self.id and bool(self.mealplan)
        with transaction.commit_manually():
            try:
                super(Menu, self).save(*args, **kwargs)
                if transfer_orders_needed:
                    self.transfer_orders(from_general=True)
                transaction.commit()
            except Exception as ex:
                transaction.rollback()
                logger.exception('Error when saving menu: ' + self.description)
                raise ex


@receiver(pre_delete, sender=Menu)
def handle_menu_pre_deletion(sender, **kwargs):
    """ If menu belongs to specific mealplan, move all orders to general menu
        if existing.
    """
    menu = kwargs.get('instance')
    if menu.mealplan is not None:
        menu.transfer_orders(from_general=False)


@receiver(post_save, sender=Menu)
def __createFlatrateOrdersForMenu(sender, **kwargs):
    menu = kwargs.get('instance')
    if menu is not None:
        in_past = DateHelper.is_past_date(menu.date)
        # Only create orders when menu time is in future
        if kwargs['created'] and menu.can_be_ordered and not in_past:
                # select active flatrates according to whether menu is on holiday or not
                if menu.is_on_holiday():
                    temp_flatrates = Flatrate.objects.active_on_holidays()
                else:
                    temp_flatrates = Flatrate.objects.active()

                # find all flatrate objects matching mealtime and weekday of the created menu
                # filter children from suspended customers out
                flatrates = temp_flatrates.filter(child__customer__is_suspended=False,
                                                meal=kwargs['instance'].meal,
                                                weekday=date.isoweekday(kwargs['instance'].date))

                if menu.mealplan_id:
                    # If menu is for specific mealplan, filter the flatrates with
                    # child's subunit's mealplan.
                    flatrates = flatrates.filter(
                        child__facility_subunit__mealplan_id=menu.mealplan_id)

                # Find all other menus with the same meal & date.
                qsMenus = Menu.objects.filter(meal=menu.meal, date=menu.date) \
                    .exclude(mealplan_id=menu.mealplan_id)
                lMenus = [dItem['id'] for dItem in qsMenus.values("id")]

                if not menu.mealplan_id:
                    # Get non-orderable mealplans from specific mealplan menus with
                    # the same meal & date
                    lNonOrderableMealPlans = qsMenus.filter(
                        can_be_ordered=False, mealplan__isnull=False) \
                        .values("mealplan_id")
                    lNonOrderableMealPlans = [dItem['mealplan_id'] for dItem in
                                            lNonOrderableMealPlans]
                    # Exclude the non-orderable mealplans above from flatrates
                    # queryset.
                    flatrates = flatrates.exclude(
                        child__facility_subunit__mealplan_id__in=
                        lNonOrderableMealPlans)

                # Order will be created only if child has no existing order for
                # the same meal & date.
                flatrates = flatrates.exclude(
                    child__order__menu__in=lMenus
                ).distinct()

                for flatrate in flatrates:
                    order = Order(
                        child=flatrate.child,
                        menu=menu,
                        quantity=1,
                        order_reason=Order.REASON_FLATRATE,
                    )
                    order.save()

        menu.create_menuimport()


@receiver(pre_save, sender=Flatrate)
def __createFlatrateOrdersForFlatrate(sender, **kwargs):

    new_flat = kwargs['instance']

    # if customer is suspended create no orders
    if new_flat.child.customer.is_suspended:
        return None

    order_action_required = False
    create_normal_orders = False
    erase_normal_orders = False
    create_holiday_orders = False
    erase_holiday_orders = False

    # flatrate does not exist in database
    if new_flat.pk is None:
        # new flatrate, create orders according to flatrate settings
        order_action_required = True
        create_normal_orders = new_flat.active
        create_holiday_orders = new_flat.active_on_holidays

    # flatrate still exists in database
    else:
        # retrieve existing flatrate and act only on changes
        old_flat = Flatrate.objects.get(pk=new_flat.pk)

        # active flatrates always activate future orders
        # inactive flatrates deactivate orders only if flatrate status has changed

        if new_flat.active or new_flat.active != old_flat.active:
            order_action_required = True
            if new_flat.active:
                create_normal_orders = True
            else:
                erase_normal_orders = True

        if new_flat.active_on_holidays or new_flat.active_on_holidays != old_flat.active_on_holidays:
            order_action_required = True
            if new_flat.active_on_holidays:
                create_holiday_orders = True
            else:
                erase_holiday_orders = True

    # return if no change in flatrate
    if not order_action_required:
        return None

    now = datetime.now()

    # find all menus from next monday on with the right weekday
    # Attention: the django QuerySet date__week_day function uses other weekday codes than
    # the python date.isoweekday() function, therefore + 1; see reference
    menus = Menu.objects.filter(meal=new_flat.meal,
                                date__gte=DateHelper.getNextMonday(),
                                date__week_day=int(new_flat.weekday)+1)

    # Filter for child's mealplan menus or general mealplan menus.
    menus = menus.filter(
        Q(mealplan_id=new_flat.child.facility_subunit.mealplan_id) |
        Q(mealplan__isnull=True))

    # Exclude general mealplan menu if a specific mealplan menu exists.
    menus = menus.order_by('meal__id', 'date', 'mealplan__id') \
        .distinct('meal__id', 'date')

    # create or erase order for the found menus
    for menu in menus:
        if menu.can_be_ordered and menu.order_timelimit > now:

            # retrieve or create orders for menus
            order, created = Order.all_objects.get_or_create(
                child=new_flat.child, menu=menu)

            # set new orders inactive as default
            if created:
                order.quantity = 0
                order.order_reason = Order.REASON_FLATRATE

            # set order status and save order
            if menu.is_on_holiday():
                if create_holiday_orders:
                    order.quantity = 1
                if erase_holiday_orders:
                    order.quantity = 0
            else:
                if create_normal_orders:
                    order.quantity = 1
                if erase_normal_orders:
                    order.quantity = 0

            # set active orders from same mealtime to inactive (prevent double booking)
            # only if order will be created
            if order.quantity > 0:
                oParallelOrders = Order.objects.filter(child=new_flat.child, menu__date=menu.date, menu__meal__mealtime=menu.meal.mealtime)
                for oPO in oParallelOrders:
                    oPO.quantity = 0
                    oPO.save()

            # save order
            order.save()

    return None


class MenuImport(models.Model):
    """Model for unique menu items which will be used in Menu Import dialog.
    This prevent duplication of data in storage, querying and rendering."""
    description = models.TextField(verbose_name="Beschreibung", blank=True)
    allergens = models.ManyToManyField(Allergen, verbose_name="Allergene")
    additionals = models.ManyToManyField(
        Additional, verbose_name="Zusatzstoffe")
    mealtimes = models.ManyToManyField(Mealtime)

    class Meta:
        verbose_name = "Menu Import"
        verbose_name_plural = "Menu Imports"

    def __unicode__(self):
        return self.description

    def prepare_menu(self, mealplan, meal, date, preparation=True):
        """Create MenuPreparation object for given mealplan & date."""
        order_timelimit = DateHelper.calculateOrderTimelimit(
            date, meal.order_day_limit_days,
            meal.order_day_limit, meal.order_time_limit)
        cancel_timelimit = DateHelper.calculateOrderTimelimit(
            date, meal.cancel_day_limit_days,
            meal.cancel_day_limit, meal.cancel_time_limit)
        MenuClass = MenuPreparation if preparation else Menu
        menu = MenuClass(
            description=self.description,
            meal=meal,
            mealplan=mealplan,
            date=date,
            order_timelimit=order_timelimit,
            cancel_timelimit=cancel_timelimit,
        )
        menu.save()

        for attr in ['allergens', 'additionals']:
            for item in getattr(self, attr).values_list('pk', flat=True):
                getattr(menu, attr).add(item)

        # Ensuring about menu prices, make it similar to saving via menu dialog
        from menus.helpers import initialize_menu_prices
        initialize_menu_prices(menu)

        return menu


class PriceGroup(models.Model):
    name = models.CharField(max_length=50, verbose_name="Bezeichnung")
    mealplan = models.ForeignKey(MealPlan, verbose_name=u"Speiseplan", null=True)

    class Meta:
        ordering = ['name']
        verbose_name = "Preisgruppe"
        verbose_name_plural = "Preisgruppen"

    def __unicode__(self):
        prefix = self.mealplan.name if self.mealplan is not None else ''
        return "%s - %s" % (prefix, self.name)

    def get_meal_default_price(self, meal):
        """Returns price value of given meal within this price group.
        """
        try:
            price = self.mealdefaultprice_set.get(meal=meal).price
        except MealDefaultPrice.DoesNotExist:
            logger.error(u'Default price missing: pricegroup={}, meal={}'.format(self, meal.name))
            price = None
        return price


class MealDefaultPrice(models.Model):
    meal = models.ForeignKey(Meal, verbose_name="Essensangebot")
    price_group = models.ForeignKey(PriceGroup, verbose_name="Preisgruppe")
    price = models.DecimalField(max_digits=4, decimal_places=2, verbose_name="Fixpreis",
                                help_text=u"Geben Sie hier bitte den Preis an, den alle Essen des gewählten Essensangebotes in dieser Preisgruppe immer haben. "
                                          u"Hat diese Preisgruppe keinen Fixpreis, geben Sie bitte 0.00 ein.")
    price_is_variable = models.BooleanField(default=False, verbose_name="Kein Fixpreis",
                                            help_text=u"Ist der Preis von Tag zu Tag verschieden (kein Fixpreis), aktivieren Sie bitte diese Checkbox.")

    class Meta:
        unique_together = ("meal", "price_group")
        verbose_name = "Preis"
        verbose_name_plural = "Preise"

    def __unicode__(self):
        uc_string =  u"%s - %s - %s" % ( self.meal.name, self.price_group.name, self.price)
        if self.price_is_variable:
            uc_string += u" - variabel"
        else:
            uc_string += u" - fix"

        return uc_string


class AbstractPrice(models.Model):
    """An abstract model that hold common information of menu price."""

    price_group = models.ForeignKey(PriceGroup, verbose_name="Preisgruppe")
    price = models.DecimalField(max_digits=4, decimal_places=2,
                                verbose_name="Preis")

    class Meta:
        abstract = True
        unique_together = ("menu", "price_group")
        verbose_name = "Preis"
        verbose_name_plural = "Preise"

    def __unicode__(self):
        return u"%s - %s - %s" % (self.menu, self.price_group, self.price)


class Price(AbstractPrice):
    menu = models.ForeignKey(Menu, verbose_name="Essen", related_name='prices')


class MenuPreparation(AbstractMenu):
    """Hold the same information as Menu with some addtional fields for
    preparing a Meal Plan. MenuPreparation will be converted into Menu after
    Meal Plan is released."""

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)

    class Meta:
        unique_together = ("meal", "date", "mealplan")

    def save(self, *args, **kwargs):
        if Menu.objects.filter(mealplan_id=self.mealplan_id,
                               meal_id=self.meal_id,
                               date=self.date).exists():
            raise IntegrityError('Released menu already exists')

        # If current meal belongs to only one mealplan, make this
        # preparation general.
        if self.mealplan and self.meal.mealplan_total == 1:
            self.mealplan = None

        super(MenuPreparation, self).save(*args, **kwargs)


class PricePreparation(AbstractPrice):
    """Hold price information for menu prepration."""
    menu = models.ForeignKey(MenuPreparation, verbose_name="Essen",
                             related_name='prices')
