# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = False

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 8
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '03.10.2016'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'WarnowService GmbH <bestellsystem@schulspeisung-warnowservice.de>'


# SEPA Configuration
SEPA = {'SEPA_NAME': 'WarnowService GmbH',
        'SEPA_EREF_PREFIX': 'WSGX',
        'SEPA_CREDITOR_ID': 'DE27ZZZ00002034834',
        'SEPA_IBAN': 'DE65130500000705007987',
        'SEPA_BIC': 'NOLADE21ROS',
        'SEPA_DEBIT_FRST_DAYS': 3,
        'SEPA_DEBIT_RCUR_DAYS': 3,
        'SEPA_FORM_COMPANY_LONG': 'WarnowService GmbH, Am Forsthof 3, 18246 Bützow',
        'SEPA_FORM_COMPANY_SHORT': 'WarnowService GmbH',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'Schulspeisung Warnowservice',
                'SITE_HTML_ADMIN_TITLE'                : 'Schulspeisung Warnowservice - Verwaltung',
                'SITE_NAME'                            : 'www.schulspeisung-warnowservice.de',
                'SITE_URL'                             : 'https://www.schulspeisung-warnowservice.de',
                'SITE_REGARDS'                         : 'Ihre WarnowService GmbH',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'WarnowService GmbH',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : '',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '',
                'INVOICE_COMPANY_TELEFONE'             : '',
                'INVOICE_COMPANY_EMAIL'                : '',
                'INVOICE_COMPANY_TAXID'                : '',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : '',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : '',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : '',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 10,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '153d5e',
                'PRINTMENUE_COLORLIGHT'                : '7baf2c',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : 'Zusatzstoffe: 1 Konservierungsstoffe; 2 Farbstoffe; 3 Geschmacksverstärker; 4 Süßungsmittel; 5 Verdickungsmittel; 6 modifizierte Stärke; 7 Stabilisatoren<br />Allergene: A Glutenhaltiges Getreide*; B Krebstiere*; C Eier*; D Fisch*; E Erdnüsse*; F Soja*; G Laktose*; H Schalenfrüchte*; I Sellerie*; J Senf*; K Sesamsamen*; L Schwefeldioxid u. Sulfite; M Lupinen*; N Weichtiere* (* und Erzeugnisse daraus)'
                }

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

