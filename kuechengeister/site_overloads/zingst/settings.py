# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = False

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 6
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '20.02.2017'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'Schulessen Zingst <info@schulessen-zingst.de>'


# SEPA Configuration
SEPA = {'SEPA_NAME': 'Gemeinde Ostseeheilbad Zingst',
        'SEPA_EREF_PREFIX': 'ZINGST',
        'SEPA_CREDITOR_ID': 'DE66GVZ00000328678',
        'SEPA_IBAN': 'DE03150505000000000604',
        'SEPA_BIC': 'NOLADE21GRW',
        'SEPA_DEBIT_FRST_DAYS': '15.',
        'SEPA_DEBIT_RCUR_DAYS': '15.',
        'SEPA_FORM_COMPANY_LONG': 'Gemeinde Ostseeheilbad Zingst, Hanshäger Straße 1, 18374 Zingst',
        'SEPA_FORM_COMPANY_SHORT': 'Gemeinde Ostseeheilbad Zingst',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'Schulessen Zingst',
                'SITE_HTML_ADMIN_TITLE'                : 'Schulessen Zingst - Verwaltung',
                'SITE_NAME'                            : 'schulessen-zingst.de',
                'SITE_URL'                             : 'https://www.schulessen-zingst.de',
                'SITE_REGARDS'                         : 'Ihre Schulküche',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'Gemeinde Ostseeheilbad Zingst',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : 'Hanshäger Straße 1',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '18374 Zingst',
                'INVOICE_COMPANY_TELEFONE'             : '038232 - 169621',
                'INVOICE_COMPANY_EMAIL'                : 'schulkueche@zingst.de',
                'INVOICE_COMPANY_TAXID'                : '',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : 'Sparkasse Vorpommern',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : 'DE03150505000000000604',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : 'NOLADE21GRW',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 10,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '6CA80E',
                'PRINTMENUE_COLORLIGHT'                : 'E4E4E4',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : '1 Farbstoff, 2 Konservierungsstoff, 3 Antioxidationsmittel, 4 Geschmacksverstärker, 5 Süßungsmittel, 6 Phosphate, 7 gewachst, 8 geschwärzt <br />Allergenkennzeichnung: enthält a Glutenhaltiges Getreide (d.h. Weizen, Roggen, Gerste, Hafer, Dinkel, Kamut oder deren Hybridstämme)*, b Krebstiere*, c Ei*, d Fisch*, e Erdnuss*, f Soja*, g Milch*, h Schalenfrüchte (Nüsse)*, i Sellerie*, j Senf*, k Sesamsamen*, l Schwefeldioxid und Sulfite, m Lupine*, n Weichtiere* (* und Erzeugnisse daraus)',
                }

# Prepaid module settings
MODULE_PREPAID = False
PREPAID_SETTINGS_CREDIT = '0.00'  # the credit the customer is allowed to have in his account
PREPAID_SETTINGS = {'SEPA_DEBIT': False,
                    'BANK_TRANSFER': True,
                    'FLATRATE': True,
                    }

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

# Accounting Module Configuration
MODULE_ACCOUNTING = True
ACCOUNTING_SETTINGS = { 'CSV_DELIMITER': ';',  # the csv delimiter
                        'CSV_ACCOUNT_NUMBER': 'Kontonummer',  # Name des Konto Kontonummer Kopffeldes
                        'CSV_ACCOUNT_ICODE': 'BLZ',  # Name des Konto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_NUMBER': 'Gegenkonto',  # Name des Gegenkonto Kontonummer Kopffeldes
                        'CSV_CONTRAACCOUNT_ICODE': 'Gegenkonto BLZ',  # Name des Gegenkonto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_OWNER': 'Gegenkonto Inhaber',  # Name des Gegenkonto Inhaber Kopffeldes
                        'CSV_DATE': 'Datum',  # Name des Datum Kopffeldes
                        'CSV_DATE_FORMAT': '%d.%m.%Y',  # date format for parsing with strptime() function
                        'CSV_VALUTA': 'Valuta',  # Name des Valuta Kopffeldes
                        'CSV_VALUTA_FORMAT': '%d.%m.%Y',  # valuta date format for parsing with strptime() function
                        'CSV_SUBJECT_1': 'Verwendungszweck',  # Name des Verwendungszweck Kopffeldes
                        'CSV_SUBJECT_2': 'Verwendungszweck 2',  # Name des Verwendungszweck 2 Kopffeldes
                        'CSV_SUBJECT_MORE': 'Weitere Verwendungszwecke',  # Name des Weitere Verwendungszwecke Kopffeldes
                        'CSV_AMOUNT': 'Betrag',  # Name des Betrag Kopffeldes
                        'CSV_TYPE': 'Art',  # Name des Buchungstyp Kopffeldes
                        'CSV_TYPE_CHOICES': {'TYPE_BANK_TRANSFER_IN': ['UEBERWEISUNGSGUTSCHRIFT',
                                                                    'SEPA-CT HABEN EINZELBUCHUNG',
                                                                    'GUTSCHR. NEUTR. UEBW. EZUE'],  # Überweisung
                                             'TYPE_DEBIT': ['SEPA-DD SAMMLER-HABEN CORE',
                                                            'SEPA-DD EINZELLB.HABEN CORE'],  # Lastschrift
                                             'TYPE_DEBIT_RETURN': ['RETOURENHUELLE LASTSCHRIFT',
                                                                   'SEPA-DD SOLL RUECKBEL. CORE'], # Rücklastschrift
                                            },
                        'DEBIT_RETURN_FEE': 'ORIGINAL',  # Kosten für Lastschriftrückgabe
                        'REMINDER_FEE': '3.00',  # Kosten für 1. Mahnung
                        'REMINDER_FEE_2': '6.00',  # Kosten für 2. Mahnung
                        'REMINDER_FEE_3': '9.00',  # Kosten für 3. Mahnung
                        'REMINDER_PAYMENT_PERIOD': 10,  # Zahlungsfrist für 1. Mahnung
                        'REMINDER_PAYMENT_PERIOD_2': 5,  # Zahlungsfrist für 2. Mahnung
                        'REMINDER_PAYMENT_PERIOD_3': 5,  # Zahlungsfrist für 3. Mahnung
                        }