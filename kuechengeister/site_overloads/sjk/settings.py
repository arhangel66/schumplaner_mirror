# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = False

# Customer model extra attributes configuration
#CUST_EXTRA = {'ACTIVE': True, 'BLANK': True, 'VERBOSE_NAME': 'Debitor-Nr.', 'UNIQUE': True, 'HELP_TEXT': 'Wird vom System automatisch vergeben.', 'READONLY': True, 'CHOICES': None}

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 6
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '07.08.2017'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'SJK-Menü Bestellsystem <bestellsystem@sjk-menue.de>'


# SEPA Configuration
SEPA = {'SEPA_NAME': '',
        'SEPA_EREF_PREFIX': '',
        'SEPA_CREDITOR_ID': '',
        'SEPA_IBAN': '',
        'SEPA_BIC': '',
        'SEPA_DEBIT_FRST_DAYS': '3',
        'SEPA_DEBIT_RCUR_DAYS': '3',
        'SEPA_FORM_COMPANY_LONG': 'Schule, Jugend, Kids & Co e.V., An der Hansalinie 10b, 48163 Münster',
        'SEPA_FORM_COMPANY_SHORT': 'Schule, Jugend, Kids & Co e.V.',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'SJK-Menü',
                'SITE_HTML_ADMIN_TITLE'                : 'SJK-Menü - Verwaltung',
                'SITE_NAME'                            : 'www.sjk-menue.de',
                'SITE_URL'                             : 'https://www.sjk-menue.de',
                'SITE_REGARDS'                         : 'Ihr Schule, Jugend, Kids & Co e.V.',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'Schule, Jugend, Kids & Co e.V.',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : '',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '',
                'INVOICE_COMPANY_TELEFONE'             : '',
                'INVOICE_COMPANY_EMAIL'                : '',
                'INVOICE_COMPANY_TAXID'                : '',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : '',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : '',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : '',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 10,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '6CA80E',
                'PRINTMENUE_COLORLIGHT'                : 'E4E4E4',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : '1 mit Farbstoff(en), 2 mit Konservierungsstoffen, 3 mit Antioxidationsmittel, 4 mit Geschmacksverstärker(n), 5 geschwefelt, 6 geschwärzt, 7 gewachst, 8 mit Phosphat, 9 mit Süssungsmittel(n), 10 enthält eine Phenylalaninquelle<br />Allergenkennzeichnung: enthält 11 Glutenhaltiges Getreide *, 12 Krebstiere*, 13 Eier*, 14 Fisch*, 15 Erdnüsse*, 16 Soja*, 17 Milch (Milcheiweiß, Laktose)*, 18 Schalenfrüchte (Nüsse)*, 19 Sellerie*, 20 Senf*, 21 Sesamsamen*, 22 Schwefeldioxid und Sulfite, 23 Lupinen*, 24 Weichtiere* (* und Erzeugnisse daraus)',
                }

# Prepaid module settings
MODULE_PREPAID = True
PREPAID_SETTINGS_CREDIT = '0.00'  # the credit the customer is allowed to have in his account
PREPAID_SETTINGS = {'SEPA_DEBIT': False,
                    'BANK_TRANSFER': True,
                    'FLATRATE': False,
                    }

MODULE_FLATRATE = False

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

# Accounting Module Configuration
MODULE_ACCOUNTING = False
ACCOUNTING_SETTINGS = { 'CSV_DELIMITER': ';',  # the csv delimiter
                        'CSV_CONTRAACCOUNT_OWNER': u'Zahlungspflichtiger/-empfänger',  # Name des Gegenkonto Inhaber Kopffeldes
                        'CSV_CONTRAACCOUNT_NUMBER': 'ZP/ZE Konto/IBAN',  # Name des Gegenkonto Kontonummer Kopffeldes
                        'CSV_CONTRAACCOUNT_ICODE': 'ZP/ZE Bankleitzahl/BIC',  # Name des Gegenkonto Bankleitzahl Kopffeldes
                        'CSV_DATE': 'Datum',  # Name des Datum Kopffeldes
                        'CSV_DATE_FORMAT': '%d.%m.%Y',  # date format for parsing with strptime() function
                        'CSV_SUBJECT_FIELDS': ['Verwendungszweck',],  # Liste mit Namen aller Verwendungszweck Kopffelder
                        'CSV_AMOUNT': 'Betrag',  # Name des Betrag Kopffeldes
                        'CSV_TYPE': 'Kategorie',  # Name des Buchungstyp Kopffeldes
                        'CSV_TYPE_CHOICES': {'TYPE_BANK_TRANSFER_IN': ['EKS Zahlungen',],  # Überweisung
                                             'TYPE_DEBIT': [],  # Lastschrift
                                             'TYPE_DEBIT_RETURN': [], # Rücklastschrift
                                             },
                        'DEBIT_RETURN_FEE': 'ORIGINAL',  # Kosten für Lastschriftrückgabe
                        'REMINDER_FEE': '3.00',  # Kosten für 1. Mahnung
                        'REMINDER_FEE_2': '6.00',  # Kosten für 2. Mahnung
                        'REMINDER_FEE_3': '9.00',  # Kosten für 3. Mahnung
                        'REMINDER_PAYMENT_PERIOD': 10,  # Zahlungsfrist für 1. Mahnung
                        'REMINDER_PAYMENT_PERIOD_2': 5,  # Zahlungsfrist für 2. Mahnung
                        'REMINDER_PAYMENT_PERIOD_3': 5,  # Zahlungsfrist für 3. Mahnung
                        }