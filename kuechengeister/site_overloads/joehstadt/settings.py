# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = False

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 6
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '13.02.2017'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'Schulessen Jöhstadt <bestellsystem@schulessen-joehstadt.de>'
NEW_CUSTOMER_INFORMATION_EMAIL = 'schulessen@joehstadt.de'

# SEPA Configuration
SEPA = {'SEPA_NAME': 'Demo GmbH',
        'SEPA_EREF_PREFIX': 'SCHULMENUEPLANER',
        'SEPA_CREDITOR_ID': 'DE0000000000000000',
        'SEPA_IBAN': 'DE00000000000000000000',
        'SEPA_BIC': 'XXX0000XXX',
        'SEPA_DEBIT_FRST_DAYS': 6,
        'SEPA_DEBIT_RCUR_DAYS': 3,
        'SEPA_FORM_COMPANY_LONG': 'Demo GmbH, Musterstraße 11, 12345 Musterstadt',
        'SEPA_FORM_COMPANY_SHORT': 'Demo GmbH',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'Schulessen Jöhstadt',
                'SITE_HTML_ADMIN_TITLE'                : 'Schulessen Jöhstadt - Verwaltung',
                'SITE_NAME'                            : 'schulessen-joehstadt.de',
                'SITE_URL'                             : 'https://www.schulessen-joehstadt.de',
                'SITE_REGARDS'                         : 'Ihr Schulessen Jöhstadt Team',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'Stadt Jöhstadt',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : 'Markt 185',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '09477 Jöhstadt',
                'INVOICE_COMPANY_TELEFONE'             : '',
                'INVOICE_COMPANY_EMAIL'                : 'info@schulessen-joehstadt.de',
                'INVOICE_COMPANY_TAXID'                : '',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : '',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : '',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : '',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 10,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '6CA80E',
                'PRINTMENUE_COLORLIGHT'                : 'E4E4E4',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : '1 Phosphat, 2 Zucker, 3 Geschmacksverstärker, 4 Farbstoff, 5 Nitrat<br />Allergenkennzeichnung: enthält A Glutenhaltiges Getreide (d.h. Dinkel, Emmer, Einkorn, Kamut oder deren Hybridstämme)*, A1 Weizen*, A2 Hartweizen*, A3 Roggen*, A4 Gerste*, A5 Hafer, B Sellerie*, C Krebstiere*, D Lupinen*, E Sesamsamen*, F Fisch*, G Senf*, H Schalenfrüchte (Nüsse)*, I Milch (Laktose)*, J Schwefeldioxid und Sulfite, K Soja*, L Erdnüsse*, M Eier*, N Weichtiere* (* und Erzeugnisse daraus)',
                }

# Prepaid module settings
MODULE_PREPAID = True
PREPAID_SETTINGS_CREDIT = '0.00'  # the credit the customer is allowed to have in his account
PREPAID_SETTINGS = {'SEPA_DEBIT': False,
                    'BANK_TRANSFER': True,
                    'FLATRATE': False,
                    }
MODULE_FLATRATE = False

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

# Accounting Module Configuration
MODULE_ACCOUNTING = True
ACCOUNTING_SETTINGS = { 'CSV_DELIMITER': ';',  # the csv delimiter
                        'CSV_FIELDNAMES': ['A', 'B', 'C', 'Buchungstag', 'E', 'VWZ1', 'Buchungstext', 'H', 'I', 'J', 'Betrag',
                                           'L', 'M', 'N', 'O', 'P', 'VWZ2', 'VWZ3', 'VWZ4', 'VWZ5', 'VWZ6', 'VWZ7', 'VWZ8',
                                           'VWZ9', 'VWZ10', 'VWZ11', 'VWZ12', 'VWZ13', 'AC', 'Name', 'AE', 'IBAN', 'BIC',
                                           'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT',
                                           'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD'],
                        'CSV_CONTRAACCOUNT_NUMBER': 'IBAN',  # Name des Gegenkonto Kontonummer Kopffeldes
                        'CSV_CONTRAACCOUNT_ICODE': 'BIC',  # Name des Gegenkonto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_OWNER': 'Name',  # Name des Gegenkonto Inhaber Kopffeldes
                        'CSV_DATE': 'Buchungstag',  # Name des Datum Kopffeldes
                        'CSV_DATE_FORMAT': '%d.%m.%Y',  # date format for parsing with strptime() function
                        'CSV_SUBJECT_FIELDS': ['VWZ1', 'VWZ2', 'VWZ3', 'VWZ4', 'VWZ5', 'VWZ6', 'VWZ7',
                                               'VWZ8', 'VWZ9', 'VWZ10', 'VWZ11', 'VWZ12', 'VWZ13'],
                        # Liste mit Namen aller Verwendungszweck Kopffelder
                        'CSV_AMOUNT': 'Betrag',  # Name des Betrag Kopffeldes
                        'CSV_TYPE': 'Buchungstext',  # Name des Buchungstyp Kopffeldes
                        'CSV_TYPE_CHOICES': {'TYPE_BANK_TRANSFER_IN': ['GUTSCHRIFT',
                                                                       'UEBERWEISUNGSGUTSCHRIFT'],  # Überweisung
                                             'TYPE_DEBIT': ['', ],  # Lastschrift
                                             'TYPE_DEBIT_RETURN': ['', ],  # Rücklastschrift
                                             },
                        'DEBIT_RETURN_FEE': 'ORIGINAL',  # Kosten für Lastschriftrückgabe
                        'REMINDER_FEE': '3.00',  # Kosten für 1. Mahnung
                        'REMINDER_FEE_2': '6.00',  # Kosten für 2. Mahnung
                        'REMINDER_FEE_3': '9.00',  # Kosten für 3. Mahnung
                        'REMINDER_PAYMENT_PERIOD': 10,  # Zahlungsfrist für 1. Mahnung
                        'REMINDER_PAYMENT_PERIOD_2': 5,  # Zahlungsfrist für 2. Mahnung
                        'REMINDER_PAYMENT_PERIOD_3': 5,  # Zahlungsfrist für 3. Mahnung
                        }