# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
NEW_CUSTOMER_REGISTRATION = True
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 8
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '03.08.2015'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'info@speisenbildung-leib-und-seele.de'


# SEPA Configuration
SEPA = {'SEPA_NAME': 'Leib und Seele Dienst am Gast GmbH',
        'SEPA_EREF_PREFIX': 'LEIBUNDSEELE',
        'SEPA_CREDITOR_ID': 'DE57ZZZ00001043342',
        'SEPA_IBAN': 'DE50800537621894056643',
        'SEPA_BIC': 'NOLADE21HAL',
        'SEPA_DEBIT_FRST_DAYS': 6,
        'SEPA_DEBIT_RCUR_DAYS': 3,
        'SEPA_FORM_COMPANY_LONG': 'Leib & Seele Dienst am Gast GmbH, Merseburger Str. 165, 06112 Halle/Saale',
        'SEPA_FORM_COMPANY_SHORT': 'Leib & Seele Dienst am Gast GmbH',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'Speisenbildung Leib & Seele - Leib & Seele Catering',
                'SITE_HTML_ADMIN_TITLE'                : 'Speisenbildung Leib & Seele - Verwaltung',
                'SITE_NAME'                            : 'speisenbildung-leib-und-seele.de',
                'SITE_URL'                             : 'http://www.speisenbildung-leib-und-seele.de/',
                'SITE_REGARDS'                         : 'Ihr Speisenbildung Leib & Seele Team',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'Leib & Seele Dienst am Gast GmbH',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : 'Merseburger Str. 165',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '06112 Halle/Saale',
                'INVOICE_COMPANY_TELEFONE'             : '0345 13 26 174',
                'INVOICE_COMPANY_EMAIL'                : 'info@speisenbildung-leib-und-seele.de',
                'INVOICE_COMPANY_TAXID'                : '110/108/1976',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : 'Saalesparkasse',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : 'DE50 8005 3762 1894 0566 43',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : 'NOLADE21HAL',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 7,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '6CA80E',
                'PRINTMENUE_COLORLIGHT'                : 'f0fae1',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : '1 mit Farbstoff, 2 mit Konservierungsmittel, 3 mit Antioxidationsmittel, 4 mit Geschmacksverstärker, 5 mit Süßungsmittel, 6 mit Phosphat, 7 gewachst, 8 geschwefelt, 9 chininhaltig, 10 koffeinhaltig<br />Allergenkennzeichnung: enthält a Glutenhaltige Getreide (Weizen, Roggen, Gerste, Hafer, Dinkel, Kamut oder Hybridstämme davon)*, b Milch (einschließlich Laktose)*, c Ei*, d Soja*, e Erdnuss*, f Schalenfrüchte (d.h. Mandeln, Haselnüsse, Walnüsse, Cashewnüsse, Pecannüsse, Paranüsse, Pistazien, Macadamianüsse und Queenslandnüsse)*, g Sellerie*, h Senf*, i Krebstiere*, j Fisch*, k Sesamsamen*, l Schwefeldioxid und Sulfite in einer Konzentration von mehr als 10 mg/kg oder 10 mg/l als Schwefeldioxid angegeben, m Lupine*, n Weichtiere (z. B. Muscheln, Schnecken, Tintenfische)* (* und Erzeugnisse daraus)<br />S = Schwein, R = Rind',
                }

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 4,50 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '4.50',  # the fee for the paper invoice as String
                          }

# Accounting Module Configuration
MODULE_ACCOUNTING = True
ACCOUNTING_SETTINGS = { 'CSV_DELIMITER': ';',  # the csv delimiter
                        'CSV_FIELDNAMES': ['A', 'B', 'C', 'Buchungstag', 'E', 'VWZ1', 'Buchungstext', 'H', 'I', 'J', 'Betrag',
                                           'L', 'M', 'N', 'O', 'P', 'VWZ2', 'VWZ3', 'VWZ4', 'VWZ5', 'VWZ6', 'VWZ7', 'VWZ8',
                                           'VWZ9', 'VWZ10', 'VWZ11', 'VWZ12', 'VWZ13', 'AC', 'Name', 'AE', 'IBAN', 'BIC',
                                           'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT',
                                           'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD'],
                        'CSV_CONTRAACCOUNT_NUMBER': 'IBAN',  # Name des Gegenkonto Kontonummer Kopffeldes
                        'CSV_CONTRAACCOUNT_ICODE': 'BIC',  # Name des Gegenkonto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_OWNER': 'Name',  # Name des Gegenkonto Inhaber Kopffeldes
                        'CSV_DATE': 'Buchungstag',  # Name des Datum Kopffeldes
                        'CSV_DATE_FORMAT': '%d.%m.%Y',  # date format for parsing with strptime() function
                        'CSV_SUBJECT_FIELDS': ['VWZ1', 'VWZ2', 'VWZ3', 'VWZ4', 'VWZ5', 'VWZ6', 'VWZ7',
                                               'VWZ8', 'VWZ9', 'VWZ10', 'VWZ11', 'VWZ12', 'VWZ13'],  # Liste mit Namen aller Verwendungszweck Kopffelder
                        'CSV_AMOUNT': 'Betrag',  # Name des Betrag Kopffeldes
                        'CSV_TYPE': 'Buchungstext',  # Name des Buchungstyp Kopffeldes
                        'CSV_TYPE_CHOICES': {'TYPE_BANK_TRANSFER_IN': ['GUTSCHRIFT',
                                                                    'UEBERWEISUNGSGUTSCHRIFT'],  # Überweisung
                                             'TYPE_DEBIT': ['EINZELLASTSCHRIFTSEINZUG', ],  # Lastschrift
                                             'TYPE_DEBIT_RETURN': ['LS RUECKBELASTUNG', ], # Rücklastschrift
                                            },
                        'DEBIT_RETURN_FEE': 'ORIGINAL',  # Kosten für Lastschriftrückgabe
                        'REMINDER_FEE'  : '0.00',  # Kosten für 1. Mahnung
                        'REMINDER_FEE_2': '3.00',  # Kosten für 2. Mahnung
                        'REMINDER_FEE_3': '5.00',  # Kosten für 3. Mahnung
                        'REMINDER_PAYMENT_PERIOD'  : 7,  # Zahlungsfrist für 1. Mahnung
                        'REMINDER_PAYMENT_PERIOD_2': 7,  # Zahlungsfrist für 2. Mahnung
                        'REMINDER_PAYMENT_PERIOD_3': 5,  # Zahlungsfrist für 3. Mahnung
                        }