# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = False

# Customer model extra attributes configuration
CUST_EXTRA = {'ACTIVE': True, 'BLANK': True, 'VERBOSE_NAME': 'Debitor-Nr.', 'UNIQUE': True, 'HELP_TEXT': 'Wird vom System automatisch vergeben.', 'READONLY': True, 'CHOICES': None}

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 6
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '07.08.2017'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'DRK-Speiseeinrichtung Schmalkalden <bestellsystem@drk-schmalkalden-essen.de>'


# SEPA Configuration
SEPA = {'SEPA_NAME': '',
        'SEPA_EREF_PREFIX': '',
        'SEPA_CREDITOR_ID': '',
        'SEPA_IBAN': '',
        'SEPA_BIC': '',
        'SEPA_DEBIT_FRST_DAYS': '3',
        'SEPA_DEBIT_RCUR_DAYS': '3',
        'SEPA_FORM_COMPANY_LONG': 'DRK-Kreisverband Schmalkalden e.V., Wilhelm-Külz-Straße 17, 98574 Schmalkalden',
        'SEPA_FORM_COMPANY_SHORT': 'DRK-Kreisverband Schmalkalden e.V.',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'DRK-Speiseeinrichtung Schmalkalden',
                'SITE_HTML_ADMIN_TITLE'                : 'DRK-Speiseeinrichtung Schmalkalden - Verwaltung',
                'SITE_NAME'                            : 'www.drk-schmalkalden-essen.de',
                'SITE_URL'                             : 'https://www.drk-schmalkalden-essen.de',
                'SITE_REGARDS'                         : 'Ihre DRK-Speiseeinrichtung Schmalkalden',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'DRK-Kreisverband Schmalkalden e.V.',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : '',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '',
                'INVOICE_COMPANY_TELEFONE'             : '',
                'INVOICE_COMPANY_EMAIL'                : '',
                'INVOICE_COMPANY_TAXID'                : '',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : '',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : '',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : '',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 10,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '6CA80E',
                'PRINTMENUE_COLORLIGHT'                : 'E4E4E4',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : 'Zusatzstoffe: 1 mit Konservierungsstoff, 2 mit Farbstoff,  3 mit Antioxidationsmittel, 4 mit Geschmacksverstärker, 5 mit Phosphat, 6 	mit Milcheiweiß, 7 geschwefelt, 8 gewachst, 9 koffeinhaltig, 9 chininhaltig, 11 mit Süßungsmittel(n), 12 mit einer Zuckerart und Süßungsmitteln, 13 enthält eine Phenylalaninquelle, 14 mit Rauch <br />Allergene: a Eier*, b Fisch, c Milch (einschließl. Laktose)*, d Schalenfrüchte (Mandeln, Haselnüsse usw.), e Sellerie*, f Senf*, g Gluten (* und daraus hergestellte Erzeugnisse)',
                }

# Prepaid module settings
MODULE_PREPAID = True
PREPAID_SETTINGS_CREDIT = '0.00'  # the credit the customer is allowed to have in his account
PREPAID_SETTINGS = {'SEPA_DEBIT': False,
                    'BANK_TRANSFER': True,
                    'FLATRATE': False,
                    }

MODULE_FLATRATE = False

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

# Accounting Module Configuration
MODULE_ACCOUNTING = False
ACCOUNTING_SETTINGS = { 'CSV_DELIMITER': ';',  # the csv delimiter
                        'CSV_ACCOUNT_NUMBER': 'Kontonummer',  # Name des Konto Kontonummer Kopffeldes
                        'CSV_ACCOUNT_ICODE': 'BLZ',  # Name des Konto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_NUMBER': 'Gegenkonto',  # Name des Gegenkonto Kontonummer Kopffeldes
                        'CSV_CONTRAACCOUNT_ICODE': 'Gegenkonto BLZ',  # Name des Gegenkonto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_OWNER': 'Gegenkonto Inhaber',  # Name des Gegenkonto Inhaber Kopffeldes
                        'CSV_DATE': 'Datum',  # Name des Datum Kopffeldes
                        'CSV_DATE_FORMAT': '%d.%m.%Y',  # date format for parsing with strptime() function
                        'CSV_VALUTA': 'Valuta',  # Name des Valuta Kopffeldes
                        'CSV_VALUTA_FORMAT': '%d.%m.%Y',  # valuta date format for parsing with strptime() function
                        'CSV_SUBJECT_1': 'Verwendungszweck',  # Name des Verwendungszweck Kopffeldes
                        'CSV_SUBJECT_2': 'Verwendungszweck 2',  # Name des Verwendungszweck 2 Kopffeldes
                        'CSV_SUBJECT_MORE': 'Weitere Verwendungszwecke',  # Name des Weitere Verwendungszwecke Kopffeldes
                        'CSV_AMOUNT': 'Betrag',  # Name des Betrag Kopffeldes
                        'CSV_TYPE': 'Art',  # Name des Buchungstyp Kopffeldes
                        'CSV_TYPE_CHOICES': {'TYPE_BANK_TRANSFER_IN': ['UEBERWEISUNGSGUTSCHRIFT',
                                                                    'SEPA-CT HABEN EINZELBUCHUNG',
                                                                    'GUTSCHR. NEUTR. UEBW. EZUE'],  # Überweisung
                                             'TYPE_DEBIT': ['SEPA-DD SAMMLER-HABEN CORE',
                                                            'SEPA-DD EINZELLB.HABEN CORE'],  # Lastschrift
                                             'TYPE_DEBIT_RETURN': ['RETOURENHUELLE LASTSCHRIFT',
                                                                   'SEPA-DD SOLL RUECKBEL. CORE'], # Rücklastschrift
                                            },
                        'DEBIT_RETURN_FEE': 'ORIGINAL',  # Kosten für Lastschriftrückgabe
                        'REMINDER_FEE': '3.00',  # Kosten für 1. Mahnung
                        'REMINDER_FEE_2': '6.00',  # Kosten für 2. Mahnung
                        'REMINDER_FEE_3': '9.00',  # Kosten für 3. Mahnung
                        'REMINDER_PAYMENT_PERIOD': 10,  # Zahlungsfrist für 1. Mahnung
                        'REMINDER_PAYMENT_PERIOD_2': 5,  # Zahlungsfrist für 2. Mahnung
                        'REMINDER_PAYMENT_PERIOD_3': 5,  # Zahlungsfrist für 3. Mahnung
                        }