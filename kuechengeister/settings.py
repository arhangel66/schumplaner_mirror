# -*- coding: utf-8 -*-

# Django settings for Schulmenueplaner project.

#########################################################################################
# IMPORTANT !!!
# Do NOT change settings.py for your local settings
# Create file /site_overloads/OVERLOAD_SITE/settings.py and customize the settings there
# Place the confidential customizations in settings_local.py
#########################################################################################

import os


# Root of the project
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))


# Name of the site specific folder in /site_overloads
OVERLOAD_SITE = ''  # overwrite in settings_local.py

# Set to TRUE if site is in DEMO modus
DEMO_MODUS = False

# The Login configuration
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/kunden/'
LOGOUT_URL = '/logout/'
LOGOUT_REDIRECT_URL = '/'


# Django Database Configuration
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dbname',
        'USER': 'user',
        'PASSWORD': 'password',
        'HOST': '',
        'PORT': '5432'
    }
}


# Django E-Mail Configuration
# EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'  # globally used, do not change
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'  # globally used, do not change
EMAIL_HOST = '81.169.157.140'  # Neuer Emailserver (mailneu.schulmenueplaner.de)', globally used, do not change
EMAIL_PORT = '25'  # globally used, do not change
EMAIL_USE_TLS = True  # globally used, do not change
EMAIL_HOST_USER = 'info@schulmenueplaner.de'  # overwrite in site_overloads/OVERLOAD_SITE/settings.py
EMAIL_HOST_PASSWORD = 'geheim'  # overwrite in site_overloads/OVERLOAD_SITE/settings.py


# Site E-Mail Configuration
DEFAULT_FROM_EMAIL = 'dev@schulmenueplaner.de'  # Sender E-Mail-Address of system generated mails
NEW_CUSTOMER_INFORMATION_EMAIL = []  # list of strings: E-Mail-Addresses that receives information about new customers; if empty no mails will be sent


# New Customer Configuration
NEW_CUSTOMER_REGISTRATION = True
EXIST_CUST_REGISTRATION_TYPE = ''
EXIST_CUST_REGISTRATION = ''
NEW_CUSTOMER_FORM_AGB_CHECKBOX = True  # show the agree to the AGB checkbox

# Customer model extra attributes configuration
CUST_EXTRA = {'ACTIVE': False, 'BLANK': True, 'VERBOSE_NAME': 'Extra', 'UNIQUE': False, 'HELP_TEXT': '', 'CHOICES': None}
CUST_EXTRA2 = {'ACTIVE': False, 'BLANK': True, 'VERBOSE_NAME': 'Extra', 'UNIQUE': False, 'HELP_TEXT': '', 'CHOICES': None}

# Child model extra attributes configuration
CHILD_EXTRA = {'ACTIVE': False, 'BLANK': True, 'VERBOSE_NAME': 'Extra', 'UNIQUE': False, 'HELP_TEXT': '', 'CHOICES': None}
CHILD_EXTRA2 = {'ACTIVE': False, 'BLANK': True, 'VERBOSE_NAME': 'Extra', 'UNIQUE': False, 'HELP_TEXT': '', 'CHOICES': None}

# Online invoice is activated automatically for new customers if set to True
ACTIVATE_ONLINE_INVOICE_FOR_NEW_CUSTOMERS = True


# SEPA Configuration
SEPA = {'SEPA_NAME': '',  # SEPA creditor name
        'SEPA_EREF_PREFIX': '',  # A prefix that is used to create the SEPA EndToEndId
        'SEPA_CREDITOR_ID': '',  # SEPA Creditor Identifier
        'SEPA_IBAN': '',  # IBAN of the creditor
        'SEPA_BIC': '',  # BIC of the creditor
        'SEPA_DEBIT_FRST_DAYS': 6,  # Banking days to calculate debit date for first debit
        'SEPA_DEBIT_RCUR_DAYS': 3,  # Banking days to calculate debit date for recurrent debits
        'SEPA_FORM_COMPANY_LONG': '',  # Company name for SEPA form long
        'SEPA_FORM_COMPANY_SHORT': '',  # Company name for SEPA form short
        }


# Site specific context
# overwrite in /site_overloads/OVERLOAD_SITE/settings.py
SITE_CONTEXT = {'SITE_HTML_TITLE': '',  # <title> der Seite
                'SITE_HTML_ADMIN_TITLE': '',  # Name der Seite in Verwaltung
                'SITE_NAME': '',  # URL der der Seite ohne http://
                'SITE_URL': '',  # URL der Seite
                'SITE_REGARDS': '',  # Grußformel, die in Mailtemplates verwendet wird
                'SITE_HEADER_SUBTITLE': '',  # Slogan unter Titel der Seite
                'SITE_HEADER_BACKGROUND': '',  # Header Hintergrundfarbe
                'INVOICE_COMPANY_NAME': '',
                'INVOICE_COMPANY_NAME_1': '',
                'INVOICE_COMPANY_NAME_2': '',
                'INVOICE_COMPANY_ADDRESS_STREET': '',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '',
                'INVOICE_COMPANY_TELEFONE': '',
                'INVOICE_COMPANY_EMAIL': '',
                'INVOICE_COMPANY_TAXID': '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME': '',
                'INVOICE_COMPANY_ACCOUNT_IBAN': '',
                'INVOICE_COMPANY_ACCOUNT_BIC': '',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS': 10,  # Zahlungsziel in Tagen
                'INVOICE_SHOW_TAX': True,  # Steuer auf Rechnung ausweisen oder nicht
                'PRINTMENUE_COLORSTRONG': '',  # Dunkle Farbe für gedruckten Speiseplan
                'PRINTMENUE_COLORLIGHT': '',  # Helle Farbe für gedruckten Speiseplan
                'MENU_FOOTER_LINE_1': '',  # Angaben direkt unter Speiseplan (z.B. Änderungen vorbehalten)
                'MENU_SUBTEXT': '',
                'MENU_ADDITIVES': '',  # Zusatzstoffe, stehen direkt unter Speiseplan
                }

# Tax rate as X / 100; Example: 19% = 19
TAX_RATE = 19

# Absence module
MODULE_ABSENCE = False

# Prepaid Module
# If set to True, the Accounting Module must be set to False
MODULE_PREPAID = False
PREPAID_SETTINGS_CREDIT = '0.00'  # the credit the customer is allowed to have in his account
PREPAID_SETTINGS = {'SEPA_DEBIT': False,  # Offer SEPA debit payment to customers
                    'BANK_TRANSFER': False,  # Offer bank transfer payment
                    'FLATRATE': False,  # Customer can use flatrate orders
                    }

# Accounting Module
# If set to True, the Accounting Module configuration has to be set in site_overloads settings
MODULE_ACCOUNTING = False
ACCOUNTING_SETTINGS = {}

# Invoice Module: activated by default, can be deactivated
# can be configured seperately for customer area. if that's not done, the MODULE_INVOICE value is used
MODULE_INVOICE = True
MODULE_INVOICE_CUST = None
MODULE_INVOICE_BILLING_MONTHS = 2  # number of month to show in /admin/kg/rechnung/abrechnung/

# Paper Invoice Module
# If set to True, the Paper Invoice Module configuration has to be set in site_overloads settings
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

# Flatrate Module/Functionality
# If set to False, flatrate links are not shown on customer and admin area
MODULE_FLATRATE = True

# The meal plan to be shown in the home page, default is the first
# mealplan. But likely it will be changed to what will fit most.
MEALPLAN_ID_HOMEPAGE = 1

MEALTIMES = {
    'breakfast': 1,
    'lunch': 2,
    'snack': 3,
    'dinner': 4,
}

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 3  # customer view
MENU_WEEKS_HOMEPAGE = 3  # homepage

# Set menu start date for new customers: menu is shown from this date on
MENU_START_DATE = '01.01.2000'

# Set canteen mode: if in this mode, there are only customers and no children
CANTEEN_MODE = False

# Enable/disable weekly order report of every child of selected facility
REPORT_WEEK_DETAILED = False

# Only has effect when in CANTEEN_MODE: Show link to chip administration
MODULE_TERMINAL = True

# show reduction functionality
MODULE_REDUCTION = True

# API key to authenticate for api usage
API_KEY = ''

# Absolute filesystem path to the directory that will hold system created invoices for the user
# Example: "/home/projectname/usercontent/invoices"
INVOICE_ROOT = os.path.join(PROJECT_ROOT, '../../usercontent/invoices')

# Absolute filesystem path to the directory that will hold system created reminders for the user
# Example: "/home/projectname/usercontent/reminders"
REMINDER_ROOT = os.path.join(PROJECT_ROOT, '../../usercontent/reminders')

# Maximum number of items shown in suggested list for importing menu
MENU_IMPORT_SUGGESTION = 20

# Disabled as now using number from mealplan
# Every week starts from Monday and has limit available days
# WEEKDAY_TOTAL = 5

# AqBanking paths
# defaults to Ubuntu 14.04 standard installation paths
AQBANKING_CLI = '/usr/bin/aqbanking-cli'
AQHBCI_TOOL4_PATH = '/usr/bin/aqhbci-tool4'
FINTS_ACCOUNTS_PATH = os.path.realpath(os.path.join(PROJECT_ROOT, '../../usercontent/fints'))

# Show kind of warning when number of orders exceeded the week limit.
# This mostly aims to the `wimpassing` overloading site only.
ORDER_EXCEED_WARNING = False

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Berlin'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'de-de'

# The Site ID
SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'static')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '../../static/'


# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, 'static'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n6%08e-8am2x&tvw1*u!i%n19^3+yyv(!&awautxlc0#yp-=q4'


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)


FIXTURE_DIRS = (os.path.join(PROJECT_ROOT, 'fixtures'))

INSTALLED_APPS = (
    'django.contrib.auth',
    'longerusername',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.flatpages',
    'raven.contrib.django.raven_compat',
    'nested_admin',
    'tinymce',
    'djcelery_email',
    'simple_history',
    'form_utils',
    'djcelery',
    'kg',
    'kg_utils',
    'menus',
    'south',
    'smart_selects',
    'bank_transactions',
    'reports',
    'absence',
)


AUTH_PROFILE_MODULE = 'kg.Customer'
AUTHENTICATION_BACKENDS = ('kg.backends.CaseInsensitiveModelBackend',)

CSRF_FAILURE_VIEW = 'error_views.cookie_error'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(name)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'log_file': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_ROOT, '../../logs/django.log'),
            'formatter': 'verbose',
        },
        'sentry': {
            'level': 'WARNING',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'formatter': 'verbose',
            'tags': {'instance': OVERLOAD_SITE},
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'formatter': 'verbose',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            # "console" should be removed in production mode
            'handlers': ['console', 'log_file', 'sentry'],
            'level': 'WARNING',
            'propagate': False,
        },
        '': {
            # "console" should be removed in production mode
            'handlers': ['console', 'log_file', 'sentry'],
            'level': 'WARNING',
            'propagate': False,
        },
    }
}

RAVEN_CONFIG = {
    # Please uncomment "dsn" and fill the real Sentry DSN below for real use
    # 'dsn': '',
}

PDF_PAGE_BREAK = '<p style="page-break-before: always" ></p>'

# set default for debugging apps and middleware, overwrite in settings_local.py if any
DEBUG_APPS = ()
DEBUG_MIDDLEWARE = ()

# Celery configuration
QUEUE_DISABLED = True

# We have tasks autodiscovered, so this is disabled
# CELERY_IMPORTS = ("kg_celery.tasks")

from celery.schedules import crontab
CELERYBEAT_SCHEDULE = {
    'remove-invalid-orders': {
        'task': 'menus.tasks.removeInvalidOrders',
        'schedule': crontab(hour=0, minute=10),
        'kwargs': {},
    },
    'auto-create-week-menus': {
        'task': 'menus.tasks.populateWeekMenus',
        'schedule': crontab(day_of_week=1, hour=2, minute=0),
        'kwargs': {},
    },
}
CELERYD_CONCURRENCY = 2

# List all models allowed from query of smart_selects
SMART_SELECTS_ALLOWED_MODELS = [
    'kg.FacilitySubunit',
]


try:
    # load confidential site specific settings
    from settings_local import *

    # load site specific settings
    OVERLOAD_DIR = "site_overloads" + "." + OVERLOAD_SITE
    exec("from %s.settings import *" % OVERLOAD_DIR)

except ImportError as e:
    pass

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request")

####  Update some settings with the overload settings  ###

# Add debug apps and classes
INSTALLED_APPS += DEBUG_APPS

# Add additional locations of template files
# important to first load overloaded templates
SITE_OVERLOAD_TEMPLATE_DIR = os.path.join(PROJECT_ROOT, 'site_overloads', OVERLOAD_SITE, 'templates')
TEMPLATE_DIRS = (SITE_OVERLOAD_TEMPLATE_DIR,
                 os.path.join(PROJECT_ROOT, 'templates'),
                 )

# Add additional locations of static files
# important to first load overloaded static files
SITE_OVERLOAD_STATICFILES_DIR = os.path.join(PROJECT_ROOT, 'site_overloads', OVERLOAD_SITE, 'static')
STATICFILES_DIRS = (SITE_OVERLOAD_STATICFILES_DIR,
                    os.path.join(PROJECT_ROOT, 'static'),
                    )

# Absolute filesystem path to the directory that will hold non public (not static) media files
# Example: "/home/projectname/site_overloads/content"
SITE_OVERLOAD_CONTENT_DIR = os.path.join(PROJECT_ROOT, 'site_overloads', OVERLOAD_SITE, 'content')

if MODULE_INVOICE_CUST is None:
    MODULE_INVOICE_CUST = MODULE_INVOICE
