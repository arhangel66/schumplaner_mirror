# -*- coding: utf-8 -*-
from django.views.generic import ListView
from django.contrib import messages
from django.shortcuts import Http404
from django.conf import settings
from django.core.context_processors import csrf
from django.utils.timezone import now
from django.core.urlresolvers import reverse
from django.shortcuts import redirect

from models import Absence
from forms import AbsenceForm


MODULE_ABSENCE = getattr(settings, 'MODULE_ABSENCE', False)


class AbsenceReportView(ListView):
    template_name = 'absence/absence_report.html'
    model = Absence
    paginate_by = 10
    object = None

    def get_queryset(self):
        queryset = super(AbsenceReportView, self).get_queryset().filter(
            child__customer=self.request.user.customer)
        return queryset.order_by('-date_from')

    def get(self, request, *args, **kwargs):
        if not MODULE_ABSENCE:
            raise Http404
        if kwargs.get('pk'):
            try:
                # Only the creator can edit (future) absence records
                self.object = Absence.objects.get(
                    child__customer=self.request.user.customer,
                    pk=kwargs['pk'])
                if self.object.date_from <= now().date():
                    raise Http404
            except Absence.DoesNotExist:
                raise Http404
        return super(AbsenceReportView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AbsenceReportView, self).get_context_data(**kwargs)
        if self.object is not None:
            absence_form = AbsenceForm(instance=self.object)
            context['object'] = self.object
        else:
            absence_form = AbsenceForm()

        children = self.request.user.customer.child_set.values_list(
            'id', 'name', 'surname')
        absence_form.fields['child'].choices = \
            [(it[0], '%s, %s' % (it[1], it[2])) for it in children]
        context['form'] = absence_form
        context.update(csrf(self.request))
        return context

    def post(self, request, *args, **kwargs):
        if not MODULE_ABSENCE:
            raise Http404
        if 'delete' in request.POST:
            try:
                absence = Absence.objects.get(
                    child__customer=request.user.customer,
                    pk=request.POST['delete'])
                absence.delete()
                messages.success(
                    request,
                    u'Die Abwesenheitsmeldung wurde gelöscht',
                )
            except Absence.DoesNotExist:
                raise Http404
            except:
                messages.error(
                    request, u'Es ist ein Fehler beim Löschen aufgetreten.',)
        else:
            if kwargs.get('pk'):
                self.object = Absence.objects.get(
                    child__customer=self.request.user.customer,
                    pk=kwargs['pk'])
            else:
                self.object = None
            form = AbsenceForm(request.POST, instance=self.object)
            if form.is_valid():
                form.save()
                messages.success(request, u'Abwesenheitsmeldung erfolgreich gespeichert.')
                # return self.get(request)
            else:
                self.object_list = self.get_queryset()
                context = self.get_context_data(object_list=self.object_list)
                context['form'] = form
                return self.render_to_response(context)
        return redirect(reverse('absence:report'))
