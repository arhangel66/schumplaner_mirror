# -*- coding: utf-8 -*-
from django.forms import ValidationError
from django.db.models import Q
from django.utils.timezone import now
from django.forms.util import ErrorList

from form_utils.forms import BetterModelForm

from models import Absence


class AbsenceForm(BetterModelForm):

    class Meta:
        model = Absence
        fieldsets = [('', {
            'fields': ['child', 'date_from', 'date_to', 'notice'],
        })]
        row_attrs = {'child': {'class': 'type-text form-group'},
                     'date_from': {'class': 'type-text form-group'},
                     'date_to': {'class': 'type-text form-group'},
                     'notice': {'class': 'type-select form-group'}}

    def __init__(self, *args, **kwargs):
        super(AbsenceForm, self).__init__(*args, **kwargs)
        self.fields['child'].widget.attrs['class'] = 'form-control'
        self.fields['date_from'].widget.attrs['class'] = 'form-control datepicker'
        self.fields['date_to'].widget.attrs['class'] = 'form-control datepicker'
        self.fields['notice'].widget.attrs['class'] = 'form-control'

    def _raise_dates(self, message=''):
        self.errors['date_from'] = ErrorList([message])
        self.errors['date_to'] = ErrorList([message])

    def clean_date_from(self):
        if self.cleaned_data['date_from'] < now().date():
            raise ValidationError(
                u'Datum von darf nicht in der Vergangenheit liegen.')
        return self.cleaned_data['date_from']

    def clean(self):
        data = super(AbsenceForm, self).clean()
        date_from = data.get('date_from')
        if date_from:
            date_to = data.get('date_to')
            # Auto set date_to equal date_from if missing
            if not date_to:
                data['date_to'] = date_to = date_from
            # Do not allow date_to before date_from
            elif date_to < date_from:
                self._raise_dates(u'Das Enddatum darf nicht vor dem Startdatum liegen.')

            # Prevent overlapping
            queryset = Absence.objects.filter(child=data['child'])
            if self.instance is not None:
                queryset = queryset.exclude(id=self.instance.id)
            overlapped = queryset.filter(
                (Q(date_from__lte=data['date_from']) & Q(date_to__gte=data['date_from'])) |
                (Q(date_from__lte=data['date_to']) & Q(date_to__gte=data['date_to'])) |
                (Q(date_from__lte=data['date_from']) & Q(date_to__gte=data['date_to'])) |
                (Q(date_from__gte=data['date_from']) & Q(date_to__lte=data['date_to']))
            ).exists()
            if overlapped:
                self._raise_dates(u'Ein Eintrag mit diesem Datum existiert schon.')

        return data
