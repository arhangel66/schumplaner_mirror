from django.template import Library
from django.db.models import Q

from absence.models import Absence


register = Library()


@register.assignment_tag
def check_absence(child, date):
    """Returns absence status of a child on a given day."""
    return Absence.objects.filter(child=child['child']).filter(
        Q(date_from__lte=date) & Q(date_to__gte=date)).exists()


@register.assignment_tag
def collect_absences(date):
    """Returns list of all child IDs being absent on a specific day."""
    queryset = Absence.objects.filter(
        Q(date_from__lte=date) & Q(date_to__gte=date))
    return queryset.values_list('child_id', flat=True)
