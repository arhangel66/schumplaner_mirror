from django.conf.urls.defaults import patterns, url
from django.contrib.auth.decorators import login_required

from absence.views import AbsenceReportView


urlpatterns = patterns('',
    url(r'^(?P<pk>\d+)?$', login_required(AbsenceReportView.as_view()), name='report'),
)
