from django.shortcuts import render_to_response
from django.template import RequestContext



def cookie_error(request, reason=""):
    return render_to_response('error_cookie.html', { 'reason' : reason }, context_instance = RequestContext(request))