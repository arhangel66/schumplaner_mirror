from datetime import datetime, date, timedelta

from django.utils import timezone

class DateHelper():
    
    @staticmethod
    def getDaysInCalendarWeek(year, calendar_week):
        day_list = []
        day = date(int(year), 2, 1)
        cyear, weekBase, dayBase = day.isocalendar()
        day += timedelta(1 - dayBase + (int(calendar_week) - weekBase)*7)
        delta = timedelta(1) 
        for i in range(6):
            day_list.append(day)
            day += delta
        day_list.append(day)
        return day_list


    @staticmethod
    def getFirstDayInIsoweek(year, week):
        """ Calculates the date for monday in the given isoweek

        %W takes the first Monday to be in week 1 but ISO defines week 1 to contain 4 January.
        So the result from datetime.strptime('2011221', '%Y%W%w')
        is off by one if the first Monday and 4 January are in different weeks.
        This is the case if 4 January is a Friday, Saturday or Sunday.
        """
        oDate = datetime.strptime('%04d-%02d-1' % (year, week), '%Y-%W-%w')
        if date(year, 1, 4).isoweekday() > 4:
            oDate -= timedelta(days=7)
        return oDate


    @staticmethod
    def getNextBusinessday(start_date):
        """
        Finds the next Businessday
        i.e. start_date = Friday --> new_date = Monday
        """
        
        if start_date.weekday() == 4:
            new_date = start_date + timedelta(days=3)
        elif start_date.weekday() == 5:
            new_date = start_date + timedelta(days=2)
        else:
            new_date = start_date + timedelta(days=1)
            
        return new_date


    @staticmethod
    def getDateByAddingBusinessdays(from_date, add_days):
        """
        Calculates the date of from_date plus X business days
        Business days are monday to friday
        """
        business_days_to_add = add_days
        current_date = from_date
        while business_days_to_add > 0:
            current_date += timedelta(days=1)
            weekday = current_date.weekday()
            if weekday >= 5:  # sunday = 6
                continue
            business_days_to_add -= 1
        return current_date


    @staticmethod
    def getNextMonday(today=date.today()):
        """
        Finds the next monday after today
        If today is monday, return the monday after today
        """
        days_ahead = 1 - today.isoweekday()
        if days_ahead <= 0: # monday already happened this week
            days_ahead += 7
        return today + timedelta(days=days_ahead)
    
    
    @staticmethod
    def getLastDayOfMonth(today=date.today()):
        if today.month == 12:
            return today.replace(day=31)
        return today.replace(month=today.month+1, day=1) - timedelta(days=1)
    
    
    @staticmethod
    def getLastDayOfLastMonth(today=date.today()):
        first_of_current_month = date(today.year, today.month, 1)
        last_of_last_month = first_of_current_month - timedelta(days=1)
        return last_of_last_month
    
    
    @staticmethod
    def getWeeks(startdate, number_of_weeks, include_start_week=True):
        lWeeks = []
        iNumberOfWeeks = int(number_of_weeks)
        start_date = startdate.isocalendar()
        start_days = DateHelper.getDaysInCalendarWeek(start_date[0], start_date[1])

        if iNumberOfWeeks >= 0:
            if include_start_week:
                lWeekRange  = range(0, iNumberOfWeeks)
            else:
                lWeekRange  = range(1, iNumberOfWeeks+1)
        else:
            if include_start_week:
                lWeekRange  = range(iNumberOfWeeks+1, 1)
            else:
                lWeekRange  = range(iNumberOfWeeks, 0)

        for i in lWeekRange:
            temp_date = start_days[0] + timedelta(weeks=i)
            temp_year, temp_week, temp_day = temp_date.isocalendar()
            dWeek = {'year' : temp_year,
                    'week' : temp_week,
                    'days' : DateHelper.getDaysInCalendarWeek(temp_year, temp_week)
                    }
            lWeeks.append(dWeek)
        return lWeeks

    
    @staticmethod
    def calculateOrderTimelimit(oDate=None, iDays=0, iDayCode=10, oTime=None):
        """ Used to calculate the order and cancel timelimits for an order
        """

        if oDate is None or oTime is None:
            return None

        # days before the menu date
        if 10 == iDayCode:
            oReturnDate =  oDate - timedelta(days=iDays)

        # days before the menu date or last friday if menu date is monday
        if 11 == iDayCode:
            iWeekday = oDate.isoweekday()
            if 1 == iWeekday:
                iDays += 3  # day is monday, add 3 days
            oReturnDate =  oDate - timedelta(days=iDays)

        # days before start of week
        if 20 == iDayCode:
            oFirstDayInIsoWeek= DateHelper.getFirstDayInIsoweek(oDate.isocalendar()[0], oDate.isocalendar()[1])
            oReturnDate =  oFirstDayInIsoWeek - timedelta(days=iDays)

        # nth day of last month
        if 30 == iDayCode:
            oLastDayOfLastMonth = DateHelper.getLastDayOfLastMonth(oDate)
            if 0 == iDays:
                iDays = 1
            try:
                oReturnDate = date(oLastDayOfLastMonth.year, oLastDayOfLastMonth.month, iDays)
            except ValueError:
                oReturnDate = oLastDayOfLastMonth

        return datetime.combine(oReturnDate, oTime)

    @staticmethod
    def is_past_date(oDate):
        """Returns True if given date is in the past."""
        oNow = timezone.now()
        if not isinstance(oDate, datetime):
            oNow = oNow.date()
        return oNow > oDate
