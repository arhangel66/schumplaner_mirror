# -*- coding: utf-8 -*-

from xml.dom.minidom import Document
from decimal import *
import random
import hashlib
import datetime
import re





class SepaHelper(object):
    def __init__(self):
        #self.sVal = sValidate
        pass

    def validateText(self, sText):
        """
        Validate text
        In version 2.7 the following characters are valid:
        [0-9A-Za-z?,-(+.)/]
        """
        # replace some known characters
        sText = sText.replace(u'é', 'e')
        sText = sText.replace(u'è', 'e')
        sText = sText.replace(u'á', 'a')
        sText = sText.replace(u'à', 'a')
        sText = sText.replace(u'í', 'i')
        sText = sText.replace(u'ä', 'ae')
        sText = sText.replace(u'ö', 'oe')
        sText = sText.replace(u'ü', 'ue')
        sText = sText.replace(u'Ä', 'Ae')
        sText = sText.replace(u'Ö', 'Oe')
        sText = sText.replace(u'Ü', 'Ue')
        sText = sText.replace(u'ß', 'ss')
        sText = sText.replace(u'č', 'c')

        # delete all invalid characters from string
        sText = re.sub('[^0-9A-Za-z?,\-(+.)/ ]', '', sText)

        return sText

    def validateIBAN(self, sIban):
        """
        Validate an IBAN Number.
        """

        bResult = re.match("[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}", sIban)

        if bResult:
            return sIban
        else:
            raise Exception("IBAN: %s is incorrect" % sIban)

    def validateBIC(self, sBic):
        """
        Validate a BIC number.Payment Information
        """

        bResult = re.match("([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)", sBic)

        if bResult:
            return sBic
        else:
            raise Exception("BIC: %s is incorrect" % sBic)

    def validateDDType(self, sDDtype):
        """
        Function to validate the Direct Debit Transaction types
        """
        if sDDtype in ("FRST", "RCUR", "FNAL", "OOFF"):
            return sDDtype
        else:
            raise Exception("DD Type: %s is incorrect" % sDDtype)

    def validateAmount(self, sAmount):
        """
        Function to validate an amount.
        """
        try:
            oAmount = Decimal(sAmount)
            return oAmount.quantize(Decimal('.01'), rounding=ROUND_DOWN)
        except:
            raise Exception("Amount: %s is incorrect" % oAmount)


    def validateDate(self, sDate):
        """
        Function to validate a ISO date.
        regex from :
        http://my.safaribooksonline.com/book/programming/regular-expressions/9780596802837/4dot-validation-and-formatting/id2983571
        """

        bResult = re.match("^([0-9]{4})(?:(1[0-2]|0[1-9])|-?(1[0-2]|0[1-9])-?)?(3[0-1]|0[1-9]|[1-2][0-9])", sDate)

        if bResult:
            return sDate
        else:
            raise Exception("Date: %s is not a correct ISO Date YYYY-MM-DD" % sDate)

    def createEndToEndId(self, sIdPrefix=''):
        """
        Create the EndToEndID for the sepa payment
        The ID is always 32 charakters long in this application

        @return the Id.
        """
        sRnd = str(random.random())
        sRnd = hashlib.md5(sRnd).hexdigest()

        if len(sIdPrefix) > 15:
            sIdPrefix = sIdPrefix[0:15]

        sEndToEndId = sIdPrefix + sRnd[0:32-len(sIdPrefix)]
        return sEndToEndId


class SEPAConfig(object):
    """
    Config Class which contains all info for the Sepa DD

    Valid sVersion: pain.008.002.02, pain.008.003.02
    Valid sBatch: String 'false' or 'true'
    """
    VERSION_26 = 1
    VERSION_27 = 2
    VERSION_30 = 3

    def __init__(self, sName, sIdPrefix, sIBAN, sBic, sBatch, sCreditorid, sCurr, sVersion="pain.008.001.02", bIbanOnly=True):

        if sVersion == "pain.008.003.02":
            self.sVersion = self.VERSION_27
        elif sVersion == "pain.008.001.02":
            self.sVersion = self.VERSION_30
        else:
            raise Exception("Configuration Error: %s is not a valid version." % sVersion)
        self.sName = sName
        self.sIdPrefix = sIdPrefix
        self.sIBAN = SepaHelper().validateIBAN(sIBAN)
        self.sBic = SepaHelper().validateBIC(sBic)
        self.sBatch = sBatch
        self.sCreditorid = sCreditorid                 
        self.sCurr = sCurr
        self.bIbanOnly = bIbanOnly


class Payment(object):
    """
    Payment class to validate and store seperate payment instructions
    """
    def __init__(self, sName, sIBAN, sBic, sAmount, sDDType, sCollection_date, sMandateid, sMandatedate, sDescription, sEndToEndId, oSepaConfig):
        if isinstance(oSepaConfig, SEPAConfig):
            self.Config = oSepaConfig

        self.sName = SepaHelper().validateText(sName)
        self.sIBAN = SepaHelper().validateIBAN(sIBAN)
        # skip IBAN validation if IBAN only is configured
        if self.Config.bIbanOnly:
            self.sBic = sBic
        else:
            self.sBic = SepaHelper().validateBIC(sBic)
        self.oAmount = SepaHelper().validateAmount(sAmount)
        self.sDDType = SepaHelper().validateDDType(sDDType)
        self.sCollectiondate = SepaHelper().validateDate(sCollection_date)
        self.sMandateid = SepaHelper().validateText(sMandateid)
        self.sMandatedate = SepaHelper().validateDate(sMandatedate)
        self.sDescription = SepaHelper().validateText(sDescription)
        self.sEndToEndId = SepaHelper().validateText(sEndToEndId)




class SepaDD(object):
    """
    SEPA SSD (Sepa Direct Debit) 1.0
    This class creates a Sepa Direct Debit XML File.
    """

    def __init__(self, poConfig, messageId=None):
        #check the config

        if isinstance(poConfig, SEPAConfig):
            self.Config = poConfig

        self.dPayments = {}
        self.oTotalAmount = Decimal(0)
        self.iPaymentCounter = 0
        if messageId:
            self.sMessageId = messageId
        else:
            self.sMessageId=self.__makeMsgId()

        self.__prepareDocument()
        self.__createGroupHeader()

        
    def __addvalue(self, poNode, sValue):
        """
        Replaces nodevalue, sets nodevalue
        """
        poNode.appendChild( self.sepaxml.createTextNode(str(sValue)))
        

    def __prepareDocument(self):
        """
        Build the main document node and set xml namespaces.
        """

        #Create the XML Instance
        self.sepaxml = Document()

        #Create the document node
        documentNode = self.sepaxml.createElement('Document')

        #set the namespace, namespace url and schema location
        documentAttributeXMLNS = self.sepaxml.createAttribute("xmlns")
        documentAttributeXMLNSXSI = self.sepaxml.createAttribute("xmlns:xsi")
        documentAttributeXMLNSXSI.value = "http://www.w3.org/2001/XMLSchema-instance"
        documentAttributeXMLNSXSISL = self.sepaxml.createAttribute("xsi:schemaLocation")
        if self.Config.sVersion == SEPAConfig.VERSION_26:
            documentAttributeXMLNS.value = "urn:iso:std:iso:20022:tech:xsd:pain.008.002.02"
            documentAttributeXMLNSXSISL.value = "urn:iso:std:iso:20022:tech:xsd:pain.008.002.02 pain.008.002.02.xsd"
        elif self.Config.sVersion == SEPAConfig.VERSION_27:
            documentAttributeXMLNS.value = "urn:iso:std:iso:20022:tech:xsd:pain.008.003.02"
            documentAttributeXMLNSXSISL.value = "urn:iso:std:iso:20022:tech:xsd:pain.008.003.02 pain.008.003.02.xsd"
        elif self.Config.sVersion == SEPAConfig.VERSION_30:
            documentAttributeXMLNS.value = "urn:iso:std:iso:20022:tech:xsd:pain.008.001.02"
            documentAttributeXMLNSXSISL.value = "urn:iso:std:iso:20022:tech:xsd:pain.008.001.02 pain.008.001.02.xsd"
        documentNode.setAttributeNode(documentAttributeXMLNS)
        documentNode.setAttributeNode(documentAttributeXMLNSXSI)
        documentNode.setAttributeNode(documentAttributeXMLNSXSISL)

        #create the Direct Debit node
        CstmrDrctDbtInitnNode = self.sepaxml.createElement("CstmrDrctDbtInitn")
        documentNode.appendChild(CstmrDrctDbtInitnNode)

        self.sepaxml.appendChild(documentNode)

        #out = self.sepaxml.toprettyxml()
        #print out
        #print dir(self.sepaxml)

    def __createGroupHeader(self):
        """
        Function to create the GroupHeader (GrpHdr) in the CstmrDrctDbtInit Node
        """

        #Retrieve the CstmrDrctDbtInitn node
        CstmrDrctDbtInitnNode = self.__getCstmrDrctDbtInitnNode()

        #Create the required nodes
        GrpHdrNode = self.sepaxml.createElement("GrpHdr")
        MsgIdNode = self.sepaxml.createElement("MsgId")
        CreDtTmNode = self.sepaxml.createElement("CreDtTm")
        NbOfTxsNode = self.sepaxml.createElement("NbOfTxs")
        CtrlSumNode = self.sepaxml.createElement("CtrlSum")
        InitgPtyNode = self.sepaxml.createElement("InitgPty")
        NmNode = self.sepaxml.createElement("Nm")

        #save the nodes for later
        self.NbOfTxsNode = NbOfTxsNode
        self.CtrlSumNode = CtrlSumNode

        #Set the values for the nodes
        self.__addvalue(MsgIdNode,self.sMessageId)

        self.__addvalue(CreDtTmNode, datetime.datetime.today().isoformat())
        self.__addvalue(NmNode,self.Config.sName)

        #Append the nodes
        InitgPtyNode.appendChild(NmNode)
        GrpHdrNode.appendChild(MsgIdNode)
        GrpHdrNode.appendChild(CreDtTmNode)
        GrpHdrNode.appendChild(NbOfTxsNode)
        GrpHdrNode.appendChild(CtrlSumNode)
        GrpHdrNode.appendChild(InitgPtyNode)

        #Append the header to its parent
        CstmrDrctDbtInitnNode.appendChild(GrpHdrNode)

    def addPayment(self, poPayment):
        """
        Public function to add payments
        @param the payment to be added in the form of an payment object
        @throws Exception if payment is invalid.
        """
        if isinstance(poPayment, Payment):
            pass
        else:
            raise Exception('addPayment: Payment is not a valid Payment object')

        # payment amount has to be greater than zero
        if poPayment.oAmount <= 0:
            return

        # save payment temporarily sorted by DD type and requested collection date
        dTempOneTypePayments = self.dPayments.get(poPayment.sDDType, {})
        lTempOneDatePayments = dTempOneTypePayments.get(poPayment.sCollectiondate, [])
        lTempOneDatePayments.append(poPayment)
        dTempOneTypePayments[poPayment.sCollectiondate] = lTempOneDatePayments
        self.dPayments[poPayment.sDDType] = dTempOneTypePayments

        # Add amount to total amount and increment payment counter
        self.oTotalAmount += poPayment.oAmount
        self.iPaymentCounter += 1

    def __createPaymentInformationNodes(self):
        """
        Creates the payment information nodes with all payments in it
        For every DDType/RequestedCollectionDate combination one PmtInf node is created
        """

        #Get the CstmrDrctDbtInitnNode 
        CstmrDrctDbtInitnNode  = self.__getCstmrDrctDbtInitnNode()

        # iterate through payments
        for sDDType, dTempOneTypePayments in self.dPayments.items():
            for sCollectionDate, lTempOneDatePayments in dTempOneTypePayments.items():
        
                #Create the required nodes, a lot of them.
                PmtInfNode             = self.sepaxml.createElement("PmtInf")
                PmtInfIdNode           = self.sepaxml.createElement("PmtInfId")
                PmtMtdNode             = self.sepaxml.createElement("PmtMtd")
                BtchBookgNode          = self.sepaxml.createElement("BtchBookg")
                NbOfTxsNode            = self.sepaxml.createElement("NbOfTxs")
                CtrlSumNode            = self.sepaxml.createElement("CtrlSum")
                PmtTpInfNode           = self.sepaxml.createElement("PmtTpInf")
                SvcLvlNode             = self.sepaxml.createElement("SvcLvl")
                Cd_SvcLvl_Node         = self.sepaxml.createElement("Cd")
                LclInstrmNode          = self.sepaxml.createElement("LclInstrm")
                Cd_LclInstrm_Node      = self.sepaxml.createElement("Cd")
                SeqTpNode              = self.sepaxml.createElement("SeqTp")
                ReqdColltnDtNode       = self.sepaxml.createElement("ReqdColltnDt")
                CdtrNode               = self.sepaxml.createElement("Cdtr")
                Nm_Cdtr_Node           = self.sepaxml.createElement("Nm")
                CdtrAcctNode           = self.sepaxml.createElement("CdtrAcct")
                Id_CdtrAcct_Node       = self.sepaxml.createElement("Id")
                IBAN_CdtrAcct_Node     = self.sepaxml.createElement("IBAN")
                CdtrAgtNode            = self.sepaxml.createElement("CdtrAgt")
                FinInstnId_CdtrAgt_Node= self.sepaxml.createElement("FinInstnId")
                BIC_CdtrAgt_Node       = self.sepaxml.createElement("BIC")
                ChrgBrNode             = self.sepaxml.createElement("ChrgBr")
                CdtrSchmeIdNode        = self.sepaxml.createElement("CdtrSchmeId")
                Id_CdtrSchmeId_Node    = self.sepaxml.createElement("Id")
                PrvtIdNode             = self.sepaxml.createElement("PrvtId")
                OthrNode               = self.sepaxml.createElement("Othr")
                Id_Othr_Node           = self.sepaxml.createElement("Id")
                SchmeNmNode            = self.sepaxml.createElement("SchmeNm")
                PrtryNode              = self.sepaxml.createElement("Prtry")

                # fill in the info for payment information node
                self.__addvalue(PmtInfIdNode,self.__makeId())
                self.__addvalue(PmtMtdNode,"DD") #Direct Debit
                self.__addvalue(BtchBookgNode,self.Config.sBatch)
                self.__addvalue(NbOfTxsNode, len(lTempOneDatePayments))

                self.__addvalue(Cd_SvcLvl_Node,"SEPA")
                self.__addvalue(Cd_LclInstrm_Node,"CORE")
                self.__addvalue(SeqTpNode, sDDType)
                self.__addvalue(ReqdColltnDtNode, sCollectionDate)
                self.__addvalue(Nm_Cdtr_Node,self.Config.sName)
                self.__addvalue(IBAN_CdtrAcct_Node,self.Config.sIBAN)
                self.__addvalue(BIC_CdtrAgt_Node,self.Config.sBic)
                self.__addvalue(ChrgBrNode,"SLEV")
                self.__addvalue(Id_Othr_Node,self.Config.sCreditorid)
                self.__addvalue(PrtryNode,"SEPA")

                # fold the nodes for payment information
                PmtInfNode.appendChild(PmtInfIdNode)
                PmtInfNode.appendChild(PmtMtdNode)
                PmtInfNode.appendChild(BtchBookgNode)
                PmtInfNode.appendChild(NbOfTxsNode)
                PmtInfNode.appendChild(CtrlSumNode)

                SvcLvlNode.appendChild(Cd_SvcLvl_Node)
                PmtTpInfNode.appendChild(SvcLvlNode)
                LclInstrmNode.appendChild(Cd_LclInstrm_Node)
                PmtTpInfNode.appendChild(LclInstrmNode)

                PmtTpInfNode.appendChild(SeqTpNode)

                PmtInfNode.appendChild(PmtTpInfNode)
                PmtInfNode.appendChild(ReqdColltnDtNode)

                CdtrNode.appendChild(Nm_Cdtr_Node)
                PmtInfNode.appendChild(CdtrNode)

                Id_CdtrAcct_Node.appendChild(IBAN_CdtrAcct_Node)
                CdtrAcctNode.appendChild(Id_CdtrAcct_Node)
                PmtInfNode.appendChild(CdtrAcctNode)

                FinInstnId_CdtrAgt_Node.appendChild(BIC_CdtrAgt_Node)
                CdtrAgtNode.appendChild(FinInstnId_CdtrAgt_Node)
                PmtInfNode.appendChild(CdtrAgtNode)

                PmtInfNode.appendChild(ChrgBrNode)

                OthrNode.appendChild(Id_Othr_Node)
                SchmeNmNode.appendChild(PrtryNode)
                OthrNode.appendChild(SchmeNmNode)
                PrvtIdNode.appendChild(OthrNode)
                Id_CdtrSchmeId_Node.appendChild(PrvtIdNode)
                CdtrSchmeIdNode.appendChild(Id_CdtrSchmeId_Node)
                PmtInfNode.appendChild(CdtrSchmeIdNode)

                oPaymentTotal = Decimal(0)  # reset payment block total

                # create DrctDbtTxInf for every payment
                for oConcretePayment in lTempOneDatePayments:

                    # create the required nodes
                    DrctDbtTxInfNode       = self.sepaxml.createElement("DrctDbtTxInf")
                    PmtIdNode              = self.sepaxml.createElement("PmtId")
                    EndToEndIdNode         = self.sepaxml.createElement("EndToEndId")
                    InstdAmtNode           = self.sepaxml.createElement("InstdAmt")
                    DrctDbtTxNode          = self.sepaxml.createElement("DrctDbtTx")
                    MndtRltdInfNode        = self.sepaxml.createElement("MndtRltdInf")
                    MndtIdNode             = self.sepaxml.createElement("MndtId")
                    DtOfSgntrNode          = self.sepaxml.createElement("DtOfSgntr")
                    DbtrAgtNode            = self.sepaxml.createElement("DbtrAgt")
                    FinInstnId_DbtrAgt_Node= self.sepaxml.createElement("FinInstnId")
                    BIC_DbtrAgt_Node       = self.sepaxml.createElement("BIC")
                    Othr_DbtrAgt_Node      = self.sepaxml.createElement("Othr")
                    DbtrNode               = self.sepaxml.createElement("Dbtr")
                    Nm_Dbtr_Node           = self.sepaxml.createElement("Nm")
                    DbtrAcctNode           = self.sepaxml.createElement("DbtrAcct")
                    Id_DbtrAcct_Node       = self.sepaxml.createElement("Id")
                    IBAN_DbtrAcct_Node     = self.sepaxml.createElement("IBAN")
                    RmtInfNode             = self.sepaxml.createElement("RmtInf")
                    UstrdNode              = self.sepaxml.createElement("Ustrd")
    
                    # fill node with info
                    if oConcretePayment.sEndToEndId != '':
                        self.__addvalue(EndToEndIdNode, oConcretePayment.sEndToEndId)
                    else:
                        self.__addvalue(EndToEndIdNode, self.__makeId())
                    InstdAmtNode.setAttribute("Ccy",self.Config.sCurr)
                    self.__addvalue(InstdAmtNode,oConcretePayment.oAmount)
                    self.__addvalue(MndtIdNode,oConcretePayment.sMandateid)
                    self.__addvalue(DtOfSgntrNode,oConcretePayment.sMandatedate)
                    if not self.Config.bIbanOnly:
                        self.__addvalue(BIC_DbtrAgt_Node,oConcretePayment.sBic)
                    else:
                        Othr_DbtrAgt_Id_Node = self.sepaxml.createElement("Id")
                        self.__addvalue(Othr_DbtrAgt_Id_Node,'NOTPROVIDED')
                        Othr_DbtrAgt_Node.appendChild(Othr_DbtrAgt_Id_Node)
                    self.__addvalue(Nm_Dbtr_Node,oConcretePayment.sName)
                    self.__addvalue(IBAN_DbtrAcct_Node,oConcretePayment.sIBAN)
                    self.__addvalue(UstrdNode,oConcretePayment.sDescription)
    
                    # fold the nodes
                    PmtIdNode.appendChild(EndToEndIdNode)
                    DrctDbtTxInfNode.appendChild(PmtIdNode)
                    DrctDbtTxInfNode.appendChild(InstdAmtNode)
    
                    MndtRltdInfNode.appendChild(MndtIdNode)
                    MndtRltdInfNode.appendChild(DtOfSgntrNode)
                    DrctDbtTxNode.appendChild(MndtRltdInfNode)
                    DrctDbtTxInfNode.appendChild(DrctDbtTxNode)
    
                    if not self.Config.bIbanOnly:
                        FinInstnId_DbtrAgt_Node.appendChild(BIC_DbtrAgt_Node)
                    else:
                        FinInstnId_DbtrAgt_Node.appendChild(Othr_DbtrAgt_Node)
                    DbtrAgtNode.appendChild(FinInstnId_DbtrAgt_Node)
                    DrctDbtTxInfNode.appendChild(DbtrAgtNode)
    
                    DbtrNode.appendChild(Nm_Dbtr_Node)
                    DrctDbtTxInfNode.appendChild(DbtrNode)
    
                    Id_DbtrAcct_Node.appendChild(IBAN_DbtrAcct_Node)
                    DbtrAcctNode.appendChild(Id_DbtrAcct_Node)
                    DrctDbtTxInfNode.appendChild(DbtrAcctNode)
    
                    RmtInfNode.appendChild(UstrdNode)
                    DrctDbtTxInfNode.appendChild(RmtInfNode)
                    PmtInfNode.appendChild(DrctDbtTxInfNode)

                    oPaymentTotal += oConcretePayment.oAmount

                # set PmtInfNode control sum
                self.__addvalue(CtrlSumNode,str(oPaymentTotal))

                # add payment information node
                CstmrDrctDbtInitnNode.appendChild(PmtInfNode)


    def __finalize(self):
        """
        Function to finalize the document, completes the header with metadata.
        """
        self.__addvalue(self.NbOfTxsNode, self.iPaymentCounter)
        self.__addvalue(self.CtrlSumNode, str(self.oTotalAmount))

    def getMessageId(self):
        return self.sMessageId

    def save(self):
        """
        Function to finalize and save the document after all payments are added.
        @return The XML to be echoed or saved to file.
        """

        self.__createPaymentInformationNodes()
        self.__finalize()
        #result = self.sepaxml.toprettyxml(encoding="UTF-8")
        result = self.sepaxml.toxml(encoding="UTF-8")
        
        return result

    def __makeMsgId(self):
        """
        Create a random Message Id fPmtInfNodeor the header, prefixed with a timestamp.
        @return the Message Id.
        """
        sRnd = str(random.random())
        sRnd = hashlib.md5(sRnd).hexdigest()[0:12]
        sTimestamp = datetime.datetime.today().strftime("%d%m%Y%S%f")
        return sTimestamp + "+" + sRnd

    def __makeId(self):
        """
        Create a random id, combined with the id prefix (truncated at 22 chars).
        @return the Id.
        """
        sRnd = str(random.random())
        sRnd = hashlib.md5(sRnd).hexdigest()[0:12]
        
        sName = self.Config.sIdPrefix
        if len(sName) > 22:
            sName = sName[0:21]
        
        return sName + "+" + sRnd

    def __getCstmrDrctDbtInitnNode(self):
        """
        Function to get the CstmrDrctDbtInitnNode from the current document.
        @return The CstmrDrctDbtInitn DOMNode.
        @throws Exception when the node does not exist or there are more then one.
        """

        CstmrDrctDbtInitnNodeList = self.sepaxml.getElementsByTagName("CstmrDrctDbtInitn")

        if len(CstmrDrctDbtInitnNodeList) != 1:
            print("Error retrieving node from document: No or Multiple CstmrDrctDbtInitn")
        else:
            return CstmrDrctDbtInitnNodeList[0]






#oPayment = Payment('Ralph Meyer', 'DE37701204007287333012', 'DABBDEMMXXX', '10', 'FRST', '2013-11-08', 'MANDATE1', '2013-01-01', 'SEPA TEST')


#oConfig =  SEPAConfig('ESSENGELDABRECHNUNG','DE25870400000203332200', 'COBADEFFXXX','1','DE91ZZZ00000647940','EUR')


#oSepadd = SepaDD(oConfig)

#oSepadd.addPayment(oPayment)
#oSepadd.addPayment(oPayment)

#print oSepadd.save()
