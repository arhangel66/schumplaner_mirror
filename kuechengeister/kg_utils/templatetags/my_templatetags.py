from django import template

from menus.models import MealPlan

from kg_utils.menu_helper import MenuRenderer
from kg_utils.date_helper import DateHelper

import settings
from settings import MEALPLAN_ID_HOMEPAGE, MENU_WEEKS_HOMEPAGE


register = template.Library()


class WithSettingNode(template.Node):
    def __init__(self, settings_var, context_var):
        self.context_var = context_var
        self.value = getattr(settings, settings_var, "")

    def render(self, context):
        extra_context = {self.context_var: self.value}
        context.update(extra_context)
        return u""

@register.tag
def with_setting(parser, token):
    """
    Assign an settings value to a variable in the current context.

    Syntax::
        {% with_setting [settings_var] as [context_var] %}
    Example::
        {% with_setting OVERLOAD_SITE as overload_site %}

    """
    parts = token.contents.split()
    if len(parts) != 4 or parts[2] != 'as':
        raise template.TemplateSyntaxError("%r expected format is 'settings_var as context_var'" % parts[0])

    return WithSettingNode(parts[1], parts[3])

@register.simple_tag(takes_context=True)
def active(context, path):
    """
    Return "active" if given url is currently opened. Use for navigation to set active item

    Syntax::
        {% active [url_starts_with] %}
    Example::
        {% active "/kunde/kinder/" %}

    """
    current_path = context['request'].path
    if current_path.startswith(path):
        return 'active'
    return ''


@register.inclusion_tag('kg/partials/index_block_menu.html')
def show_index_menu(mealplan=MEALPLAN_ID_HOMEPAGE,
                    week_total=MENU_WEEKS_HOMEPAGE):
    """Render the given meal plan with preset number of weeks."""
    # Check if the mealplan exists
    if MealPlan.objects.filter(pk=mealplan).count():
        oNow = MenuRenderer.getMenuStartDate()
        lMenus = []
        lWeeks = DateHelper.getWeeks(oNow, week_total)
        for dWeek in lWeeks:
            item = dict()
            menu_html, small_view_menu_html = MenuRenderer.getMenuHtml(
                dWeek['year'], dWeek['week'])
            item = {'menu_html': menu_html,
                    'small_view_menu_html': small_view_menu_html,
                    'week': dWeek['week']}
            lMenus.append(item)
        return {
            'menus': lMenus,
            'weeks': lWeeks,
            'selected': oNow.isocalendar()[1],
        }
