from django import template
from settings import PROJECT_ROOT, SITE_CONTEXT, SEPA
  
register = template.Library()  


# site context
@register.simple_tag
def site_context(name):
    if name in SITE_CONTEXT:
        return SITE_CONTEXT[name]
    else:
        return ''


# SEPA config
@register.simple_tag
def sepa_context(name):
    if name in SEPA:
        return SEPA[name]
    else:
        return ''


@register.filter
def index(the_list, i):
    """ Dead simple filter for getting list element """
    return the_list[int(i)]

@register.filter
def negate(value):
    """ Negates the value """
    return -value

@register.simple_tag
def commit_hash():
    """
    Read first 8 Bytes of file version.txt and return it
    The version.txt file is created via Git hook on deployment.
    DO NOT check in version.txt into repository.
    """
    try:
        sCommitHash = ''
        with open(PROJECT_ROOT + '/version.txt', 'r') as fp:
            sCommitHash = fp.readline(8)
        return sCommitHash.strip()
    except Exception:
        return ''