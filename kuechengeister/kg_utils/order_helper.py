# -*- coding: utf-8 -*-

from __future__ import with_statement
from datetime import datetime
from decimal import Decimal

from django.db import transaction

from kg.models import Order, OrderHistory, Customer, Child, Reduction, CustomerAccountTransaction
from menus.models import Menu
from settings import MODULE_PREPAID, PREPAID_SETTINGS_CREDIT, CANTEEN_MODE


def get_order_history(oChild, oMenu, sOrder='desc'):
    """
    Retrieves and returns the full history of an order as list
    """
    lOrderHistory = []

    # retrieve OrderHistory
    oOrderHistory = OrderHistory.objects.filter(child=oChild, menu=oMenu)
    if sOrder == 'desc':
        oOrderHistory = oOrderHistory.order_by('-last_modified')
    else:
        oOrderHistory = oOrderHistory.order_by('last_modified')
    for oOrder in oOrderHistory:
        dOrder = {'last_modified': oOrder.last_modified}

        if oOrder.modified_by:
            dOrder['user'] = oOrder.modified_by
        else:
            dOrder['user'] = 'System'

        if oOrder.modified_by:
            try:
                dOrder['user_name'] = Customer.objects.get(user=oOrder.modified_by)
            except Customer.DoesNotExist:
                dOrder['user_name'] = ''
        else:
            dOrder['user_name'] = ''

        if oOrder.quantity < 1:
            dOrder['status'] = 'abbestellt'
        else:
            dOrder['status'] = 'bestellt (%d)' % oOrder.quantity

        lOrderHistory.append(dOrder)

    # retrieve Order
    qsLastOrder = Order.all_objects.filter(child=oChild, menu=oMenu)
    for oOrder in qsLastOrder:
        dOrder = {'last_modified': oOrder.order_time}

        if oOrder.modified_by:
            dOrder['user'] = oOrder.modified_by
        else:
            dOrder['user'] = 'System'

        if oOrder.modified_by:
            try:
                dOrder['user_name'] = Customer.objects.get(user=oOrder.modified_by)
            except Customer.DoesNotExist:
                dOrder['user_name'] = ''
        else:
            dOrder['user_name'] = ''

        if oOrder.quantity < 1:
            dOrder['status'] = 'abbestellt'
        else:
            dOrder['status'] = 'bestellt (%d)' % oOrder.quantity

        if oOrder.pickup_time > 0:
            dOrder['pickup_time'] = datetime.fromtimestamp(oOrder.pickup_time)

        if sOrder == 'desc':
            lOrderHistory = [dOrder, ] + lOrderHistory
        else:
            lOrderHistory.append(dOrder)

    return lOrderHistory


def order_menu(iMenuId, iChildId, iQuantity, oRequest=None, bAtomic=True,
               reason=None):
    """
    Order or cancel a menu

    :param iMenuId: The id of the menu
    :param iChildId: The id of the child
    :param iQuantity: The quantity of the order
    :param oRequest: The request object
    :param bAtomic: Apply atomic transaction (default)
    :returns: dictionary
    """

    iQuantity = int(iQuantity)
    if iQuantity < 0:
        iQuantity = 0

    sMessage = u"Es ist ein Fehler aufgetreten!"
    oMenu = None

    sid = transaction.savepoint()

    try:
        oChild = Child.objects.get(pk=iChildId)
        oCustomer = oChild.customer
        oMenu = Menu.objects.get(pk=iMenuId)
        oMealPlan = oChild.facility_subunit.mealplan

        # retrieve existing order or create new one, set new order status
        try:
            # search in all objects, also in the canceled ones
            oOrder = Order.all_objects.get(child=oChild, menu=oMenu)
        except Order.DoesNotExist:
            oOrder = Order(
                child=oChild,
                menu=oMenu,
                modified_by=oRequest.user,
                order_reason=reason,
            )

        iOldQuantity = oOrder.quantity  # save old quantity

        # check if menu can be ordered
        if not oMenu.can_be_ordered:
            sMessage = u"Essen kann nicht bestellt werden."
            raise StandardError

        # Not allow quantity greater than pre-defined limit of the meal plan
        if iQuantity > oMealPlan.max_order_quantity:
            sMessage = u"Bestellmenge darf nicht höher sein als %d" % \
                oMealPlan.max_order_quantity
            raise StandardError

        # make some permission checks, if user is not staff
        if not oRequest.user.is_staff:

            # check if customer has permission to change order for that child
            if oCustomer != oRequest.user.get_profile():
                raise StandardError

            # check if customer is suspended
            if oCustomer.is_suspended:
                raise StandardError

        dMenuPrice = oMenu.get_price(child=oChild,
                                     price_group=oChild.facility_subunit.price_group, )

        # check timelimit for order and cancelation if not staff
        if iOldQuantity > iQuantity:
            # cancelation
            if not oRequest.user.is_staff:
                if oMenu.cancel_timelimit < datetime.now():
                    sMessage = u"Abbestellung nicht mehr möglich."
                    raise StandardError
        elif iOldQuantity < iQuantity:
            # order
            if not oRequest.user.is_staff:
                if oMenu.order_timelimit < datetime.now():
                    sMessage = u"Bestellung nicht mehr möglich."
                    raise StandardError

            # check for orders in same Mealtime on the same day
            if not CANTEEN_MODE and not oMealPlan.duplicate_order_mealtime:
                oMealtime = oMenu.meal.mealtime
                qsSimilarOrders = Order.objects.filter(
                    child=oChild,
                    menu__in=(Menu.objects.filter(
                        date=oMenu.date,
                        meal__mealtime=oMealtime).exclude(pk=oMenu.id)))
                if qsSimilarOrders.exists():
                    sMessage = u"Es kann nur ein %s pro Tag bestellt werden." \
                        % oMenu.meal.mealtime.name
                    raise StandardError

            # check against account balance (with credit) if module prepaid is active
            if MODULE_PREPAID:
                # TODO: implement ordering of more than one in prepaid mode
                if iQuantity > 1:
                    raise Exception('Implement ordering more than on in prepaid mode!!!')
                if dMenuPrice['price'] > (oCustomer.account_balance + Decimal(PREPAID_SETTINGS_CREDIT)):
                    sMessage = u"Guthaben nicht ausreichend: %s €" % str(oCustomer.account_balance).replace('.', ',')
                    raise StandardError

        # everything ok, save order if something changed
        if iOldQuantity != iQuantity:
            oOrder.quantity = iQuantity
            oOrder.modified_by = oRequest.user
            oOrder.price = dMenuPrice['price']
            oOrder.order_time = datetime.now()
            if dMenuPrice['reduction_id'] > 0:
                oOrder.reduction = Reduction.objects.get(pk=dMenuPrice['reduction_id'])
            oOrder.save()

        # if module prepaid is activated, make the corresponding CAT booking
        if MODULE_PREPAID:
            # TODO: implement ordering of more than one in prepaid mode
            if iQuantity > 1:
                raise Exception('Implement ordering more than one in prepaid mode!!!')
            if iOldQuantity < iQuantity:
                # create CAT for menu order
                oNewCAT = CustomerAccountTransaction(customer=oCustomer,
                                                     transaction_id="order_%d" % oOrder.id,  # set order id as identifier
                                                     amount=dMenuPrice['price'].copy_negate(),
                                                     description=u"%s, %s, %s %s" % (
                                                                    oMenu.meal.name,
                                                                    oMenu.date.strftime('%d.%m.%Y'),
                                                                    oChild.surname,
                                                                    oChild.name),
                                                     type=CustomerAccountTransaction.TYPE_CHARGE,
                                                     status=CustomerAccountTransaction.STATUS_OPEN)
                oNewCAT.save()
            else:
                # delete corresponding CAT
                if oCustomer and oOrder.id > 0:
                    try:
                        oExistingCAT = CustomerAccountTransaction.objects.get(customer=oCustomer,
                                                                              transaction_id="order_%d" % oOrder.id)
                        oExistingCAT.delete()
                    except CustomerAccountTransaction.DoesNotExist as exc:
                        # TODO: log exception
                        pass
                    except Exception as exc:
                        # TODO: log exception
                        pass

    except Exception as exc:
        if bAtomic:
            transaction.savepoint_rollback(sid)
            return {'success': False,
                    'quantity': oOrder.quantity,
                    'order_possible': oMenu.order_timelimit > datetime.now(),
                    'message': sMessage}
        raise exc
    else:
        if bAtomic:
            transaction.savepoint_commit(sid)
        return {'success': True,
                'quantity': oOrder.quantity,
                'order_possible': oMenu.order_timelimit > datetime.now(),
                'message': ''}


@transaction.commit_manually
def cancel_dependent_menus(iMenuId, iChildId, oRequest=None):
    """Reduces all order quantities of menus depending to a given one to zero.
    This is normally called when the required menu order quantity is cancelled.
    """
    lDependents = Menu.objects.filter(required_menu__pk=iMenuId).\
        values_list('pk', flat=True)
    try:
        for iDependentId in lDependents:
            order_menu(iDependentId, iChildId, 0, oRequest, bAtomic=False)
        transaction.commit()
    except Exception as exc:
        transaction.rollback()
        raise exc


def get_day_order_info(child, order_date):
    """ Return brief order information in given day of a child. This will
        used mostly by the small view customer order.
    """
    order_info = {
        'orders': [],
        'mealtimes': [],
        'lunches': [],
    }
    for order in Order.objects.select_related().filter(
            child=child, menu__date=order_date):
        menu = order.menu
        order_info['orders'].append(order)
        order_info['mealtimes'].append(menu.meal.mealtime.name_short)
        # Further checks for lunch order
        if menu.meal.mealtime.is_lunch:
            order_info['lunches'].append(menu.description)
    return order_info
