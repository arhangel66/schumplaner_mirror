from datetime import date, datetime
from decimal import Decimal

from django.db.models import Q
from django.template import RequestContext
from django.template.loader import render_to_string

from kg.models import Order, ChildReduction, Child
from kg_utils.date_helper import DateHelper
from kg_utils.order_helper import get_day_order_info
from menus.models import Meal, Menu, Price, MealPlan

from settings import SITE_CONTEXT, MENU_START_DATE, MODULE_PREPAID, OVERLOAD_SITE


class MenuRenderer():

    @staticmethod
    def getMenuStartDate():
        """ Gives back the menu start date
        There can be configured a MENU_START_DATE in the settings
        If this is done, the menu will be shown from this date on
        """
        oMenuStartDate = datetime.strptime(MENU_START_DATE, '%d.%m.%Y')
        if oMenuStartDate > datetime.now():
            return oMenuStartDate
        else:
            return datetime.now()

    @staticmethod
    def isMenuAvailable(year, week):
        """
        Checks if there are menu items for the given week
        """
        days = DateHelper.getDaysInCalendarWeek(year, week)
        if Menu.objects.filter(date__range=(days[0], days[-1])):
            return True
        else:
            return False

    @staticmethod
    def getMenuHtml(year, week, mealtime_id=0, child=None, form=False,
                    form_action='', request=None):
        """
        Returns the rendered HTML for the menu for the given week and mealtimes
        """
        lDays = DateHelper.getDaysInCalendarWeek(year, week)
        oMealPlan = None
        bShowPrice = True
        price_group = None

        if child:
            # This returns meals in aither specific or general meal plans. It
            # needs further filtering (in template, perhaps) in order to hide
            # unavailable meal for a given day.
            oMealPlan = child.facility_subunit.mealplan
            price_group = child.facility_subunit.price_group
            bShowPrice = oMealPlan.show_price
            lDays = lDays[:oMealPlan.weekdays]
            qsAvailableMeals = Meal.objects.getAvailableInRange(
                Q(periods__mealplan__facilitysubunit=child.facility_subunit),
                lDays[0], lDays[-1], allergy=child.order_allergy_meal)
        else:
            qsAvailableMeals = Meal.objects.filter(
                menu__date__range=(lDays[0], lDays[-1])).distinct()
        qsAvailableMeals = qsAvailableMeals.order_by('sortKey')

        if int(mealtime_id) > 0:
            qsAvailableMeals = qsAvailableMeals.filter(
                mealtime__id=mealtime_id)

        qsWeekMenus = Menu.objects.filter(
            date__range=(lDays[0], lDays[-1]),
            meal__in=qsAvailableMeals
            ).order_by('date')

        menu_list = []
        # Extra info, mostly for small view
        week_orders = [None]*len(lDays)
        for oMeal in qsAvailableMeals:
            day_list = []
            for day_id, day in enumerate(lDays):
                params = {
                    'checked': False,
                    'disabled': True,
                    'order_possible': True,
                    'quantity': 0
                }
                price = Decimal('0.00')
                price_reduced = None  # for later use, NOT for child reduction right now
                sAllergens = ''
                qsMenu = qsWeekMenus.filter(date=day, meal=oMeal)

                if child:
                    # only show menus that are in childs facility subunit mealplan
                    qsMenu = qsMenu.filter(meal__periods__mealplan__facilitysubunit=child.facility_subunit)
                    # Filter for child's mealplan menus or general menus.
                    qsMenu = qsMenu.filter(
                        Q(mealplan__isnull=True) |
                        Q(mealplan_id=child.facility_subunit.mealplan_id)) \
                        .order_by('meal__id', 'date', 'mealplan__id') \
                        .distinct('meal__id', 'date')
                else:
                    # Only show general menus on public menu
                    qsMenu = qsMenu.filter(mealplan__isnull=True)

                if qsMenu.exists() and not qsMenu[0].disabled:
                    oMenu = qsMenu[0]

                    # get allergens and additionals
                    lAllergens = list(oMenu.additionals.values_list('name_short', flat=True)) + list(oMenu.allergens.values_list('name_short', flat=True))
                    sAllergens = ', '.join(lAllergens)

                    # if child is not None and facility is not inactive, get order detail data
                    if child and child.facility.is_active:
                        # get additional infos
                        params['order_limit'] = oMenu.order_timelimit.strftime("%d.%m.%y %H:%M")
                        params['cancel_limit'] = oMenu.cancel_timelimit.strftime("%d.%m.%y %H:%M")
                        params['can_be_ordered'] = oMenu.can_be_ordered  # menu can be ordered or not
                        params['order_possible'] = oMenu.can_be_ordered and oMenu.order_timelimit > datetime.now()
                        params['cancelable'] = oMenu.cancel_timelimit > datetime.now()

                        # check order timelimit
                        if form and (params['cancelable'] or params['order_possible']):
                            params['disabled'] = False

                        # get price for menu
                        if bShowPrice and oMenu.can_be_ordered:
                            if MODULE_PREPAID and form and child:
                                price = oMenu.get_price(price_group, child)['price']
                            else:
                                price = oMenu.get_price(price_group)
                                # hack for Hytera
                                # TODO: solve this in a more general way
                                if OVERLOAD_SITE == 'hytera':
                                    price_reduced = price * Decimal('0.8')

                        # get order details
                        qsOrder = Order.objects.select_related().filter(
                            child=child, menu=oMenu)
                        if qsOrder:
                            order = qsOrder[0]
                            params['quantity'] = order.quantity
                            params['checked'] = True
                            # Small view infomation
                            if not week_orders[day_id]:
                                week_orders[day_id] = {
                                    'orders': [],
                                    'mealtimes': [],
                                    'lunches': [],
                                }
                            week_orders[day_id]['orders'].append(order)
                            week_orders[day_id]['mealtimes'].append(
                                oMenu.meal.mealtime.name_short)
                            # Further checks for lunch order
                            if order.menu.meal.mealtime.is_lunch:
                                week_orders[day_id]['lunches'].append(
                                    order.menu.description)

                    day_list.append({'menu': oMenu,
                                     'allergens': sAllergens,
                                     'price': price,
                                     'price_reduced': price_reduced,
                                     'params': params})
                else:
                    day_list.append({})
            menu_list.append({'day_list': day_list, 'mealtime': oMeal})

        if form:
            mealplan = child.facility_subunit.mealplan
            d_context = {
                'menu_list': menu_list,
                'week_orders': week_orders,
                'days': lDays,
                'mealtimes': qsAvailableMeals,
                'child': child,
                'facility_string': child.facility.name,
                'action': form_action,
                'max_order_quantity': mealplan.max_order_quantity,
            }
        else:
            d_context = {
                'menu_list': menu_list,
                'days': lDays,
                'mealtimes': qsAvailableMeals
            }

        # load site specific context
        d_context.update(SITE_CONTEXT)

        if form:
            big_view_html = render_to_string('kg/utils_menu_form.html', d_context, context_instance=RequestContext(request))
            small_view_html = render_to_string('kg/utils_small_view_menu.html', d_context, context_instance=RequestContext(request))
            return big_view_html, small_view_html
        else:
            big_view_html = render_to_string('kg/utils_menu.html', d_context)
            small_view_html = render_to_string('kg/utils_small_view_index_menu.html', d_context)
            return big_view_html, small_view_html

    @staticmethod
    def getDayOrderBrief(iChildId, oDate):
        # Generate brief day order status for small view
        oChild = Child.objects.get(id=iChildId)
        return render_to_string(
            'kg/partials/small_view_order_brief.html',
            get_day_order_info(oChild, oDate)
        ).strip()
